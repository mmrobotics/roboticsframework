﻿# Robotics Framework

**Contributors**:

 - AJ Sperry
 - Michael Bentley

 **Contains Some Source Code Originally Written By:**
 
 - Arthur Mahoney
 - Katie Popek
 - Andrew Petruska
 - Sam Wright

---
## Introduction

> :warning: The Robotics Framework is a CMake project and makes heavy use of standard and custom CMake cache variables. If you do not understand how to work with CMake or CMake cache variables, it is highly recommended that you take the time to [learn these concepts](https://bitbucket.org/mmrobotics/lab-wiki/wiki/necessary_skills/CMake.md) before proceeding.

The Robotics Framework is a ROS-like, Qt-based C++ framework that is meant to be used as a foundation upon which robotics projects can be created. It should be thought of as the "glue" that abstracts away many of the logistical complications of writing robotics software, allowing you to focus on a specific robotics application. The framework is ROS-like in that it provides standard methods for message passing, but differs from ROS in that it creates a single (multi-threaded) process/program. This means message passing can be much faster when compared to ROS but you don't get the improved robustness typical of multi-process applications (i.e. in multi-process applications, if one process crashes, it doesn't cause the whole robotics application to crash).

The framework provides the following functionality and features:

- The Robotics Toolbox, a collection of types and functions for linear algebra and mathematical operations in robotics
- A collection of utility classes for timing (e.g. running a function at a specific time interval), threading, basic optimization, signal processing and filtering, and magnetics
- ROS-like message passing between objects/classes (in the same process, built on top of the Qt signal-slot architecture)
- Pseudo-real-time data visualization and data recording
- Robot and hardware implementations/abstractions ([Yaskawa Motoman MH5 robot arm](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/Yaskawa_Motoman_MH5_Robot_Arm.md), [SAMM](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/SAMM.md), [Space Explorer 3D Mouse](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/SpaceExplorer_3D_Mouse.md))
- A framework for writing and managing control loops and controllers
- Computer vision workflows built on top of OpenCV including a substantially featured computer vision GUI, camera calibration workflows, support for ROIs (Regions of Interest) and ROI servoing, and camera drivers ([Flea 3 Camera](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/Flea3_Camera.md))

The framework is currently only compatible with Linux and, specifically, Ubuntu Linux is recommended. The framework is divided into several components such that you can use only what you need and avoid installing libraries that unwanted components depend on. Each component has library dependencies that must be installed prior to building and installing the framework. Details for installing the dependencies of each component are given in the component description pages linked below.

**I want to use ROS. Can I still use the Robotics Framework?**

Absolutely! There is nothing about this framework that prevents it from being used in a ROS node. In fact, it is encouraged!!

---
## Development Setup and the Core Component

The development setup page describes how to build, install, and use the framework in your projects. The Core component is the only component that can be used alone. All other optional components require the Core component.

- [Development Setup](docs/development_setup.md)
- [Core Component](docs/core_component.md)

The Core component consists of:

- The Robotics Toolbox, a collection of types and functions for linear algebra and mathematical operations in robotics
- ROS-like message passing between objects/classes (in the same process, built on top of the Qt signal-slot architecture)
- Pseudo-real-time data visualization
- Data recording
- Timing and threading
- Signal processing and filtering
- Magnetics
- General robot abstractions
- Yaskawa Motoman MH5 robot arm implementation
- A framework for writing and managing control loops and controllers


---
## Optional Components

Links to pages describing the various optional components of the Robotics Framework are found below. These components are optional because they have additional library dependencies that some users may want to avoid installing if they don't have need of the component. As such, these components must be manually enabled during the build/install process via corresponding CMake cache variables (see [Development Setup](docs/development_setup.md)). The relevant cache variables as well as the additional dependencies for each component are described in their respective component description pages.

Some components have sub-components. These sub-components have dependencies in addition to the dependencies of the parent component and also require that the parent component be enabled. If a sub-component is enabled and its parent component is not, CMake will throw an error requesting that the parent component be enabled too.

- [Computer Vision](docs/cv_component.md)
    - Flea 3 Camera (sub-component)
- [SAMM](docs/samm_component.md)
    - SAMM Interface (sub-component)
- [Optimization](docs/optimization_component.md)
- [Space Explorer 3D Mouse](docs/space_explorer_component.md)
