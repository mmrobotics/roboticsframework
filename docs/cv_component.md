# Computer Vision Component

The primary computer vision component and its sub-component(s) are described below. Each description will specify library dependencies. Details of library dependencies are given after the component descriptions.

To include a component in the Robotics Framework build, set the corresponding CMake variable listed below to `true`. To use a component in a framework-dependent project, use the corresponding component name listed below in `find_package()` after the `COMPONENTS` keyword. Remember that sub-components require the primary component.

---
## Primary Component

- **CMake Variable**: ROBOTICS_USE_CV
- **Component Name**: `CV`
    - example usage: `find_package(RoboticsFramework REQUIRED COMPONENTS Core CV)`
- **Library Dependencies**:
    - OpenCV (see below)

The computer vision component adds the `CV` directory which contains a very large set of functionality meant to make computer vision and working with cameras easier. This component provides workflows for:

- calibrating and localizing monocular camera pairs
- recording video and taking still images
- writing and running custom computer vision algorithms (for monocular camera pairs)
- performing software-based visual servoing

The notable classes are:

- `Camera`
- `CameraPair`
- `ChessboardCalibrationPattern`
- `ComputerVisionUI`
- `ComputerVisionTracker`
- `ROIServo`
- `CustomProjectionMethod`

A `RobotController` (a class from the `Control` directory of the Core component) for calibrating the location of a `CameraPair` relative to the base frame of a robot is also provided as `CameraPairCalibrationRobotController`. Several computer vision tracking algorithms are implemented under `CV/CVTrackers`. Since this component is so large and complicated, it is recommended that you start by reading the documentation of each class in the order they are listed above. This component also adds some `Tool` classes under `Robots/RobotTools` that can be used for calibration.

---
## Sub-Components

### Flea 3 Camera

- **CMake Variable**: ROBOTICS_USE_CV_Flea3Camera
- **Component Name**: `Flea3Camera`
    - example usage: `find_package(RoboticsFramework REQUIRED COMPONENTS Core CV Flea3Camera)`
- **Library Dependencies**:
    - FlyCapture2 (see below)

An implementation of the `Camera` interface for the [Flea 3 Camera](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/Flea3_Camera.md). Found in `CV/Cameras/Flea3Camera.h` which defines a `Flea3Camera` class. The FlyCapture2 library has been **discontinued** by FLIR and only supports up to Ubuntu 18.04 LTS. The FlyCapture2 library has been superseded by the Spinnaker library. In order to make this sub-component work on later versions of Ubuntu, one will have to make another new implementation of the `Flea3Camera` class that uses the Spinnaker library instead.

---
## Library Dependencies

### OpenCV

- **Recommended Install Method**: APT Package
- **APT Package**: `libopencv-dev`
- **Modules Used** (for reference): `core`, `calib3d`, `imgproc`, `highgui`, `imgcodecs`, `videoio`
- See the [OpenCV Computer Vision Library](https://bitbucket.org/mmrobotics/lab-wiki/wiki/useful_resources/OpenCV.md) lab wiki page for more details.

### FlyCapture2

- **Recommended Install Method**: Install Debian packages provided by FLIR using their provided installation scripts
- See the [Flea 3 Camera](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/Flea3_Camera.md) lab wiki page for details of installing this library.
