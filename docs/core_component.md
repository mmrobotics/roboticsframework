# Core Component

This component is included in the Robotics Framework build by default. To use this component in a framework-dependent project, use the component name listed below in `find_package()` after the `COMPONENTS` keyword.

- **Component Name**: `Core`
    - example usage: `find_package(RoboticsFramework REQUIRED COMPONENTS Core)`
- **Library Dependencies**:
    - Eigen Linear Algebra Template Library (see below)
    - Qt Application Framework (see below)

The various parts of the Robotics Framework Core component are introduced below. Following this introduction, the library dependencies of the Core component are explained.

---
## Parts of the Core Component

### Robotics Toolbox

The Robotics Toolbox contains many types and functions that are essential for mathematical operations in robotics. These include:

- representations of 3D rigid body state (i.e. position, orientation, etc.)
- vectors and matrices of various sizes and shapes (which are simply type definitions of the `Eigen::Matrix` class)
- functions for converting between representations of orientation
- functions for forming skew-symmetric or diagonal matrices
- and more

The objects in the toolbox are under the `Robo` namespace and are included by including the `RoboticsToolbox.h` header file found in the top level of the framework source code.

### Message Passing and Pseudo-Real-Time Data Visualization/Recording

A common need in robotics applications is the ability to pass data from one entity to another. For example, one entity may be reading data from a DAQ card and another entity might be processing that data for some sort of localization algorithm. Yet another entity might take that processed localization data and use it to control a robot arm. The Robotics Framework makes it easy to pass data from one instance of a C++ class object to another. The objects do not need to know any details of the objects they are sending the data to or getting the data from, enabling great flexibility and modularity.

The actual data passing itself is implemented by the signal-slot mechanism of the [Qt Application Framework](https://www.qt.io/product/framework). If you simply want to pass data from one object to another, the idea is to make both objects inherit from `QObject` (which enables the use of signals and slots for those classes) and then have the data producing object emit a signal (i.e. call one of its signal member functions) where the parameters of the signal (i.e. the literal function parameters of the signal function) contain the data that you want to send. The consuming object has a slot member function with parameters that are compatible with those of the signal. When the signal and slot are connected using the `QObject::connect()` function, the consuming object will receive the data in the slot function any time the producing object emits the signal. The consuming object can either process the received data in the slot function itself or save it for later processing. The beauty of using the Qt signal-slot mechanism is that signals and slots are built to be thread-safe. If the producer is emitting a signal from one thread and the consumer is processing the signal in another thread, the *queued* connection type will pass the data between these two threads in a thread-safe way. It is highly recommended that you pass data between objects in this way even if the objects are running in the same thread because the modularity makes for cleaner and more readable code. Further, the option to move objects to different threads later is available without the need to worry about data serialization.

The parameters of Qt signals can be custom types if desired (e.g. any class you may wish to define) and can be connected to slots that have "compatible" parameter types. There are various aspects that determine compatibility, but one tremendously useful aspect is that signal and corresponding slot parameter types are compatible if the signal parameter type can be implicitly converted to the corresponding slot parameter type. As an example, the rigid body state types in the `Robo` namespace are structured such that the object that represents a 6-DOF pose (position and orientation) inherits from the objects that represent position and orientation. In other words, the `Robo::Pose6DOF` **is** a `Robo::Position` and it **is** a `Robo::Orientation`. This means that a signal that has a `Robo::Pose6DOF` as its parameter can not only be connected to a slot that has a `Robo::Pose6DOF` parameter, but also be connected to a slot that has a `Robo::Position` or a `Robo::Orientation` parameter. This allows incredible flexibility when connecting producing objects to consuming objects. Such flexibility is not easily available in ROS. See the Qt documentation to learn how to properly pass messages using the signal-slot mechanism. This documentation also contains details on the few type restrictions on signal parameters. The `StateDataUI` class documentation also contains an example of writing a data producing class.

Although message passing is actually implemented by Qt, the Robotics Framework adds additional features that allow for pseudo-real-time visualization and recording of these messages. This is done with the `StateMessage` and `StateDataUI` classes. See the documentation of these classes for more details. Note that all of the rigid body state types in the `Robo` namespace are already set up to inherit from `StateMessage` and as a result can be used with `StateDataUI`. To read recorded data into MATLAB, use the [libmatlab](https://bitbucket.org/mmrobotics/libmatlab/src/master/) repository.

### Robots

Classes for robot implementations are found in the `Robots` directory of the framework. Many of these classes are interface classes meant to be inherited by robot implementation classes. These interface classes include:

- `GenericRobot`
- `WorkspaceControllableRobot`
- `SerialLinkRobot`
- `ToolableRobot`

See the documentation for each interface class for more details.

The `Robots` directory also contains implementations of these interfaces for specific robots. Currently only the [Yaskawa Motoman MH5](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/Yaskawa_Motoman_MH5_Robot_Arm.md) robot is implemented as part of the Core component because its implementation does not have additional library dependencies. This implementation is found in `Robots/Motoman` and the implementation class is called `MH5Robot`. The MH5 Robot must be configured in accordance with the [Yaskawa Motoman MH5](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/Yaskawa_Motoman_MH5_Robot_Arm.md) wiki page. The `SAMM` and `MH5SAMMDipoleRobot` implementations require the [SAMM optional component](samm_component.md).

Several `Tool` objects for `ToolableRobot` implementations can be found in `Robots/RobotTools`.

### Control

The `Control` directory of the framework contains the following classes:

- `RobotController`
- `WorkspaceWaypointController`
- `RobotControlUI`

These classes can be used to control and implement custom controllers for robots that implement the interfaces from the `Robots` directory. The `Control` directory also contains a couple PID controller implementations for arbitrary vector spaces in `Control/PID`.

### Utilities

The `Utilities` directory of the framework contains many classes that have useful miscellaneous functionality. The `DateTime` class is useful for real-world clock time. The `Filters` namespace, found in the `Filters.h` header, contains various filters (e.g. low-pass, moving average, Kalman, etc.).  The `SignalProc` namespace, found in the `SignalProcessing.h` header, contains signal processing algorithms that are not strictly filters. The `Numerical` namespace, found in the `NumericalMethods.h` header, contains any other numerical methods or representations that are not related to filtering or signal processing.

The `Magnetics` namespace, found in the `Magnetics.h` header, contains logic for computing various magnetic quantities. The `Recorder` class can be used to write data to file (although it is recommended to use the `StateDataUI` for most data recording situations). The `Timing` namespace, found in the `Timing.h` header, contains a lot of functionality for timing and repeatedly running code at a constant interval (e.g. for a control loop). Specifically, see the `Timing::Timer` and `Timing::CodeStopwatch` classes. Lastly the `Thread` class allows you to create a thread similar to the `QThread` class. It is recommended however that `Timing::Timer` be used instead of `Thread` if you wish to run code at a constant interval. `Thread` is more suited to blocking or asynchronous code that you want to run in a separate thread.

> :warning: It is important to note that both the `Timing::Timer` and `Thread` classes allow for setting thread priorities using *Linux real-time priorities*. In order to use these higher thread priorities, additional operating system configuration is needed. Please read the documentation for these classes carefully as they contain the necessary instructions.

### Qt GUI Elements

The `QtGUIElements` directory contains various simple but useful Qt widgets found in `Labels.h` and `Buttons.h`. It also has the `WindowGroup` class that can be used to group multiple Qt windows together so that when one closes, they all close. This directory also contains a couple 3rd-party widgets (found online). These include `qcustomplot` and `qledindicator`.

---
## Core Component Library Dependencies

### Eigen Linear Algebra Template Library

- **Recommended Install Method**: APT Package
- **APT Package**: `libeigen3-dev`
- **Alternative Installation Methods**:
    - Download source and install with CMake
- This library is entirely a header library and therefore really requires no setup other than downloading the files and including them in your project. However, to set up this library for use with the Robotics Framework you must either install using APT or install downloaded source files using CMake. Both of these methods will make the library discoverable by CMake.

### Qt Application Framework & Qt Creator

- **Recommended Install Method**: APT Package
- **APT Package**: `qt5-default`, `qtcreator`
- **Alternative Installation Methods**:
     - Qt Web Installer
     - Qt Maintenance Tool
- **Required Version**: Qt 5
- The [Qt Application Framework and Qt Creator](https://bitbucket.org/mmrobotics/lab-wiki/wiki/necessary_skills/Qt.md) page on the lab wiki contains detailed instructions for installing Qt 5.
- If you do not install using the APT Package, and you are not using Qt Creator to build, you will need to know the path to the CMake files for your Qt installation in order to set the `CMAKE_PREFIX_PATH` CMake variable properly. This path is a subdirectory of the directory chosen during the Qt installation. For example, if Qt is installed in the user's home directory `<home>`, the path you must give to CMake will take the form `<home>/Qt/<qt_version>/<compiler>` where `<qt_version>` is the version of Qt you wish to use (multiple versions can be installed at the same time) and `<compiler>` represents the target platform/compiler you are using. Examples include `gcc_64` and `mingwXX_64` (XX refers to the version of mingw). If you are using Qt Creator, it will automatically set `CMAKE_PREFIX_PATH` according to the Qt version selected in the active kit.
- This framework uses the following Qt 5 modules: `Core`, `Gui`, `Widgets`, and `PrintSupport`.
