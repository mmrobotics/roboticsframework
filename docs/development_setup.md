# Robotics Framework Development Setup

---
## Introduction

> :warning: The Robotics Framework is a CMake project and makes heavy use of standard and custom CMake cache variables. If you do not understand how to work with CMake or CMake cache variables, it is highly recommended that you take the time to [learn these concepts](https://bitbucket.org/mmrobotics/lab-wiki/wiki/necessary_skills/CMake.md) before proceeding.

This document describes how to build and install the framework and its API documentation such that it is ready to use in another project. The easiest method to use the framework is to build and install it as a [CMake config-file package](https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html#id1). This method builds and installs the framework as a library on your system that is easily added to another CMake project using `find_package()` in that project's `CMakeLists.txt` file.

Since the config-file package method compiles and installs the framework, it is a bit cumbersome if and when changes need to be made to the framework itself. For small changes (e.g. minor bug fixes) this is not a major problem. However, if you will need to perform substantial development in the framework (e.g. add a new feature) and iteratively test these changes in a framework-dependent project, the source code can be added directly to a CMake project using `add_subdirectory()` in that project's `CMakeLists.txt` file instead of using `find_package()`. With this approach, a CMake target for each enabled framework component is added to the project. These targets and their associated source code will automatically show up in most IDEs making it easy to edit framework source files alongside project source files. Further, the framework will build alongside the framework-dependent project, removing the extra build/install steps that would be needed each time the framework is changed, thus making iteration faster.

In summary, the two framework development use-cases are:

1. **Just use the framework (easiest method):** Build and install the framework somewhere on your system as a CMake config-file package. Use the framework in other CMake projects using `find_package()` in those projects' `CMakeLists.txt` files.
2. **Develop the framework along with a framework-dependent project (advanced)**: Add the framework source code directly to a CMake project as a set of additional CMake targets using `add_subdirectory()` in the framework-dependent project's `CMakeLists.txt` file.

The [*Installation and Usage as a CMake Config-file Package*](#installation-and-usage-as-a-cmake-config-file-package) and [*Add Framework Source as CMake Targets in Framework-dependent Project*](#add-framework-source-as-cmake-targets-in-framework-dependent-project) sections below are dedicated to explaining how to set up each of these development use-cases respectively. However, before that, some important information is provided to make sure your system is set up properly with the development tools needed to build the framework. The last section discusses details about building and viewing the framework API documentation. New users of the framework will find that the API documentation is crucial to learning how to use it.


---
## Development Tools Setup, Installing Framework Dependencies, and Ensuring CMake Can Find Dependencies

Before you can get started using the framework, you will need to install software on your system that is used to perform the build and install process (e.g. a compiler, Make, CMake, Doxygen, etc.). Below is a list of the software you will need to install. Most of this software is easily available through APT and therefore the associated APT package name(s) will be listed (see the [Lab Wiki Linux page](https://bitbucket.org/mmrobotics/lab-wiki/wiki/necessary_skills/Linux.md) for details on installing software using APT). If alternative installation methods are available, these will be discussed.

### Build Essentials (GCC compiler, Make build tool, etc.)

- **APT Package**: `build-essential`
- This package contains the GCC C and C++ compilers, the Make build tool (which CMake will use to invoke the compiler during building), and other software development tools. It should be installed pretty much before anything else.

### Git (Version Control System)

- **APT Package**: `git`
- This framework uses Git for version control and you will need to use it to clone the source code to your computer. You will also need it when modifying the framework itself. Please please **PLEASE** use Git for managing the source code! If you do not know Git, **LEARN IT**! Please see the [Lab Wiki Git page](https://bitbucket.org/mmrobotics/lab-wiki/wiki/necessary_skills/Git.md) for more details.
  
### CMake

- **APT Packages**: `cmake` (version 3.10 or higher), `cmake-qt-gui` (optional but very useful)
- **Alternative Installation Methods**:
    - Install alongside a Qt installation
    - Install as part of an IDE (if available)
    - Install from the CMake website
- The Robotics Framework requires a **CMake version of at least 3.10**. The version of CMake found in the Ubuntu APT repositories for Ubuntu 18.04 or newer will meet this requirement. If you are running an older version of Ubuntu (you shouldn't be) then you will need to use an alternative method for installing CMake.
- If you install CMake with APT, you can optionally install the CMake GUI with the `cmake-qt-gui` package. This provides a graphical user interface for using CMake and can make browsing/editing the CMake cache much easier than doing so with the command line. Note that this GUI only works with the CMake instance installed by APT.
- CMake can alternatively be installed alongside the Qt Application Framework, which is a [dependency of the framework](core_component.md#core-component-library-dependencies) and needs to be installed anyway. Installing CMake with Qt makes it easily accessible from within Qt's IDE, Qt Creator. Other IDEs may also offer a CMake installation. Be aware that installing CMake in this way will likely not make it available on the command line.
- If no other method is suitable, CMake can always be installed by following the instructions from the CMake website.
  
### Doxygen (Used to generate API documentation)

- **APT Packages**: `doxygen`, `graphviz` (optional but very useful)
- Doxygen is used to automatically create documentation using the comments within source code. This framework is set up to easily generate documentation locally in HTML format (i.e. a browsable local "website"). Since the framework is configured to use the `dot` tool from the `graphviz` APT package, make sure to also install `graphviz`. Having `graphviz` installed will add nice class hierarchy diagrams to the documentation.
- [See below](#building-and-viewing-framework-api-documentation-using-doxygen) for details of building the framework API documentation.

### An IDE (Integrated Development Environment)

- You will need an IDE for code editing. The following IDEs are recommended:
    - [Qt Creator](https://bitbucket.org/mmrobotics/lab-wiki/wiki/necessary_skills/Qt.md)
    - [Visual Studio Code](https://bitbucket.org/mmrobotics/lab-wiki/wiki/useful_resources/Visual_Studio_Code.md)

### Installing Framework Dependencies

The above list only contains the development tools that are necessary to build or develop the framework. In addition to these tools, the framework source code itself depends on software libraries that must be installed on your system. As mentioned previously, each component of the framework has its own dependencies. You will need to install the dependencies for each component you wish to use. Each component's documentation page contains a list of dependencies and how best to install them. Most of these dependencies can be installed with APT, but if alternative methods are available or recommended, the component page will have those details. **At a minimum, you must install the dependencies of the [Core component](core_component.md)**. See the [README](../README.md) for links to the other component documentation pages.

### Ensuring CMake Can Find Dependencies

The primary method for adding a library dependency to a CMake project is to use `find_package()`. The Robotics Framework, which itself is a CMake project, uses this function extensively to find its dependencies. By default, the `find_package()` function searches for packages/dependencies in a predefined list of system locations. If a dependency has been installed using APT, it will be installed into the default system locations (`/usr/lib`, `/usr/include`, etc.) and these locations are searched by `find_package()`. Therefore, CMake usually does not have trouble finding dependencies installed with APT.

If you do not install a dependency into a default search location, CMake will complain that it cannot find the dependency. Common scenarios where the dependency is not installed into a default search location include:

- Building the dependency from source and installing it into a subdirectory of your home directory (or any other directory that is not a default search location)
- Building a dependency from source, but not installing it at all (i.e. the build directory of the dependency acts like the install location)
- Installing Qt using the Qt Online Installer

If you have installed a dependency in such a way and CMake complains that it cannot find the dependency, you must explicitly tell CMake where that dependency is by adding the path to the dependency in the `CMAKE_PREFIX_PATH` CMake cache variable. Here is an example of doing so from the command line where a dependency has been installed into `$HOME/packages` (note that `$HOME` is an environment variable that expands to the current user's home directory):

```
cmake -DCMAKE_PREFIX_PATH=$HOME/packages <other-arguments> <src-dir>
```

The exact path that you must add to `CMAKE_PREFIX_PATH` can be tricky to determine as there is an elaborate set of possible paths that are appended to the paths in `CMAKE_PREFIX_PATH` during the search. Further, `find_package()` is looking for specific CMake files belonging to the dependency and knowing where these are can be less than straightforward (e.g. the path for the [CMake files of a Qt instance](core_component.md#qt-application-framework--qt-creator) installed with the Qt Online Installer is not obvious). See the [find_package() documentation](https://cmake.org/cmake/help/latest/command/find_package.html) for details of this process.

> :warning: Qt Creator automatically adds the path corresponding to the Qt instance selected in the "Kit" to `CMAKE_PREFIX_PATH`. If you are using Qt Creator and running into linker errors related to Qt, this may be due to a Qt version mismatch between the version used by the framework and the version used by a framework-dependent project. This mismatch may have inadvertently been caused by Qt Creator mucking with `CMAKE_PREFIX_PATH` in a way that you were not expecting.

It is important to also mention that the locations of dependencies previously found with `find_package()` are stored in the CMake cache and therefore the finding process does not occur again on subsequent builds. If you have previously built against a particular instance of a dependency and you wish to build against a different instance of the same dependency (e.g. you realized you needed a newer version of the dependency and you built/installed it from source), you may need to clear the CMake cache for the change to take effect.


---
## Installation and Usage as a CMake Config-file Package

> :warning: Before proceeding, ensure that the dependencies for the framework components you intend to use are installed on your system. See [this section above](#development-tools-setup-installing-framework-dependencies-and-ensuring-cmake-can-find-dependencies).

To start, you must clone this repository to a location of your preference. It is recommended to clone the repository to a subdirectory of your home directory (e.g. clone into `$HOME/projects/`). Once cloned, the framework can be built and installed on your system as a [CMake config-file package](https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html#id1) which is easily added as a library dependency to other CMake projects using `find_package()` in their `CMakeLists.txt` files.

### Building and Installing

Building and installing is very straightforward. After cloning the framework and changing directory into the cloned repository:

```
mkdir build
cd build
cmake ../
make install
```

If any framework dependencies are not installed in default search locations, you will need to add these locations to `CMAKE_PREFIX_PATH` as described above.

### Installation Location

CMake normally installs projects into `/usr/local` by default. However, this installation location requires `sudo` privileges for the `make install` command above and would install the framework for all users on the system. Since the Robotics Framework depends on the Qt Application Framework (see the [Core component](core_component.md) page for details), and framework-dependent projects will therefore also need to depend on Qt, it is very important that the same version of Qt is used when building the framework and when building the framework-dependent project. Once the Robotics Framework is built and installed, it may not be possible to determine which version of Qt was used to build it. Therefore, this makes an instance of the framework installed into `/usr/local` virtually useless to any other users that may attempt to use it because they cannot determine which version of Qt to use when building their framework-dependent project. For these reasons, it is not recommended to install the framework into `/usr/local`. Instead it should be installed somewhere in the user's home directory. This way, each user on the system has full control and awareness of the versions of the dependencies used to build the framework. To help ensure that installation into `/usr/local` does not happen by mistake, the framework is configured to install in `$HOME/frameworks/RoboticsFramework` by default.

If you wish to change the default install location, you can change the `CMAKE_INSTALL_PREFIX` CMake variable before the configuration step (or reconfigure after changing it). Please ensure that the last directory in the path is called `RoboticsFramework` (which is the CMake project name used by the framework). There are many ways to set `CMAKE_INSTALL_PREFIX` (e.g. from the command line, using the CMake GUI, from your IDE's own interface). As an example of doing this from the command line at configure time:

```
cmake -DCMAKE_INSTALL_PREFIX=$HOME/some/other/path/RoboticsFramework <other-arguments> ../
```

### Enabling Optional Components

The above `cmake` commands will configure the framework to only build and install the Core component. If you wish to enable optional components or build the documentation, you must enable the associated CMake variable specified by the documentation page for each component.
As an example of doing this from the command line at configure time:

```
cmake -DROBOTICS_DOCS=true -DROBOTICS_USE_CV=true <other-arguments> ../
```

The above example will configure the build to additionally build the [API documentation](#building-and-viewing-framework-api-documentation-using-doxygen) and the [Computer Vision](cv_component.md) component. Add additional `-D` arguments for each component variable you wish to enable.

### Using the Installed Framework in a Framework-dependent Project

> :warning: It is important that you do not forget to enable framework components during building and installing. If you do not do this, they will be missing when you try to use them in a framework-dependent project.

Once built and installed, the framework is ready to be used in any number of CMake projects. However, since the framework will not be installed in a location that CMake's `find_package()` function searches by default, we must explicitly tell CMake to look in the installed location by appending it to the list of paths contained in the `CMAKE_PREFIX_PATH` CMake variable. In the same way that adding paths to `CMAKE_PREFIX_PATH` may have been necessary for finding dependencies while building the framework, adding the path of the framework installation to `CMAKE_PREFIX_PATH` will certainly be necessary when building the framework-dependent project because it depends on the framework. This can be done just like setting any other CMake variable, but keep in mind that it is usually best to append to the variable rather than overwrite the entire variable. As an example of doing this from the framework-dependent project's `CMakeLists.txt` file:

```
# Append the framework installation directory to CMAKE_PREFIX_PATH so it can be found 
list(APPEND CMAKE_PREFIX_PATH $ENV{HOME}/frameworks)
```

Note that in this case, the `RoboticsFramework` directory at the end of the path can be omitted because `find_package()` will automatically look for a directory with the same name as the package (in this case `RoboticsFramework`). The `CMAKE_PREFIX_PATH` must be set properly before the call to `find_package()` that finds the framework. After ensuring `CMAKE_PREFIX_PATH` is set appropriately, the framework is included in a project from the project's `CMakeLists.txt` file using:

```
# Find the Robotics Framework as an imported target that can be linked against
find_package(RoboticsFramework REQUIRED COMPONENTS Core <optional-components>)

# Create an example target called exampleTarget
add_executable(exampleTarget src/example_executable.cpp)

# Link the Robotics Framework to the exampleTarget executable target
target_link_libraries(exampleTarget PUBLIC RoboticsFramework::RoboticsFramework)
```

Each Robotics Framework component used by the framework-dependent project must be specified in the `find_package()` call. The exact text to put in the `find_package()` call is provided on each component's documentation page. The Robotics Framework defines a target named `RoboticsFramework::RoboticsFramework`. Any target created with `add_executable()` or `add_library()` that depends on the framework needs to link against the `RoboticsFramework::RoboticsFramework` target using a `target_link_libraries()` call as in the example above.

Here is a minimal example `CMakeLists.txt` file for the framework-dependent project:

```
# Robotics Framework requires at least version 3.10
cmake_minimum_required(VERSION 3.10)

project(ExampleProject LANGUAGES CXX)

# Set the c++ standard
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Make the uic, moc, and rcc Qt components be run automatically
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

# Append the framework installation directory to CMAKE_PREFIX_PATH so it can be found 
list(APPEND CMAKE_PREFIX_PATH $ENV{HOME}/frameworks)

# Find the Robotics Framework as an imported target that can be linked against
find_package(RoboticsFramework REQUIRED COMPONENTS Core)

# Find Qt5
find_package(Qt5 REQUIRED COMPONENTS Core)

# Create an example target called exampleTarget
add_executable(exampleTarget src/example_executable.cpp)

# Link the Robotics Framework to the exampleTarget executable target
target_link_libraries(exampleTarget PUBLIC RoboticsFramework::RoboticsFramework Qt5::Core)
```

Notice in the example above that `Qt5` is also found and the target is linked against it. This is because you will very likely also need at least the Core component of Qt in the framework-dependent project.


---
## Add Framework Source as CMake Targets in Framework-dependent Project

> :warning: Before proceeding, ensure that the dependencies for the framework components you intend to use are installed on your system. See [this section above](#development-tools-setup-installing-framework-dependencies-and-ensuring-cmake-can-find-dependencies).

To start, you must clone this repository to a location of your preference. It is recommended to clone the repository to a subdirectory of your home directory (e.g. clone into `$HOME/projects/`). Once cloned, the source code itself (organized into CMake targets) can be added directly to other CMake projects using `add_subdirectory()` in their `CMakeLists.txt` files. In this way, the framework and the framework-dependent project merge to become one monolithic project that all gets built at the same time. Therefore, dependencies and CMake variables relevant to building the framework become dependencies and CMake variables of the framework-dependent project. In addition to whatever CMake configuration is necessary for the framework-dependent project, all of the instructions for properly configuring the build of the framework itself will apply to the monolithic project. That means,

1. Instead of calling `find_package()` in the framework-dependent project's `CMakeLists.txt` file to find the framework, the `add_subdirectory()` function is called. Note that there may still be other `find_package()` calls for other dependencies of the framework-dependent project.
2. The `CMAKE_PREFIX_PATH` CMake cache variable needs to be set appropriately to [make framework dependencies discoverable](#ensuring-cmake-can-find-dependencies) before calling the `add_subdirectory()` function. A framework installation path is not added to `CMAKE_PREFIX_PATH` because the framework does not get installed with this method (the framework is part of the framework-dependent project now).
3. Framework components need to be enabled through the use of the associated `ROBOTICS_*` CMake variables [as described above](#enabling-optional-components). Note that in this context it is less likely you will want to set these variables from the command line. Instead you will likely enable them through your IDE or with the CMake GUI.
4. Framework-dependent targets need to be linked against the framework targets using `target_link_libraries()` [as described above](#using-the-installed-framework-in-a-framework-dependent-project).
5. Since framework-dependent projects typically do not get installed (they are simply built and executed), instructions related to installation can be ignored.

The proper usage of `add_subdirectory()` is shown below:

```
add_subdirectory(<path-to-framework-source> RoboticsFramework)
```

The `<path-to-framework-source>` is the path to the top level `CMakeLists.txt` file of the framework repository (at present this corresponds to the root directory of the framework repository). It is usually best to avoid hard-coding your home directory into the provided path so the path should look something like `$ENV{HOME}/path/to/framework/src`. The second argument in the `add_subdirectory()` call above specifies the name of the subdirectory of the build directory into which the framework build artifacts should be placed. This argument is fairly arbitrary but is necessary to know when looking for the built [API Documentation](#building-and-viewing-framework-api-documentation-using-doxygen) for the framework.


Here is a minimal example `CMakeLists.txt` file for the framework-dependent project. In this example, the framework has been cloned into `$HOME/projects/roboticsframework`. Also in this example, the Eigen library, which is a dependency of the Robotics Framework, has been installed into `$HOME/libraries` instead of being installed into the default system location with APT.

```
# Robotics Framework requires at least version 3.10
cmake_minimum_required(VERSION 3.10)

project(ExampleProject LANGUAGES CXX)

# Set the c++ standard
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Make the uic, moc, and rcc Qt components be run automatically
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

# Append the path to the Eigen library to CMAKE_PREFIX_PATH so it is discoverable by CMake
list(APPEND CMAKE_PREFIX_PATH $ENV{HOME}/libraries)

# Add the RoboticsFramework source and targets
add_subdirectory($ENV{HOME}/projects/roboticsframework RoboticsFramework)

# Find Qt5
find_package(Qt5 REQUIRED COMPONENTS Core)

# Create an example target called exampleTarget
add_executable(exampleTarget src/example_executable.cpp)

# Link the Robotics Framework to the exampleTarget executable target
target_link_libraries(exampleTarget PUBLIC RoboticsFramework::RoboticsFramework Qt5::Core)
```

Notice in the example above that `Qt5` is also found and the target is linked against it. This is because you will very likely also need at least the Core component of Qt in the framework-dependent project.


---
## A Note on Including Framework Headers in Source Files of the Framework-dependent Project

Both use-cases above will set things up such that paths to framework header files are specified relative to the `src/` directory of the framework repository. These paths must be used when including framework headers in source files of the framework-dependent project. The following examples demonstrate this:

- Header: `RoboticsToolbox.h`
    - Header location in repository: `src/RoboticsToolbox.h`
    - Include directive: `#include "RoboticsToolbox.h`
- Header: `Timing.h`
    - Header location in repository: `src/Utilities/Timing.h`
    - Include directive: `#include "Utilities/Timing.h`
- Header: `MH5Robot.h`
    - Header location in repository: `src/Robots/Motoman/MH5Robots.h`
    - Include directive: `#include "Robots/Motoman/MH5Robot.h`


---
## Building and Viewing Framework API Documentation (using Doxygen)

The Robotics Framework is documented such that it can be used with Doxygen to generate browsable API documentation in HTML format. This documentation is necessary to learn what classes/functions are available and how to use them. Building documentation is disabled by default. To enable the building of documentation, the following CMake variable must be set to `true`:

- **CMake Variable**: `ROBOTICS_DOCS`

If documentation is enabled, a custom target named `RoboticsFramework_doxygen` will be defined. This target will be included in the default target of the build system and therefore will not need to be manually selected at build time. As long as `ROBOTICS_DOCS` is set to `true` the build will automatically also build the API documentation. For reference, the [FindDoxygen CMake module](https://cmake.org/cmake/help/latest/module/FindDoxygen.html) is used to find Doxygen and create/configure the `RoboticsFramework_doxygen` target.

When installing the framework, the documentation will be installed in `$CMAKE_INSTALL_PREFIX/share/RoboticsFramework/doc/html`. When using the framework via `add_subdirectory()`, the documentation will be built into `/path/to/build/directory/<add-subdirectory-argument>/html`. The value of `<add-subdirectory-argument>` is whatever was chosen as the second argument to the `add_subdirectory()` function. For the example `CMakeLists.txt` file above, the value of `<add-subdirectory-argument>` would be `RoboticsFramework`. The documentation can be viewed in a web browser by opening the `index.html` file found inside one of these directories.

Note that only those components of the framework that are enabled will appear in the generated API documentation.

If you wish to use Doxygen with source code in a project that uses the Robotics Framework, then you must make sure to have the following Doxygen configuration variable settings:

```
INCLUDE_PATH = /path/to/roboticsframework/src
MACRO_EXPANSION = YES
EXPAND_ONLY_PREDEF = YES
EXPAND_AS_DEFINED = STATE_MESSAGE STATE_MESSAGE_RECORD_ONLY
``` 

To set a particular Doxygen configuration variable, simply use the CMake `set()` function and prepend the variables above with `DOXYGEN_`. For example:

```
set(DOXYGEN_INCLUDE_PATH /path/to/roboticsframework/src)
set(DOXYGEN_MACRO_EXPANSION YES)
set(DOXYGEN_EXPAND_ONLY_PREDEF YES)
set(DOXYGEN_EXPAND_AS_DEFINED
    STATE_MESSAGE
    STATE_MESSAGE_RECORD_ONLY
)
```

The `INCLUDE_PATH` Doxygen configuration variable must be set to the path to the `/src` directory in the cloned framework repository. If you are not using an installed instance of the framework (i.e. using `add_subdirectory()`), you must manually specify `INCLUDE_PATH`. However, if you are using an installed instance of the framework via `find_package()`, you can alternatively set `INCLUDE_PATH` with the following in your `CMakeLists.txt` file:

```
get_target_property(rfcore_include_dir RoboticsFramework::Core INTERFACE_INCLUDE_DIRECTORIES)
set(DOXYGEN_INCLUDE_PATH ${rfcore_include_dir})
```

Also, preprocessing must be enabled (which it should be by default).