# SAMM Component

The primary SAMM component and its sub-component(s) are described below. Each description will specify library dependencies. Details of library dependencies are given after the component descriptions.

To include a component in the Robotics Framework build, set the corresponding CMake variable listed below to `true`. To use a component in a framework-dependent project, use the corresponding component name listed below in `find_package()` after the `COMPONENTS` keyword. Remember that sub-components require the primary component.

---
## Primary Component

- **CMake Variable**: ROBOTICS_USE_SAMM
- **Component Name**: `SAMM`
    - example usage: `find_package(RoboticsFramework REQUIRED COMPONENTS Core SAMM)`
- **Library Dependencies**:
    - Sensoray 626 (s626) PCI DAQ Card, API, and Driver (used to control the SAMM hardware) (see below)

The SAMM component adds the `Robots/SAMM` directory which contains the `SAMM` class, an implementation of the [SAMM device](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/SAMM.md) designed by Sam Wright. This implementation implements the robot interfaces from the `Robots` directory (which is part of the Core component). The SAMM is controlled via the [Sensoray 626 (s626) PCI DAQ](https://bitbucket.org/mmrobotics/s626-daq/src/master/) card. This component also creates a magnetic dipole robot in `Robots/MagneticDipoleRobots` called `MH5SAMMDipoleRobot` which uses the `SAMM` and the `MH5Robot` to create a hybrid robot able to control the state of a spherical magnet.

See the [SAMM - Spherical-Actuator-Magnet Manipulator](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/SAMM.md) for technical details about the SAMM in its implementation, including details of the parts that are implemented in this framework. The API documentation for the above mentioned classes is also helpful.

---
## Sub-Components

### SAMM Interface

- **CMake Variable**: ROBOTICS_USE_SAMM_SAMMInterface
- **Component Name**: `SAMMInterface`
    - example usage: `find_package(RoboticsFramework REQUIRED COMPONENTS Core SAMM SAMMInterface)`
- **Library Dependencies**:
    - OpenGL (see below)

A graphical user interface for controlling the SAMM as a stand alone unit. Found in `Robots/SAMM/SAMMInterface.h` which defines a `SAMMInterface` class.

---
## Library Dependencies

### Sensoray 626 (s626) PCI DAQ Card, API, and Driver (used to control the SAMM hardware)

- **Recommended Install Method**: Use the lab's `s626-daq` repository
- See the [s626-daq](https://bitbucket.org/mmrobotics/s626-daq/src/master/) repository for the details of installing and setting up an s626 card for use with this framework. Your computer may already be set up to use an s626 card. Before installing and setting up an s626 card, please verify that it has not already been set up on your computer.

### OpenGL

- **Recommended Install Method**: APT Package
- **APT Packages**: `libgl1-mesa-dev`, `libglu1-mesa-dev`
- These libraries are used to interact with OpenGL 3D rendering contexts.
