# Space Explorer 3D Mouse Component

The primary Space Explorer 3D Mouse component and its sub-component(s) are described below. Each description will specify library dependencies. Details of library dependencies are given after the component descriptions.

To include a component in the Robotics Framework build, set the corresponding CMake variable listed below to `true`. To use a component in a framework-dependent project, use the corresponding component name listed below in `find_package()` after the `COMPONENTS` keyword. Remember that sub-components require the primary component.

---
## Primary Component

- **CMake Variable**: ROBOTICS_USE_SPACE_EXPLORER
- **Component Name**: `SpaceExplorer`
    - example usage: `find_package(RoboticsFramework REQUIRED COMPONENTS Core SpaceExplorer)`
- **Library Dependencies**:
    - SpaceNav (see below)

This component allows you to use the Space Explorer 3D mouse from 3DConnexion. It adds the `SpaceExplorer` class to `Hardware/InputDevices`. It also adds the `SpaceExplorerFilePoseProducer` class to the `PoseProducers` directory which can be used as a class that produces a 6-DOF pose controlled by the Space Explorer 3D mouse.

---
## Sub-Components

This component does not currently have any sub components.

---
## Library Dependencies

### SpaceNav

- **Recommended Install Method**: APT Package
- **APT Packages**: `spacenavd`, `libspnav-dev`
- See the [SpaceExplorer 3D Mouse](https://bitbucket.org/mmrobotics/lab-wiki/wiki/lab_equipment/SpaceExplorer_3D_Mouse.md) lab wiki page for more details.