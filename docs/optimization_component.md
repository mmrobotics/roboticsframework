# Optimization Component

The primary Optimization component and its sub-component(s) are described below. Each description will specify library dependencies. Details of library dependencies are given after the component descriptions.

To include a component in the Robotics Framework build, set the corresponding CMake variable listed below to `true`. To use a component in a framework-dependent project, use the corresponding component name listed below in `find_package()` after the `COMPONENTS` keyword. Remember that sub-components require the primary component.

---
## Primary Component

- **CMake Variable**: ROBOTICS_USE_OPTIMIZATION
- **Component Name**: `Optimization`
    - example usage: `find_package(RoboticsFramework REQUIRED COMPONENTS Core Optimization)`
- **Library Dependencies**:
    - GNU Scientific Library (see below)

The Optimization component adds the `Optimization.h` header file to the `Utilities` directory. This component contains several optimization algorithms such as root finding, computing the pseudo inverse, and minimizing n-dimensional quadratic functions.

---
## Sub-Components

This component does not currently have any sub components.

---
## Library Dependencies

### GNU Scientific Library

- **Recommended Install Method**: APT Package
- **APT Package**: `libgsl-dev`
- This library has many mathematical functions and solvers. See the GNU Scientific Library documentation for more details.
