# Forward on the REQUIRED and QUIET keywords sent to find_package()
set(_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
    set(_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED REQUIRED)
endif()
set(_${CMAKE_FIND_PACKAGE_NAME}_QUIET)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    set(_${CMAKE_FIND_PACKAGE_NAME}_QUIET QUIET)
endif()

# Find dependencies needed by this component
# Note: we use find_package() for Qt5 instead of find_dependency() because
#   prior to CMake 3.15, they had a bug with packages that include components.
#   But we need to manually forward the QUIET and REQUIRED args that were
#   provided by the calling context.
# Note: we otherwise use find_package() instead of find_dependency() because we
#   have observed differences in import behavior between the two, specifically
#   for finding GSL (find_dependency() used the pkgconfig (gsl.pc - I think) file
#   rather than the FindGSL.cmake that comes with CMake)
find_package(Qt5
    ${_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED}
    ${_${CMAKE_FIND_PACKAGE_NAME}_QUIET}
    COMPONENTS Core Gui Widgets PrintSupport
)
find_package(Eigen3
    ${_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED}
    ${_${CMAKE_FIND_PACKAGE_NAME}_QUIET}
)

# Include the CMake auto-generated file with targets and dependencies
include("${CMAKE_CURRENT_LIST_DIR}/${CMAKE_FIND_PACKAGE_NAME}.cmake")
