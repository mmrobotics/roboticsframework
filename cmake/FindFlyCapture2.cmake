#
# Author:   Michael Bentley
#
# Finds libflycapture, both header files and link libraries
# This will define the following variables
#
#   FlyCapture2_FOUND   - True if we can find libflycapture and header files
#   FlyCapture2_INCLUDE_DIRS - directories to be included to use FlyCapture2
#   FlyCapture2_LIBRARIES - Path to libflycapture
#
# and the following imported targets
#
#   FlyCapture2::FlyCapture2
#

include(roboframework_find_simple_library)
roboframework_find_simple_library(
    NAME ${CMAKE_FIND_PACKAGE_NAME}
    EXAMPLE_HEADER flycapture/FlyCapture2.h
    LIBRARY flycapture
)
