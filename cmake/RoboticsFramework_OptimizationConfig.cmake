# Make sure dependent components are already loaded
if (NOT TARGET RoboticsFramework::Core)
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND False)
    return()
endif()

# Forward on the REQUIRED and QUIET keywords sent to find_package()
set(_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
    set(_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED REQUIRED)
endif()
set(_${CMAKE_FIND_PACKAGE_NAME}_QUIET)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    set(_${CMAKE_FIND_PACKAGE_NAME}_QUIET QUIET)
endif()

# Find additional dependencies needed by this component
# Note: we otherwise use find_package() instead of find_dependency() because we
#   have observed differences in import behavior between the two, specifically
#   for finding GSL (find_dependency() used the pkgconfig (gsl.pc - I think) file
#   rather than the FindGSL.cmake that comes with CMake)
find_package(GSL
    ${_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED}
    ${_${CMAKE_FIND_PACKAGE_NAME}_QUIET}
)

# Include the CMake auto-generated file with targets and dependencies
include("${CMAKE_CURRENT_LIST_DIR}/${CMAKE_FIND_PACKAGE_NAME}.cmake")
