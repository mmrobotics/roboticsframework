#
# Author:   Michael Bentley
#
# This file is used by the find_package() command to define imported library
# targets for the components of the RoboticsFramework library.  These library
# targets are used when linking against the framework from dependent projects.
# The RoboticsFramework is separated into components such that one does not
# need to build and install unnecessary parts of the framework.  When loading
# framework targets using find_package(), one must specify which components
# are needed by the project since each component may or may not be available
# (except for the Core component which is not an optional component).
#
# For example:
#
#   find_package(RoboticsFramework REQUIRED COMPONENTS Core CV Optimization)
#
# will create the following targets
#
# - RoboticsFramework::Core
# - RoboticsFramework::CV
# - RoboticsFramework::Optimization
#
# plus one additional target
#
# - RoboticsFramework::RoboticsFramework
#
# This additional target is an alias for including all of the imported
# components (e.g. in the example above it is an alias that includes Core, CV,
# and Optimization).  When linking against the framework, you can choose to use
# this alias for convenience, or link your target(s) against each component
# individually.
#
# This file manages dependencies between components and will automatically
# include components that are dependencies of requested components.  Those will
# also be included in the RoboticsFramework::RoboticsFramework target.  This
# file relies on the config CMake module for each component which handles
# the component's dependencies and creates the component's imported library
# target.
#

# Much of this logic was copied from Qt5Config.cmake and from the book
# Professional CMake


# Make sure at least one component was specified
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS)
  set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
    "The ${CMAKE_FIND_PACKAGE_NAME} package requires at least one component"
  )
  set(${CMAKE_FIND_PACKAGE_NAME}_FOUND False)
  return()
endif()


# ======================================
# Handle dependencies between components
# ======================================

# Local variable to have the list of components to find
set(_${CMAKE_FIND_PACKAGE_NAME}_COMPS
    ${${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS})

# All components depend on the Core component
if (NOT Core IN_LIST _${CMAKE_FIND_PACKAGE_NAME}_COMPS)
    list(INSERT _${CMAKE_FIND_PACKAGE_NAME}_COMPS 0 Core) # prepend
endif()

# If component is in the list, then insert to_add just before it
# This is useful for ensuring a component dependency is included before that
# component.
function(try_insert_before component to_add)
    set(listname _${CMAKE_FIND_PACKAGE_NAME}_COMPS)
    if (${component} IN_LIST ${listname})
        list(FIND ${listname} ${component} idx)
        list(INSERT ${listname} ${idx} ${to_add})
        set(${listname} "${${listname}}" PARENT_SCOPE)
    endif()
endfunction()

# Other inter-component dependencies
try_insert_before(Flea3Camera CV)     # Flea3Camera -> CV
try_insert_before(SAMMInterface SAMM) # SAMMInterface -> SAMM
try_insert_before(SAMM Optimization)  # SAMM -> Optimization

# Make sure each component is only specified once
list(REMOVE_DUPLICATES _${CMAKE_FIND_PACKAGE_NAME}_COMPS)

# ======================================
# End of dependencies between components
# ======================================


# Report to the console the found framework version
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    message(STATUS
        "${CMAKE_FIND_PACKAGE_NAME} version ${${CMAKE_FIND_PACKAGE_NAME}_VERSION}"
    )
endif()

# Forward on the REQUIRED and QUIET keywords sent to find_package()
set(_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
    set(_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED REQUIRED)
endif()
set(_${CMAKE_FIND_PACKAGE_NAME}_QUIET)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    set(_${CMAKE_FIND_PACKAGE_NAME}_QUIET QUIET)
endif()

# Set an empty value for NOTFOUND_MESSAGE - which indicates no errors finding components
set(_${CMAKE_FIND_PACKAGE_NAME}_NOTFOUND_MESSAGE)

# Append this directory to the module path to use FindXXX.cmake files
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})

# Include separate cmake config files for only the specified components
foreach(module ${_${CMAKE_FIND_PACKAGE_NAME}_COMPS})
    find_package(${CMAKE_FIND_PACKAGE_NAME}_${module}
        ${_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED}
        ${_${CMAKE_FIND_PACKAGE_NAME}_QUIET}
        PATHS ${CMAKE_CURRENT_LIST_DIR}
    )
    if (NOT ${CMAKE_FIND_PACKAGE_NAME}_${module}_FOUND
        AND ${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED_${module}
    )
        set(_${CMAKE_FIND_PACKAGE_NAME}_NOTFOUND_MESSAGE
            "${_${CMAKE_FIND_PACKAGE_NAME}_NOTFOUND_MESSAGE}Failed to find ${CMAKE_FIND_PACKAGE_NAME} component \"${module}\"\n"
        )
    else()
        message(STATUS "${CMAKE_FIND_PACKAGE_NAME}: Found component ${module}")
    endif()
endforeach()

# Handle errors from finding components
if (_${CMAKE_FIND_PACKAGE_NAME}_NOTFOUND_MESSAGE)
    set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
        "${_${CMAKE_FIND_PACKAGE_NAME}_NOTFOUND_MESSAGE}"
    )
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND False)
endif()

# Create a super dummy target that depends on all of the imported components
# RoboticsFramework::RoboticsFramework
set(_${CMAKE_FIND_PACKAGE_NAME}_COMP_TARGETS)
foreach(module ${_${CMAKE_FIND_PACKAGE_NAME}_COMPS})
    list(APPEND _${CMAKE_FIND_PACKAGE_NAME}_COMP_TARGETS
        ${CMAKE_FIND_PACKAGE_NAME}::${module}
    )
endforeach()
add_library(_${CMAKE_FIND_PACKAGE_NAME}_${CMAKE_FIND_PACKAGE_NAME} INTERFACE)
set_target_properties(_${CMAKE_FIND_PACKAGE_NAME}_${CMAKE_FIND_PACKAGE_NAME}
    PROPERTIES
        INTERFACE_LINK_LIBRARIES "${_${CMAKE_FIND_PACKAGE_NAME}_COMP_TARGETS}"
)
add_library(
    ${CMAKE_FIND_PACKAGE_NAME}::${CMAKE_FIND_PACKAGE_NAME}
    ALIAS
    _${CMAKE_FIND_PACKAGE_NAME}_${CMAKE_FIND_PACKAGE_NAME}
)
