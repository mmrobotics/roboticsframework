# Make sure dependent components are already loaded
if (NOT TARGET RoboticsFramework::Core)
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND False)
    return()
endif()

# Forward on the REQUIRED and QUIET keywords sent to find_package()
set(_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
    set(_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED REQUIRED)
endif()
set(_${CMAKE_FIND_PACKAGE_NAME}_QUIET)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    set(_${CMAKE_FIND_PACKAGE_NAME}_QUIET QUIET)
endif()

# Find additional dependencies needed by this component
# Note: we use find_package() for OpenCV instead of find_dependency() because
#   prior to CMake 3.15, they had a bug with packages that include components.
#   But we need to manually forward the QUIET and REQUIRED args that were
#   provided by the calling context.
find_package(OpenCV
    ${_${CMAKE_FIND_PACKAGE_NAME}_REQUIRED}
    ${_${CMAKE_FIND_PACKAGE_NAME}_QUIET}
    COMPONENTS core calib3d imgproc highgui imgcodecs videoio
)

# Include the CMake auto-generated file with targets and dependencies
include("${CMAKE_CURRENT_LIST_DIR}/${CMAKE_FIND_PACKAGE_NAME}.cmake")
