# Finds a simple library using an example header file and library name
#
# Signature:
#
#   roboframework_find_simple_library(
#       NAME <name>
#       EXAMPLE_HEADER <header-file>
#       LIBRARY <library>
#       [PATH_SUFFIXES <path1> <path2> ...]
#   )
#
# Parameters:
#
# - NAME: name of the package.  This should match Xxx from the FindXxx.cmake
#   file that uses this function.
# - EXAMPLE_HEADER: header file to find using find_file()
# - LIBRARY: static or shared library to find using find_library()
# - PATH_SUFFIXES: (optional) forwarded on to find_file() and find_library()
#
# This is to be used from within a FindXxx.cmake file.  For example:
#
#   roboframework_find_simple_library(
#       NAME ${CMAKE_FIND_PACKAGE_NAME}
#       EXAMPLE_HEADER spnav.h
#       LIBRARY spnav
#   )
#
# This will define the following variables into the parent scope
#
# - <NAME>_FOUND
# - <NAME>_INCLUDE_DIRS
# - <NAME>_LIBRARIES
#
# and defines the following targets
#
# - <NAME>::<NAME>
#
function(roboframework_find_simple_library)
    set(one_val_args NAME EXAMPLE_HEADER LIBRARY)
    set(multi_val_args PATH_SUFFIXES)
    cmake_parse_arguments(PKG "" "${one_val_args}" "${multi_val_args}" ${ARGN})

    # Find the library paths
    find_path(${PKG_NAME}_INCLUDE_DIR "${PKG_EXAMPLE_HEADER}"
        DOC "Include directory for ${PKG_NAME} header files"
        PATH_SUFFIXES "${PKG_PATH_SUFFIXES}"
    )
    find_library(${PKG_NAME}_LIBRARY "${PKG_LIBRARY}"
        DOC "Path to ${PKG_LIBRARY} library for the ${PKG_NAME} package"
        PATH_SUFFIXES "${PKG_PATH_SUFFIXES}"
    )
    mark_as_advanced(FORCE
        ${PKG_NAME}_INCLUDE_DIR
        ${PKG_NAME}_LIBRARY
    )

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(${PKG_NAME}
        FOUND_VAR ${PKG_NAME}_FOUND
        REQUIRED_VARS
            ${PKG_NAME}_INCLUDE_DIR
            ${PKG_NAME}_LIBRARY
    )
    set(${PKG_NAME}_FOUND ${${PKG_NAME}_FOUND} PARENT_SCOPE)

    if (${PKG_NAME}_FOUND)
        set(${PKG_NAME}_INCLUDE_DIRS "${${PKG_NAME}_INCLUDE_DIR}" PARENT_SCOPE)
        set(${PKG_NAME}_LIBRARIES "${${PKG_NAME}_LIBRARY}" PARENT_SCOPE)

        if (NOT TARGET "${PKG_NAME}::${PKG_NAME}")

            # Create imported library target (e.g., SpaceNav::SpaceNav)
            add_library("${PKG_NAME}::${PKG_NAME}" UNKNOWN IMPORTED)
            set_target_properties("${PKG_NAME}::${PKG_NAME}" PROPERTIES
                IMPORTED_LOCATION "${${PKG_NAME}_LIBRARY}"
                INTERFACE_INCLUDE_DIRECTORIES "${${PKG_NAME}_INCLUDE_DIRS}"
            )

        endif()
    endif()
endfunction()
