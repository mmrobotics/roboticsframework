#
# Author:   Michael Bentley
#
# Finds libspnav, both header files and link libraries
# This will define the following variables
#
#   SpaceNav_FOUND   - True if we can find libspnav and header files
#   SpaceNav_INCLUDE_DIRS - directories to be included to use SpaceNav
#   SpaceNav_LIBRARIES - Path to libspnav
#
# and the following imported targets
#
#   SpaceNav::SpaceNav
#

include(roboframework_find_simple_library)
roboframework_find_simple_library(
    NAME ${CMAKE_FIND_PACKAGE_NAME}
    EXAMPLE_HEADER spnav.h
    LIBRARY spnav
)
