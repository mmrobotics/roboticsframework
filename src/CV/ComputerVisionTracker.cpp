#include "ComputerVisionTracker.h"
#include "Utilities/Timing.h"


ComputerVisionTracker::ComputerVisionTracker() {

	timer = new QTimer(this);
	timer->setTimerType(Qt::PreciseTimer);
	connect(timer, &QTimer::timeout, this, &ComputerVisionTracker::trackMain);

}


/**
 * @brief Starts the QTimer that triggers the trackMain() function. This function is called by
 * ComputerVisionUI when starting the computer vision tracking. It can be called multiple times,
 * but must be called from the thread that ComputerVisionTracker has affinity to.
 */
void ComputerVisionTracker::startTimer() {

	// Use a UniqueConnection so if this function gets called multiple times, it does not add multiple
	// connections
	connect(QThread::currentThread(), &QThread::finished, timer, &QTimer::stop, Qt::UniqueConnection);
	timer->start();

}


/**
 * @brief Enables the ComputerVisionTracker. This function is called by ComputerVisionUI when starting
 * the computer vision tracking.
 */
void ComputerVisionTracker::enable() {

	// Initialize the variables used to compute update rate
	lastUpdateTimestamp = -1.0;
	actualUpdateRate.reset();

	// Start in the state that we do not have pending processing ROIs
	pendingProcessingROI1 = false;
	pendingProcessingROI2 = false;

	// Set the enabled variable to true
	enabled = true;

}


/**
 * @brief Disables the ComputerVisionTracker. This function is called by ComputerVisionUI when stopping
 * the computer vision tracking.
 */
void ComputerVisionTracker::disable() {

	enabled = false;

}


/**
 * @brief Returns a copy of the processed images. This function is called by ComputerVisionUI when displaying
 * the processed video feed.
 * @param frame1 cv::Mat into which the processed frame 1 is copied
 * @param frame2 cv::Mat into which the processed frame 2 is copied
 */
void ComputerVisionTracker::getProcessedFrames(cv::Mat& frame1, cv::Mat& frame2) const {

	returnProcessedFrame1.copyTo(frame1);
	returnProcessedFrame2.copyTo(frame2);

}


/**
 * @brief Returns the rate at which computer vision tracking is actually occurring. This function is called
 * by ComputerVisionUI to display the actual computer vision update rate.
 * @return
 */
double ComputerVisionTracker::getActualUpdateRate() const {

	if (enabled)
		return actualUpdateRate.value();
	else
		return 0.0;

}


/**
 * @brief This function is the receiving slot for the CameraPair::newFrames() signal and simply stores the parameters for
 * later use in trackMain() and track() which are run on the QTimer. ComputerVisionUI makes the necessary connection for
 * you.
 * @param frame1 Frame from camera 1
 * @param frame2 Frame from camera 2
 * @param camera1ROIPosition The position of the camera ROI for `frame1`
 * @param camera2ROIPosition The position of the camera ROI for `frame2`
 */
void ComputerVisionTracker::receiveNewFrames(const cv::Mat& frame1, const cv::Mat& frame2, const cv::Point& camera1ROIPosition, const cv::Point& camera2ROIPosition) {

	currentFrame1 = frame1;
	currentFrame2 = frame2;
	currentCamera1ROIPosition = camera1ROIPosition;
	currentCamera2ROIPosition = camera2ROIPosition;

}


/**
 * @brief This function is a slot meant to receive a new processing ROI for camera 1 from an instance of ROIServo.
 * ComputerVisionUI makes the necessary connection for you.
 * @param new_ROI The new processing ROI expressed with respect to the camera ROI
 * @param corresponding_camera_ROI_position The position of the camera ROI that corresponds with the new processing ROI
 */
void ComputerVisionTracker::setProcessingROICamera1(const cv::Rect& new_ROI, cv::Point corresponding_camera_ROI_position) {

	// Make sure the new ROI fits the current camera ROI. If not, don't accept the new ROI and print a warning to the console
	if (new_ROI.x < 0 || new_ROI.y < 0 || new_ROI.x + new_ROI.width > currentFrame1.size().width || new_ROI.y + new_ROI.height > currentFrame1.size().height) {
		std::cerr << "COMPUTER_VISION_TRACKER: The new processing ROI provided by the ROI servo for camera 1 is invalid. Are you sure your servo implementation is correct? WARNING" << std::endl;
		return;
	}

	// Store the new processing ROI, but don't let it take effect yet. That is trackMain()'s responsibility.
	nextProcessingROI1 = new_ROI;
	expectedCamera1ROIPosition = corresponding_camera_ROI_position;

	// Let trackMain() know that we have a pending processing ROI
	pendingProcessingROI1 = true;

}


/**
 * @brief This function is a slot meant to receive a new processing ROI for camera 2 from an instance of ROIServo.
 * ComputerVisionUI makes the necessary connection for you.
 * @param new_ROI The new processing ROI expressed with respect to the camera ROI
 * @param corresponding_camera_ROI_position The position of the camera ROI that corresponds with the new processing ROI
 */
void ComputerVisionTracker::setProcessingROICamera2(const cv::Rect& new_ROI, cv::Point corresponding_camera_ROI_position) {

	// Make sure the new ROI fits the current camera ROI. If not, don't accept the new ROI and print a warning to the console
	if (new_ROI.x < 0 || new_ROI.y < 0 || new_ROI.x + new_ROI.width > currentFrame2.size().width || new_ROI.y + new_ROI.height > currentFrame2.size().height) {
		std::cerr << "COMPUTER_VISION_TRACKER: The new processing ROI provided by the ROI servo for camera 2 is invalid. Are you sure your servo implementation is correct? WARNING" << std::endl;
		return;
	}

	// Store the new processing ROI, but don't let it take effect yet. That is trackMain()'s responsibility.
	nextProcessingROI2 = new_ROI;
	expectedCamera2ROIPosition = corresponding_camera_ROI_position;

	// Let trackMain() know that we have a pending processing ROI
	pendingProcessingROI2 = true;

}


/**
 * @brief This function is the main starting point for each iteration of computer vision tracking. It is responsible for
 * computing the actual update rate, extracting the processing portion from each frame, calling track() with the processing
 * frames, and emitting the target bounding boxes for each ROIServo.
 */
void ComputerVisionTracker::trackMain() {

	if (enabled) {

		// Compute the actual update rate
		double time = Timing::getSysTimeHighPrecision();
		if (lastUpdateTimestamp > 0)
			actualUpdateRate.update(1000.0 / (time - lastUpdateTimestamp));
		lastUpdateTimestamp = time;

		// If we have pending processing ROI's and the camera ROI position matches the position we are expecting,
		// then start using the next processing ROI. Otherwise keep using the one we have.
		if (pendingProcessingROI1 && currentCamera1ROIPosition.x == expectedCamera1ROIPosition.x && currentCamera1ROIPosition.y == expectedCamera1ROIPosition.y) {
			processingROI1 = nextProcessingROI1;
			pendingProcessingROI1 = false;
		}
		if (pendingProcessingROI2 && currentCamera2ROIPosition.x == expectedCamera2ROIPosition.x && currentCamera2ROIPosition.y == expectedCamera2ROIPosition.y) {
			processingROI2 = nextProcessingROI2;
			pendingProcessingROI2 = false;
		}

		// Perform the tracking as specified by the derived class
		track(currentFrame1(processingROI1),
			  currentFrame2(processingROI2),
			  currentCamera1ROIPosition + processingROI1.tl(),
			  currentCamera2ROIPosition + processingROI2.tl());

		// If we do not have a pending processing ROI, then emit new target bounding boxes (which essentially invokes
		// the ROIServo classes in this thread since the connection made in ComputerVisionUI is a direct connection.
		if (!pendingProcessingROI1)
			emit newTargetBoundingBoxCamera1(cam1TargetBoundingBox.centerX, cam1TargetBoundingBox.centerY, cam1TargetBoundingBox.width, cam1TargetBoundingBox.height);
		if (!pendingProcessingROI2)
			emit newTargetBoundingBoxCamera2(cam2TargetBoundingBox.centerX, cam2TargetBoundingBox.centerY, cam2TargetBoundingBox.width, cam2TargetBoundingBox.height);

	}

}
