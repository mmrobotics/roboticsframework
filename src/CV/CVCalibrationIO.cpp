#include "CVCalibrationIO.h"
#include <iostream>
#include <fstream>
#include <opencv2/core.hpp>


namespace CVCalibIO {

	/**
	 * @brief This function writes the intrinsic calibration information for a camera to file.
	 * The intrinsic calibration information of a camera consists of:
	 *
	 * 1. %Camera matrix - 3x3 matrix. See OpenCV documentation for details.
	 * 2. Distortion parameters - The number and meaning of the distortion parameters follow the
	 * conventions of OpenCV. See OpenCV documentation for details.
	 * @param filename File into which the calibration information should be written
	 * @param camera_matrix 3x3 calibrated camera matrix
	 * @param distortion_params Distortion coefficients
	 * @return True if successful
	 */
	bool writeCameraIntrinsics(const char* filename, const cv::Matx33d& camera_matrix, const cv::Mat& distortion_params) {

		std::ofstream file(filename, std::ios::binary);

		// Make sure file was opened correctly
		if (!file.is_open() || !file.good()) {
			std::cout << "CV_CALIB_IO: Could not create or write to camera intrinsics file " << filename << ". ERROR" << std::endl;
			return false;
		}

		// Write the 3x3 camera matrix
		for (int row = 0; row < 3; ++row)
			for (int col = 0; col < 3; ++col)
				file.write(reinterpret_cast<const char*>(&camera_matrix(row, col)), sizeof(double));

		// Write the number of distortion parameters
		int num_dist_params = distortion_params.size().height;
		file.write(reinterpret_cast<char*>(&num_dist_params), sizeof(int));

		// Write the distortion parameters
		for (int i = 0; i < num_dist_params; ++i)
			file.write(reinterpret_cast<const char*>(&distortion_params.at<double>(i)), sizeof(double));

		// Make sure the file IO didn't fail
		if (file.fail()) {
			std::cout << "CV_CALIB_IO: Failed while writing the camera intrinsics file " << filename << ". ERROR" << std::endl;
			file.close();
			return false;
		}

		file.close();

		return true;

	}


	/**
	 * @brief Reads the contents of the intrinsic calibration file specified by `filename` into the other
	 * parameters. The calibration file specified must be one that was created using writeCameraIntrinsics().
	 * See the documentation of writeCameraIntrinsics() for the details of the calibration parameters.
	 * @param filename File (created by writeCameraIntrinsics()) from which to read the calibration information
	 * @param camera_matrix 3x3 camera matrix
	 * @param distortion_params Distortion coefficients
	 * @return True if successful
	 */
	bool readCameraIntrinsics(const char* filename, cv::Matx33d& camera_matrix, cv::Mat& distortion_params) {

		std::ifstream file(filename, std::ios::binary);

		// Make sure file was opened correctly
		if (!file.is_open() || !file.good()) {
			std::cout << "CV_CALIB_IO: Could not open camera intrinsics file " << filename << ". ERROR" << std::endl;
			return false;
		}

		// Read the 3x3 camera matrix
		for (int row = 0; row < 3; ++row)
			for (int col = 0; col < 3; ++col)
				file.read(reinterpret_cast<char*>(&camera_matrix(row, col)), sizeof(double));

		// Read the number of distortion parameters
		int num_dist_params;
		file.read(reinterpret_cast<char*>(&num_dist_params), sizeof(int));

		// Initialize the distortion parameter cv::Mat object based on the number of parameters (which was just read in)
		distortion_params = cv::Mat(num_dist_params, 1, CV_64F, cv::Scalar::all(0));

		// Read the distortion parameters
		for (int i = 0; i < num_dist_params; ++i)
			file.read(reinterpret_cast<char*>(&distortion_params.at<double>(i)), sizeof(double));

		// Make sure the file IO didn't fail
		if (file.fail()) {
			std::cout << "CV_CALIB_IO: Failed while reading from camera intrinsics file " << filename << ". ERROR" << std::endl;
			file.close();
			return false;
		}

		file.close();

		return true;

	}


	/**
	 * @brief This function writes a rigid body transformation to file. The function expects just the top three rows of
	 * a standard 4x4 rigid body transformation matrix (i.e. a 3x4 matrix). This function can be used to write rigid body
	 * transformations that represent the transformation between two cameras, or between a camera and the world frame.
	 * Make sure to use a descriptive name for the file so it is clear what transformation the file contains.
	 * @param filename File into which the transformation should be written
	 * @param transform Top three rows of a 4x4 transformation matrix as described above
	 * @return True if successful
	 */
	bool writeRigidTransform(const char* filename, const cv::Matx34d& transform) {

		std::ofstream file(filename, std::ios::binary);

		// Make sure file was opened correctly
		if (!file.is_open() || !file.good()) {
			std::cout << "CV_CALIB_IO: Could not create or write to rigid transform file " << filename << ". ERROR" << std::endl;
			return false;
		}

		// Write the transformation
		for (int row = 0; row < 3; ++row)
			for (int col = 0; col < 4; ++col)
				file.write(reinterpret_cast<const char*>(&transform(row, col)), sizeof(double));

		// Make sure the file IO didn't fail
		if (file.fail()) {
			std::cout << "CV_CALIB_IO: Failed while writing the rigid transform file " << filename << ". ERROR" << std::endl;
			file.close();
			return false;
		}

		file.close();

		return true;

	}


	/**
	 * @brief Reads the contents of the rigid body transformation file specified by `filename` into the provided
	 * `transform` variable. The file specified must be one that was created using writeRigidTransform(). The
	 * read in transformation is a 3x4 matrix that represents the top three rows of a standard 4x4 rigid body
	 * transformation matrix.
	 * @param filename File (created by writeRigidTransform()) from which to read the rigid body transformation
	 * @param transform Top three rows of 4x4 transformation matrix as described above
	 * @return True if successful
	 */
	bool readRigidTransform(const char* filename, cv::Matx34d& transform) {

		std::ifstream file(filename, std::ios::binary);

		// Make sure file was opened correctly
		if (!file.is_open() || !file.good()) {
			std::cout << "CV_CALIB_IO: Could not open rigid transform file " << filename << ". ERROR" << std::endl;
			return false;
		}

		// Read the transformation
		for (int row = 0; row < 3; ++row)
			for (int col = 0; col < 4; ++col)
				file.read(reinterpret_cast<char*>(&transform(row, col)), sizeof(double));

		// Make sure the file IO didn't fail
		if (file.fail()) {
			std::cout << "CV_CALIB_IO: Failed while reading from rigid transform file " << filename << ". ERROR" << std::endl;
			file.close();
			return false;
		}

		file.close();

		return true;

	}


	/**
	 * @brief Writes the fundamental matrix for a camera pair to file.
	 * @param filename File into which the fundamental matrix should be written
	 * @param F Fundamental matrix
	 * @return True if successful
	 */
	bool writeFundamentalMatrix(const char* filename, const cv::Matx33d& F) {

		std::ofstream file(filename, std::ios::binary);

		// Make sure file was opened correctly
		if (!file.is_open() || !file.good()) {
			std::cout << "CV_CALIB_IO: Could not open fundamental matrix file " << filename << ". ERROR" << std::endl;
			return false;
		}

		// Write the fundamental matrix
		for (int row = 0; row < 3; ++row)
			for (int col = 0; col < 3; ++col)
				file.write(reinterpret_cast<const char*>(&F(row, col)), sizeof(double));

		// Make sure the file IO didn't fail
		if (file.fail()) {
			std::cout << "CV_CALIB_IO: Failed while writing the fundamental matrix file " << filename << ". ERROR" << std::endl;
			file.close();
			return false;
		}

		file.close();

		return true;

	}


	/**
	 * @brief Reads the fundamental matrix from the file specified by `filename` into the provided `F` variable. The file
	 * specified must be one that was created using writeFundamentalMatrix().
	 * @param filename File (created by writeFundamentalMatrix()) from which to read the fundamental matrix
	 * @param F Fundamental matrix
	 * @return True if successful
	 */
	bool readFundamentalMatrix(const char* filename, cv::Matx33d& F) {

		std::ifstream file(filename, std::ios::binary);

		// Make sure file was opened correctly
		if (!file.is_open() || !file.good()) {
			std::cout << "CV_CALIB_IO: Could not open fundamental matrix file " << filename << ". ERROR" << std::endl;
			return false;
		}

		// Read the fundamental matrix
		for (int row = 0; row < 3; ++row)
			for (int col = 0; col < 3; ++col)
				file.read(reinterpret_cast<char*>(&F(row, col)), sizeof(double));

		// Make sure the file IO didn't fail
		if (file.fail()) {
			std::cout << "CV_CALIB_IO: Failed while reading from fundamental matrix file " << filename << ". ERROR" << std::endl;
			file.close();
			return false;
		}

		file.close();

		return true;

	}

}
