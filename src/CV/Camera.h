#ifndef CAMERA_H
#define CAMERA_H

#include <opencv2/core.hpp>
#include <QObject>
#include <CV/OpenCVQtMetaTypeDeclarations.h>

/**
 * @brief The Camera class is an abstract class that acts as a standard interface for cameras that can be used by the computer
 * vision framework classes (e.g. CameraPair, etc.). For each model of camera, if you create a class that implements this
 * interface and complies with the specifications of each function, the camera can then be used with the computer vision
 * framework. It is very important that your implementation exactly complies with the specification for each function or you
 * will run into problems.
 *
 * The interface essentially allows the user to start and stop video capture as well as control the camera's ROI and frame rate.
 * It is also possible to determine the ROI and frame rate limits for the camera, which may depend on current settings such as
 * ROI size, Format 7 mode, etc. Some of these settings may be camera specific (e.g. Format 7 Mode) and therefore cannot be set
 * through this interface. These camera specific settings must be set though the interface of the specific camera implementation.
 *
 * When the camera is capturing video, you can obtain a copy of the image using the getFrame() functions or by connecting to
 * the newFrame() Qt signal. The frame provided in the newFrame() signal is a reference to the frame stored internally so you
 * should <b>not</b> modify that parameter in your slots because other entities could also be connected to the newFrame() signal
 * and you will affect the image they see. Specifically, the ComputerVisionTracker classes use the newFrame() signal.
 *
 * The interface requires OpenCV. As such, all cv::Mat frame images acquired through this interface (either through the getFrame()
 * function or the newFrame() signal) are in BGR format, which is standard for OpenCV. If you are implementing your own type
 * of Camera, make sure to follow this standard. Also, make sure you are emitting the newFrame() signal each time a new image is
 * received from your camera and that you are allocating new memory for the cv:Mat object for each frame as well. This way
 * we can take advantage of the built-in reference counting of cv::Mat objects. This reference counting makes using Qt signals
 * to pass images between classes really easy and efficient since copying a cv:Mat header is very efficient and any slot that
 * receives a copy of the cv::Mat header will have a reference to the common memory location for that image. Once all the slots
 * are done with the cv::Mat, the memory will be released automatically. Again, this means that slots should <b>not</b> modify
 * the cv::Mat object passed by the signal.
 *
 * See Flea3Camera for an example of how you should be implementing this class.
 */
class Camera : public QObject {

	Q_OBJECT

	public:
		Camera() {

			// Register signal types
			qRegisterMetaType<cv::Mat>();
			qRegisterMetaType<cv::Point>();

		}

		virtual ~Camera() = default;

		/**
		 * @brief Starts video capture from the camera. Uses the currently set frame rate and ROI.
		 * @return
		 */
		virtual bool startVideoStream() = 0;

		/**
		 * @brief Starts video capture from the camera with the specified frame rate. Uses the currently set ROI. This function
		 * is equivalent to calling setFPS() with `desiredFrameRate` and then calling startVideoStream() with no parameters if
		 * setFPS() returns true. Therefore, the rules for the desired frame rate as described by the documentation for setFPS()
		 * apply and the video capture will not be started if setFPS() would fail with the desired frame rate.
		 * @param desiredFrameRate Desired frame rate for the camera in frames per second
		 * @return
		 */
		virtual bool startVideoStream(float desiredFrameRate) = 0;

		/**
		 * @brief Starts video capture from the camera with the specified frame rate and ROI. This function is equivalent to
		 * calling setFPS() with `desiredFrameRate` and setROI() with `desiredROI` and then calling startVideoStream() with
		 * no parameters only if both setFPS() <i>and</i> setROI() return true. Therefore, the rules for the desired frame rate
		 * as described by the documentation for setFPS() apply and the video capture will not be started if setFPS() would fail
		 * with the desired frame rate. Furthermore, the desired ROI is subject to any rules and potential modification as
		 * described in the documentation for setROI() and video capture will also not be started if setROI() would fail with the
		 * desired ROI.
		 * @param desiredFrameRate Desired frame rate for the camera in frames per second
		 * @param desiredROI Desired ROI for video capture. This variable may be modified to reflect any adjustments to the requested
		 * ROI made by the camera according to camera specific ROI requirements.
		 * @return
		 */
		virtual bool startVideoStream(float desiredFrameRate, cv::Rect& desiredROI) = 0;

		/**
		 * @brief Stops video capture from the camera. The currently set frame rate and ROI are not changed.
		 * @return
		 */
		virtual bool stopVideoStream() = 0;

		/**
		 * @brief Returns the state of video capture. True if the camera is currently capturing video.
		 * @return
		 */
		virtual bool isVideoStreaming() const = 0;

		/**
		 * @brief Returns true if the camera is in a state of error.
		 * @return
		 */
		virtual bool isInError() const= 0;

		/**
		 * @brief Copies the most recently received frame to the given cv::Mat object. This function uses cv::Mat::copyTo() so be aware
		 * that changes to the cv::Mat object done by this function will be reflected in any other cv::Mat headers that refer to the same
		 * memory. The frame represents the ROI as returned by getROI() (i.e. it will not necessarily be the same size as the largest
		 * ROI possible for the camera).
		 * @param frame cv::Mat object into which the most recent frame will be copied
		 * @return True if success, false if the camera is not currently streaming
		 */
		virtual bool getFrame(cv::Mat& frame) const = 0;

		/**
		 * @brief This overload also gets the current frame in the same way, but also provides the corresponding ROI associated with
		 * that frame. Use this function when you need both the frame and the ROI rather than using getFrame() and getROI() separately
		 * because requesting them separately may result in a frame and ROI that do not correspond.
		 * @param frame cv::Mat object into which the most recent frame will be copied
		 * @param correspondingROI The corresponding ROI for the frame
		 * @return True if success, false if the camera is not currently streaming
		 */
		virtual bool getFrame(cv::Mat& frame, cv::Rect& correspondingROI) const = 0;

		/**
		 * @brief Returns the ROI associated with the most recently received frame. If the camera is not streaming, returns the current
		 * ROI setting that will be used when startVideoStream() is called without an ROI parameter. If you want both the current frame
		 * and its corresponding ROI, use the getFrame() function instead. This function is for when you only need the ROI.
		 * @return
		 */
		virtual cv::Rect getROI() const = 0;

		/**
		 * @brief Returns the largest ROI size achievable by this camera. This corresponds to the full camera frame.
		 * @return
		 */
		virtual cv::Size getMaxROISize() const = 0;

		/**
		 * @brief Returns the smallest ROI size achievable by this camera.
		 * @return
		 */
		virtual cv::Size getMinROISize() const = 0;

		/**
		 * @brief This function takes a desired camera ROI and adjusts it such that it meets any restrictions inherent to the camera. It does
		 * not make changes to the camera in any way. The adjustments made to the ROI are exactly the same as those made to the parameters of
		 * setROI() and setROIPosition(). In fact, this function behaves essentially the same as setROI() but does not actually set the ROI
		 * of the camera and can be used at any time (not just when the camera is not streaming). If the size of the ROI is too large, then
		 * this function returns false. The purpose of this function is to allow determination of what ROI will take effect if setROI() or
		 * setROIPosition() are used without actually making the change. It is primarily used by the ROIServo class and it is not expected
		 * that you should need to use this function since setROI() and setROIPosition() make the same adjustments. If the camera does not
		 * support ROI functionality, then this function sets `x` and `y` to 0 and `width` and `height` to be the same as that returned by
		 * getMaxROISize().
		 * @param x Desired x position of ROI to validate (0 corresponds to the left side of the image)
		 * @param y Desired y position of ROI to validate (0 corresponds to the top side of the image)
		 * @param width Desired width of ROI to validate
		 * @param height Desired height of ROI to validate
		 * @return
		 */
		virtual bool validateROI(int& x, int& y, int& width, int& height) const = 0;

		/**
		 * @brief Sets the ROI of the camera. Cannot be used when isVideoStreaming() returns true (i.e. this function will do nothing
		 * and return false if the camera is streaming). As most cameras may have to make adjustments to the requested ROI to fit
		 * certain restrictions, the given ROI variable will be modified to reflect these adjustments. You can inspect the value of
		 * the provided ROI variable after the call to this function to determine the actual ROI that was set. If the desired ROI
		 * extends beyond the camera's full frame, the desired ROI will be adjusted by shifting the ROI to reside within the full
		 * frame of the camera. However, if the ROI is larger than the result of getMaxROISize(), then this function does nothing and
		 * returns false.
		 *
		 * The ROI takes effect immediately (because the camera is not currently streaming). In other words, after this function returns,
		 * the getROI() function will return the same ROI that was set. Furthermore, the change to the ROI will be reflected in the
		 * frame rates returned by getMaxFPS() and getMinFPS() if the new ROI has an effect on these values. If a camera does not
		 * support ROI functionality, this function returns false unless the requested ROI matches the full camera frame ROI, in
		 * which case it returns true but effectively does nothing.
		 *
		 * @param newROI Desired ROI to set. This parameter will be modified by the camera to reflect the ROI that actually takes
		 * effect do to necessary adjustments that may be required by the camera.
		 * @return True if successful, false as specified above
		 */
		virtual bool setROI(cv::Rect& newROI) = 0;

		/**
		 * @brief Sets the ROI of the camera to the maximum ROI which is the full camera frame. This function is equivalent to
		 * calling setROI() using an ROI with the same size as that currently returned by getMaxROISize(). Therefore, it cannot
		 * be used while the camera is streaming video. Unless an error occurs, this function always returns true. Therefore,
		 * cameras that do not support ROI functionality will always return true.
		 * @return True if successful, false in an error occurred
		 */
		virtual bool setROIToMax() = 0;

		/**
		 * @brief Sets the position of the ROI of the camera. Can be used anytime (i.e. whether the camera is streaming or not). As most
		 * cameras may have to make adjustments to the requested ROI position to fit certain restrictions, the given position variables
		 * will be modified to reflect these adjustments. If the desired position causes the ROI to extend beyond the camera's full
		 * frame, the desired ROI will be adjusted by shifting the ROI to reside within the full frame of the camera. If the camera is
		 * not streaming, the changes to the ROI take effect immediately (i.e. getROI() reflects the change). If the camera is streaming,
		 * the changes may not immediately be reflected in the result of getROI(). In this case, the change will be reflected once the
		 * camera receives an image with the new ROI. However, the change to the ROI will be reflected immediately in the frame rates
		 * returned by getMaxFPS() and getMinFPS() if the new ROI has an effect on these values. If the camera does not support ROI
		 * functionality, or does not support changing the ROI position during streaming, this function returns false.
		 * @param x Desired x position of ROI (0 corresponds to the left side of the image)
		 * @param y Desired y position of ROI (0 corresponds to the top side of the image)
		 * @return True if successful, false as specified above
		 */
		virtual bool setROIPosition(int& x, int& y) = 0;

		/**
		 * @brief Returns the current desired frame rate of the camera (not the actual/achieved frame rate) in frames per second. If the camera is not
		 * streaming, returns the current frame rate setting that will be used when startVideoStream() is called without a frame rate parameter.
		 * @return
		 */
		virtual float getFPS() = 0;

		/**
		 * @brief Returns the maximum frame rate achievable by this camera in frames per second given the current settings of
		 * the camera (i.e. ROI size, Format 7 mode, etc). Some of these settings may be specific to a particular implementation
		 * of the Camera class (e.g. Format 7 mode) and therefore cannot be controlled through this interface.
		 * @return
		 */
		virtual float getMaxFPS() = 0;

		/**
		 * @brief Returns the absolute maximum frame rate the camera is capable of, regardless of the current camera settings. The number
		 * returned from this function is always the same value.
		 * @return
		 */
		virtual float getAbsoluteMaxFPS() const = 0;

		/**
		 * @brief Returns the minimum frame rate for this camera. This may depend on camera settings in a way similar to the
		 * maximum frame rate.
		 * @return
		 */
		virtual float getMinFPS() = 0;

		/**
		 * @brief Sets the frame rate of the camera. If the desired frame rate is larger than the current result of getMaxFPS()
		 * or smaller than the current result of getMinFPS() then this function will fail (i.e do nothing and return false). As
		 * the frame rate limits for a particular camera may change depending on the current camera settings (i.e. ROI size,
		 * Format 7 mode, etc), it is recommended that you check that the desired frame rate is within these bounds any time you
		 * set the frame rate. This function can be used anytime, but it is expected that you will primarily set the frame rate
		 * via the startVideoStream() function. If the camera does not support changing the frame rate while it is streaming video
		 * (i.e. when isVideoStreaming() returns true), then this function will also fail.
		 * @param desiredFrameRate Desired frame rate for the camera in frames per second
		 * @return True if successful, false otherwise
		 */
		virtual bool setFPS(float desiredFrameRate) = 0;

	signals:
		/**
		 * @brief A Qt signal emitted by the camera every time a new frame is received. The signal consists of a cv::Mat header
		 * which points to the frame, a cv::Point which contains the corresponding camera ROI position, and a timestamp.
		 *
		 * <b>Notes for Camera implementation:</b> There are very important rules about the computation of the timestamp. The
		 * timestamp must be in milliseconds and must be time registered with the system time or the CameraPair class will not be able
		 * to match frame pairs. For example, if the zero of your timestamps does not correspond to the system time zero point (e.g.
		 * the zero corresponds to the time the program started) then you must use Timing::getSysTime() (or another function from the
		 * Timing namespace that gets the system time if you need higher than millisecond precision) to convert it so it has the same
		 * zero as the system time. Ideally, the timestamps should be as close as possible to the actual system time corresponding to
		 * when the frame was taken. If the camera you are implementing supports embedded timestamp information, then this should be
		 * possible (see Flea3Camera for an example). Otherwise the best you may be able to do is capture the system time using
		 * Timing::getSysTime() (or similar) whenever you receive a new frame. However, be aware that computing timestamps this way can
		 * cause sputters in CameraPair frame rate if the interval between new frames is not consistent. For example, the interval
		 * between new frames obtained with the FlyCapture library, upon which the Flea3Camera relies, is not constant because the library
		 * performs some processing on the frame before handing it to the Flea3Camera class and this processing time does not take
		 * the same amount of time for every frame. Although the average frame rate matches the desired frame rate, if the Flea3Camera
		 * class did not use the embedded timestamp information to compute timestamps, a CameraPair made of two Flea3Camera objects would
		 * have an inconsistent frame rate. This problem arises because the Flea3Camera class is at the mercy of the FlyCapture library
		 * to determine when to obtain a new frame. In the implementation of your camera, you may be responsible for determining the moment
		 * a new frame is obtained. In this scenario, you can ensure the interval is consistent and the CameraPair frame rate should not
		 * suffer.
		 * @param frame A cv::Mat header which references the last received frame. <b>DO NOT MODIFY THIS PARAMETER IN YOUR SLOTS</b>
		 * @param cameraROIPosition The corresponding camera ROI position
		 * @param timestamp Timestamp in milliseconds registered with the system time which corresponds to the time the frame was captured
		 */
		void newFrame(const cv::Mat& frame, const cv::Point& cameraROIPosition, double timestamp);

};

#endif // CAMERA_H
