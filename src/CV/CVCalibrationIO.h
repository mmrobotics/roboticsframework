#ifndef CVCALIBRATIONIO_H
#define CVCALIBRATIONIO_H

#include <opencv2/core.hpp>

/**
 * The CVCalibIO namespace contains functions for reading and writing computer vision calibration files
 * that are compatible with CameraPair. All of the files are in binary format so if you want to create
 * a calibration file that can be used by CameraPair, you must create the file using the functions
 * provided here. Specifically, it is recommended that you create intrinsic calibration files through
 * the use of ComputerVisionUI since it is already built to walk you though the workflow of calibrating
 * the intrinsic calibration of the cameras in a camera pair. See the documentation for each function for
 * more details on the particular information each function writes to file.
 */
namespace CVCalibIO {

	bool writeCameraIntrinsics(const char* filename, const cv::Matx33d& camera_matrix, const cv::Mat& distortion_params);
	bool readCameraIntrinsics(const char* filename, cv::Matx33d& camera_matrix, cv::Mat& distortion_params);

	bool writeRigidTransform(const char* filename, const cv::Matx34d& transform);
	bool readRigidTransform(const char* filename, cv::Matx34d& transform);

	bool writeFundamentalMatrix(const char* filename, const cv::Matx33d& F);
	bool readFundamentalMatrix(const char* filename, cv::Matx33d& F);

}

#endif // CVCALIBRATIONIO_H
