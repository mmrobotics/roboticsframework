#ifndef COMPUTER_VISION_TRACKER_H
#define COMPUTER_VISION_TRACKER_H

#include <QObject>

#include <CV/CameraPair.h>
#include <Utilities/Filters.h>


/**
 * @brief The ComputerVisionTracker class is used to implement computer vision tracking algorithms that can be used with the computer
 * vision framework.
 *
 * The class is an abstract class that handles all of the logistical parts of computer vision tracking and leaves you to be able to
 * focus on just the tracking algorithm itself. In order to create your own computer vision tracking algorithm, you must create a
 * class that is derived from ComputerVisionTracker and implements the track() function. The track() function receives a pair of
 * images from a CameraPair which can be processed in any way you choose. The function also receives the location of the pair of
 * images within the full camera frame. The class has a pointer to the CameraPair which is used to access the camera calibration
 * information necessary for triangulation. It is expected that the result of your tracking will be emitted at the end of your
 * track() function as a StateMessage signal so the rest of your program can consume the tracking data. See the documentation of
 * the track() function for details on how to implement your own tracking algorithm.
 *
 * The class also supports the idea of displaying a pair of processed images so you can visually verify any stage of image processing
 * you wish. The ComputerVisionUI is set up to display these processed images. All you have to do is set them using
 * setReturnProcessedFrame1() and setReturnProcessedFrame2() at any point in your implementation of the track() function.
 *
 * Lastly, the class is designed to work with ROIServo and therefore allows you to specify a bounding box around the target(s) that
 * you want to keep in view when servoing the camera and/or processing ROI so the ROIServo can make sure you don't lose track of
 * your target(s). This makes use of the newTargetBoundingBoxCamera1() and newTargetBoundingBoxCamera2() signals which you should not
 * need to emit or connect to as this is already built into the framework. Again, see the documentation for the track() function for
 * more details.
 *
 * The track() function will be executed in the thread that ComputerVisionTracker is assigned to. When creating an instance of the
 * tracker on the stack in main() this means your tracking algorithm will run in the GUI thread. If you want your tracking algorithm
 * to run in a separate thread from the GUI thread, this can be done by moving it to another event-loop by creating an instance of
 * QThread, moving the tracker to that thread via QThread::moveToThread(), and then starting the QThread event loop with QThread::start().
 * Note that the ROIServo will always run in the same thread as ComputerVisionTracker because ComputerVisionUI uses direct
 * connections when connecting an ROIServo to ComputerVisionTracker.
 *
 * Other than implementing the track() function in your own ComputerVisionTracker classes, there is nothing you need to do. The rest
 * is taken care of by ComputerVisionTracker and ComputerVisionUI.
 */
class ComputerVisionTracker : public QObject {

	Q_OBJECT

	public:
		ComputerVisionTracker();
		void startTimer();
		void enable();
		void disable();
		void getProcessedFrames(cv::Mat& frame1, cv::Mat& frame2) const;
		double getActualUpdateRate() const;

	private:
		bool enabled = false;
		cv::Mat currentFrame1;
		cv::Mat currentFrame2;
		cv::Point currentCamera1ROIPosition;
		cv::Point currentCamera2ROIPosition;
		cv::Rect processingROI1;
		cv::Rect processingROI2;

		cv::Mat returnProcessedFrame1;
		cv::Mat returnProcessedFrame2;

		struct TargetBoundingBox {

			int centerX = 0;
			int centerY = 0;
			int width = 1;
			int height = 1;

		};

		bool pendingProcessingROI1 = false;
		bool pendingProcessingROI2 = false;
		cv::Point expectedCamera1ROIPosition;
		cv::Point expectedCamera2ROIPosition;
		cv::Rect nextProcessingROI1;
		cv::Rect nextProcessingROI2;

		QTimer* timer;

		Filters::MovingAverageFilter<double, 15> actualUpdateRate;
		double lastUpdateTimestamp;

		friend class ComputerVisionUI;

	private slots:
		void receiveNewFrames(const cv::Mat& frame1, const cv::Mat& frame2, const cv::Point& camera1ROIPosition, const cv::Point& camera2ROIPosition);
		void setProcessingROICamera1(const cv::Rect& new_ROI, cv::Point corresponding_camera_ROI_position);
		void setProcessingROICamera2(const cv::Rect& new_ROI, cv::Point corresponding_camera_ROI_position);
		void trackMain();

	signals:
		void newTargetBoundingBoxCamera1(int boundingBoxCenterX, int boundingBoxCenterY, int boundingBoxWidth, int boundingBoxHeight);
		void newTargetBoundingBoxCamera2(int boundingBoxCenterX, int boundingBoxCenterY, int boundingBoxWidth, int boundingBoxHeight);

	protected:
		const CameraPair* camera_pair;
		TargetBoundingBox cam1TargetBoundingBox;
		TargetBoundingBox cam2TargetBoundingBox;

		/**
		 * @brief This function is used to set the processed frame you want displayed in ComputerVisionUI for camera 1.
		 * The cv::Mat object you provide must be a 3 channel 8-bit BGR format. This function only saves a cv::Mat header
		 * to the processed frame so if you modify the provided frame through any other cv::Mat headers after this function
		 * call during your track() implementation, the modifications will be reflected in what you see in ComputerVisionUI.
		 * @param returnProcessedFrame The processed frame in BGR format
		 */
		void setReturnProcessedFrame1(const cv::Mat& returnProcessedFrame) { returnProcessedFrame1 = returnProcessedFrame; }

		/**
		 * @brief This function is used to set the processed frame you want displayed in ComputerVisionUI for camera 2.
		 * The cv::Mat object you provide must be a 3 channel 8-bit BGR format. This function only saves a cv::Mat header
		 * to the processed frame so if you modify the provided frame through any other cv::Mat headers after this function
		 * call during your track() implementation, the modifications will be reflected in what you see in ComputerVisionUI.
		 * @param returnProcessedFrame The processed frame in BGR format
		 */
		void setReturnProcessedFrame2(const cv::Mat& returnProcessedFrame) { returnProcessedFrame2 = returnProcessedFrame; }

		/**
		 * @brief This function is where the computer vision algorithm is implemented. Override this function to implement any
		 * arbitrary computer vision tracking algorithm. The function provides you with an image to process from each camera
		 * as well as the position of the top-left corner of that frame within the full frame of the camera. This position is
		 * important for triangulation because the cameras are calibrated using the full camera frame and therefore you want
		 * your image locations to be expressed in the full camera frame coordinates (as opposed to camera ROI or processing
		 * ROI coordinates). You may use whatever computer vision techniques you wish. When you need to access the camera pair
		 * calibration information for triangulation, this can be accessed through the `camera_pair` variable which is a const
		 * pointer to the CameraPair object. It is expected that the resulting tracking data (e.g. 3D postion or pose) will be
		 * emitted as a StateMessage signal at the end of your track() implementation, but ultimately it is up to you how the
		 * result gets propagated to the rest of your program (I strongly suggest you use the StateMessage system as it enables
		 * good modularity and separation of concerns). If you wish to be able to display some stage of the image processing in
		 * the ComputerVisionUI, use setReturnProcessedFrame1() and setReturnProcessedFrame2() in your track() implementation.
		 * Lastly, if you wish to use an ROIServo, make sure to set the members of the `cam1TargetBoundingBox` and
		 * `cam2TargetBoundingBox` variables in your track() implementation. These variables are protected members of
		 * ComputerVisionTracker and are used after your track() implementation completes to perform the ROI servoing. They are
		 * of type TargetBoundingBox and contain the following members: `centerX`, `centerY`, `width`, and `height`. These are
		 * the center location, width, and height of the bounding box surrounding the target(s) you wish to keep in view. The
		 * position of the center must be expressed relative to the top-left corner of the full camera frame.
		 * @param processingFrame1 Frame to process from camera 1
		 * @param processingFrame2 Frame to process from camera 2
		 * @param processingFrameAbsPosition1 The position of the top-left corner of the processing frame with respect to the
		 * full camera frame
		 * @param processingFrameAbsPosition2 The position of the top-left corner of the processing frame with respect to the
		 * full camera frame
		 */
		virtual void track(const cv::Mat& processingFrame1,
						   const cv::Mat& processingFrame2,
						   const cv::Point& processingFrameAbsPosition1,
						   const cv::Point& processingFrameAbsPosition2) = 0;

};

#endif // COMPUTER_VISION_TRACKER_H
