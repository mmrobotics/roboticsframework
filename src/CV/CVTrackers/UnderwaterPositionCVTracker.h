#ifndef UNDERWATER_POSITION_CV_TRACKER_H
#define UNDERWATER_POSITION_CV_TRACKER_H

#include <opencv2/core.hpp>

#include <CV/ComputerVisionTracker.h>
#include <RoboticsToolbox.h>
#include <Utilities/Filters.h>


/**
 * @brief <b>This class has not been tested yet! It should not be used right now.</b>
 */
class UnderwaterPositionCVTracker : public ComputerVisionTracker {

	Q_OBJECT

	private:
		static constexpr double water_refractive_index = 1.3330;
		static constexpr double air_refractive_index = 1.000293;

		double d1, d2;

		Filters::LowPassFilter<cv::Mat> operation_image_intensity_filter;
		cv::Mat temporary_shifted_filter;
		cv::Matx23f shift_transform;

		// Center distance kernels
		cv::Mat small_kernel;
		cv::Mat medium_kernel;
		cv::Mat large_kernel;
		cv::Mat operation_kernel;

		cv::Point2d previous_processing_ROI_full_frame_position;

		cv::Point2d pixelCam1;
		cv::Point2d pixelCam2;

		cv::Vec3d positionCameraFrame;
		cv::Vec3d positionWorldFrame;

		void track(const cv::Mat& processingFrame1,
				   const cv::Mat& processingFrame2,
				   const cv::Point& processingFrameAbsPosition1,
				   const cv::Point& processingFrameAbsPosition2) override;
		cv::Mat processImage(const cv::Mat& image, const cv::Point2d& processing_ROI_full_frame_position, cv::Point2d& position);
		void triangulate();

	public:
		// The d1 and d2 constants are the distances between the tank wall and the camera 1 and camera 2 origins, respectively.  (Camera 2 is the camera nearest to the robot arm.)
		UnderwaterPositionCVTracker(double d1, double d2);

	signals:
		void newPosition(const Robo::Position& position, long timestamp);

};

#endif // UNDERWATER_POSITION_CV_TRACKER_H
