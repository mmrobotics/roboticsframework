#ifndef POSITION_CV_TRACKER_H
#define POSITION_CV_TRACKER_H

#include <opencv2/core.hpp>

#include <CV/ComputerVisionTracker.h>
#include <RoboticsToolbox.h>
#include <Utilities/Filters.h>


class PositionCVTracker : public ComputerVisionTracker {

	Q_OBJECT

	private:
		// Center distance kernels
		cv::Mat small_kernel;
		cv::Mat medium_kernel;
		cv::Mat large_kernel;

		cv::Scalar hsv_lowerBound;
		cv::Scalar hsv_upperBound;

		// Morphological structuring elements
		cv::Mat erosion_element;
		cv::Mat dilation_element;

		Filters::MovingAverageFilter<cv::Point2d, 10 /* 25 */> positionFilter1;
		Filters::MovingAverageFilter<cv::Point2d, 10 /* 25 */> positionFilter2;

		cv::Point2d pixelCam1;
		cv::Point2d pixelCam2;

		cv::Vec3d positionCameraFrame;
		cv::Vec3d positionWorldFrame;

		void track(const cv::Mat& processingFrame1,
				   const cv::Mat& processingFrame2,
				   const cv::Point& processingFrameAbsPosition1,
				   const cv::Point& processingFrameAbsPosition2) override;
		cv::Mat processImage(const cv::Mat& image, cv::Point2d& position);
		void triangulate();

	public:
		PositionCVTracker();

	signals:
		void newPosition(const Robo::Position& position, long timestamp);

};

#endif // POSITION_CV_TRACKER_H
