#include "CapsulePoseCVTracker.h"
#include <iostream>
#include <vector>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include "CV/CVUtils.h"
#include "Utilities/Timing.h"


CapsulePoseCVTracker::CapsulePoseCVTracker()
	: small_kernel(41, 41, CV_8UC1),
	  medium_kernel(61, 61, CV_8UC1),
	  large_kernel(81, 81, CV_8UC1) {

	// Register signal types
	qRegisterMetaType<Robo::Pose5DOF>();

	// using higher res cameras so use a larger kernel. Use 81x81 for a bigger capsule
	CVUtils::createCenterDistanceKernel(small_kernel);
	CVUtils::createCenterDistanceKernel(large_kernel);
	CVUtils::createCenterDistanceKernel(medium_kernel);

	// art used these when there was no samm in the frame
//	hsv_lowerBound = Scalar(color, 50, 50);
//	hsv_upperBound = Scalar(color+30, 200, 200);

	// use this color inRange for if you dont have it surrounded by white and the lights aren't being diffused
//	hsv_lowerBound = cv::Scalar(70, 100, 50);
//	hsv_upperBound = cv::Scalar(100, 255, 130);
//	hsv_lowerBound = cv::Scalar(50, 50, 30);
//	hsv_upperBound = cv::Scalar(100, 255, 255);

	// use this color range if have everything set up for videos with light diffused and the bottom covered with white.
//	hsv_lowerBound = cv::Scalar(75, 100, 50);
//	hsv_upperBound = cv::Scalar(100, 255, 80);

	// values from objectTracking method
//	hsv_lowerBound = cv::Scalar(24, 28, 77);
//	hsv_upperBound = cv::Scalar(88, 118, 255);

//	hsv_lowerBound = cv::Scalar(50, 28, 20);
//	hsv_upperBound = cv::Scalar(88, 118, 255);

	// using straight tubes
//	hsv_lowerBound = cv::Scalar(49, 52, 36);
//	hsv_upperBound = cv::Scalar(102, 168, 169);

	// using rounded tube
	hsv_lowerBound = cv::Scalar(48, 19, 39);
	hsv_upperBound = cv::Scalar(116, 161, 88);

	int erosion_size = 2;
	int dilate_size = 2;
	erosion_element = getStructuringElement(cv::MORPH_ELLIPSE,
											cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1));
	dilation_element = getStructuringElement(cv::MORPH_ELLIPSE,
											 cv::Size(2 * dilate_size + 1, 2 * dilate_size + 1));

}


void CapsulePoseCVTracker::track(const cv::Mat& processingFrame1,
								 const cv::Mat& processingFrame2,
								 const cv::Point& processingFrameAbsPosition1,
								 const cv::Point& processingFrameAbsPosition2) {

	setReturnProcessedFrame1(processImage(processingFrame1, pixelCam1, directionCam1));
	setReturnProcessedFrame2(processImage(processingFrame2, pixelCam2, directionCam2));

	// Convert position to be in terms of the full image
	pixelCam1 += cv::Point2d(processingFrameAbsPosition1);
	pixelCam2 += cv::Point2d(processingFrameAbsPosition2);

	// Set the servo hints (The size of the bounding box is not used, leave at default)
	cam1TargetBoundingBox.centerX = pixelCam1.x;
	cam1TargetBoundingBox.centerY = pixelCam1.y;
	cam2TargetBoundingBox.centerX = pixelCam2.x;
	cam2TargetBoundingBox.centerY = pixelCam2.y;

	// Filter the positions
	pixelCam1 = positionFilter1.update(pixelCam1);
	pixelCam2 = positionFilter2.update(pixelCam2);

	triangulate();

}


cv::Mat CapsulePoseCVTracker::processImage(const cv::Mat& image, cv::Point2d& position, cv::Vec2d& direction) {

	// Scale down the image by 50% if it is larger than 300x300.
	double scale;
	cv::Mat operation_image;
	if ((image.rows > 300) && (image.cols > 300)) {
		scale = 0.5;
		cv::resize(image, operation_image, cv::Size(0,0), scale, scale, cv::INTER_NEAREST);
	}
	else {
		scale = 1.0;
		operation_image = image;
	}

	// Convert the image to HSV color space
	cv::Mat operation_image_hsv;
	cv::cvtColor(operation_image, operation_image_hsv, cv::COLOR_BGR2HSV);

	// Apply color threshold in HSV space
	cv::Mat operation_image_intensity;
	cv::inRange(operation_image_hsv, hsv_lowerBound, hsv_upperBound, operation_image_intensity);

	// Apply morphological operations (erode and dilate)
//	cv::Mat erosion_dst;
//	cv::Mat dilate_dst;
//	cv::erode(operation_image_intensity, erosion_dst, erosion_element);
//	cv::dilate(erosion_dst, dilate_dst, dilation_element);

	// Apply filter which makes the center of the object take on a maximum value
	cv::Mat operation_filter_result;
	//cv::filter2D(dilate_dst, operation_filter_result, CV_32S, medium_kernel, cv::Point(-1, -1), 0, cv::BORDER_CONSTANT); // performs DFT correlation for kernels larger than 11x11
	//cv::filter2D(operation_image_intensity, operation_filter_result, CV_32S, large_kernel, cv::Point(-1, -1), 0, cv::BORDER_CONSTANT); // performs DFT correlation for kernels larger than 11x11
	cv::GaussianBlur(operation_image_intensity, operation_filter_result, cv::Size(81, 81), 15 /*20*/); // biases the center pixels of the object more than the edges, but is faster since Gaussian kernels are separable

	// Find the center of the object
	cv::Point max_point;
	cv::minMaxLoc(operation_filter_result, nullptr, nullptr, nullptr, &max_point);

	position = max_point / scale;

	// FIXME: The calculation of direction below has not been tested yet!

	// Find the direction of the principle axis of the capsule's projection onto the image plane
	cv::Mat findContoursFrame;
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	operation_filter_result.convertTo(findContoursFrame, CV_8U);
	cv::findContours(findContoursFrame, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );

	// Find the rotated rectangles and ellipses for each contour
	cv::RotatedRect minEllipse;

	std::vector<cv::Point> largest_contour;
	double cA = 0.0;
	double thetaRad = 0;
//	int posX = 0;
//	int posY = 0;
	for(unsigned long i = 0; i < contours.size(); i++ ) {

		if (contourArea(contours[i]) > cA) {
			cA = contourArea(contours[i]);
			largest_contour = contours[i];
		}

	}

	if (largest_contour.size() > 5 ) {

		minEllipse = fitEllipse(cv::Mat(largest_contour));

		cv::Moments M;
		M = moments(largest_contour, true);
//		posX = static_cast<int>(M.m10 / M.m00);
//		posY = static_cast<int>(M.m01 / M.m00);

		thetaRad = 0.5 * atan2((2*M.nu11),(M.nu20 - M.nu02));

	}

	direction(0) = cos(thetaRad);
	direction(1) = sin(thetaRad);

	cv::Mat return_image;
	cv::cvtColor(operation_image_intensity, return_image, cv::COLOR_GRAY2BGR);
	cv::circle(return_image, max_point, 5, cv::Scalar(255, 0, 0), 3);
	cv::arrowedLine(return_image, max_point, max_point + 20 * cv::Point(direction), cv::Scalar(255, 0, 0), 3);

	return return_image;

}


void CapsulePoseCVTracker::triangulate() {

	std::vector<cv::Point2d> input_array_1 = { pixelCam1, pixelCam1 + 100 * cv::Point2d(directionCam1) };
	std::vector<cv::Point2d> input_array_2 = { pixelCam2, pixelCam2 + 100 * cv::Point2d(directionCam2) };
	std::vector<cv::Point2d> output_array_1;
	std::vector<cv::Point2d> output_array_2;

	// Unroll the camera matrix and the lens distortion to get to the normalized points (i.e. the points on the image
	// plane that is parallel to the x-y plane of the camera frame and resides 1 unit in the z-direction. See OpenCV
	// documentation for details.)
	cv::undistortPoints(input_array_1, output_array_1, camera_pair->cameraMatrix1(), camera_pair->distortionParams1());
	cv::undistortPoints(input_array_2, output_array_2, camera_pair->cameraMatrix2(), camera_pair->distortionParams2());

	// Determine the projection line for both pixels in the Camera 2 frame
	cv::Vec3d u1 = camera_pair->get_2R1() * cv::Vec3d(output_array_1[0].x, output_array_1[0].y, 1.0);
	cv::Vec3d u2(output_array_2[0].x, output_array_2[0].y, 1.0);

	// Find the point where both lines are the closest
	cv::Matx22d A;
	A << u1.dot(u1), -u2.dot(u1), u1.dot(u2), -u2.dot(u2);
	cv::Vec2d b;
	b << -camera_pair->get_2T21().dot(u1), -camera_pair->get_2T21().dot(u2);
	cv::Vec2d soln = A.inv() * b;
	double z1 = soln(0);
	double z2 = soln(1);

	// Select the midpoint of the shortest segment connecting both lines
	positionCameraFrame = 0.5 * (camera_pair->get_2T21() + z1*u1 + z2*u2);

	// Transform to world coordinates
	positionWorldFrame = camera_pair->camera2ToWorldFrameTransform() * cv::Vec4d(positionCameraFrame(0), positionCameraFrame(1), positionCameraFrame(2), 1);

	// Find the orientation in the camera frame
	// Get two more vectors for points on the principle axis line
	cv::Vec3d v1 = camera_pair->get_2R1() * cv::Vec3d(output_array_1[1].x, output_array_1[1].y, 1.0);
	cv::Vec3d v2(output_array_2[1].x, output_array_2[1].y, 1.0);

	// Take the cross product to get the vector normal to the plane in which the two vectors lie for each of the image planes
	cv::Vec3d c1 = cv::normalize(v1.cross(u1));
	cv::Vec3d c2 = cv::normalize(v2.cross(u2));
	// Take cross product of two orthogonal vectors to find vector that is parallel to the line where the planes intersect
	cv::Vec3d capsuleHeading = c1.cross(c2);

	// Transform capsule heading to world frame.
	capsuleHeadingWorldFrame = camera_pair->camera2ToWorldFrameTransform() * cv::Vec4d(capsuleHeading(0), capsuleHeading(1), capsuleHeading(2), 0);

	// Form the Pose5DOF
	pose.position = Robo::Vector3d(positionWorldFrame(0), positionWorldFrame(1), positionWorldFrame(2));
	pose.heading = Robo::Vector3d(capsuleHeadingWorldFrame(0), capsuleHeadingWorldFrame(1), capsuleHeadingWorldFrame(2));;

	// Emit the new capsule pose
	emit newPose(pose, Timing::getSysTime());

}
