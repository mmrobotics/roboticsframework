#ifndef CAPSULE_POSE_CV_TRACKER_H
#define CAPSULE_POSE_CV_TRACKER_H

#include <opencv2/core.hpp>

#include <CV/ComputerVisionTracker.h>
#include <RoboticsToolbox.h>
#include <Utilities/Filters.h>


/**
 * @brief <b>This class has not been tested yet! It should not be used right now.</b>
 */
class CapsulePoseCVTracker : public ComputerVisionTracker {

	Q_OBJECT

	private:
		// Center distance kernels
		cv::Mat small_kernel;
		cv::Mat medium_kernel;
		cv::Mat large_kernel;

		cv::Scalar hsv_lowerBound;
		cv::Scalar hsv_upperBound;

		// Morphological structuring elements
		cv::Mat erosion_element;
		cv::Mat dilation_element;

		Filters::MovingAverageFilter<cv::Point2d, 25> positionFilter1;
		Filters::MovingAverageFilter<cv::Point2d, 25> positionFilter2;

		cv::Point2d pixelCam1;
		cv::Point2d pixelCam2;
		cv::Vec2d directionCam1;
		cv::Vec2d directionCam2;

		cv::Vec3d positionCameraFrame;
		cv::Vec3d positionWorldFrame;
		cv::Vec3d capsuleHeadingWorldFrame;
		Robo::Pose5DOF pose;

		void track(const cv::Mat& processingFrame1,
				   const cv::Mat& processingFrame2,
				   const cv::Point& processingFrameAbsPosition1,
				   const cv::Point& processingFrameAbsPosition2) override;
		cv::Mat processImage(const cv::Mat& image, cv::Point2d& position, cv::Vec2d& direction);
		void triangulate();

	public:
		CapsulePoseCVTracker();

	signals:
		void newPose(const Robo::Pose5DOF& pose, long timestamp);

};

#endif // CAPSULE_POSE_CV_TRACKER_H
