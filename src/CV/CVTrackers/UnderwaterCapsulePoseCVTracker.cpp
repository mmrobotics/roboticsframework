#include "UnderwaterCapsulePoseCVTracker.h"
#include <iostream>
#include <opencv2/imgproc.hpp>
#include "CV/CVUtils.h"
#include "Utilities/Timing.h"


UnderwaterCapsulePoseCVTracker::UnderwaterCapsulePoseCVTracker(double d1, double d2)
	: d1(d1),
	  d2(d2),
	  operation_image_intensity_filter(10, 89),
	  shift_transform(cv::Matx23f::eye()),
	  small_kernel(9, 9, CV_8UC1),
	  medium_kernel(41, 41, CV_8UC1),
	  large_kernel(81, 81, CV_8UC1),
	  previous_processing_ROI_full_frame_position(0.0, 0.0) {

	// Register signal types
	qRegisterMetaType<Robo::Pose5DOF>();

	CVUtils::createCenterDistanceKernel(small_kernel);
	CVUtils::createCenterDistanceKernel(medium_kernel);
	CVUtils::createCenterDistanceKernel(large_kernel);

}


void UnderwaterCapsulePoseCVTracker::track(const cv::Mat& processingFrame1,
										   const cv::Mat& processingFrame2,
										   const cv::Point& processingFrameAbsPosition1,
										   const cv::Point& processingFrameAbsPosition2) {

	setReturnProcessedFrame1(processImage(processingFrame1, processingFrameAbsPosition1, pixelCam1, directionCam1));
	setReturnProcessedFrame2(processImage(processingFrame2, processingFrameAbsPosition2, pixelCam2, directionCam2));

	// Convert position to be in terms of the full image
	pixelCam1 += cv::Point2d(processingFrameAbsPosition1);
	pixelCam2 += cv::Point2d(processingFrameAbsPosition2);

	// Set the servo hints (The size of the bounding box is not used, leave at default)
	cam1TargetBoundingBox.centerX = pixelCam1.x;
	cam1TargetBoundingBox.centerY = pixelCam1.y;
	cam2TargetBoundingBox.centerX = pixelCam2.x;
	cam2TargetBoundingBox.centerY = pixelCam2.y;

	triangulate();

}


cv::Mat UnderwaterCapsulePoseCVTracker::processImage(const cv::Mat& image, const cv::Point2d& processing_ROI_full_frame_position, cv::Point2d& position, cv::Vec2d& direction) {

	// Scale down the image by 50% if it is larger than 300x300.
	double scale;
	cv::Mat operation_image;
	if ((image.rows > 300) && (image.cols > 300)) {
		scale = 0.5;
		resize(image, operation_image, cv::Size(0,0), scale, scale, cv::INTER_NEAREST);
		resize(large_kernel, operation_kernel, cv::Size(0,0), scale, scale, cv::INTER_NEAREST);
	}
	else {
		scale = 1.0;
		operation_image = image;
		operation_kernel = large_kernel;
	}

	// Convert the image to HSV color space
	cv::Mat operation_image_hsv;
	cv::cvtColor(operation_image, operation_image_hsv, cv::COLOR_BGR2HSV);

	// Apply color threshold in HSV space
	int color = 75;
	cv::Mat operation_image_intensity;
	cv::inRange(operation_image_hsv, cv::Scalar(color, 100, 30), cv::Scalar(color+25, 230, 200), operation_image_intensity);

	// Apply a filter (in time) to the image (I think here we are shifting the already filtered image so that it will line up
	// with the new image when we call the update() function
	cv::Mat operation_image_intensity_float;
	operation_image_intensity.convertTo(operation_image_intensity_float, CV_32F);
	if (operation_image_intensity_filter.value().size() != operation_image_intensity_float.size()) {
		operation_image_intensity_filter.seed(cv::Mat(operation_image_intensity_float.rows, operation_image_intensity_float.cols, CV_32F, cv::Scalar::all(0)));
		temporary_shifted_filter = cv::Mat(operation_image_intensity_float.rows, operation_image_intensity_float.cols, CV_32F, cv::Scalar::all(0));
	}
	else {
		shift_transform(0, 2) = previous_processing_ROI_full_frame_position.x - processing_ROI_full_frame_position.x;
		shift_transform(1, 2) = previous_processing_ROI_full_frame_position.y  - processing_ROI_full_frame_position.y;

		cv::warpAffine(operation_image_intensity_filter.value(), temporary_shifted_filter, shift_transform, operation_image_intensity_filter.value().size());
		temporary_shifted_filter.copyTo(operation_image_intensity_filter.value());

		operation_image_intensity_filter.update(operation_image_intensity_float);
	}
	previous_processing_ROI_full_frame_position = cv::Point2d(processing_ROI_full_frame_position.x, processing_ROI_full_frame_position.y);

	// Apply filter which makes the center of the object take on a maximum value
	cv::Mat operation_filter_result;
	cv::filter2D(operation_image_intensity_filter.value(), operation_filter_result, CV_32F, operation_kernel, cv::Point(-1, -1), 0, cv::BORDER_CONSTANT); // performs DFT correlation for kernels larger than 11x11
	//cv::GaussianBlur(operation_image_intensity, operation_filter_result, Size(41, 41), 15); // biases the center pixels of the object more than the edges, but is faster since Gaussian kernels are separable


	// Find the center of the object
	double max_val;
	cv::Point max_point;
	cv::minMaxLoc(operation_filter_result, nullptr, &max_val, nullptr, &max_point);

	operation_filter_result *= (255.0 / max_val);

	position = max_point / scale;

	// Compute the expected value (weighted mean or center of mass)
	double sum_x = 0.0;
	double sum_y = 0.0;
	double total_weight = 0.0;
	for (int cc = 0; cc < operation_filter_result.rows; ++cc) {
		for (int dd = 0; dd < operation_filter_result.cols; ++dd) {

			double weight = double(operation_filter_result.at<float>(cc, dd));

			if (weight > 0) {
				sum_x += weight * dd;
				sum_y += weight * cc;
				total_weight += weight;
			}

		}
	}
	cv::Point2d weighted_mean(sum_x / total_weight, sum_y / total_weight);

	// Compute the covariance matrix (second central mass moment)
	cv::Mat cov(2, 2, CV_64F, 0.0);
	for (int aa = 0; aa < operation_filter_result.rows; ++aa) {
		for (int bb = 0; bb < operation_filter_result.cols; ++bb) {

			float weight = operation_filter_result.at<float>(aa, bb);

			if (weight > 0) {
				cov.at<double>(0, 0) += std::pow(weight, 2.0) * (bb - weighted_mean.x) * (bb - weighted_mean.x);
				cov.at<double>(1, 1) += std::pow(weight, 2.0) * (aa - weighted_mean.y) * (aa - weighted_mean.y);
				cov.at<double>(1, 0) += std::pow(weight, 2.0) * (bb - weighted_mean.x) * (aa - weighted_mean.y);
			}

		}
	}
	cov.at<double>(0,1) = cov.at<double>(1,0); // Covariance matrix is symmetric
	cov /= pow(total_weight, 2.0);

	// Get the principle axes of the object from the eigen vectors of the covariance matrix
	cv::Mat eigenvalues;
	cv::Mat eigenvectors;
	cv::eigen(cov, eigenvalues, eigenvectors);
	direction(0) = eigenvectors.row(0).at<double>(0);
	direction(1) = eigenvectors.row(0).at<double>(2);

	// TODO: I'm not sure why we are doing something different for position here
	if ((image.rows > 300) && (image.cols > 300)) {

	}
	else if (total_weight > 0) {
		position = weighted_mean / scale;
	}

	cv::Mat return_image;
	cv::cvtColor(operation_filter_result, return_image, cv::COLOR_GRAY2BGR);
	cv::circle(return_image, max_point, 5, cv::Scalar(255, 0, 0), 3);
	cv::arrowedLine(return_image, max_point, max_point + 20 * cv::Point(direction), cv::Scalar(255, 0, 0), 3);

	return return_image;
}


void UnderwaterCapsulePoseCVTracker::triangulate() {

	// Not implemented yet
	emit newPose(pose, Timing::getSysTime());
		
}
