#include "PositionCVTracker.h"
#include <iostream>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include "CV/CVUtils.h"
#include "Utilities/Timing.h"


PositionCVTracker::PositionCVTracker()
	: small_kernel(41, 41, CV_8UC1),
	  medium_kernel(61, 61, CV_8UC1),
	  large_kernel(81, 81, CV_8UC1) {

	// Register signal types
	qRegisterMetaType<Robo::Position>();

	// using higher res cameras so use a larger kernel. Use 81x81 for a bigger capsule
	CVUtils::createCenterDistanceKernel(small_kernel);
	CVUtils::createCenterDistanceKernel(large_kernel);
	CVUtils::createCenterDistanceKernel(medium_kernel);

	// art used these when there was no samm in the frame
//	hsv_lowerBound = Scalar(color, 50, 50);
//	hsv_upperBound = Scalar(color+30, 200, 200);

	// use this color inRange for if you dont have it surrounded by white and the lights aren't being diffused
//	hsv_lowerBound = cv::Scalar(70, 100, 50);
//	hsv_upperBound = cv::Scalar(100, 255, 130);
//	hsv_lowerBound = cv::Scalar(50, 50, 30);
//	hsv_upperBound = cv::Scalar(100, 255, 255);

	// use this color range if have everything set up for videos with light diffused and the bottom covered with white.
//	hsv_lowerBound = cv::Scalar(75, 100, 50);
//	hsv_upperBound = cv::Scalar(100, 255, 80);

	// values from objectTracking method
//	hsv_lowerBound = cv::Scalar(24, 28, 77);
//	hsv_upperBound = cv::Scalar(88, 118, 255);

//	hsv_lowerBound = cv::Scalar(50, 28, 20);
//	hsv_upperBound = cv::Scalar(88, 118, 255);

	// using straight tubes
//	hsv_lowerBound = cv::Scalar(49, 52, 36);
//	hsv_upperBound = cv::Scalar(102, 168, 169);

//	// using rounded tube
//	hsv_lowerBound = cv::Scalar(48, 19, 39);
//	hsv_upperBound = cv::Scalar(116, 161, 88);

//	// using purple dot
//	hsv_lowerBound = cv::Scalar(116, 71, 20);
//	hsv_upperBound = cv::Scalar(122, 160, 255);

	// using green dot
	hsv_lowerBound = cv::Scalar(75, 71, 20);
	hsv_upperBound = cv::Scalar(85, 160, 255);

	int erosion_size = 2;
	int dilate_size = 2;
	erosion_element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
												cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1));
	dilation_element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
												 cv::Size(2 * dilate_size + 1, 2 * dilate_size + 1));

}


void PositionCVTracker::track(const cv::Mat& processingFrame1,
							  const cv::Mat& processingFrame2,
							  const cv::Point& processingFrameAbsPosition1,
							  const cv::Point& processingFrameAbsPosition2) {

	setReturnProcessedFrame1(processImage(processingFrame1, pixelCam1));
	setReturnProcessedFrame2(processImage(processingFrame2, pixelCam2));

	// Convert position to be in terms of the full image
	pixelCam1 += cv::Point2d(processingFrameAbsPosition1);
	pixelCam2 += cv::Point2d(processingFrameAbsPosition2);

	// Set the servo hints (The size of the bounding box is not used, leave at default)
	cam1TargetBoundingBox.centerX = pixelCam1.x;
	cam1TargetBoundingBox.centerY = pixelCam1.y;
	cam2TargetBoundingBox.centerX = pixelCam2.x;
	cam2TargetBoundingBox.centerY = pixelCam2.y;

	// Filter the positions
	pixelCam1 = positionFilter1.update(pixelCam1);
	pixelCam2 = positionFilter2.update(pixelCam2);

	triangulate();

}


cv::Mat PositionCVTracker::processImage(const cv::Mat& image, cv::Point2d& position) {

	// Scale down the image by 50% if it is larger than 300x300.
	double scale;
	cv::Mat operation_image;
	if ((image.rows > 300) && (image.cols > 300)) {
		scale = 0.5;
		cv::resize(image, operation_image, cv::Size(0,0), scale, scale, cv::INTER_NEAREST);
	}
	else {
		scale = 1.0;
		operation_image = image;
	}

	// Convert the image to HSV color space
	cv::Mat operation_image_hsv;
	cv::cvtColor(operation_image, operation_image_hsv, cv::COLOR_BGR2HSV);

	// Apply color threshold in HSV space
	cv::Mat operation_image_intensity;
	cv::inRange(operation_image_hsv, hsv_lowerBound, hsv_upperBound, operation_image_intensity);

	// Apply morphological operations (erode and dilate)
//	cv::Mat erosion_dst;
//	cv::Mat dilate_dst;
//	cv::erode(operation_image_intensity, erosion_dst, erosion_element);
//	cv::dilate(erosion_dst, dilate_dst, dilation_element);

	// Apply filter which makes the center of the object take on a maximum value
	cv::Mat operation_filter_result;
	//cv::filter2D(dilate_dst, operation_filter_result, CV_32S, medium_kernel, cv::Point(-1, -1), 0, cv::BORDER_CONSTANT); // performs DFT correlation for kernels larger than 11x11
	//cv::filter2D(operation_image_intensity, operation_filter_result, CV_32S, large_kernel, cv::Point(-1, -1), 0, cv::BORDER_CONSTANT); // performs DFT correlation for kernels larger than 11x11
	cv::GaussianBlur(operation_image_intensity, operation_filter_result, cv::Size(81, 81), 15 /*20*/); // biases the center pixels of the object more than the edges, but is faster since Gaussian kernels are separable

	// Find the center of the object
	cv::Point max_point;
	cv::minMaxLoc(operation_filter_result, nullptr, nullptr, nullptr, &max_point);

	position = max_point / scale;

	cv::Mat return_image;
	cv::cvtColor(operation_image_intensity, return_image, cv::COLOR_GRAY2BGR);
	cv::circle(return_image, max_point, 5, cv::Scalar(255, 0, 0), 3);

	return return_image;

}


void PositionCVTracker::triangulate() {

	std::vector<cv::Point2d> input_array_1 = { pixelCam1 };
	std::vector<cv::Point2d> input_array_2 = { pixelCam2 };
	std::vector<cv::Point2d> output_array_1;
	std::vector<cv::Point2d> output_array_2;

	// Unroll the camera matrix and the lens distortion to get to the normalized points (i.e. the points on the image
	// plane that is parallel to the x-y plane of the camera frame and resides 1 unit in the z-direction. See OpenCV
	// documentation for details.)
	cv::undistortPoints(input_array_1, output_array_1, camera_pair->cameraMatrix1(), camera_pair->distortionParams1());
	cv::undistortPoints(input_array_2, output_array_2, camera_pair->cameraMatrix2(), camera_pair->distortionParams2());

	// Determine the projection line for both pixels in the Camera 2 frame
	cv::Vec3d u1 = camera_pair->get_2R1() * cv::Vec3d(output_array_1[0].x, output_array_1[0].y, 1.0);
	cv::Vec3d u2(output_array_2[0].x, output_array_2[0].y, 1.0);

	// Find the point where both lines are the closest
	cv::Matx22d A;
	A << u1.dot(u1), -u2.dot(u1), u1.dot(u2), -u2.dot(u2);
	cv::Vec2d b;
	b << -camera_pair->get_2T21().dot(u1), -camera_pair->get_2T21().dot(u2);
	cv::Vec2d soln = A.inv() * b;
	double z1 = soln(0);
	double z2 = soln(1);

	// Select the midpoint of the shortest segment connecting both lines
	positionCameraFrame = 0.5 * (camera_pair->get_2T21() + z1*u1 + z2*u2);

	// Transform to world coordinates
	positionWorldFrame = camera_pair->camera2ToWorldFrameTransform() * cv::Vec4d(positionCameraFrame(0), positionCameraFrame(1), positionCameraFrame(2), 1);

	// Emit the new position
	emit newPosition(Robo::Vector3d(positionWorldFrame(0), positionWorldFrame(1), positionWorldFrame(2)), Timing::getSysTime());

}
