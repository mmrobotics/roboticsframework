#include "UnderwaterPositionCVTracker.h"
#include <iostream>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include "CV/CVUtils.h"
#include "Utilities/Timing.h"


UnderwaterPositionCVTracker::UnderwaterPositionCVTracker(double d1, double d2)
	: d1(d1),
	  d2(d2),
	  operation_image_intensity_filter(10, 89),
	  shift_transform(cv::Matx23f::eye()),
	  small_kernel(9, 9, CV_8UC1),
	  medium_kernel(41, 41, CV_8UC1),
	  large_kernel(81, 81, CV_8UC1),
	  previous_processing_ROI_full_frame_position(0.0, 0.0) {

	// Register signal types
	qRegisterMetaType<Robo::Position>();

	CVUtils::createCenterDistanceKernel(small_kernel);
	CVUtils::createCenterDistanceKernel(medium_kernel);
	CVUtils::createCenterDistanceKernel(large_kernel);

}


void UnderwaterPositionCVTracker::track(const cv::Mat& processingFrame1,
										const cv::Mat& processingFrame2,
										const cv::Point& processingFrameAbsPosition1,
										const cv::Point& processingFrameAbsPosition2) {

	setReturnProcessedFrame1(processImage(processingFrame1, processingFrameAbsPosition1, pixelCam1));
	setReturnProcessedFrame2(processImage(processingFrame2, processingFrameAbsPosition2, pixelCam2));

	// Convert position to be in terms of the full image
	pixelCam1 += cv::Point2d(processingFrameAbsPosition1);
	pixelCam2 += cv::Point2d(processingFrameAbsPosition2);

	// Set the servo hints (The size of the bounding box is not used, leave at default)
	cam1TargetBoundingBox.centerX = pixelCam1.x;
	cam1TargetBoundingBox.centerY = pixelCam1.y;
	cam2TargetBoundingBox.centerX = pixelCam2.x;
	cam2TargetBoundingBox.centerY = pixelCam2.y;

	triangulate();

}


cv::Mat UnderwaterPositionCVTracker::processImage(const cv::Mat& image, const cv::Point2d& processing_ROI_full_frame_position, cv::Point2d& position) {

	// Scale down the image by 50% if it is larger than 300x300.
	double scale;
	cv::Mat operation_image;
	if ((image.rows > 300) && (image.cols > 300)) {
		scale = 0.5;
		resize(image, operation_image, cv::Size(0,0), scale, scale, cv::INTER_NEAREST);
		resize(large_kernel, operation_kernel, cv::Size(0,0), scale, scale, cv::INTER_NEAREST);
	}
	else {
		scale = 1.0;
		operation_image = image;
		operation_kernel = large_kernel;
	}

	// Convert the image to HSV color space
	cv::Mat operation_image_hsv;
	cv::cvtColor(operation_image, operation_image_hsv, cv::COLOR_BGR2HSV);

	// Apply color threshold in HSV space
	int color = 75;
	cv::Mat operation_image_intensity;
	cv::inRange(operation_image_hsv, cv::Scalar(color, 100, 30), cv::Scalar(color+25, 230, 200), operation_image_intensity);

	// Apply a filter (in time) to the image (I think here we are shifting the already filtered image so that it will line up
	// with the new image when we call the update() function
	cv::Mat operation_image_intensity_float;
	operation_image_intensity.convertTo(operation_image_intensity_float, CV_32F);
	if (operation_image_intensity_filter.value().size() != operation_image_intensity_float.size()) {
		operation_image_intensity_filter.seed(cv::Mat(operation_image_intensity_float.rows, operation_image_intensity_float.cols, CV_32F, cv::Scalar::all(0)));
		temporary_shifted_filter = cv::Mat(operation_image_intensity_float.rows, operation_image_intensity_float.cols, CV_32F, cv::Scalar::all(0));
	}
	else {
		shift_transform(0, 2) = previous_processing_ROI_full_frame_position.x - processing_ROI_full_frame_position.x;
		shift_transform(1, 2) = previous_processing_ROI_full_frame_position.y - processing_ROI_full_frame_position.y;

		cv::warpAffine(operation_image_intensity_filter.value(), temporary_shifted_filter, shift_transform, operation_image_intensity_filter.value().size());
		temporary_shifted_filter.copyTo(operation_image_intensity_filter.value());

		operation_image_intensity_filter.update(operation_image_intensity_float);
	}
	previous_processing_ROI_full_frame_position = processing_ROI_full_frame_position;

	// Apply filter which makes the center of the object take on a maximum value
	cv::Mat operation_filter_result;
	cv::filter2D(operation_image_intensity_filter.value(), operation_filter_result, CV_32F, operation_kernel, cv::Point(-1, -1), 0, cv::BORDER_CONSTANT); // performs DFT correlation for kernels larger than 11x11
	//cv::GaussianBlur(operation_image_intensity, operation_filter_result, Size(41, 41), 15); // biases the center pixels of the object more than the edges, but is faster since Gaussian kernels are separable

	// Find the center of the object
	double max_val;
	cv::Point max_point;
	cv::minMaxLoc(operation_filter_result, nullptr, &max_val, nullptr, &max_point);

	operation_filter_result *= (255.0 / max_val);

	// Convert position to be in terms of the full image
	position = max_point / scale;

	// The following uses the center of mass to compute the position rather than the above method which uses minMaxLoc()
	// Uncomment if you want to use this.

//	// Compute the expected value (weighted mean or center of mass)
//	double sum_x = 0.0;
//	double sum_y = 0.0;
//	double total_weight = 0.0;
//	for (int cc = 0; cc < operation_filter_result.rows; ++cc) {
//		for (int dd = 0; dd < operation_filter_result.cols; ++dd) {

//			double weight = double(operation_filter_result.at<float>(cc, dd));

//			if (weight > 0) {
//				sum_x += weight * dd;
//				sum_y += weight * cc;
//				total_weight += weight;
//			}

//		}
//	}
//	cv::Point2d weighted_mean(sum_x / total_weight, sum_y / total_weight);

//	if (total_weight > 0 && ((image.rows <= 300) || (image.cols <= 300))) {
//		position = weighted_mean / scale;
//	}

	cv::Mat return_image;
	cv::cvtColor(operation_filter_result, return_image, cv::COLOR_GRAY2BGR);
	cv::circle(return_image, max_point, 5, cv::Scalar(255, 0, 0), 3);

	return return_image;

}


void UnderwaterPositionCVTracker::triangulate() {

	std::vector<cv::Point2d> input_array_1 = { pixelCam1 };
	std::vector<cv::Point2d> input_array_2 = { pixelCam2 };
	std::vector<cv::Point2d> output_array_1;
	std::vector<cv::Point2d> output_array_2;

	// Unroll the camera matrix and the lens distortion to get to the normalized points (i.e. the points on the image
	// plane that is parallel to the x-y plane of the camera frame and resides 1 unit in the z-direction. See OpenCV
	// documentation for details.)
	cv::undistortPoints(input_array_1, output_array_1, camera_pair->cameraMatrix1(), camera_pair->distortionParams1());
	cv::undistortPoints(input_array_2, output_array_2, camera_pair->cameraMatrix2(), camera_pair->distortionParams2());

	cv::Vec3d n1(1, 0, 0);
	cv::Vec3d n2(0, 0, 1);

	cv::Vec3d r1 = d1 * camera_pair->get_2R1() * cv::Vec3d(output_array_1[0].x, output_array_1[0].y, 1.0);
	cv::Vec3d r2 = d2 * cv::Vec3d(output_array_2[0].x, output_array_2[0].y, 1.0);
	
	cv::Vec3d r1_hat = cv::normalize(r1);
	cv::Vec3d r2_hat = cv::normalize(r2);
	
	double cos_theta_1 = r1_hat.dot(n1);
	double cos_theta_2 = r2_hat.dot(n2);
	
	double refraction_ratio = air_refractive_index / water_refractive_index;
	double refraction_ratio_sqd = pow(refraction_ratio, 2.0);
	
	double cos_alpha_1 = sqrt(1.0 - refraction_ratio_sqd*(1.0 - cos_theta_1*cos_theta_1));
	double cos_alpha_2 = sqrt(1.0 - refraction_ratio_sqd*(1.0 - cos_theta_2*cos_theta_2));
	
	cv::Vec3d l1 = refraction_ratio * r1_hat + (cos_alpha_1 - refraction_ratio*cos_theta_1) * n1;
	cv::Vec3d l2 = refraction_ratio * r2_hat + (cos_alpha_2 - refraction_ratio*cos_theta_2) * n2;
	
	cv::Matx22d A;
	A << 1.0, -l2.dot(l1), l1.dot(l2), -1.0;
	
	cv::Vec2d b;
	b << (r2-r1-camera_pair->get_2T21()).dot(l1), (r2-r1-camera_pair->get_2T21()).dot(l2);
	
	cv::Vec2d soln = A.inv() * b;
	
	double z1 = soln(0);
	double z2 = soln(1);
	
	// Select the midpoint of the shortest segment connecting both lines
	positionCameraFrame = (camera_pair->get_2T21() + r1 + r2 + z1*l1 + z2*l2) / 2.0;

	// Transform to world coordinates
	positionWorldFrame = camera_pair->camera2ToWorldFrameTransform() * cv::Vec4d(positionCameraFrame(0), positionCameraFrame(1), positionCameraFrame(2), 1);

	// Emit the new position
	emit newPosition(Robo::Vector3d(positionWorldFrame(0), positionWorldFrame(1), positionWorldFrame(2)), Timing::getSysTime());
		
}
