#include "Flea3Camera.h"
#include <iostream>
#include <sstream>
#include <cmath>
#include <opencv2/imgproc.hpp>
#include "Utilities/Timing.h"

using namespace std;
using namespace FlyCapture2;

// Definition of static member for the BusManager. We only need one instance of the BusManager for
// all instances of Flea3Camera. Thus it is a static member of the class.
BusManager Flea3Camera::busMgr;
bool Flea3Camera::camerasFound = false; // Also a static member


/**
 * @brief Constructs a Flea3Camera object which connects to the physical Flea3 camera connected to the
 * computer with the specified serial number.
 * @param serialNumber The serial number of the Flea3 camera with which this object is meant to connect
 */
Flea3Camera::Flea3Camera(unsigned int serialNumber) {

	if (!camerasFound)
		camerasFound = findCameras();

	if (camerasFound) {

		// Get Camera Id
		error = busMgr.GetCameraFromSerialNumber(serialNumber, &cam_ID);
		if (error != PGRERROR_OK) {
			state = ERROR;
			PrintError(error);
			return;
		}

	}
	else {
		state = ERROR;
		return;
	}

	cout << "FLEA_3_CAMERA: Found camera with serial number: " << serialNumber << endl;

    // Complete Camera Setup
	if (!setupCamera())
		state = ERROR;

}


Flea3Camera::~Flea3Camera() {

	// Stop any streaming
	bool success = true;
	if (!stopVideoStream())
		success = false;

	// Turn off the camera
	if (!turnOffCamera())
		success = false;

	// Disconnect the camera
	error = cam.Disconnect();
	if (error != PGRERROR_OK) {
		success = false;
		PrintError( error );
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Could not disconnect the camera." << endl;
	}

	if (success)
		cout << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Successfully disconnected." << endl;

}


/**
 * @brief Finds whether any Flea3 cameras are currently connected to the computer.
 * @return True if at least one Flea3 camera is connected to the computer
 */
bool Flea3Camera::findCameras() {

    unsigned int numCameras = 0;
	error = busMgr.GetNumOfCameras(&numCameras);
	if (error != PGRERROR_OK) {
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Error getting number of cameras." << endl << endl;
		return false;
    }

	if (numCameras == 0) {
		cerr << "FLEA_3_CAMERA: No attached cameras found." << endl << endl;
		return false;
    }

	cout << "FLEA_3_CAMERA: Found cameras - number attached: " << numCameras << endl << endl;
	return true;

}


/**
 * @brief Sets up the Flea3 camera.
 * @return
 */
bool Flea3Camera::setupCamera() {

	// Connect to the camera
	if (!cam.IsConnected()) {

		error = cam.Connect(&cam_ID);
		if (error != PGRERROR_OK) {
			state = ERROR;
			PrintError( error );
			cerr << "FLEA_3_CAMERA: Error connecting to camera" << endl;
			return false;
		}

	}

	// Get the camera information
	error = cam.GetCameraInfo(&cam_Info);
	if (error != PGRERROR_OK) {
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Error getting camera info." << endl;
		return false;
	}

	// Check that the camera is turned on, if not, turn it on
	if(!isOn() && !turnOnCamera()) {
		state = ERROR;
		return false;
	}

	// Get the camera configuration
	error = cam.GetConfiguration( &cam_config );
	if (error != PGRERROR_OK) {
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Error getting camera configuration." << endl;
		return false;
	}

	// Make sure the camera is not still streaming from a previously crashed program
	error = cam.StopCapture();
	if (error != PGRERROR_OK && error != PGRERROR_ISOCH_NOT_STARTED) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Failed to make sure camera was not still streaming from a previously crashed program." << endl;
		return false;
	}

	state = READY;

	// INITIALIZE FORMAT 7 MODE_0  Mode 0 allows manipulation of ROI, Mode 8 is the same, but has a maximum size for the ROI for a slightly faster fps

//	const Mode k_fmt7Mode = FlyCapture2::MODE_8;
	const Mode k_fmt7Mode = FlyCapture2::MODE_0;
//	const PixelFormat k_fmt7PixFmt = FlyCapture2::PIXEL_FORMAT_RGB;
	const PixelFormat k_fmt7PixFmt = FlyCapture2::PIXEL_FORMAT_422YUV8; // use when you want faster frame rates for slight reduction in color


	// Get information for ROI real-time shifting
	/*
	 * FROM COMMUNICATION WITH PT GREY
	 * To calculate the address of the IMAGE_POSITION register (when using format7
	 * mode 0) please refer to the example below, or you may also refer to section 5.6
	 * of the Point Grey Digital Camera Register Reference:
	 * 1. Read the inquiry register for format mode 0 (2E0h), Register value =0x003C0280 **** SWITCH STATEMENT ABOVE ****
	 * 2. Multiple the value by 4, Register value * 4 = 0xF00A00
	 * 3. Remove the 0xF prefix from the result, Base = 0xA00
	 * 4. Add IMAGE_POSITION offset to the base register, IMAGE_POSITION register address = 0xA00 + 008 = 0xA08
	 */

//	error = cam.ReadRegister(0x300,&ROI_P0S_ADDRESS); // Mode 8
//	error = cam.ReadRegister(0x2F0,&ROI_P0S_ADDRESS); // Mode 4
	error = cam.ReadRegister(0x2E0, &ROI_P0S_ADDRESS); // Mode 0
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not get camera ROI register address information." << endl;
		return false;
	}

	ROI_P0S_ADDRESS = (4 * ROI_P0S_ADDRESS) & 0x0fffff;
	ROI_SIZ_ADDRESS = ROI_P0S_ADDRESS;
	ROI_P0S_ADDRESS += 0x008;  // section A.3.2.1
	ROI_SIZ_ADDRESS += 0x00C;  // section A.3.2.1

	// Query for available Format 7 modes
	bool supported;
	fmt7Info.mode = k_fmt7Mode;
	error = cam.GetFormat7Info(&fmt7Info, &supported);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Error getting Format 7 information from camera." << endl;
		return false;
	}

	PrintFormat7Capabilities();

	if (!supported) {
		state = ERROR;
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " The chosen Format 7 mode is not supported." << endl;
		return false;
	}
	if ((k_fmt7PixFmt & fmt7Info.pixelFormatBitField) == 0) {
		state = ERROR;
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " The chosen pixel format is not supported." << endl;
		return false;
	}

	// Set up default Format 7 settings
	fmt7ImageSettings.mode = k_fmt7Mode;
	fmt7ImageSettings.offsetX = 0;
	fmt7ImageSettings.offsetY = 0;
	fmt7ImageSettings.width = fmt7Info.maxWidth;
	fmt7ImageSettings.height = fmt7Info.maxHeight;
	if(k_fmt7Mode == MODE_8) {
		// Maximum recommended size according to technical reference
		fmt7ImageSettings.width = 688;
		fmt7ImageSettings.height = 504;
	}
	fmt7ImageSettings.pixelFormat = k_fmt7PixFmt;

	// Validate the settings to make sure that they are valid
	bool valid;
	error = cam.ValidateFormat7Settings(&fmt7ImageSettings, &valid, &fmt7PacketInfo);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Error validating Format 7 settings." << endl;
		return false;
	}
	if (!valid) {
		state = ERROR;
		cerr <<  "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Format 7 settings are not valid." << endl;
		return false;
	}

	// Set the settings to the camera
	error = cam.SetFormat7Configuration(&fmt7ImageSettings, fmt7PacketInfo.recommendedBytesPerPacket);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Error setting Format 7 configuration." << endl;
		return false;
	}

	cout << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Maximum frame rate (fps): " << getMaxFPS() << endl;

	if (!setFPS(30)) {
		state = ERROR;
		return false;
	}

	// Start capturing frames
	error = cam.StartCapture();
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Error starting capture." << endl;
		return false;
	}

	state = VIDEO_MODE;

	// Use the automatic property capabilities of the camera to set the image properties. Do this while camera is streaming.
	if (!autoSetAllProps()) {
		state = ERROR;
		return false;
	}

	// Stop capturing frames
	error = cam.StopCapture();
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not stop capture." << endl;
		return false;
	}

	// Setup embedded image info to allow for ROI position broadcast and capture timestamps
	EmbeddedImageInfo fmt7ImageInfo;
	error = cam.GetEmbeddedImageInfo(&fmt7ImageInfo);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not get embedded image info." << endl;
		return false;
	}

	fmt7ImageInfo.ROIPosition.onOff = true;
	fmt7ImageInfo.timestamp.onOff = true;
	error = cam.SetEmbeddedImageInfo(&fmt7ImageInfo);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not set embedded image info." << endl;
		return false;
	}

	// Store the current ROI
	currentROI = cv::Rect(fmt7ImageSettings.offsetX, fmt7ImageSettings.offsetY, fmt7ImageSettings.width, fmt7ImageSettings.height);

	cout << "FLEA_3_CAMERA: Camera READY!" << endl << endl;

	state = READY;
	return true;

} // End: void Flea3Camera::setupCamera()


/**
 * @brief Automatically sets all the properties of the camera.
 * @return
 */
bool Flea3Camera::autoSetAllProps() {

	const int numProp = 14;

	FlyCapture2::PropertyType property[numProp] = { FlyCapture2::GAIN,
													FlyCapture2::BRIGHTNESS,
													FlyCapture2::GAMMA,
													FlyCapture2::AUTO_EXPOSURE,
													FlyCapture2::HUE,
													FlyCapture2::SHARPNESS,
													FlyCapture2::WHITE_BALANCE,
													FlyCapture2::SATURATION,
													FlyCapture2::IRIS,
													FlyCapture2::FOCUS,
													FlyCapture2::ZOOM,
													FlyCapture2::PAN,
													FlyCapture2::TILT,
													FlyCapture2::SHUTTER };

    // Auto Set Properties
	for(int i = 0; i < numProp; i++) {
		if (!autoSetProp(property[i]))
			return false;
    }

	// Wait 500 ms for everything to set
	Timing::wait(500);

	return true;

}


/**
 * @brief Automatically sets the desired property.
 * @param type The property type to automatically set
 * @return
 */
bool Flea3Camera::autoSetProp( FlyCapture2::PropertyType type ) {

    // Auto Set Gain
    PropertyInfo info;
    info.type = type;
	error = cam.GetPropertyInfo(&info);
	if (error != PGRERROR_OK) {
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not get property information." << endl;
		return false;
	}

    if( info.present ) {

        Property prop;
        prop.type = type;
		error = cam.GetProperty(&prop);
		if (error != PGRERROR_OK) {
			PrintError(error);
			cerr << "FLEA_3_CAMERA: Could not get property to set a property." << endl;
			return false;
		}

        prop.autoManualMode = false;
        prop.onOff = info.onOffSupported;

		if (type == GAMMA) {

			prop.onOff = true;
			prop.autoManualMode = false;
			prop.absControl = true;
			//prop.absValue = 2.2f;
			prop.absValue = 1.8f;
			error = cam.SetProperty(&prop);

		}
		else if(info.onePushSupported) {

			// auto set it once
            prop.onePush = true;
			error = cam.SetProperty(&prop);

		}
		else if (info.autoSupported) {

			// let it auto set for a 10 frames worth than stop changing
            prop.autoManualMode = true;
			error = cam.SetProperty(&prop);
			if (error != PGRERROR_OK) {
				PrintError(error);
				cerr << "FLEA_3_CAMERA: Could not auto set property." << endl;
				return false;
			}
			Timing::wait(static_cast<long>(1000 * 10 / getFPS()));  // wait for about 10 frames
			prop.autoManualMode = false;
			error = cam.SetProperty(&prop);

		}
		else {

			// otherwise dont mess with it
			return true;

		}

		if (error != PGRERROR_OK) {
			PrintError(error);
			cerr << "FLEA_3_CAMERA: Could not auto set property." << endl;
			return false;
		}

    }// end if property is present

	return true;

}


/**
 * @brief This function gets called automatically by FlyCapture when a new frame is ready. It is
 * responsible for converting the frame to BGR format, storing it for later use and emitting the
 * newFrame() signal.
 * @param image
 */
void Flea3Camera::receiveImage(FlyCapture2::Image* image ) {

	// Obtain ROI and timestamp information for this particular frame from the image metadata
	ImageMetadata imageMetadata = image->GetMetadata();

	// Extract the frame timestamp, see Flea3 documentation for details on how the data is embedded
	frameTimestamp = 1000 * ((imageMetadata.embeddedTimeStamp >> 25) + ((imageMetadata.embeddedTimeStamp >> 12) & 0x1fff) / 8000.0);

	// If this is the first frame received, we need to initialize some of the timestamp variables
	if (!gotFirstFrame) {
		frameTimestampZero = frameTimestamp;
		lastFrameTimestamp = 0;
		firstFrameSysTimestamp = Timing::getSysTime();
		gotFirstFrame = true;
	}

	// Zero the frame timestamp
	frameTimestamp -= frameTimestampZero;

	// Detect if the frame timestamp has reached overflow, if so, increment the frame timestamp overflow counter.
	if (frameTimestamp < lastFrameTimestamp)
		frameTimestampOverflowCount++;
	lastFrameTimestamp = frameTimestamp;

	// Allocate memory for a copy of the image as a cv::Mat object. The allocation only takes a few microseconds
	// and is necessary to take advantage of the smart referencing of cv::Mat objects.
	cv::Mat cvImage(currentROI.height, currentROI.width, CV_8UC3);

	// Convert the image to BGR format. The incoming image is contained in a FlyCapture2::Image object. We want to use
	// the Convert() member variable to do the conversion since we don't need to know the origin format, however, this
	// function requires the destination to also be a FylCapture2::Image object. To avoid extra copying, we simply
	// create a "wrapper" object (i.e. the FlyCapture2::Image object is constructed with the data buffer of the cv::Mat
	// object we previously created) which will allow the Convert() function to place the BGR data directly into the
	// previously allocated cv:Mat object.
	Image flycap_image_wrapper(cvImage.data, imageDataSize);
	error_callback = image->Convert(PIXEL_FORMAT_BGR, &flycap_image_wrapper);
	if (error_callback != PGRERROR_OK) {
		cerr << "FLEA_3_CAMERA: Error inside camera streaming callback function (from callback thread)" << endl;
		PrintError(error_callback);
	}

	// Copy bytes 12-23 to bytes 0-11. The first four pixels (12 bytes) are currently tainted by ROI and timestamp data
	// instead of image data
	for(unsigned int i = 0; i < 12; i++)
		cvImage.data[i] = cvImage.data[i + 12];

	frameMutex.lockForWrite();
	// Update the currently stored frame
	currentFrame = cvImage;
	// Update ROI information from image metadata, see Flea 3 documentation for details on how the data is embedded
	currentROI.x = imageMetadata.embeddedROIPosition >> 16;		// x position is the first two bytes
	currentROI.y = imageMetadata.embeddedROIPosition & 0xffff;	// y position is the second two bytes
	frameMutex.unlock();

	// Emit the new frame
	emit newFrame(currentFrame, currentROI.tl(), firstFrameSysTimestamp + (128000 * frameTimestampOverflowCount) + frameTimestamp);

}


bool Flea3Camera::getFrame(cv::Mat& frame) const {

	if (state == VIDEO_MODE) {

		// The receiveImage() function is automatically called when a camera receives a frame in
		// a thread started and managed by the FlyCapture library. Therefore, the frame stored in
		// currentFrame is the most current.
		frameMutex.lockForRead();
		cv::Mat tmp = currentFrame;
		frameMutex.unlock();

		tmp.copyTo(frame);

		return true;

	}
	cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Cannot get frame. Camera not streaming." << endl;
	return false;

}


bool Flea3Camera::getFrame(cv::Mat& frame, cv::Rect& correspondingROI) const {

	if (state == VIDEO_MODE) {

		// The receiveImage() function is automatically called when a camera receives a frame in
		// a thread started and managed by the FlyCapture library. Therefore, the frame stored in
		// currentFrame is the most current.
		frameMutex.lockForRead();
		cv::Mat tmp = currentFrame;
		correspondingROI = currentROI;
		frameMutex.unlock();

		tmp.copyTo(frame);

		return true;

	}
	cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Cannot get frame. Camera not streaming." << endl;
	return false;

}


bool Flea3Camera::startVideoStream() {

	if (state == VIDEO_MODE) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Cannot start streaming because camera is currently streaming." << endl;
		return false;
	}

	if (state == ERROR) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Cannot start streaming because camera is currently in a state of error." << endl;
		return false;
	}

	// Initialize CV frame to the correct size
	currentFrame = cv::Mat(currentROI.height, currentROI.width, CV_8UC3);
	imageDataSize = 3 * currentROI.height * currentROI.width;

	// Initialize frame timestamp variables
	gotFirstFrame = false;
	frameTimestampOverflowCount = 0;

	// Start isochronous capture with the callback
	error = cam.StartCapture(Flea3Camera::receiveImageWrapper, reinterpret_cast<void*>(this));
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError( error );
		cerr << "FLEA_3_CAMERA: Could not start camera." << endl;
		return false;
	}

	// Use the automatic property capabilities of the camera to set the image properties. Do this while camera is streaming.
	if (!autoSetAllProps()) {
		state = ERROR;
		return false;
	}

	state = VIDEO_MODE;

	return true;

}


bool Flea3Camera::startVideoStream(float desiredFrameRate) {

	if (state == VIDEO_MODE) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Cannot start streaming because camera is currently streaming." << endl;
		return false;
	}

	if (state == ERROR) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Cannot start streaming because camera is currently in a state of error." << endl;
		return false;
	}

	if (setFPS(desiredFrameRate)) {

		cout << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " - Frame Rate Set To " << desiredFrameRate << endl;
		return startVideoStream();

	}

	return false;

}


bool Flea3Camera::startVideoStream(float desiredFrameRate, cv::Rect& desiredROI) {

	if (state == VIDEO_MODE) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Cannot start streaming because camera is currently streaming." << endl;
		return false;
	}

	if (state == ERROR) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Cannot start streaming because camera is currently in a state of error." << endl;
		return false;
	}

	if (setFPS(desiredFrameRate) && setROI(desiredROI)) {

		cout << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " - Frame Rate Set To " << desiredFrameRate << endl;
		return startVideoStream();

	}

	return false;

}


bool Flea3Camera::stopVideoStream() {

	if (state == VIDEO_MODE) {

		error = cam.StopCapture();
		if (error != PGRERROR_OK) {
			state = ERROR;
			PrintError( error );
			cerr << "FLEA_3_CAMERA: Could not stop camera." << endl;
			return false;
		}

		state = READY;
		return true;

	}

	if (state == ERROR) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Trying to stop video stream on a camera in error state." << endl;
	}

	return (state == READY);

}


bool Flea3Camera::isVideoStreaming() const {

    return (state == VIDEO_MODE);

}


bool Flea3Camera::isInError() const {

	return (state == ERROR);

}


cv::Rect Flea3Camera::getROI() const {

	return currentROI;

}


cv::Size Flea3Camera::getMaxROISize() const {

	return cv::Size(fmt7Info.maxWidth, fmt7Info.maxHeight);

}


cv::Size Flea3Camera::getMinROISize() const {

	return cv::Size(fmt7Info.imageHStepSize, fmt7Info.imageVStepSize);

}


bool Flea3Camera::validateROI(int& x, int& y, int& width, int& height) const {

	// According to the Camera interface, this function should also do nothing if the requested ROI is too big
	if (width > static_cast<int>(fmt7Info.maxWidth) || height > static_cast<int>(fmt7Info.maxHeight))
		return false;

	// Since the Format 7 ROI cannot take on any arbitrary width, height, or offset, modify the
	// input ROI to be a valid Format 7 shape and location
	width = std::round(static_cast<float>(width) / fmt7Info.imageHStepSize) * fmt7Info.imageHStepSize;
	height = std::round(static_cast<float>(height) / fmt7Info.imageVStepSize) * fmt7Info.imageVStepSize;
	x = (x / fmt7Info.offsetHStepSize) * fmt7Info.offsetHStepSize; // Allow truncation from integer math
	y = (y / fmt7Info.offsetVStepSize) * fmt7Info.offsetVStepSize; // Allow truncation from integer math

	// If the ROI extends beyond the frame, shift it until it fits
	if (x < 0)
		x = 0;
	else if ((x + width) > static_cast<int>(fmt7Info.maxWidth))
		x = fmt7Info.maxWidth - width;

	if (y < 0)
		y = 0;
	else if ((y + height) > static_cast<int>(fmt7Info.maxHeight))
		y = fmt7Info.maxHeight - height;

	return true;

}


bool Flea3Camera::setROI(cv::Rect& newROI) {

	// If the camera is streaming, according to the Camera interface this function should do nothing and return false
	if (state == VIDEO_MODE)
		return false;

	// Validate the new ROI
	if (!validateROI(newROI.x, newROI.y, newROI.width, newROI.height)) {
		std::cerr << "FLEA_3_CAMERA: Could not set ROI. ROI size too big" << std::endl;
		return false;
	}

	// Validate the new Format 7 Settings
	bool valid;
	fmt7ImageSettings.offsetX = newROI.x;
	fmt7ImageSettings.offsetY = newROI.y;
	fmt7ImageSettings.width = newROI.width;
	fmt7ImageSettings.height = newROI.height;
	error = cam.ValidateFormat7Settings(&fmt7ImageSettings, &valid, &fmt7PacketInfo);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
	}

	// Check the settings are valid
	if (!valid) {
		state = ERROR;
		cerr << "FLEA_3_CAMERA: Could not set ROI: Format 7 settings are not valid!" << endl;
		return false;
	}

	// Set the Format 7 settings to the camera
	error = cam.SetFormat7Configuration(&fmt7ImageSettings, fmt7PacketInfo.recommendedBytesPerPacket );
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not set ROI: Format 7 Configuration could not be set!" << endl;
		return false;
	}

	// Make sure class representation of current ROI is accurate
	currentROI.x = newROI.x;
	currentROI.y = newROI.y;
	currentROI.width = newROI.width;
	currentROI.height = newROI.height;

	return true;

}


bool Flea3Camera::setROIToMax() {

	cv::Rect maxROI(cv::Point(0, 0), getMaxROISize());
	return setROI(maxROI);

}


bool Flea3Camera::setROIPosition(int& x, int& y) {

	// Validate the position
	validateROI(x, y, currentROI.width, currentROI.height);

	// The (x, y) postion of the ROI is stored in a 32 bit register (first 16 bits are x, last 16 bits are y)
	unsigned int position;
	position = (x << 16) + y;

	error = cam.WriteRegister(ROI_P0S_ADDRESS, position);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not set the ROI of the camera in real time." << endl;
		return false;
	}

	// If the camera is not streaming, make sure the internal ROI variable is consistent with the new setting
	if (state != VIDEO_MODE) {

		currentROI.x = x;
		currentROI.y = y;

	}

	return true;

}


float Flea3Camera::getFPS() {

	Property frmRate;
	frmRate.type = FRAME_RATE;
	error = cam.GetProperty(&frmRate);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Cannot get frame rate from camera." << endl;
		return -1.0;
	}

	return frmRate.absValue;

}


float Flea3Camera::getMaxFPS() {

    PropertyInfo propInfo;
    propInfo.type = FRAME_RATE;
	error = cam.GetPropertyInfo(&propInfo);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Cannot get frame rate information from camera." << endl;
		return -1.0;
	}

	return propInfo.absMax;

}


float Flea3Camera::getAbsoluteMaxFPS() const {

	return 120.0f;

}


float Flea3Camera::getMinFPS() {

    PropertyInfo propInfo;
    propInfo.type = FRAME_RATE;
	error = cam.GetPropertyInfo(&propInfo);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Cannot get frame rate information from camera." << endl;
		return -1.0;
	}
    return propInfo.absMin;

}


bool Flea3Camera::setFPS(float desiredFrameRate) {

	// Get maximum and minimum frame rates allowable
	PropertyInfo propInfo;
	propInfo.type = FRAME_RATE;
	error = cam.GetPropertyInfo(&propInfo);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError( error );
		cerr << "FLEA_3_CAMERA: Could not get camera frame rate information in order to set the frame rate." << endl;
		return false;
	}

	// Check if the desired frame rate is out of bounds. If so, the function should fail
	if (desiredFrameRate > propInfo.absMax) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Desired frame rate of " << desiredFrameRate << " is larger than current maximum of " << propInfo.absMax << " Frames/Sec" << endl;
		return false;
	}
	if (desiredFrameRate < propInfo.absMin) {
		cerr << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << " Desired frame rate of " << desiredFrameRate << " is smaller than current minimum of " << propInfo.absMin << " Frames/Sec" << endl;
		return false;
	}

    Property frmRate;
    frmRate.type = FRAME_RATE;
    // Set frame rate property Control Parameters
    frmRate.onOff = true;
    frmRate.onePush = false;
    frmRate.absControl = true;
    frmRate.autoManualMode = false;
    // Set Frame Rate value
	frmRate.absValue = desiredFrameRate;

	// Set the frame rate to camera
	error = cam.SetProperty( &frmRate);
    if (error != PGRERROR_OK) {
		state = ERROR;
        PrintError( error );
		cerr << "FLEA_3_CAMERA: Could not set camera frame rate." << endl;
        return false;
    }

	return true;

}


/**
 * @brief Returns a float value equivalent to the camera temperature in
 * 	degrees C.
 * @return
 */
float Flea3Camera::getTemp() {

	if (state == ERROR)
		return -273.15f;

	Property temperature;
	temperature.type = TEMPERATURE;
	error = cam.GetProperty(&temperature);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not get camera temperature." << endl;
		return -273.15f;
	}

	return (temperature.valueA - 2731.0f) / 10.0f;

}


bool Flea3Camera::tempInRange() {

    /*
	 * The following is a message to Andrew from Point Grey about the acceptable maximum
	 * temperature for this camera.
	 *
     *  Hi Andrew,
     *
	 *   We do not perform tests on what the maximum internal temperature a camera can
	 *   reach, since this depends on what imaging mode (frame rate and resolution -
	 *   affects the pixel clock) the camera is operating in, what the ambient
	 *   temperature is at the moment, whether the camera has heat-sink-like devices
	 *   (ex. lens) attached.
     *
	 *   Personally I have observed temperature as high as 75-80C on some of our
	 *   cameras. This will not affect the functionality of the camera.
     *
	 *  Regards,
	 *  Nina [PT GREY TECH SUPPORT]
     *
     * Choose Max Storage Temp of 60C
     */

	if(state == ERROR) {
        return false;
	}

	float temp = getTemp();


	// Make sure temperature is possible
	while ((temp > 85  || temp < -25) && temp > -270) // maximum and minimum sensor values by Table 1.1 of manual
        temp = getTemp();

	if (temp > 60) {
		cout << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << "Temperature: " << temp << "C exceeds maximum operational limits." << endl;
        return false;
    }
	if (temp < 0) {
		// Not really an issue requiring turning off the camera.
		cout << "FLEA_3_CAMERA: Camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << "Temperature: " << temp << "C exceeds minimum operational limits." << endl;
        return false;
    }

    return true;

}


/**
 * @brief Commands the camera to turn on.
 * @return True of successful
 */
bool Flea3Camera::turnOnCamera() {

	if(!isOn()) {

		// Power on the camera
		const unsigned int k_cameraPower = 0x610;
		const unsigned int k_powerVal = 0x80000000;
		cout << "FLEA_3_CAMERA: Turning on camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
		error  = cam.WriteRegister(k_cameraPower, k_powerVal);
		if (error != PGRERROR_OK) {
			PrintError(error);
			cerr << "FLEA_3_CAMERA: Could not turn on camera "<< cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
			return false;
		}

		// Wait for camera to complete power-up
		const unsigned int maxTries = 10;
		unsigned int tries = 0;
		do {
			Timing::wait(200); // Sleep 200 ms
			tries ++;
		} while (!isOn() && tries < maxTries);

		if (tries == maxTries) {
			cerr << "FLEA_3_CAMERA: Could not turn on camera "<< cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
			return false;
		}

	}

	state = READY;

	return true;

}


/**
 * @brief Commands the camera to turn off.
 * @return True of successful
 */
bool Flea3Camera::turnOffCamera() {

	if (isOn()) {

		// Power off the camera
		const unsigned int k_cameraPower = 0x610;
		const unsigned int k_powerVal = 0x00000000;
		cout << "FLEA_3_CAMERA: Turning off camera " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
		error  = cam.WriteRegister(k_cameraPower, k_powerVal);
		if (error != PGRERROR_OK) {
			state = ERROR;
			PrintError(error);
			cerr << "FLEA_3_CAMERA: Could not turn off camera "<< cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
			return false;
		}

		// Wait for camera to complete power-down
		const unsigned int maxTries = 10;
		unsigned int tries = 0;
		do {
			Timing::wait(200);
			tries++;
		} while(isOn() && tries < maxTries);

		if (tries == maxTries) {
			cerr << "FLEA_3_CAMERA: Could not turn off camera "<< cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
			return false;
		}

	}

	if (state != ERROR)
		return true;

	return false;

}


/**
 * @brief Returns true if the camera is on
 * @return
 */
bool Flea3Camera::isOn() {

	const unsigned int k_cameraPower = 0x610;
	const unsigned int k_powerVal = 0x80000000; // First bit of register indicates on/off status (1 -> on, 0 -> off)
	unsigned int regVal = 0;

	error = cam.ReadRegister(k_cameraPower, &regVal);
	if (error != PGRERROR_OK) {
		state = ERROR;
		PrintError(error);
		cerr << "FLEA_3_CAMERA: Could not read register to check if the camera is on." << endl;
		return false;
	}

	return (regVal & k_powerVal);

}


/**
 * @brief Returns the state of the camera
 * @return
 */
Flea3Camera::CAMERA_STATE Flea3Camera::getState() {

    return state;

}


/**
 * @brief Returns the state of the camera as an std::string
 * @return
 */
std::string Flea3Camera::getState_str() {

	switch(state) {
		case Flea3Camera::CAMERA_STATE::ERROR:
			return "ERROR";
		case Flea3Camera::CAMERA_STATE::READY:
			return "READY";
		case Flea3Camera::CAMERA_STATE::VIDEO_MODE:
			return "VIDEO MODE";
	}

	return "UNKNOWN";

}


/**
 * @brief Prints to screen the various parameters provided by a	Point Grey Property Info type.
 * Valid types are: BRIGHTNESS, AUTO_EXPOSURE, SHARPNESS, WHITE_BALLANCE, HUE, SATURATION, GAMMA,
 * IRIS, FOCUS, ZOOM, PAN, TILT, SHUTTER, GAIN, TRIGGER_MODE, TRIGGER_DELAY, FRAME_RATE,
 * TEMPERATURE. Some of these will not be valid for any camera and will print out that they	are
 * not "present" for the camera. Generally these give the maximum and minimum values for a Property
 * as well as other useful information.
 *
 *  Example of use:
 * 	FlyCapture2::PropertyInfo propInfo;
 * 	prop.type = FlyCapture2::BRIGHTNESS;
 * 	cam.GetPropertyInfo(& propInfo); // where "cam" is a FlyCapture2::camera object.
 * 	printProperty(propInfo);
 * @param propInfo
 */
void Flea3Camera::printPropertyInfo(FlyCapture2::PropertyInfo propInfo) {

	cout << "FlyCapture: Print Property Info for Camera: " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
    switch(propInfo.type) {
		case BRIGHTNESS:
			cout << "Type: BRIGHTNESS" << endl;
			break;
		case AUTO_EXPOSURE:
			cout << "Type: AUTO_EXPOSURE " << endl;
			break;
		case SHARPNESS:
			cout << "Type: SHARPNESS" << endl;
			break;
		case WHITE_BALANCE:
			cout << "Type: WHITE_BALANCE" << endl;
			break;
		case HUE:
			cout << "Type: HUE" << endl;
			break;
		case SATURATION:
			cout << "Type: SATURATION" << endl;
			break;
		case GAMMA:
			cout << "Type: GAMMA" << endl;
			break;
		case IRIS:
			cout << "Type: IRIS" << endl;
			break;
		case FOCUS:
			cout << "Type: FOCUS" << endl;
			break;
		case ZOOM:
			cout << "Type: ZOOM" << endl;
			break;
		case PAN:
			cout << "Type: PAN" << endl;
			break;
		case TILT:
			cout << "Type: TILT" << endl;
			break;
		case SHUTTER:
			cout << "Type: SHUTTER" << endl;
			break;
		case GAIN:
			cout << "Type: GAIN" << endl;
			break;
		case TRIGGER_MODE:
			cout << "Type: TRIGGER_MODE" << endl;
			break;
		case TRIGGER_DELAY:
			cout << "Type: TRIGGER_DELAY" << endl;
			break;
		case FRAME_RATE:
			cout << "Type: FRAME_RATE" << endl;
			break;
		case TEMPERATURE:
			cout << "Type: TEMPERATURE" << endl;
			break;
		case UNSPECIFIED_PROPERTY_TYPE:
			cout << "Type: UNSPECIFIED_PROPERTY_TYPE" << endl;
			break;
		default:
			cout << "Unknown" << endl;
			break;
    }

    if (propInfo.present)
		cout << "Present: " << "YES" << endl;
    else
		cout << "Present: " << "NO" << endl;

    if (propInfo.absValSupported)
        cout << "absValSupported: " << "YES" << endl;
    else
        cout << "absValSupported: " << "NO" << endl;

    if (propInfo.autoSupported)
        cout << "autoSupported: " << "YES" << endl;
    else
        cout << "autoSupported: " << "NO" << endl;

    if (propInfo.onOffSupported)
		cout << "On/Off Supported: " << "YES" << endl;
    else
		cout << "On/Off Supported: " << "NO" << endl;

    if (propInfo.manualSupported)
        cout << "manualSupported: " << "Yes" << endl;
    else
        cout << "manualSupported: " << "No" << endl;

    if (propInfo.onePushSupported)
        cout << "onePushSupported: " << "Yes" << endl;
    else
        cout << "onePushSupported: " << "No" << endl;

    if (propInfo.readOutSupported)
        cout << "readOutSupported: " << "Yes" << endl;
    else
        cout << "readOutSupported: " << "No" << endl;

    cout << "abs MAX: " << propInfo.absMax << " " << propInfo.pUnits << endl;
    cout << "abs MIN: " << propInfo.absMin << " " << propInfo.pUnits << endl;
    cout << "int MAX: " << propInfo.max << endl;
    cout << "int MIN: " << propInfo.min << endl;

} // End: void Flea3Camera::printPropertyInfo(PropertyInfo prop)


/**
 * @brief Prints to screen the various parameters provided by a Point Grey Property type. Valid types
 * are: BRIGHTNESS, AUTO_EXPOSURE, SHARPNESS, WHITE_BALLANCE, HUE, SATURATION, GAMMA, IRIS, FOCUS,
 * ZOOM, PAN, TILT, SHUTTER, GAIN, TRIGGER_MODE, TRIGGER_DELAY, FRAME_RATE, TEMPERATURE. Some of these
 * will not be valid for any camera and will print out that they are not "present" for the camera.
 * Generally these provide the current state of the specified type.
 *
 * 	Example of use:
 * 	FlyCapture2::Property prop;
 * 	prop.type = FlyCapture2::BRIGHTNESS;
 * 	cam.GetProperty(& prop); // where "cam" is a FlyCapture2::camera object.
 * 	printProperty(prop);
 * @param prop
 */
void Flea3Camera::printProperty(Property prop) {

	cout << "FlyCapture: Print Property for Camera: " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
    switch(prop.type) {
		case BRIGHTNESS:
			cout << "Type: BRIGHTNESS" << endl;
			break;
		case AUTO_EXPOSURE:
			cout << "Type: AUTO_EXPOSURE " << endl;
			break;
		case SHARPNESS:
			cout << "Type: SHARPNESS" << endl;
			break;
		case WHITE_BALANCE:
			cout << "Type: WHITE_BALANCE" << endl;
			break;
		case HUE:
			cout << "Type: HUE" << endl;
			break;
		case SATURATION:
			cout << "Type: SATURATION" << endl;
			break;
		case GAMMA:
			cout << "Type: GAMMA" << endl;
			break;
		case IRIS:
			cout << "Type: IRIS" << endl;
			break;
		case FOCUS:
			cout << "Type: FOCUS" << endl;
			break;
		case ZOOM:
			cout << "Type: ZOOM" << endl;
			break;
		case PAN:
			cout << "Type: PAN" << endl;
			break;
		case TILT:
			cout << "Type: TILT" << endl;
			break;
		case SHUTTER:
			cout << "Type: SHUTTER" << endl;
			break;
		case GAIN:
			cout << "Type: GAIN" << endl;
			break;
		case TRIGGER_MODE:
			cout << "Type: TRIGGER_MODE" << endl;
			break;
		case TRIGGER_DELAY:
			cout << "Type: TRIGGER_DELAY" << endl;
			break;
		case FRAME_RATE:
			cout << "Type: FRAME_RATE" << endl;
			break;
		case TEMPERATURE:
			cout << "Type: TEMPERATURE" << endl;
			break;
		case UNSPECIFIED_PROPERTY_TYPE:
			cout << "Type: UNSPECIFIED_PROPERTY_TYPE" << endl;
			break;
		default:
			cout << "Unknown" << endl;
			break;
    }

    if (prop.present)
		cout << "Present: " << "YES" << endl;
    else
		cout << "Present: " << "NO" << endl;

    if (prop.absControl)
        cout << "Abs Control: " << "ON" << endl;
    else
        cout << "Abs Control: " << "OFF" << endl;

    if (prop.onePush)
		cout << "One Push: " << "ON" << endl;
    else
		cout << "One Push: " << "OFF" << endl;

    if (prop.onOff)
        cout << "On/ Off: " << "ON" << endl;
    else
        cout << "On/ Off: " << "OFF" << endl;

    if (prop.autoManualMode)
        cout << "Auto Man Mode: " << "ON" << endl;
    else
        cout << "Auto Man Mode: " << "OFF" << endl;

    cout << "Abs value: " << prop.absValue << endl;
    cout << "Value A: " << prop.valueA << endl;
    cout << "Value B: " << prop.valueB << endl;

} // End: void Flea3Camera::printProperty(Property prop)


/**
 * @brief Prints the FlyCapture2 build info on the screen (taken from FlyCapture SDK examples found in /usr/src/flycapture)
 */
void Flea3Camera::PrintBuildInfo() {

	FC2Version fc2Version;
	Utilities::GetLibraryVersion( &fc2Version );
	ostringstream version;
	version << "FlyCapture2 library version: " << fc2Version.major << "."
			<< fc2Version.minor << "." << fc2Version.type << "."
			<< fc2Version.build;
	cout << version.str() << endl;

}


/**
 * @brief Prints camera info on screen (taken from FlyCapture SDK examples found in /usr/src/flycapture)
 */
void Flea3Camera::PrintCameraInfo() {

	cout << endl;
	cout << "Camera Information for Camera: " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
	cout << "  Serial number - " << cam_Info.serialNumber << endl;
	cout << "  Camera model - " << cam_Info.modelName << endl;
	cout << "  Camera vendor - " << cam_Info.vendorName << endl;
	cout << "  Sensor - " << cam_Info.sensorInfo << endl;
	cout << "  Resolution - " << cam_Info.sensorResolution << endl;
	cout << "  Firmware version - " << cam_Info.firmwareVersion << endl;
	cout << "  Firmware build time - " << cam_Info.firmwareBuildTime << endl;
	cout << endl;

}


/**
 * @brief Prints the FlyCapture2 error trace on the screen
 * @param error
 */
void Flea3Camera::PrintError(Error error) {

	cerr << "FLEA_3_CAMERA: Error from FlyCapture for Camera: " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
    error.PrintErrorTrace();

}


/**
 * @brief Prints the Format 7 capabilities of the camera (taken from FlyCapture SDK examples found in /usr/src/flycapture)
 */
void Flea3Camera::PrintFormat7Capabilities() {

	cout << "FLEA_3_CAMERA: Format 7 Capabilities for Camera: " << cam_Info.modelName << " S/N: " << cam_Info.serialNumber << endl;
	cout << "  Max image pixels: (width, height) -> (" << fmt7Info.maxWidth << ", "
		 << fmt7Info.maxHeight << ")" << endl;
	cout << "  Image Unit size: (Hstep, VStep) -> (" << fmt7Info.imageHStepSize << ", "
		 << fmt7Info.imageVStepSize << ")" << endl;
	cout << "  Offset Unit size: (Hstep, Vstep) -> (" << fmt7Info.offsetHStepSize << ", "
		 << fmt7Info.offsetVStepSize << ")" << endl;
	cout << "  Pixel format bitfield: 0x" << fmt7Info.pixelFormatBitField << endl;

}
