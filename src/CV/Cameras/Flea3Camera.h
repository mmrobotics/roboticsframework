#ifndef FLEA_3_CAMERA_H
#define FLEA_3_CAMERA_H

#include <string>
#include <opencv2/core.hpp>
#include <flycapture/FlyCapture2.h>
#include <CV/Camera.h>
#include <QReadWriteLock>


/**
 * @brief The Flea3Camera class is a wrapper for the Flea3 camera from Point Grey Research (now owned by FLIR)
 * and implements the Camera interface for use with computer vision. Requires OpenCV and FlyCapture2. See the
 * Robotics Framework README.md for more details.
 */
class Flea3Camera: public Camera {

	Q_OBJECT

	public:
		enum CAMERA_STATE { ERROR, READY, VIDEO_MODE };

		Flea3Camera(unsigned int serialNumber);
		~Flea3Camera() override;

		// Camera Class Functions
		bool startVideoStream() override;
		bool startVideoStream(float desiredFrameRate) override;
		bool startVideoStream(float desiredFrameRate, cv::Rect& desiredROI) override;
		bool stopVideoStream() override;
		bool isVideoStreaming() const override;
		bool isInError() const override;
		bool getFrame(cv::Mat& frame) const override;
		bool getFrame(cv::Mat& frame, cv::Rect& correspondingROI) const override;

		// Region of Interest Functions
		cv::Rect getROI() const override;
		cv::Size getMaxROISize() const override;
		cv::Size getMinROISize() const override;
		bool validateROI(int& x, int& y, int& width, int& height) const override;
		bool setROI(cv::Rect& newROI) override;
		bool setROIToMax() override;
		bool setROIPosition(int& x, int& y) override;


		// Frame Rate Functions
		float getFPS() override;
		float getMaxFPS() override;
		float getAbsoluteMaxFPS() const override;
		float getMinFPS() override;
		bool setFPS(float desiredFrameRate) override;

		// Obtain temperature information
		float getTemp();
		bool tempInRange();

		// Print FlyCapture Info
		void PrintBuildInfo();  // Prints FlyCapture2 SDK Information
		void PrintCameraInfo(); // Prints Camera Specific Information like Serial number
		void PrintFormat7Capabilities();

		// Get camera status
		CAMERA_STATE getState();
		std::string getState_str();
		bool isOn();

	private:
		CAMERA_STATE state;
		cv::Mat currentFrame;				// Last Frame Received
		unsigned int imageDataSize;
		mutable QReadWriteLock frameMutex;
		long frameTimestamp;				// Timestamp of the received frame in milliseconds (overflows at 128 seconds)
		long frameTimestampZero;			// The frame timestamp of the first frame, which is considered the zero frame timestamp
		long lastFrameTimestamp;			// The (zeroed) timestamp of the last frame, used to detect when the frame timestamp has reached overflow
		long frameTimestampOverflowCount;	// A monotonically increasing count of the number of times the frame timestamp has reached overflow
		long firstFrameSysTimestamp;		// The system time corresponding to the first received frame, used to register frame timestamps to system time
		bool gotFirstFrame;					// Boolean used to initialize some of the frame timestamp variables on the first received frame

		cv::Rect currentROI;				// Current ROI the camera is set to use
		unsigned int ROI_P0S_ADDRESS;
		unsigned int ROI_SIZ_ADDRESS;

		// FlyCapture related variables
		static FlyCapture2::BusManager busMgr;					// Finds Connected Cameras (static because we only need one bus manager for all cameras)
		static bool camerasFound;
		FlyCapture2::Camera	cam;								// FlyCapture's Camera object
		FlyCapture2::PGRGuid cam_ID;							// FlyCapture's Camera Identification object
		FlyCapture2::CameraInfo cam_Info;						// FlyCapture's Camera Info object
		FlyCapture2::FC2Config cam_config;						// FlyCapture's Camera Configuration object
		FlyCapture2::Error error;								// FlyCapture's Error object
		FlyCapture2::Error error_callback;						// Error object for the callback thread to avoid data race for error
		FlyCapture2::Format7Info fmt7Info;
		FlyCapture2::Format7ImageSettings fmt7ImageSettings;
		FlyCapture2::Format7PacketInfo fmt7PacketInfo;

		bool findCameras();
		bool setupCamera(); // Turns the camera on and initializes properties and Format 7 settings
		bool turnOnCamera();
		bool turnOffCamera();

		bool autoSetAllProps();
		bool autoSetProp(FlyCapture2::PropertyType type);

		void PrintError(FlyCapture2::Error error);
		void printProperty(FlyCapture2::Property prop);
		void printPropertyInfo(FlyCapture2::PropertyInfo propInfo);

		void receiveImage(FlyCapture2::Image* image);

		static void receiveImageWrapper(FlyCapture2::Image* p_image, const void* Obj) {

			reinterpret_cast<Flea3Camera*>(const_cast<void*>(Obj))->receiveImage(p_image);

		}

};

#endif // FLEA_3_CAMERA_H
