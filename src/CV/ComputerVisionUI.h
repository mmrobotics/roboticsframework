#ifndef COMPUTER_VISION_UI_H
#define COMPUTER_VISION_UI_H

#include <string>

#include <QtCore>
#include <QtWidgets>

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>

#include <QtGUIElements/Buttons.h>
#include <CV/CameraPair.h>
#include <CV/ChessboardCalibrationPattern.h>
#include <CV/CustomProjectionMethod.h>
#include <CV/ComputerVisionTracker.h>
#include <CV/ROIServo.h>
#include <RoboticsToolbox.h>
#include <Utilities/Filters.h>
#include <Utilities/Timing.h>


class ProjectionSocket;		// Forward declaration needed to resolve circular dependency
class VideoDisplayWidget;	// Forward declaration to let ComputerVisionUI be the first class declaration in the file

/**
 * @brief The ComputerVisionUI class is a dynamic user interface designed to be the primary tool for any computer vision tasks
 * a user may have. It can be used to display and/or record video from a camera pair, to calibrate the intrinsic parameters of
 * a camera pair and/or determine their poses relative to each other and a world frame, to project 3D pose information from
 * arbitrary sources onto the camera pair feed, and to perform arbitrary computer vision tracking algorithms with or without
 * the use of ROIs and ROI servos.
 *
 * Below, each of the use cases (modes) of this class are explained in detail.
 *
 * <h2>Basic Usage</h2>
 *
 * In its most basic usage, you simply provide a CameraPair object to the constructor of the class and you will be able to view
 * the video from the camera pair as well as record that video and take still shots. These capabilities are available in every
 * mode and for every type of CameraPair you provide (see CameraPair for an explanation of the types of CameraPair objects).
 * The constructor also accepts an optional ComputerVisionUI::Options object which can be used to change default settings in the
 * user interface and is used to provide the components necessary for some of the features described below. See the documentation
 * for ComputerVisionUI::Options for details on what each of the options do. The options that are relevant for basic usage are
 * `outputDirectory` and `updateRate`. With these you can specify the directory where the output of the user interface will be
 * placed as well as the update rate of the user interface. The update rate directly controls the frame rate of the camera when
 * computer vision tracking is not being performed. The video recording will run at the update rate as well and will record
 * exactly what is seen in the user interface. If for whatever reason (user instigated or otherwise) the feed from the cameras
 * changes size while recording is occurring, the recording will be stopped automatically, what was recorded so far will be saved
 * to file, and the user will be notified. If you `ctrl + click` on the video feed, the user interface will display the RGB and
 * CSV values of the pixel under the cursor. This is very convenient when designing computer vision tracking algorithms. The basic
 * mode that provides only these features is called <b>video only mode</b>, but these features are available in every mode. If you
 * provide a CameraPair that is calibrated and not localized, then you will also enter video only mode, but you will additionally
 * be able to use the undistorted video feed.
 *
 * <h2>Calibration</h2>
 *
 * If you provide a CameraPair that is uncalibrated and not localized and you provide a valid ChessboardCalibrationPattern in
 * the ComputerVisionUI::Options object, then ComputerVisionUI will enter <b>calibration mode</b>. Calibration mode is used to
 * calibrate the intrinsic parameters (camera matrix and distortion coefficients) of each camera in the provided CameraPair
 * object and optionally compute the transformation from the camera 1 frame to the camera 2 frame and the fundamental matrix
 * of the CameraPair. The camera 1 frame to camera 2 frame transformation may be required by later calibration workflows to
 * localize the CameraPair.
 *
 * This mode will set the CameraPair to display the full camera frame so calibration can be done with the full camera frame.
 * In order to calibrate the camera pair, the following steps must be taken:
 *
 * 1. Take 30-60 samples of the chessboard pattern in as many poses as possible within view of the cameras. A sample is taken
 * either by pressing the enter key, return key, or clicking on the "TAKE SAMPLE" button. Make sure to take samples with the
 * chessboard in view of both cameras, but also make sure to take samples at the edges of each camera even if they are not in
 * view of both cameras. This way you can make sure that the cameras are calibrated correctly individually while also gathering
 * the necessary images to compute the camera 1 to camera 2 frame transformation later if desired. Each sample will appear in
 * the user interface so you can review that the samples taken so far are sufficient.
 * 2. Click the "CALIBRATE CAMERA INTRINSICS" button to calibrate the individual camera intrinsic parameters. Once completed,
 * the intrinsic calibration files will be written. This also makes the undistorted video feed available so you can visually
 * verify the distortion calibration of each individual camera.
 * 3. Optionally click the "CALIBRATE 1-TO-2 TRANSFORM" button to compute the camera 1 to camera 2 frame transformation as well
 * as the fundamental matrix and write these data to file.
 *
 * The error of each calibration stage is printed in the user interface for reference and is written to text files corresponding
 * to each calibration file produced. Calibration file writing uses the functions in the CVCalibIO namespace which provides a way
 * to read the data again for later use (particularly by CameraPair). All files are written in a directory named
 * `CVUICalibrationOutput` which is created in the directory specified by the `outputDirectory` option.
 *
 * You may control the distortion model that is used for each camera via the `numDistortionParams_cam1` and
 * `numDistortionParams_cam2` options. These options specify the number of distortion parameters used by ComputerVisionUI when
 * calibrating the cameras. This class uses OpenCV for all calibration and therefore is subject to the conventions of that library.
 * In OpenCV the distortion model is controlled by how many distortion parameters are used. The number of distortion parameters
 * can be 4, 5, 8, 12, or 14 depending on which distortion models you wish to use. See the OpenCV documentation for details of
 * each model.
 *
 * The transformation between camera frames consists of the orientation of the camera 1 frame expressed in the camera 2 frame
 * (i.e. A 3x3 rotation matrix with columns <SUP>2</SUP>R<SUB>1</SUB> = [<SUP>2</SUP>x<SUB>1</SUB> <SUP>2</SUP>y<SUB>1</SUB>
 * <SUP>2</SUP>z<SUB>1</SUB>]) and the translation vector from the camera 2 frame to the camera 1 frame expressed in the camera
 * 2 frame (i.e. A 3x1 vector <SUP>2</SUP>t<SUB>21</SUB>).
 *
 * <h2>Projecting 3D Pose Information</h2>
 *
 * If a CameraPair that is calibrated and localized is provided, then ComputerVisionUI will enter <b>computer vision mode</b>.
 * In this mode it is possible to project 3D pose information from arbitrary sources onto the camera video feed for a sort of
 * augmented reality. The 3D pose information that can be projected is one of the following:
 *
 * 1. A 3D position (Robo::Position)
 * 2. A 3D position and a 2D heading vector (Robo::Pose5DOF)
 * 3. A 3D frame (Robo::Pose6DOF)
 *
 * The source of this information can be a ComputerVisionTracker, the result of some other localization algorithm, or any other
 * source that produces one of the above types of 3D pose information. The sources are connected to ComputerVisionUI using the
 * same approach that StateDataUI uses (see StateDataUI). Each source must emit a Qt signal with at least one parameter. The
 * first parameter must be a const reference to one of the three types from the Robo namespace above. The source's signal is then
 * connected to a compatible slot of a ProjectionSocket which is created and managed by ComputerVisionUI. The slots are:
 *
 * 1. ProjectionSocket::receivePosition()
 * 2. ProjectionSocket::receivePose5DOF()
 * 3. ProjectionSocket::receivePose6DOF()
 *
 * Each of these slots may be connected to a signal whose first parameter may be implicitly converted to the type it accepts. However,
 * each slot will only see the type that it accepts and any additional information will not be projected.
 *
 * The ProjectionSocket is created using ComputerVisionUI::operator[]() by providing a unique name for the socket (which will be
 * used to identify the socket in the user interface). The creation of the ProjectionSocket and the connection of signal to slot
 * are meant to be performed in the same line in the main() function as follows (assume an object `s` of type `Source` has a Qt
 * signal named `newPoseInformation()` and is already created and the ComputerVisionUI is named `cvUI`):
 *
 * ~~~{.cpp}
 * QObject::connect(&s, &Source::newPoseInformation, cvUI["Socket Name"], &ProjectionSocket::receivePosition);
 * ~~~
 *
 * The fourth argument in the QObject::connect() function may be any of the three ProjectionSocket slots. The parameter of the
 * `Source::newPoseInformation()` signal in the specific connection above may be any of the three options because all of them are
 * positions (i.e. they can be implicitly converted to a Robo::Position). Keep in mind however that this will only ever project the
 * position part of any compatible signal. If the source produces Robo::Pose6DOF and you want to project the full 6 DOF pose, you
 * need to connect to the ProjectionSocket::receivePose6DOF() slot instead. If a socket with the provided name already exists, it
 * will be as if the connection was never made. Each ProjectionSocket can be connected to exactly one signal and that signal can
 * only be connected to one of the three slots.
 *
 * Making any number of projection sockets will produce a projection socket table in the user interface. This is where you can
 * view the socket's status and control how the data arriving on the socket is displayed. Each row corresponds to each projection
 * socket that was created. In the type column, the type of the data being displayed is shown (not necessarily the same as the type
 * of data arriving on the socket due to the implicit conversion mentioned above). A type of PENDING means the socket has not yet
 * received any data. A type of IDLE means the socket has not received data recently and will not be projected onto the feed. You
 * can control how ComputerVisionUI decides when to consider projection sockets idle through the `projectionIdleMaxTimeout` and
 * `projectionTimeIntervalMultiplier` options. Each projection socket is also assigned an ID number that is used to label it in the
 * video feed. You can toggle the display of the label as well as the socket itself in the socket table. Finally, you can control the
 * length of the arrows for 5 and 6 DOF pose information.
 *
 * By default, ComputerVisionUI does not project pose information if is is within a certain distance of each camera. Specifically it
 * does not project poses if the z component of any part of the pose is less than a certain threshold. This threshold is controllable
 * via the `projectionMinZCamera1_mm` and `projectionMinZCamera2_mm` options.
 *
 * Projection works even in undistorted video feed and when ROIs (discussed in the next section) are used.
 *
 * The default projection algorithm for each camera uses cv::projectPoints(). If you must use a custom projection method (perhaps to
 * account for light passing through multiple media), you may provide a CustomProjectionMethod object for one or both cameras (the
 * method may need to be different for each camera) in the ComputerVisionUI::Options object.
 *
 * <h2>Computer Vision Tracking</h2>
 *
 * If a ComputerVisionTracker is provided in `cvTracker` of the ComputerVisionUI::Options object along with a CameraPair that is calibrated
 * and localized, then ComputerVisionUI will display controls for computer vision tracking in addition to entering computer vision mode.
 * The controls for computer vision tracking largely revolve around starting and stopping the tracking, setting tracking frame rates, and
 * controlling the use of ROIs.
 *
 * You may specify the main computer vision frame rate and then start the computer vision tracking with the "START" button. The
 * ComputerVisionTracker runs as fast as it can, therefore, the frame rate chosen should be considered the localization rate of the tracker.
 * The actual rate the ComputerVisionTracker is running is displayed for convenience. The main computer vision is always started using the
 * full camera frames. This is done so that an initial lock on the target(s) can be achieved before using any sort of ROI. If the frame rate
 * you choose is too fast for the camera given its current settings, then the tracking will fail to start and a message will be displayed
 * notifying of the current maximum frame rate. Once computer vision tracking has been started, you may view the processed frames (if the
 * particular ComputerVisionTracker provided is set up to specify them) by using the processed video feed. See the documentation of
 * ComputerVisionTracker for details on how to write your own computer vision tracking algorithms. Note that the result of computer vision
 * tracking will not automatically be displayed on the video feed. You must connect to a projection socket as described above.
 *
 * The rest of the computer vision tracking controls deal with the use of ROIs. An ROI (Region of Interest) is a rectangular sub region of a
 * frame. There are two types of ROIs that the computer vision framework deals with. The first is the camera ROI. This ROI is set on the camera
 * and essentially tells the camera to only stream the ROI rather than the full frame. This is useful because some cameras can run at higher
 * frame rates depending on the size of the ROI it is asked to produce. Most cameras that support the use of ROIs do not support changing the
 * size of the ROI while the camera is running, but do support changing its location. Furthermore, cameras that support an ROI typically place
 * restrictions on the set of sizes and locations it can be set to.
 *
 * The other type of ROI is the processing ROI. This ROI can be thought of as an ROI of the camera ROI and is the portion of the camera ROI that
 * the ComputerVisionTracker actually processes to perform tracking. This is useful because computer vision processing is typically very heavy
 * and it can reduce the processing time required by the tracking algorithm. The processing ROI does not have any restriction on the set of
 * locations or sizes it can have other than the fact that it must reside completely within the camera ROI. The processing ROI can even change
 * size while the cameras are running.
 *
 * You may either use a fixed ROI or servo the ROIs. If the target(s) you are tracking will not move very much, then fixed ROIs may be suitable,
 * but it is likely that you will need to servo the ROIs around to keep the target(s) in view. This is done by providing two ROIServo classes
 * in the `ROIServoCamera1` and `ROIServoCamera2` variables of the options object. The default ROIServo will simply center the ROIs on the
 * target(s), but you can implement your own servo algorithm if needed. See the documentation for ROIServo to see how this is done.
 *
 * You may start using ROIs by pressing the "START ROI" button. This will allow you to see a "preview" of the ROIs. A blue box represents the camera
 * ROI and a red box represents the processing ROI. The ROIs can always be hidden with the "Hide ROIs" checkbox. You may chose between fixed and servo
 * if servos have been provided. When using the servo option, the preview will show how the servo would behave, but does not actually affect the
 * camera or processing ROIs (i.e. they stay at the full frame at this stage). You may specify a frame rate to use with the ROIs since this
 * could be much higher than the maximum frame rate for the cameras when using the full frame. You can also specify the ROI mode. The ROI modes
 * are:
 *
 * 1. %Camera ROI Only
 * 2. Processing ROI Only
 * 3. Both
 *
 * These modes are pretty self-explanatory. You can use the mode to decide which combination of ROIs is needed for your situation.
 *
 * Finally, you can specify the size and location of each ROI depending on the mode and whether you are using fixed or servo ROIs (e.g. the
 * location cannot be specified when using servos). ComputerVisionUI takes care of making sure the ROI sizes and locations are always valid so
 * you do not need to worry about the requirements of specific cameras.
 *
 * Every time the mode is changed, the ROI size and location settings are returned to their default settings. The default settings can be
 * customized using the `defaultCameraROI_cam1`, `defaultCameraROI_cam2`, `defaultProcessingROI_cam1`, and `defaultProcessingROI_cam2`
 * variables of the options object. The default tracking frame rates can also be set using the `cvFrameRate` and `ROIFrameRate` variables.
 *
 * Once the settings are to your liking, the ROIs are engaged by pressing the "ENGAGE ROI" button. This will briefly stop the cameras and
 * make computer vision tracking start using the ROI settings. If you are servoing a camera ROI, then the camera ROI will be shown on a black
 * background that is the size of the full camera frame. The camera ROI will move over this background as it is servoed. This is useful to
 * identify where the camera ROI currently is relative to the full camera frame.
 */
class ComputerVisionUI : public QWidget {

	Q_OBJECT

	public:
		/**
		 * @brief The Options struct is used to set various options for the ComputerVisionUI as well as
		 * set the various necessary computer vision related components. See the documentation for each member variable
		 * for details of each setting as well as the ComputerVisionUI documentation for more information.
		 */
		struct Options {

			Options() {}

			/**
			 * @brief This is the directory that ComputerVisionUI will use to output calibration files, videos, etc. The
			 * default value is "Output/". Note that it is a QDir and therefore you must use forward slashes for the
			 * directory separator on every platform.
			 */
			QDir outputDirectory = QDir("Output");

			/**
			 * @brief This number sets the update rate of the user interface. This is the rate at which camera feed
			 * will be displayed and processed and therefore also corresponds to the frame rate of recorded video. The
			 * default update rate is 24 Hz. The camera pair is also started at the update rate, but if the update rate
			 * is too fast for the camera pair, the camera pair rate will be throttled to its maximum. If this occurs,
			 * the user interface update rate remains the same.
			 */
			double updateRate = 24.0;

			/**
			 * @brief Struct containing the parameters that describe the calibration chessboard pattern. If you
			 * provide an uncalibrated and not localized CameraPair to ComputerVisionUI, then ComputerVisionUI will
			 * check whether the user has assigned this variable to decide whether to enter calibration mode.
			 */
			ChessboardCalibrationPattern chessboardPattern;

			/**
			 * @brief This specifies the number of distortion parameters desired when calibrating camera 1. The organization
			 * and meaning of distortion parameters follows that of OpenCV, therefore the only valid values are: 4, 5, 8,
			 * 12, or 14. The default setting is 5. See the documentation for ComputerVisionUI and OpenCV for more details.
			 */
			int numDistortionParams_cam1 = 5;
			/**
			 * @brief This specifies the number of distortion parameters desired when calibrating camera 2. The organization
			 * and meaning of distortion parameters follows that of OpenCV, therefore the only valid values are: 4, 5, 8,
			 * 12, or 14. The default setting is 5. See the documentation for ComputerVisionUI and OpenCV for more details.
			 */
			int numDistortionParams_cam2 = 5;

			/**
			 * @brief This setting is used to set the maximum amount of time (in milliseconds) a projection socket will wait
			 * for new data before treating the socket as idle. The default timeout is 3000 (3 seconds).
			 */
			int projectionIdleMaxTimeout = 3000;

			/**
			 * @brief This setting is used by projection sockets when determining if the socket has become idle. The
			 * projection socket automatically determines the interval between data arrivals and will wait that amount
			 * of time multiplied by this setting before considering the socket idle. This is useful for variable rate
			 * data or data that may be arriving at a constant rate, but may have missing arrivals (i.e. data loss). To
			 * turn this feature off and only use the 'projectionIdleMaxTimeout' setting to determine idleness, set this
			 * value to 0.
			 * The default multiplier is 5.
			 */
			int projectionTimeIntervalMultiplier = 5;

			/**
			 * @brief This setting specifies the minimum distance in front of camera 1 (i.e. minimum z component) in
			 * millimeters required to project a projection socket onto camera 1. The default value is 50 and must be
			 * larger than 0.
			 */
			int projectionMinZCamera1_mm = 50;

			/**
			 * @brief This setting specifies the minimum distance in front of camera 2 (i.e. minimum z component) in
			 * millimeters required to project a projection socket onto camera 2. The default value is 50 and must be
			 * larger than 0.
			 */
			int projectionMinZCamera2_mm = 50;

			/**
			 * @brief Variable used to supply a custom projection algorithm for camera 1.
			 */
			CustomProjectionMethod* customProjectionMethodCamera1 = nullptr;

			/**
			 * @brief Variable used to supply a custom projection algorithm for camera 2.
			 */
			CustomProjectionMethod* customProjectionMethodCamera2 = nullptr;

			/**
			 * @brief Variable used to supply the computer vision tracker you wish to use. If you provide a calibrated and
			 * localized CameraPair to ComputerVisionUI, then ComputerVisionUI will check whether the user has assigned this
			 * variable to decide whether to display computer vision controls.
			 */
			ComputerVisionTracker* cvTracker = nullptr;

			/**
			 * @brief This specifies the default frame rate used for computer vision. If not specified, the default value
			 * is taken from `updateRate`.
			 */
			double cvFrameRate = 0.0;

			/**
			 * @brief This specifies the default frame rate used for computer vision when ROIs are engaged. If not specified,
			 * the default value is taken from `cvFrameRate`. If `cvFrameRate` is also not specified, then it is taken from
			 * `updateRate`.
			 */
			double ROIFrameRate = 0.0;

			/**
			 * @brief Variable used to supply the ROIServo for camera 1.
			 */
			ROIServo* ROIServoCamera1 = nullptr;

			/**
			 * @brief Variable used to supply the ROIServo for camera 2.
			 */
			ROIServo* ROIServoCamera2 = nullptr;

			/**
			 * @brief This specifies the default camera ROI settings to be used for camera 1. The default is an ROI that is
			 * centered and half the size of the full camera frame. You do not need to specify the entire ROI. Any part of
			 * the ROI you do not change (i.e. leave as -1) will behave as the default.
			 */
			cv::Rect defaultCameraROI_cam1 = cv::Rect(-1, -1, -1, -1);

			/**
			 * @brief This specifies the default camera ROI settings to be used for camera 2. The default is an ROI that is
			 * centered and half the size of the full camera frame. You do not need to specify the entire ROI. Any part of
			 * the ROI you do not change (i.e. leave as -1) will behave as the default.
			 */
			cv::Rect defaultCameraROI_cam2 = cv::Rect(-1, -1, -1, -1);

			/**
			 * @brief This specifies the default processing ROI settings to be used for camera 1. The default is an ROI that
			 * is centered within the camera ROI and half the size of the camera ROI. You do not need to specify the entire
			 * ROI. Any part of the ROI you do not change (i.e. leave as -1) will behave as the default.
			 */
			cv::Rect defaultProcessingROI_cam1 = cv::Rect(-1, -1, -1, -1);

			/**
			 * @brief This specifies the default processing ROI settings to be used for camera 2. The default is an ROI that
			 * is centered within the camera ROI and half the size of the camera ROI. You do not need to specify the entire
			 * ROI. Any part of the ROI you do not change (i.e. leave as -1) will behave as the default.
			 */
			cv::Rect defaultProcessingROI_cam2 = cv::Rect(-1, -1, -1, -1);

		};

		ComputerVisionUI(CameraPair& cameraPair, Options options = Options(), QWidget* parent = nullptr);
		ProjectionSocket* operator[](const char* socketName);

	protected:
		void closeEvent(QCloseEvent* event);
		void keyPressEvent(QKeyEvent* event);

	private:
		// Mode variables
		enum UIMode { VIDEO_ONLY, CALIBRATION, COMPUTER_VISION };
		enum VideoFeedMode { RAW, UNDISTORTED, PROCESSED };
		enum ROIMode { CAMERA_ONLY, PROCESSING_ONLY, BOTH };
		enum ROIType { FIXED, SERVO };
		UIMode uiMode = VIDEO_ONLY;
		VideoFeedMode feedMode = RAW;
		ROIMode roiMode = CAMERA_ONLY;
		ROIType roiType = FIXED;

		// Camera variables
		CameraPair& cameraPair;
		float cameraPairFrameRate;
		cv::Size maxCam1ROISize, maxCam2ROISize;
		cv::Size minCam1ROISize, minCam2ROISize;

		// User interface options
		Options uiOpt;

		// Update rate variables
		Filters::MovingAverageFilter<double, 5> actualUpdateRate;
		long lastUpdateTime;

		// Image variables
		cv::Mat current_image_1, current_image_2;
		cv::Rect ROIcam1, ROIcam2;

		// Video recording variables
		cv::VideoWriter video_recorder_1, video_recorder_2;
		cv::Size recording_size_1, recording_size_2;
		Timing::Timer recordingThread;
		QMutex imageMutex;

		// Variables used only in calibration mode
		bool isCameraPairAtFullFrame = false;
		bool individualCameraIntrinsicsCalibrated = false;
		int calibrationFlags;
		cv::Size pattern_size;
		std::vector<cv::Point3f> object_points;
		std::vector<std::vector<cv::Point3f>> object_points_arrayOfarrays;
		std::vector<std::vector<cv::Point2f>> point_list_1, point_list_2;
		std::vector<std::vector<cv::Point2f>> point_list_found_in_both_1, point_list_found_in_both_2;
		cv::Matx33d camera_matrix_1, camera_matrix_2;
		cv::Mat distortion_params_1, distortion_params_2;
		cv::Matx33d R; // Orientation of Camera 1 frame expressed in Camera 2 frame (i.e. Rotation from Camera 1 frame to Camera 2 frame)
		cv::Vec3d T; // Translation from Camera 2 to Camera 1 (expressed in the Camera 2 frame)
		cv::Matx33d E, F; // E -> Stereo Essential Matrix, F -> Stereo Fundamental Matrix

		// Variables used by the 3D pose projection feature
		QHash<QString, ProjectionSocket*> projectionSockets;
		int nextSocketID = 0;	// ID to use for the next created ProjectionSocket
		QList<QLabel*> projectionSocketTypeLabels;

		struct ProjectionLabelInfo {

			cv::Point2d originCam1;
			cv::Point2d originCam2;
			int socketLabel;

		};

		// QWidgets for video feed (always present)
		QVBoxLayout* overallLayout;
		VideoDisplayWidget* camera_display_1;
		VideoDisplayWidget* camera_display_2;
		SwitcherButton* record_button;
		StandardButton* still_button;
		QRadioButton* rawVideoFeedRadioButton;
		QRadioButton* undistortedVideoFeedRadioButton;
		QRadioButton* processedVideoFeedRadioButton;
		QLabel* updateRateLabel;
		QLabel* actualCameraFrameRateLabel;
		QLabel* desiredCameraFrameRateLabel;
		QTextEdit* outputDisplay;
		QHBoxLayout* bottomRowLayout;

		// QWidget for calibration mode
		QListWidget* imagesList;

		// QWidgets for computer vision
		SwitcherButton* startCVButton;
		QDoubleSpinBox* frameRateSpinBox;
		QLabel* cvUpdateRateLabel;
		SwitcherButton* startROIButton;
		SwitcherButton* engageROIButton;
		QGroupBox* ROIModeControls;
		QWidget* ROIControls;
		QDoubleSpinBox* ROIFrameRateSpinBox;
		QGroupBox* ROITypeControls;
		QGroupBox* camera1ROIcontrols;
		QGroupBox* camera2ROIcontrols;
		QSpinBox* cameraROI1_x;
		QSpinBox* cameraROI1_y;
		QSpinBox* cameraROI1_width;
		QSpinBox* cameraROI1_height;
		QSpinBox* processingROI1_x;
		QSpinBox* processingROI1_y;
		QSpinBox* processingROI1_width;
		QSpinBox* processingROI1_height;
		QSpinBox* cameraROI2_x;
		QSpinBox* cameraROI2_y;
		QSpinBox* cameraROI2_width;
		QSpinBox* cameraROI2_height;
		QSpinBox* processingROI2_x;
		QSpinBox* processingROI2_y;
		QSpinBox* processingROI2_width;
		QSpinBox* processingROI2_height;
		QCheckBox* hideROICheckBox;

		// QWidgets for projection sockets
		QWidget* socketTable;
		QGridLayout* socketTableLayout;
		QScrollArea* scrollArea;

		QTimer* update_timer;

		// Utility functions
		void displayMsg(const QString& text, bool newLine = true);
		void displayError(const QString& text, bool newLine = true);
		bool startCameras(double frameRate, bool throttle = false);

		friend class VideoDisplayWidget; // Allow VideoDisplayWidget to access private members of ComputerVisionUI

	private slots:
		// UI update slots
		void update();
		void handleProjectionSockets(cv::Mat& image_1, cv::Mat& image_2);
		void computeArrowDrawOrder(const std::vector<cv::Point3d>& arrowTips, QVector<unsigned long>& order);
		void drawROIs(cv::Mat& image_1, cv::Mat& image_2);

		// Recording slots
		void recordButtonClicked();
		bool recordFrames();
		void recordingSizeMismatchHandler();
		void executeStillShot();

		// Video feed mode slots
		void switchToRawMode();
		void switchToUndistortMode();
		void switchToProcessedMode();

		// Calibration slots
		void recordSample();
		void performIndividualIntrinsicsCalibration();
		void performCamera1To2TransformCalibration();

		// Computer vision slots
		void enableComputerVision();
		void startROIs();
		void engageROIs();
		void switchToCameraOnlyROIMode();
		void switchToProcessingOnlyROIMode();
		void switchToBothROIMode();
		void switchToFixedROIType();
		void switchToServoROIType();
		void updateROIShapeControlsEnabledState();
		void resetROIs();
		void cameraROI1_x_valueChanged(int value);
		void cameraROI1_y_valueChanged(int value);
		void cameraROI2_x_valueChanged(int value);
		void cameraROI2_y_valueChanged(int value);
		void cameraROI1_width_valueChanged(int value);
		void cameraROI1_height_valueChanged(int value);
		void cameraROI2_width_valueChanged(int value);
		void cameraROI2_height_valueChanged(int value);
		void processingROI1_width_valueChanged(int value);
		void processingROI1_height_valueChanged(int value);
		void processingROI2_width_valueChanged(int value);
		void processingROI2_height_valueChanged(int value);

	signals:
		void recordingSizeMismatch();

};


/**
 * @brief The ProjectionSocket class is used by ComputerVisionUI and should not be created directly.
 */
class ProjectionSocket : public QObject {

	Q_OBJECT

	private:
		enum Type { PENDING, IDLE, POSITION, POSE5DOF, POSE6DOF };
		const char* name;
		const int ID;
		Type type = PENDING;
		Robo::Position position;
		Robo::Pose5DOF pose5DOF;
		Robo::Pose6DOF pose6DOF;
		Robo::Vector3d headingTip;
		Robo::Vector3d xAxis;
		Robo::Vector3d yAxis;
		Robo::Vector3d zAxis;
		long timeOfReceive;				// Used to determine if the socket is idle
		long receiveInterval;			// Used to determine if the socket is idle
		int arrowLength = 50;			// millimeters
		bool shouldDisplay = true;		// Whether data from this socket should be displayed or not
		bool shouldShowLabel = true;	// Whether the label should be displayed or not

		ProjectionSocket(const char* name, const int ID) : name(name), ID(ID) {}

		bool getProjectableData(std::vector<cv::Point3d>& projData);
		inline void storeTime();

		friend class ComputerVisionUI;

	public slots:
		void receivePosition(const Robo::Position& position);
		void receivePose5DOF(const Robo::Pose5DOF& pose);
		void receivePose6DOF(const Robo::Pose6DOF& pose);
		void enableDisplay(int state);
		void enableLabel(int state);
		void changeArrowLength(int length);

};


/**
 * @brief The VideoDisplayWidget class is used by ComputerVisionUI and should not be created directly.
 */
class VideoDisplayWidget : public QLabel {

	Q_OBJECT

	private:
		cv::Mat final_image;
		int width;
		int height;
		ComputerVisionUI* ui;
		QString display_text;
		bool image_displayed;

	public:
		VideoDisplayWidget(int width, int height, ComputerVisionUI* ui, QString display_text = "", QWidget *parent = nullptr);

		void updateImage(const cv::Mat& display_image);
		void noImage();

	protected:
		void mousePressEvent(QMouseEvent * e);

};

#endif // COMPUTER_VISION_UI_H
