#include "CVUtils.h"
#include <cmath>
#include <iostream>
#include <vector>

namespace CVUtils {

	/**
	 * @brief Populates the provided cv::Mat object such that it represents a center distance
	 * image kernel for use in image processing. A center distance kernel is a kernel where
	 * each element contains the distance from the walls of the kernel (i.e. the center pixel has
	 * the highest value, the pixels around that have the next highest value, and so on). It has a
	 * similar shape to a gaussian kernel. Note that this function will not change the size of the
	 * provided cv::Mat object, therefore the size of the kernel is implicitly specified by the size
	 * of the provided cv:Mat object.
	 * @param matrix cv::Mat of type uint8_t into which the kernel should be created.
	 */
	void createCenterDistanceKernel(cv::Mat& matrix) {

		// Compute the center of the kernel
		cv::Point center((matrix.rows - 1) / 2, (matrix.cols - 1) / 2);

		double max_distance = center.x;

		// Populate the kernel
		for (int aa = 0; aa < matrix.rows; ++aa) {
			for (int bb = 0; bb < matrix.cols; ++bb) {

				double distance = std::sqrt(std::pow(aa - center.x, 2) + std::pow(bb - center.y, 2));

				if (distance <= max_distance)
					matrix.at<uint8_t>(aa, bb) = static_cast<uint8_t>(std::floor(max_distance - distance));
				else
					matrix.at<uint8_t>(aa, bb) = 0;

			}
		}

	}


	/**
	 * @brief Computes the rigid body transformation between two sets of point correspondences representing the same rigid body.
	 * Put another way, this function computes the rigid body transformation between two frames using two point sets that
	 * represent the same rigid body as seen by each frame.
	 * @param pointsFrame1 A set of points representing the rigid body expressed in frame 1
	 * @param pointsFrame2 A set of points representing the rigid body expressed in frame 2
	 * @param transform a 3x4 matrix representing the top three rows of the rigid body transformation from frame 1 to frame 2
	 * @return True if successful, false otherwise
	 */
	bool computeRigidBodyTransform(const std::vector<cv::Point3d>& pointsFrame1, const std::vector<cv::Point3d>& pointsFrame2, cv::Matx34d& transform) {

		// Check that the point sets contain the same number of points
		if (pointsFrame1.size() != pointsFrame2.size()) {
			std::cerr << "CV_UTILS(computeRigidBodyTransform): The point sets must be of the same size. ERROR" << std::endl;
			return false;
		}

		// Compute the centroids
		cv::Point3d set1_centroid{0, 0, 0};
		cv::Point3d set2_centroid{0, 0, 0};
		for (unsigned long i = 0; i < pointsFrame1.size(); i++) {
			set1_centroid += pointsFrame1[i];
			set2_centroid += pointsFrame2[i];
		}
		set1_centroid /= static_cast<double>(pointsFrame1.size());
		set2_centroid /= static_cast<double>(pointsFrame2.size());

		// Compute the covariance matrix
		cv::Matx33d cov = cv::Matx33d::zeros();
		for (unsigned long i = 0; i < pointsFrame1.size(); i++)
			cov += cv::Vec3d(pointsFrame1[i] - set1_centroid) * cv::Vec3d(pointsFrame2[i] - set2_centroid).t();

		// Calculate the SVD of the covariance matrix
		cv::SVD svd(cov);

		// Compute the rotation and translation
		cv::Mat R = (svd.u * svd.vt).t();
		cv::Vec3d t = cv::Vec3d(set2_centroid) - cv::Matx33d(R) * cv::Vec3d(set1_centroid);

		// Check that the determinant of R is +1
		if (std::abs(cv::determinant(R) - 1.0) > 1e-10) {
			std::cerr << "CV_UTILS(computeRigidBodyTransform): The resulting rotation matrix was not a member of SO(3) (determinant = " << cv::determinant(R) << " != +1). ERROR" << std::endl;
			return false;
		}

		// Form the full 3x4 transform matrix
		cv::hconcat(R, t, transform);
		return true;

	}

}
