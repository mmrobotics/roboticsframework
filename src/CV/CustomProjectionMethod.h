#ifndef CUSTOM_PROJECTION_METHOD_H
#define CUSTOM_PROJECTION_METHOD_H

#include <vector>
#include <opencv2/core.hpp>


/**
 * @brief The CustomProjectionMethod class is used to implement your own algorithm for projecting 3D points expressed in a camera frame onto the image
 * plane of the camera. For example, if the light is traveling through multiple media (e.g. air and water) you may need to account for the way
 * the medium interfaces bend light.
 */
class CustomProjectionMethod {

	public:
		virtual ~CustomProjectionMethod() = default;

		/**
		 * @brief This function projects 3D points expressed in the camera frame onto the camera's image plane. Implement this function
		 * to implement your custom projection method. The implementer of this function must populate the `cameraPoints` variable.
		 * @param pointsInCameraFrame A list of 3d points expressed in the camera frame
		 * @param cameraMatrix The camera matrix
		 * @param distortionParams The camera distortion parameters
		 * @param cameraPoints The output list of 2D image points that correspond to the projection of each 3D point in `pointsInCameraFrame`
		 */
		virtual void projectPoints(const std::vector<cv::Point3d>& pointsInCameraFrame, const cv::Matx33d& cameraMatrix, const cv::Mat& distortionParams, std::vector<cv::Point2d> cameraPoints) const = 0;

};

#endif // CUSTOM_PROJECTION_METHOD_H
