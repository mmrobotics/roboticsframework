#include "ROIServo.h"
#include <iostream>


/**
 * @brief This function is the Qt slot that is intended to receive the ComputerVisionTracker::newTargetBoundingBoxCamera1() or
 * ComputerVisionTracker::newTargetBoundingBoxCamera2() signals and is the main servoing function. Depending on the mode of the
 * ROIServo, this function calls computeCameraROI() and/or computeProcessingROI() which are where the actual servoing
 * algorithms are implemented. This slot receives information from ComputerVisionTracker about the target(s) that it wants to
 * keep in view. This information is the position and size of a bounding box around the target(s). The position is the center
 * of the box expressed from the top-left corner of the full frame ROI.
 * @param boundingBoxCenterX X coordinate of the center of the bounding box of the target(s)
 * @param boundingBoxCenterY Y coordinate of the center of the bounding box of the target(s)
 * @param boundingBoxWidth Width of the bounding box of the target(s)
 * @param boundingBoxHeight Height of the bounding box of the target(s)
 */
void ROIServo::performServoing(int boundingBoxCenterX, int boundingBoxCenterY, int boundingBoxWidth, int boundingBoxHeight) {

	// Only perform the servoing if the servo has been started
	if (started) {

		// Start with servoing the camera ROI first if the mode involves servoing the camera ROI
		if (mode == CAM_ROI || mode == BOTH) {

			computeCameraROI(cv::Point(boundingBoxCenterX, boundingBoxCenterY), cv::Size(boundingBoxWidth, boundingBoxHeight), camera_ROI_Size, camera_ROI_Position);
			// Validate the ROI. Note that the camera_ROI_position class member will contain the adjusted position after this line
			camera->validateROI(camera_ROI_Position.x, camera_ROI_Position.y, camera_ROI_Size.width, camera_ROI_Size.height);

			// Set the ROI of the camera if the servo is engaged
			if (engaged)
				if (!camera->setROIPosition(camera_ROI_Position.x, camera_ROI_Position.y))
					std::cerr << "ROI_SERVO: The provided camera does not support servoing the ROI. ERROR" << std::endl;

		}

		// Servo the processing ROI if the mode involves servoing the processing ROI
		if (mode == PROC_ROI || mode == BOTH) {

			computeProcessingROI(cv::Point(boundingBoxCenterX, boundingBoxCenterY), cv::Size(boundingBoxWidth, boundingBoxHeight), camera_ROI_Size, camera_ROI_Position, processing_ROI_Size, processing_ROI_Position);
			// Emit a new processing ROI
			if (engaged)
				emit newProcessingROI(cv::Rect(processing_ROI_Position, processing_ROI_Size), camera_ROI_Position);

		}

	}

}


/**
 * @brief Resets the servo. This function gets called whenever the ROI servo is started (not engaged) by the ComputerVisionUI. The default
 * implementation does nothing, but the function is provided in case more complicated algorithms need it. Even though this function gets
 * called from the GUI thread (because it is called by ComputerVisionUI) and therefore has a chance to be called from a different thread
 * than the thread that ROIServo is running in, you should not need a mutex when implementing this function since it will only be called
 * when the ROI servo is off (meaning no thread should be actively running computeCameraROI() or computeProcessingROI when this function
 * is called).
 */
void ROIServo::reset() {

	/* Do nothing */

}


/**
 * @brief This function is where the computation of the next camera ROI position is performed. The default implementation is to center the
 * camera ROI on the center of the bounding box of the target(s).
 *
 * <b>Notes for implementers</b>: The resulting camera ROI position is placed in the `camera_ROI_Position` parameter. The other parameters
 * of the function are used to compute the camera ROI position. It is not necessary that you check that the computed camera ROI position does
 * not hang outside the full frame of the camera because this will be checked and adjusted for you.
 * @param boundingBoxCenter Center of the bounding box of the target(s) expressed from the top-left corner of the full camera frame
 * @param boundingBoxSize Size of the bounding box of the target(s). It is unlikely this will be useful for servoing the camera ROI as the size
 * of the camera ROI cannot change.
 * @param camera_ROI_Size The size of the camera ROI
 * @param camera_ROI_Position The output of the function which is the position of the camera ROI (top-left corner)
 */
void ROIServo::computeCameraROI(cv::Point boundingBoxCenter, cv::Size boundingBoxSize,
								cv::Size camera_ROI_Size, cv::Point& camera_ROI_Position) {

	Q_UNUSED(boundingBoxSize)

	// Center the camera ROI on the bounding box
	camera_ROI_Position = boundingBoxCenter - cv::Point(camera_ROI_Size.width / 2, camera_ROI_Size.height / 2);

}


/**
 * @brief This function is where the computation of the next processing ROI is performed. The default implementation is to center the
 * processing ROI on the center of the bounding box of the target(s) and leave the size the same.
 *
 * <b>Notes for implementers</b>: The resulting processing ROI position is placed in the	`processing_ROI_Position` parameter and the
 * resulting processing ROI size is placed in the `processing_ROI_Size` parameter. The position must be expressed with respect to the
 * camera ROI. In other words, the position is the position of the top-left corner of the processing ROI relative to the top-left
 * corner of the current camera ROI. You must make sure the processing ROI stays entirely within the camera ROI. If the processing ROI
 * hangs outside the camera ROI, the expected behavior is that it will be adjusted to be flush with any overhanging edge. The size cannot
 * grow to be larger than the camera ROI size. The current camera ROI information is provided in the parameters in order to meet these
 * requirements. If the ROIServo is in the BOTH mode, then the camera ROI will be the one set by computeCameraROI() including any
 * adjustments made by the camera itself.
 * @param boundingBoxCenter Center of the bounding box of the target(s) expressed from the top-left corner of the full camera frame
 * @param boundingBoxSize Size of the bounding box of the target(s)
 * @param camera_ROI_Size The size of the camera ROI
 * @param camera_ROI_Position The current position of the camera ROI
 * @param processing_ROI_Size The size of the processing ROI. This is the output of the function.
 * @param processing_ROI_Position The position of the processing ROI expressed with respect to the camera ROI. This is the output of the
 * function.
 */
void ROIServo::computeProcessingROI(cv::Point boundingBoxCenter, cv::Size boundingBoxSize,
									cv::Size camera_ROI_Size, cv::Point camera_ROI_Position,
									cv::Size& processing_ROI_Size, cv::Point& processing_ROI_Position) {

	Q_UNUSED(boundingBoxSize)

	// Center the processing ROI on the bounding box
	processing_ROI_Position = boundingBoxCenter - cv::Point(processing_ROI_Size.width / 2, processing_ROI_Size.height / 2);

	// Transform ROI position to be in terms of the camera ROI
	processing_ROI_Position -= camera_ROI_Position;

	// If the processing ROI extends beyond the camera ROI, adjust it so it fits inside the camera ROI
	if (processing_ROI_Position.x < 0)
		processing_ROI_Position.x = 0;
	if (processing_ROI_Position.y < 0)
		processing_ROI_Position.y = 0;
	if (processing_ROI_Position.x + processing_ROI_Size.width > camera_ROI_Size.width)
		processing_ROI_Position.x = camera_ROI_Size.width - processing_ROI_Size.width;
	if (processing_ROI_Position.y + processing_ROI_Size.height > camera_ROI_Size.height)
		processing_ROI_Position.y = camera_ROI_Size.height - processing_ROI_Size.height;

}
