#ifndef OPENCVQTMETATYPEDECLARATIONS_H
#define OPENCVQTMETATYPEDECLARATIONS_H

#include <opencv2/core.hpp>
#include <QObject>

// Declare cv::Mat and cv::Point as metatypes to the Qt system so they can be used in
// computer vision framework signals
Q_DECLARE_METATYPE(cv::Mat)
Q_DECLARE_METATYPE(cv::Rect)
Q_DECLARE_METATYPE(cv::Point)

#endif // OPENCVQTMETATYPEDECLARATIONS_H
