#include "CameraPair.h"
#include <algorithm>
#include "CVCalibrationIO.h"


/**
 * @brief Constructs a CameraPair object with the given Camera objects. If you only provide two camera objects, the CameraPair is
 * considered uncalibrated and not localized. If you provide valid intrinsics files in the `options` object then the camera pair is
 * considered calibrated and not localized. If you additionally provide at least two of three valid transform files in the `options`
 * object for then the camera pair is considered calibrated and localized. ComputerVisionUI will automatically
 * identify these situations and present an interface that is most useful for the context.
 * @param camera_1 A Camera object to act as camera 1
 * @param camera_2 A Camera object to act as camera 2
 * @param options The options object specifying paths to calibration files
 */
CameraPair::CameraPair(Camera& camera_1, Camera& camera_2, Options options)
	: camera_1(camera_1),
	  camera_2(camera_2),
	  camera_matrix_1(cv::Matx33d::zeros()),
	  camera_matrix_2(cv::Matx33d::zeros()),
	  _2R1(cv::Matx33d::eye()),
	  _wR1(cv::Matx33d::eye()),
	  _wR2(cv::Matx33d::eye()),
	  _2T21(cv::Vec3d::all(0)),
	  _wTw1(cv::Vec3d::all(0)),
	  _wTw2(cv::Vec3d::all(0)),
	  camera_1_to_camera_2_frame_transform(cv::Matx34d::eye()),
	  camera_2_to_camera_1_frame_transform(cv::Matx34d::eye()),
	  camera_1_to_world_frame_transform(cv::Matx34d::eye()),
	  world_to_camera_1_frame_transform(cv::Matx34d::eye()),
	  camera_2_to_world_frame_transform(cv::Matx34d::eye()),
	  world_to_camera_2_frame_transform(cv::Matx34d::eye()) {

	// Register signal types
	qRegisterMetaType<cv::Mat>();
	qRegisterMetaType<cv::Point>();

	// Read camera intrinsic calibration files if provided
	if (options.camera_1_intrinsics_filename && options.camera_2_intrinsics_filename &&
		CVCalibIO::readCameraIntrinsics(options.camera_1_intrinsics_filename, camera_matrix_1, distortion_params_1) &&
		CVCalibIO::readCameraIntrinsics(options.camera_2_intrinsics_filename, camera_matrix_2, distortion_params_2)) {

		isCalibrated = true;

	}

	// Read transform files (at least 2 of 3 must be provided to consider the camera pair localized
	if (options.camera_1_to_camera_2_transform_filename && CVCalibIO::readRigidTransform(options.camera_1_to_camera_2_transform_filename, camera_1_to_camera_2_frame_transform)) {

		// Compute the inverse of the transform (Must form the full 4x4 matrix to take the inverse)
		cv::Matx44d transform_sq;
		cv::vconcat(camera_1_to_camera_2_frame_transform, cv::Matx14d(0, 0, 0, 1), transform_sq);
		camera_2_to_camera_1_frame_transform = transform_sq.inv().get_minor<3, 4>(0, 0);

		// Extract the rotation and translation for camera_1_to_camera_2_frame_transform
		_2R1 = camera_1_to_camera_2_frame_transform.get_minor<3, 3>(0, 0);
		_2T21 = cv::Mat(camera_1_to_camera_2_frame_transform.get_minor<3, 1>(0, 3));

		have_1_to_2 = true;

	}

	if (options.camera_1_to_world_transform_filename && CVCalibIO::readRigidTransform(options.camera_1_to_world_transform_filename, camera_1_to_world_frame_transform)) {

		// Compute the inverse of the transform (Must form the full 4x4 matrix to take the inverse)
		cv::Matx44d transform_sq;
		cv::vconcat(camera_1_to_world_frame_transform, cv::Matx14d(0, 0, 0, 1), transform_sq);
		world_to_camera_1_frame_transform = transform_sq.inv().get_minor<3, 4>(0, 0);

		// Extract the rotation and translation for camera_1_to_world_frame_transform
		_wR1 = camera_1_to_world_frame_transform.get_minor<3, 3>(0, 0);
		_wTw1 = cv::Mat(camera_1_to_world_frame_transform.get_minor<3, 1>(0, 3));

		have_1_to_w = true;

	}

	if (options.camera_2_to_world_transform_filename && CVCalibIO::readRigidTransform(options.camera_2_to_world_transform_filename, camera_2_to_world_frame_transform)) {

		// Compute the inverse of the transform (Must form the full 4x4 matrix to take the inverse)
		cv::Matx44d transform_sq;
		cv::vconcat(camera_2_to_world_frame_transform, cv::Matx14d(0, 0, 0, 1), transform_sq);
		world_to_camera_2_frame_transform = transform_sq.inv().get_minor<3, 4>(0, 0);

		// Extract the rotation and translation for camera_2_to_world_frame_transform
		_wR2 = camera_2_to_world_frame_transform.get_minor<3, 3>(0, 0);
		_wTw2 = cv::Mat(camera_2_to_world_frame_transform.get_minor<3, 1>(0, 3));

		have_2_to_w = true;

	}

	// Compute the third transform if 2 of 3 were provided
	if (have_1_to_2 && have_1_to_w && !have_2_to_w) {

		// Compute the missing transformation (computation is done with 4x4 transformations)
		cv::Matx44d camera_1_to_world_frame_transform_sq, camera_2_to_camera_1_frame_transform_sq;
		cv::vconcat(camera_1_to_world_frame_transform, cv::Matx14d(0, 0, 0, 1), camera_1_to_world_frame_transform_sq);
		cv::vconcat(camera_2_to_camera_1_frame_transform, cv::Matx14d(0, 0, 0, 1), camera_2_to_camera_1_frame_transform_sq);
		cv::Matx44d camera_2_to_world_frame_transform_sq = camera_1_to_world_frame_transform_sq * camera_2_to_camera_1_frame_transform_sq;
		camera_2_to_world_frame_transform = camera_2_to_world_frame_transform_sq.get_minor<3, 4>(0, 0);

		// Compute the inverse of the transform (Must use the full 4x4 matrix to take the inverse)
		world_to_camera_2_frame_transform = camera_2_to_world_frame_transform_sq.inv().get_minor<3, 4>(0, 0);

		// Extract the rotation and translation for camera_2_to_world_frame_transform
		_wR2 = camera_2_to_world_frame_transform.get_minor<3, 3>(0, 0);
		_wTw2 = cv::Mat(camera_2_to_world_frame_transform.get_minor<3, 1>(0, 3));

		have_2_to_w = true;

	}
	else if (have_1_to_2 && have_2_to_w && !have_1_to_w) {

		// Compute the missing transformation (computation is done with 4x4 transformations)
		cv::Matx44d camera_2_to_world_frame_transform_sq, camera_1_to_camera_2_frame_transform_sq;
		cv::vconcat(camera_2_to_world_frame_transform, cv::Matx14d(0, 0, 0, 1), camera_2_to_world_frame_transform_sq);
		cv::vconcat(camera_1_to_camera_2_frame_transform, cv::Matx14d(0, 0, 0, 1), camera_1_to_camera_2_frame_transform_sq);
		cv::Matx44d camera_1_to_world_frame_transform_sq = camera_2_to_world_frame_transform_sq * camera_1_to_camera_2_frame_transform_sq;
		camera_1_to_world_frame_transform = camera_1_to_world_frame_transform_sq.get_minor<3, 4>(0, 0);

		// Compute the inverse of the transform (Must use the full 4x4 matrix to take the inverse)
		world_to_camera_1_frame_transform = camera_1_to_world_frame_transform_sq.inv().get_minor<3, 4>(0, 0);

		// Extract the rotation and translation for camera_1_to_world_frame_transform
		_wR1 = camera_1_to_world_frame_transform.get_minor<3, 3>(0, 0);
		_wTw1 = cv::Mat(camera_1_to_world_frame_transform.get_minor<3, 1>(0, 3));

		have_1_to_w = true;

	}
	else if (have_1_to_w && have_2_to_w && !have_1_to_2) {

		// Compute the missing transformation (computation is done with 4x4 transformations)
		cv::Matx44d world_to_camera_2_frame_transform_sq, camera_1_to_world_frame_transform_sq;
		cv::vconcat(world_to_camera_2_frame_transform, cv::Matx14d(0, 0, 0, 1), world_to_camera_2_frame_transform_sq);
		cv::vconcat(camera_1_to_world_frame_transform, cv::Matx14d(0, 0, 0, 1), camera_1_to_world_frame_transform_sq);
		cv::Matx44d camera_1_to_camera_2_frame_transform_sq = world_to_camera_2_frame_transform_sq * camera_1_to_world_frame_transform_sq;
		camera_1_to_camera_2_frame_transform = camera_1_to_camera_2_frame_transform_sq.get_minor<3, 4>(0, 0);

		// Compute the inverse of the transform (Must use the full 4x4 matrix to take the inverse)
		camera_2_to_camera_1_frame_transform = camera_1_to_camera_2_frame_transform_sq.inv().get_minor<3, 4>(0, 0);

		// Extract the rotation and translation for camera_1_to_camera_2_frame_transform
		_2R1 = camera_1_to_camera_2_frame_transform.get_minor<3, 3>(0, 0);
		_2T21 = cv::Mat(camera_1_to_camera_2_frame_transform.get_minor<3, 1>(0, 3));

		have_1_to_2 = true;

	}

}


CameraPair::~CameraPair() {

	stop();

}



// FIXME: Need to figure out why the camera pair frame rate sometimes seems to oscillate between the desired, and some very low value.
//        I think maybe what could be happening is the two cameras are not running at exactly the same rate and so they get
//        out of sync maybe?? Need to investigate this
/**
 * @brief This function gets connected to the Camera::newFrame() signal of camera 1. Each time a new frame is received, it
 * checks to see if a frame from camera 2 has recently arrived (specifically if it has arrived within half the time
 * between frames according to the current frame rate). If so, it emits both frames as a pair. If not, it saves the
 * frame so receiveCamera2Frame() can emit the pair.
 * @param frame The new frame from camera 1
 * @param cameraROIPosition The corresponding postion of the camera ROI
 * @param timestamp The timestamp in milliseconds and registered with the system time
 */
void CameraPair::receiveCamera1Frame(const cv::Mat& frame, const cv::Point& cameraROIPosition, double timestamp) {

	mutex.lock();

	if (timestamp - halfFrameInterval < lastReceivedCamera2Timestamp) {

		if (lastFramePairTime > 0)
			actualFPS.update(1000.0 / (timestamp - lastFramePairTime));
		lastFramePairTime = timestamp;

		emit newFrames(frame, lastReceivedCamera2Frame, cameraROIPosition, lastReceivedCamera2ROIPosition);

	}

	lastReceivedCamera1Frame = frame;
	lastReceivedCamera1ROIPosition = cameraROIPosition;
	lastReceivedCamera1Timestamp = timestamp;

	mutex.unlock();

}


/**
 * @brief This function gets connected to the Camera::newFrame() signal of camera 2. Each time a new frame is received, it
 * checks to see if a frame from camera 1 has recently arrived (specifically if it has arrived within half the time
 * between frames according to the current frame rate). If so, it emits both frames as a pair. If not, it saves the
 * frame so receiveCamera1Frame() can emit the pair.
 * @param frame The new frame from camera 2
 * @param cameraROIPosition The corresponding postion of the camera ROI
 * @param timestamp The timestamp in milliseconds and registered with the system time
 */
void CameraPair::receiveCamera2Frame(const cv::Mat& frame, const cv::Point& cameraROIPosition, double timestamp) {

	mutex.lock();

	if (timestamp - halfFrameInterval < lastReceivedCamera1Timestamp) {

		if (lastFramePairTime > 0)
			actualFPS.update(1000.0 / (timestamp - lastFramePairTime));
		lastFramePairTime = timestamp;

		emit newFrames(lastReceivedCamera1Frame, frame, lastReceivedCamera1ROIPosition, cameraROIPosition);

	}

	lastReceivedCamera2Frame = frame;
	lastReceivedCamera2ROIPosition = cameraROIPosition;
	lastReceivedCamera2Timestamp = timestamp;

	mutex.unlock();

}


/**
 * @brief Starts the camera pair at the desired frame rate. The function uses Camera::startVideoStream and as such is subject
 * to the same rules and behavior as that function. Both cameras must run at the same frame rate.
 * @param desiredFrameRate The desired frame rate of the camera pair
 * @return
 */
bool CameraPair::start(float desiredFrameRate) {

	if (!camera_1.startVideoStream(desiredFrameRate))
		return false;
	if (!camera_2.startVideoStream(desiredFrameRate)) {
		camera_1.stopVideoStream();
		return false;
	}

	// Reset the frame timestamps
	lastReceivedCamera1Timestamp = -1;
	lastReceivedCamera2Timestamp = -1;

	// Store half the frame interval
	halfFrameInterval = (1000.0 / desiredFrameRate) / 2;

	// Reset actual FPS calculation
	actualFPS.seed(desiredFrameRate);
	lastFramePairTime = -1;

	// Connect the new frames from each camera
	connect(&camera_1, &Camera::newFrame, this, &CameraPair::receiveCamera1Frame, Qt::DirectConnection);
	connect(&camera_2, &Camera::newFrame, this, &CameraPair::receiveCamera2Frame, Qt::DirectConnection);

	return true;

}


/**
 * @brief Starts the camera pair at the desired frame rate and with the desired camera ROIs. The function uses
 * Camera::startVideoStream and as such is subject to the same rules and behavior as that function. Both cameras
 * must run at the same frame rate but may have different ROI settings.
 * @param desiredFrameRate The desired frame rate of the camera pair
 * @param ROI_cam_1 Desired ROI for camera 1
 * @param ROI_cam_2 Desired ROI for camera 2
 * @return
 */
bool CameraPair::start(float desiredFrameRate, cv::Rect& ROI_cam_1, cv::Rect& ROI_cam_2) {

	if (!camera_1.startVideoStream(desiredFrameRate, ROI_cam_1))
		return false;
	if (!camera_2.startVideoStream(desiredFrameRate, ROI_cam_2)) {
		camera_1.stopVideoStream();
		return false;
	}

	// Reset the frame timestamps
	lastReceivedCamera1Timestamp = -1;
	lastReceivedCamera2Timestamp = -1;

	// Store half the frame interval
	halfFrameInterval = (1000.0 / desiredFrameRate) / 2;

	// Reset actual FPS calculation
	actualFPS.seed(desiredFrameRate);
	lastFramePairTime = -1;

	// Connect the Camera::newFrame() signal from each camera, use a direct connection
	connect(&camera_1, &Camera::newFrame, this, &CameraPair::receiveCamera1Frame, Qt::DirectConnection);
	connect(&camera_2, &Camera::newFrame, this, &CameraPair::receiveCamera2Frame, Qt::DirectConnection);

	return true;

}


/**
 * @brief Stops the camera pair.
 * @return False if unsuccessful
 */
bool CameraPair::stop() {

	// Disconnect the Camera::newFrame() signal from each camera
	disconnect(&camera_1, &Camera::newFrame, this, &CameraPair::receiveCamera1Frame);
	disconnect(&camera_2, &Camera::newFrame, this, &CameraPair::receiveCamera2Frame);

	if (!camera_1.stopVideoStream())
		return false;
	if (!camera_2.stopVideoStream())
		return false;

	return true;

}


/**
 * @brief Returns true if the camera pair is currently started (streaming video).
 * @return
 */
bool CameraPair::isVideoStreaming() const {

	return camera_1.isVideoStreaming() && camera_2.isVideoStreaming();

}


/**
 * @brief Returns true if the camera pair is currently in an error state.
 * @return
 */
bool CameraPair::isInError() const {

	return camera_1.isInError() || camera_2.isInError();

}


/**
 * @brief Grabs a copy of the most recent frame from each camera. This function just calls Camera::getFrame()
 * on each camera and therefore any rules or behavior of that function apply.
 * @param frame_cam_1 cv::Mat object into which the most recent frame from camera 1 will be copied
 * @param frame_cam_2 cv::Mat object into which the most recent frame from camera 2 will be copied
 * @return True if successful, false otherwise
 */
bool CameraPair::grabFrames(cv::Mat& frame_cam_1, cv::Mat& frame_cam_2) const {

	if (!camera_1.getFrame(frame_cam_1) || !camera_2.getFrame(frame_cam_2))
		return false;

	return true;

}


/**
 * @brief Grabs a copy of the most recent frame from each camera as well as each respective camera ROI. If you need
 * both the frames and the camera ROIs, this function should be used instead of using grabFrames() and getROIs()
 * separately. This function just calls Camera::getFrame() on each camera and therefore any rules or behavior of
 * that function apply.
 * @param frame_cam_1 cv::Mat object into which the most recent frame from camera 1 will be copied
 * @param frame_cam_2 cv::Mat object into which the most recent frame from camera 2 will be copied
 * @param ROI_cam_1 Corresponding camera ROI for camera 1
 * @param ROI_cam_2 Corresponding camera ROI for camera 2
 * @return True if successful, false otherwise
 */
bool CameraPair::grabFrames(cv::Mat& frame_cam_1, cv::Mat& frame_cam_2, cv::Rect& ROI_cam_1, cv::Rect& ROI_cam_2) const {

	if (!camera_1.getFrame(frame_cam_1, ROI_cam_1) || !camera_2.getFrame(frame_cam_2, ROI_cam_2))
		return false;

	return true;

}


/**
 * @brief Puts the current camera ROI for each camera into the parameters of the function. This function just calls
 * Camera::getROI() on each camera and therefore any rules or behavior of that function apply.
 * @param ROI_cam_1 Variable into which the camera ROI for camera 1 is placed
 * @param ROI_cam_2 Variable into which the camera ROI for camera 2 is placed
 */
void CameraPair::getROIs(cv::Rect& ROI_cam_1, cv::Rect& ROI_cam_2) const {

	ROI_cam_1 = camera_1.getROI();
	ROI_cam_2 = camera_2.getROI();

}


/**
 * @brief Puts the maximum camera ROI size for each camera into the parameters of the function. This function just
 * calls Camera::getMaxROISize() on each camera and therefore any rules or behavior of that function apply.
 * @param ROI_cam_1 Variable into which the maximum camera ROI size for camera 1 is placed
 * @param ROI_cam_2 Variable into which the maximum camera ROI size for camera 2 is placed
 */
void CameraPair::getMaxROISizes(cv::Size& ROI_cam_1, cv::Size& ROI_cam_2) const {

	ROI_cam_1 = camera_1.getMaxROISize();
	ROI_cam_2 = camera_2.getMaxROISize();

}


/**
 * @brief Puts the minimum camera ROI size for each camera into the parameters of the function. This function just
 * calls Camera::getMinROISize() on each camera and therefore any rules or behavior of that function apply.
 * @param ROI_cam_1 Variable into which the minimum camera ROI size for camera 1 is placed
 * @param ROI_cam_2 Variable into which the minimum camera ROI size for camera 2 is placed
 */
void CameraPair::getMinROISizes(cv::Size& ROI_cam_1, cv::Size& ROI_cam_2) const {

	ROI_cam_1 = camera_1.getMinROISize();
	ROI_cam_2 = camera_2.getMinROISize();

}


/**
 * @brief Sets the ROI for each camera. This function just calls Camera::setROI() on each camera and therefore any rules
 * or behavior of that function apply. The most relevant of these rules are that the parameters will be adjusted based
 * on restrictions defined by each camera and the function cannot be used when the camera pair is currently started.
 * @param ROI_cam_1 Desired ROI for camera 1, potentially modified by the function call to reflect the ROI that actually
 * takes effect
 * @param ROI_cam_2 Desired ROI for camera 2, potentially modified by the function call to reflect the ROI that actually
 * takes effect
 * @return True if successful
 */
bool CameraPair::setROIs(cv::Rect& ROI_cam_1, cv::Rect& ROI_cam_2) {

	return camera_1.setROI(ROI_cam_1) && camera_2.setROI(ROI_cam_2);

}


/**
 * @brief Sets the ROI for each camera to their respective maximum value. This function just calls Camera::setROIToMax()
 * on each camera and therefore any rules or behavior of that function apply.
 * @return True if successful
 */
bool CameraPair::setROIsToMax() {

	return camera_1.setROIToMax() && camera_2.setROIToMax();

}


/**
 * @brief Returns the maximum frame rate of the camera pair in frames per second, where the maximum frame rate is the smaller
 * of the maximum frame rate for each camera. This function uses Camera::getMaxFPS() on each camera and therefore any rules
 * or behavior of that function apply. The most notable rule is that the maximum frame rate is dependent on the current
 * settings of each camera and may not be equal to the absolute maximum frame rate the cameras are capable of. Note that if
 * this function returns a number that is smaller than the result of getMinFPS() then the cameras do not have overlapping
 * frame rate ranges and therefore are not compatible as a camera pair (e.g. you will never be able to start the camera pair).
 * @return The maximum frame rate of the camera pair based on current camera settings
 */
float CameraPair::getMaxFPS() const {

	float maxFrameRateCam1 = camera_1.getMaxFPS();
	float maxFrameRateCam2 = camera_2.getMaxFPS();

	return std::min(maxFrameRateCam1, maxFrameRateCam2);

}


/**
 * @brief Returns the absolute maximum frame rate of the camera pair in frames per second, where the absolute maximum frame
 * rate is the smaller of the absolute maximum frame rate for each camera. This function uses Camera::getAbsoluteMaxFPS()
 * on each camera and therefore any rules or behavior of that function apply. This function will always return the same
 * number regardless of the current camera settings. Note that if this function returns a number that is smaller than the
 * result of getMinFPS() then the cameras do not have overlapping frame rate ranges and therefore are not compatible as a
 * camera pair (e.g. you will never be able to start the camera pair).
 * @return The absolute maximum frame rate of the camera pair
 */
float CameraPair::getAbsoluteMaxFPS() const {

	float absMaxFrameRateCam1 = camera_1.getAbsoluteMaxFPS();
	float absMaxFrameRateCam2 = camera_2.getAbsoluteMaxFPS();

	return std::min(absMaxFrameRateCam1, absMaxFrameRateCam2);

}


/**
 * @brief Returns the minimum frame rate of the camera pair in frames per second, where the minimum frame rate is the larger
 * of the minimum frame rate for each camera. Note that if this function returns a number that is larger than the result of
 * getMaxFPS() then the cameras do not have overlapping frame rate ranges and therefore are not compatible as a camera pair
 * (e.g. you will never be able to start the camera pair).
 * @return The minimum frame rate of the camera pair
 */
float CameraPair::getMinFPS() const {

	float minFrameRateCam1 = camera_1.getMinFPS();
	float minFrameRateCam2 = camera_2.getMinFPS();

	return std::max(minFrameRateCam1, minFrameRateCam2);

}


/**
 * @brief Returns the actual frame rate of the camera pair. The actual frame rate of the camera pair is defined as the rate
 * at which the newFrames() signal is emitted and does not necessarily match the actual frame rate of either camera
 * individually. See documentation for Camera::newFrame() for a discussion of why this may occur. This function may be called
 * any time. If the camera pair is not started, it will return 0.
 * @return The actual frame rate of the camera pair or 0 if the camera pair is not started
 */
float CameraPair::getActualFPS() const {

	if (isVideoStreaming())
		return actualFPS.value();
	else
		return 0.0f;

}


/**
 * @brief Returns true if the CameraPair has been given a valid pair of intrinsic calibration files.
 * @return
 */
bool CameraPair::isPairCalibrated() const {

	return isCalibrated;

}


/**
 * @brief Returns true if the CameraPair has been given a valid camera 1 to camera 2 transformation file.
 * @return
 */
bool CameraPair::hasCamera1ToCamera2FrameTransform() const {

	return have_1_to_2;

}


/**
 * @brief Returns true if the CameraPair has been given a valid camera 1 to world frame transformation file.
 * @return
 */
bool CameraPair::hasCamera1ToWorldFrameTransform() const {

	return have_1_to_w;

}


/**
 * @brief Returns true if the CameraPair has been given a valid camera 2 to world frame transformation file.
 * @return
 */
bool CameraPair::hasCamera2ToWorldFrameTransform() const {

	return have_2_to_w;

}


/**
 * @brief Returns true of the CameraPair has been given a valid set of transformation files (i.e. at least two
 * of three).
 * @return
 */
bool CameraPair::isPairLocalized() const {

	return have_1_to_2 && have_1_to_w && have_2_to_w;

}


/**
 * @brief Returns a reference to the camera matrix for camera 1 which is useful in computer vision tracking algorithms.
 * The value returned by this function is only valid if isPairCalibrated() returns true. See OpenCV documentation for
 * the `calib3d` module for more information.
 * @return
 */
const cv::Matx33d& CameraPair::cameraMatrix1() const {

	return camera_matrix_1;

}


/**
 * @brief Returns a reference to the camera matrix for camera 2 which is useful in computer vision tracking algorithms.
 * The value returned by this function is only valid if isPairCalibrated() returns true. See OpenCV documentation for
 * the `calib3d` module for more information.
 * @return
 */
const cv::Matx33d& CameraPair::cameraMatrix2() const {

	return camera_matrix_2;

}


/**
 * @brief Returns a camera matrix for camera 1 that has been adjusted for the ROI given in the parameter. This is useful
 * for tasks such as removing distortion from an image that is an ROI of the full frame. Specifically, this function
 * adjusts the principle point in the camera matrix by subtracting the position of the ROI. The value returned by this
 * function is only valid if isPairCalibrated() returns true.
 * See https://stackoverflow.com/questions/22437737/opencv-camera-calibration-of-an-image-crop-roi-submatrix and OpenCV
 * documentation for the `calib3d` module for more information.
 * @param roi ROI of the frame
 * @return
 */
cv::Matx33d CameraPair::ROIAdjustedCameraMatrix1(const cv::Rect& roi) const {

	cv::Matx33d adjustedCameraMatrix = camera_matrix_1;

	adjustedCameraMatrix(0, 2) = camera_matrix_1(0, 2) - roi.x; // cx
	adjustedCameraMatrix(1, 2) = camera_matrix_1(1, 2) - roi.y; // cy

	return adjustedCameraMatrix;

}


/**
 * @brief Returns a camera matrix for camera 2 that has been adjusted for the ROI given in the parameter. This is useful
 * for tasks such as removing distortion from an image that is an ROI of the full frame. Specifically, this function
 * adjusts the principle point in the camera matrix by subtracting the position of the ROI. The value returned by this
 * function is only valid if isPairCalibrated() returns true.
 * See https://stackoverflow.com/questions/22437737/opencv-camera-calibration-of-an-image-crop-roi-submatrix and OpenCV
 * documentation for the `calib3d` module for more information.
 * @param roi ROI of the frame
 * @return
 */
cv::Matx33d CameraPair::ROIAdjustedCameraMatrix2(const cv::Rect& roi) const {

	cv::Matx33d adjustedCameraMatrix = camera_matrix_2;

	adjustedCameraMatrix(0, 2) = camera_matrix_2(0, 2) - roi.x; // cx
	adjustedCameraMatrix(1, 2) = camera_matrix_2(1, 2) - roi.y; // cy

	return adjustedCameraMatrix;

}


double CameraPair::fx1() const {

	return camera_matrix_1(0,0);

}


double CameraPair::fy1() const {

	return camera_matrix_1(1,1);

}


double CameraPair::cx1() const {

	return camera_matrix_1(0,2);

}


double CameraPair::cy1() const {

	return camera_matrix_1(1,2);

}


double CameraPair::fx2() const {

	return camera_matrix_2(0,0);

}


double CameraPair::fy2() const {

	return camera_matrix_2(1,1);

}


double CameraPair::cx2() const {

	return camera_matrix_2(0,2);

}


double CameraPair::cy2() const {

	return camera_matrix_2(1,2);

}


/**
 * @brief Returns a reference to the distortion parameters for camera 1 which is useful in computer vision tracking algorithms.
 * The value returned by this function is only valid if isPairCalibrated() returns true. See OpenCV documentation for the
 * `calib3d` module for more information.
 * @return
 */
const cv::Mat& CameraPair::distortionParams1() const {

	return distortion_params_1;

}


/**
 * @brief Returns a reference to the distortion parameters for camera 2 which is useful in computer vision tracking algorithms.
 * The value returned by this function is only valid if isPairCalibrated() returns true. See OpenCV documentation for the
 * `calib3d` module for more information.
 * @return
 */
const cv::Mat& CameraPair::distortionParams2() const {

	return distortion_params_2;

}


/**
 * @brief Returns a reference to the orientation matrix which represents the orientation of camera 1 expressed in the camera
 * 2 frame (i.e. the rotation from the camera 1 frame to the camera 2 frame). This is equivalent to the first three columns of
 * the result of camera1ToCamera2FrameTransform() and is useful in computer vision tracking algorithms. The value returned by
 * this function is only valid if hasCamera1ToCamera2FrameTransform() returns true. See OpenCV documentation for the `calib3d`
 * module for more information.
 * @return
 */
const cv::Matx33d& CameraPair::get_2R1() const {

	return _2R1;

}


/**
 * @brief Returns a reference to the orientation matrix which represents the orientation of camera 1 expressed in the world
 * frame (i.e. the rotation from the camera 1 frame to the world frame). This is equivalent to the first three columns of
 * the result of camera1ToWorldFrameTransform() and is useful in computer vision tracking algorithms. The value returned by
 * this function is only valid if hasCamera1ToWorldFrameTransform() returns true. See OpenCV documentation for the `calib3d`
 * module for more information.
 * @return
 */
const cv::Matx33d& CameraPair::get_wR1() const {

	return _wR1;

}


/**
 * @brief Returns a reference to the orientation matrix which represents the orientation of camera 2 expressed in the world
 * frame (i.e. the rotation from the camera 2 frame to the world frame). This is equivalent to the first three columns of
 * the result of camera2ToWorldFrameTransform() and is useful in computer vision tracking algorithms. The value returned by
 * this function is only valid if hasCamera2ToWorldFrameTransform() returns true. See OpenCV documentation for the `calib3d`
 * module for more information.
 * @return
 */
const cv::Matx33d& CameraPair::get_wR2() const {

	return _wR2;

}


/**
 * @brief Returns a reference to the position of the camera 1 origin relative to the camera 2 origin expressed in the camera 2
 * frame. This is equivalent to the last column of the result of camera1ToCamera2FrameTransform() and is useful in computer
 * vision tracking algorithms. The value returned by this function is only valid if hasCamera1ToCamera2FrameTransform() returns
 * true. See OpenCV documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Vec3d& CameraPair::get_2T21() const {

	return _2T21;

}


/**
 * @brief Returns a reference to the position of the camera 1 origin relative to the world origin expressed in the world frame.
 * This is equivalent to the last column of the result of camera1ToWorldFrameTransform() and is useful in computer vision
 * tracking algorithms. The value returned by this function is only valid if hasCamera1ToWorldFrameTransform() returns true. See
 * OpenCV documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Vec3d& CameraPair::get_wTw1() const {

	return _wTw1;

}


/**
 * @brief Returns a reference to the position of the camera 2 origin relative to the world origin expressed in the world frame.
 * This is equivalent to the last column of the result of camera2ToWorldFrameTransform() and is useful in computer vision
 * tracking algorithms. The value returned by this function is only valid if hasCamera2ToWorldFrameTransform() returns true. See
 * OpenCV documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Vec3d& CameraPair::get_wTw2() const {

	return _wTw2;

}


/**
 * @brief Returns a reference to the 3x4 transformation matrix (i.e. the top three rows of the standard 4x4 transformation matrix)
 * which transforms coordinates in the camera 1 frame to coordinates in the camera 2 frame. This is useful in computer vision tracking
 * algorithms. The value returned by this function is only valid if hasCamera1ToCamera2FrameTransform() returns true. See OpenCV
 * documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Matx34d& CameraPair::camera1ToCamera2FrameTransform() const {

	return camera_1_to_camera_2_frame_transform;

}


/**
 * @brief Returns a reference to the 3x4 transformation matrix (i.e. the top three rows of the standard 4x4 transformation matrix)
 * which transforms coordinates in the camera 2 frame to coordinates in the camera 1 frame. This is useful in computer vision tracking
 * algorithms. The value returned by this function is only valid if hasCamera1ToCamera2FrameTransform() returns true. See OpenCV
 * documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Matx34d& CameraPair::camera2ToCamera1FrameTransform() const {

	return camera_2_to_camera_1_frame_transform;

}


/**
 * @brief Returns a reference to the 3x4 transformation matrix (i.e. the top three rows of the standard 4x4 transformation matrix)
 * which transforms coordinates in the camera 1 frame to coordinates in the world frame. This is useful in computer vision tracking
 * algorithms. The value returned by this function is only valid if hasCamera1ToWorldFrameTransform() returns true. See OpenCV
 * documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Matx34d& CameraPair::camera1ToWorldFrameTransform() const {

	return camera_1_to_world_frame_transform;

}


/**
 * @brief Returns a reference to the 3x4 transformation matrix (i.e. the top three rows of the standard 4x4 transformation matrix)
 * which transforms coordinates in the world frame to coordinates in the camera 1 frame. This is useful in computer vision tracking
 * algorithms. The value returned by this function is only valid if hasCamera1ToWorldFrameTransform() returns true. See OpenCV
 * documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Matx34d& CameraPair::worldToCamera1FrameTransform() const {

	return world_to_camera_2_frame_transform;

}


/**
 * @brief Returns a reference to the 3x4 transformation matrix (i.e. the top three rows of the standard 4x4 transformation matrix)
 * which transforms coordinates in the camera 2 frame to coordinates in the world frame. This is useful in computer vision tracking
 * algorithms. The value returned by this function is only valid if hasCamera2ToWorldFrameTransform() returns true. See OpenCV
 * documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Matx34d& CameraPair::camera2ToWorldFrameTransform() const {

	return camera_2_to_world_frame_transform;

}


/**
 * @brief Returns a reference to the 3x4 transformation matrix (i.e. the top three rows of the standard 4x4 transformation matrix)
 * which transforms coordinates in the world frame to coordinates in the camera 2 frame. This is useful in computer vision tracking
 * algorithms. The value returned by this function is only valid if hasCamera2ToWorldFrameTransform() returns true. See OpenCV
 * documentation for the `calib3d` module for more information.
 * @return
 */
const cv::Matx34d& CameraPair::worldToCamera2FrameTransform() const {

	return world_to_camera_2_frame_transform;

}
