#ifndef CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER_H
#define CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER_H

#include <vector>
#include <opencv2/core.hpp>
#include <QDir>

#include <Control/WorkspaceWaypointController.h>
#include <Robots/WorkspaceControllableRobot.h>
#include <CV/CameraPair.h>
#include <CV/ChessboardCalibrationPattern.h>
#include <RoboticsToolbox.h>


/**
 * @brief The CameraPairCalibrationRobotController class is used to compute the rigid body transformation
 * from camera 2 of a camera pair to the robot frame of a robot.
 *
 * The calibration requires that a chessboard tool be attached to a ToolableRobot that is also a
 * WorkspaceControllableRobot (e.g. the MH5Robot). This chessboard tool must have a standard black and white
 * calibration chessboard mounted on it and a corresponding Tool must be created such that the origin of the
 * tool is the actual physical location of one of the corner points of the chessboard. In other words, the
 * origin of the final frame of the tooled robot must be one of the corner points of a calibration chessboard
 * such that calling setWorkspaceState() can be used to place that origin corner in space. The controller also
 * requires the details of this calibration pattern in a ChessboardCalibrationPattern object. It is easy to
 * make the Tool object also be a ChessboardCalibrationPattern as in the ChessboardTool class so you do not
 * need two classes. This class requires a calibrated CameraPair object that has also been given a camera 1
 * to camera 2 transformation which is used for triangulation (see CameraPair for more details). This class
 * also is expected to be given to a RobotControlUI object (although this is not necessary).
 *
 * This controller is a WorkspaceWaypointController that places the chessboard tool in a series of known
 * locations (using the WorkspaceControllableRobot) and takes a pair of pictures at each location using the
 * calibrated camera pair. A 3D point in the camera 2 frame is triangulated from each image pair taken at
 * each location in order to form two point clouds: one of the locations of the origin of the chessboard in
 * the robot's base frame and another of the same points expressed in the camera 2 frame. These two point
 * clouds are used to find the rigid body transformation from the camera 2 frame to the robot base frame. If
 * the chessboard cannot be found by one or both of the cameras at a particular waypoint, the controller will
 * attempt again. After several attempts, the waypoint will simply not be included in the calibration and the
 * controller will move on to the next waypoint.
 *
 * The waypoints in the cloud are controlled via variables in the Options struct given to the constructor.
 * The waypoints are a 3D grid specified by three directions, a number of steps in each direction, and the
 * step size for all steps. The grid is traversed by raster scanning the first two directions (starting with
 * the first direction), stepping in the third direction and repeating the raster scan in the first two
 * directions. This pattern repeats until the entire grid has been traversed. The starting point of the
 * grid is the location where the chessboard pattern resides at the moment the reset() function is called
 * (the reset() function must be called before starting the controller) so make sure the robot starts in
 * a position where it will not collide with the safety limits while traversing the grid. The directions
 * must be linearly independent and it is recommended that they are orthogonal and correspond to the
 * x-y-z axes. The number of steps in each direction does not mean the number of "rows" or "columns" of
 * the grid in that direction. It literally means the number of steps in that direction. For example, if
 * you leave the number of steps for the third direction at 0, then the waypoint grid will be a plane in
 * the first two directions. If all of the number of steps are set at 0, the starting point of the
 * grid will be the only waypoint (rather than having no waypoints). Since the estimation of a 3D rigid
 * body transformation requires points that are not coplanar, you should not set any of the steps to be
 * 0.
 *
 * The Options struct allows for a couple other settings. You can set the orientation of the chessboard
 * tool so it can be seen from both cameras in the camera pair. You also need to specify the index of
 * the chessboard corner that corresponds to the origin of the chessboard. See the documentation for
 * the `pattern_origin_index` variable of the Options struct for details on how to determine this index.
 * Finally, you can set the directory into which all the calibration files will be placed. The resulting
 * calibration file will be in this directory and be called `camera_2_to_robot_frame_transform.dat`. This
 * file can be used with CameraPair to create a calibrated and localized CameraPair object.
 */
class CameraPairCalibrationRobotController : public WorkspaceWaypointController<Robo::Pose6DOF> {

	Q_OBJECT

	public:

		/**
		 * @brief The Options struct is used to set various options for the CameraPairCalibrationRobotController.
		 * See the documentation for each member variable for details of each setting.
		 */
		struct Options {

			Options() {}

			/**
			 * @brief This is the directory that CameraPairCalibrationRobotController will use to output
			 * calibration files, images, etc. The default value is "Output/". Note that it is a QDir and
			 * therefore you must use forward slashes for the directory separator on every platform.
			 */
			QDir outputDirectory = QDir("Output");

			/**
			 * @brief This variable sets the translation speed in mm/s between samples (waypoints). The default
			 * value is 10 mm/s.
			 */
			double translation_speed = 10.0;

			/**
			 * @brief This variable contains the desired fixed orientation of the chessboard tool attached to
			 * the provided WorkspaceControllableRobot expressed in the robot base frame. Set this variable
			 * such that the chessboard can be seen by both cameras in the camera pair.
			 */
			Robo::Matrixd33 orientation = Robo::I33;

			/**
			 * @brief This variable is a unit vector representing the first direction of motion in the grid
			 * expressed in the robot base frame. It must be linearly independent from `dir2` and `dir3`.
			 * Defaults to (1, 0, 0)<SUP>T</SUP>.
			 */
			Robo::Vector3d dir1 = Robo::UnitX();

			/**
			 * @brief This variable is a unit vector representing the second direction of motion in the grid
			 * expressed in the robot base frame. It must be linearly independent from `dir1` and `dir3`.
			 * Defaults to (0, 1, 0)<SUP>T</SUP>.
			 */
			Robo::Vector3d dir2 = Robo::UnitY();

			/**
			 * @brief This variable is a unit vector representing the third direction of motion in the grid
			 * expressed in the robot base frame. It must be linearly independent from `dir1` and `dir2`.
			 * Defaults to (0, 0, 1)<SUP>T</SUP>.
			 */
			Robo::Vector3d dir3 = Robo::UnitZ();

			/**
			 * @brief This variable specifies the number of steps to take in the first direction. Note that
			 * this specifies the number of steps, not number of "entries" in that direction, meaning that a
			 * value of 1 means that there will be two "entries" or "rows" in that direction. Defaults to 0.
			 */
			int dir1_steps = 0;

			/**
			 * @brief This variable specifies the number of steps to take in the second direction. Note that
			 * this specifies the number of steps, not number of "entries" in that direction, meaning that a
			 * value of 1 means that there will be two "entries" or "rows" in that direction. Defaults to 0.
			 */
			int dir2_steps = 0;

			/**
			 * @brief This variable specifies the number of steps to take in the third direction. Note that
			 * this specifies the number of steps, not number of "entries" in that direction, meaning that a
			 * value of 1 means that there will be two "entries" or "rows" in that direction. Defaults to 0.
			 */
			int dir3_steps = 0;

			/**
			 * @brief This variable specifies the size of steps taken in every direction in millimeters.
			 * Defaults to 10 mm.
			 */
			int stepSize = 10;

			/**
			 * @brief This variable is used to set the index in the list of chessboard corner points found
			 * by cv::findChessboardCorners() that corresponds to the origin of the chessboard tool. This
			 * index can be determined by running the controller for a few waypoints to get a few pictures
			 * from the controller. These pictures will have the corner points drawn on the chessboard. The
			 * 0 index corresponds to the corner with the solid blue circle drawn on it. Each subsequent
			 * corner in the list can be determined by following the lines from this first corner. The last
			 * corner in the list will have a solid green circle drawn on it. Once you have determined the
			 * index. Run the controller again with the correct index in this variable.
			 */
			unsigned long pattern_origin_index = 0;

		};

		CameraPairCalibrationRobotController(WorkspaceControllableRobot& robot, CameraPair& camera_pair, ChessboardCalibrationPattern chessboardPattern, Options options = Options());
		virtual ~CameraPairCalibrationRobotController() override = default;

		bool reset() override;

	private:
		WorkspaceControllableRobot& robot;
		CameraPair& camera_pair;
		Options opt;

		// Calibration chessboard pattern parameters
		cv::Size pattern_size;
		double pattern_step;

		// Camera calibration variables
		std::vector<cv::Point3d> camera_2_frame_3D_points;
		std::vector<cv::Point3d> robot_frame_3D_points;

		cv::Point3d triangulate(cv::Point2d pixelCam1, cv::Point2d pixelCam2);

		bool waypointReached() override;
		bool waypointsDone() override;

};

#endif // CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER_H
