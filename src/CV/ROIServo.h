#ifndef ROI_SERVO_H
#define ROI_SERVO_H

#include <QObject>

#include <opencv2/core.hpp>

#include <CV/Camera.h>
#include <CV/OpenCVQtMetaTypeDeclarations.h>


/**
 * @brief The ROIServo class is used for computer vision applications that want to use either camera or processing ROIs
 * that are adjusted in real time to follow objects being tracked by a ComputerVisionTracker.
 *
 * A camera ROI is a subregion of a camera's full frame that gets sent by a camera rather than the full frame (often in
 * order to achieve higher frame rates). A processing ROI is a subregion of a camera ROI that is the region of the frame
 * that a ComputerVisionTracker will actually see and process for computer vision tracking algorithms. Using a processing
 * ROI can drastically reduce the processing time required by computer vision tracking algorithms and therfore increase
 * tracking rates. However, in order to obtain the benefits of using a camera and/or processing ROI, it may be required
 * that the position of the ROIs (and perhaps the size of the processing ROI) be adjusted to follow the target(s) moving
 * about the full view of the cameras. This class is intended to meet that need and additionally allow for arbitrary
 * servoing algorithms if necessary.
 *
 * There are three different modes for an ROIServo:
 *
 * 1. Camera ROI Only (CAM_ROI) - This mode servos the camera ROI only and always leaves the processing ROI to cover the
 * entire camera ROI.
 * 2. Processing ROI Only (PROC_ROI) - This mode servos the processing ROI only and leaves the camera ROI to some fixed
 * ROI (not necessarily the full camera frame).
 * 3. Both ROIs (BOTH) - This mode servos both the camera and processing ROI simultaneously.
 *
 * ROIServo receives a Qt signal from ComputerVisionTracker which contains the information that ROIServo needs to
 * perform its servoing. This information is the position and size of a bounding box around the target(s) the
 * ComputerVisionTracker wants to keep in view. The position is the center of the box expressed from the top-left
 * corner of the full frame ROI.
 *
 * The ROIServo class has a default implementation which simply centers the ROIs on the position portion of the bounding
 * box from ComputerVisionTracker. This implementation will suffice for most cases. However if you need a more sophisticated
 * algorithm, this can be implemented by deriving a new class from ROIServo and overriding the computeCameraROI() and
 * computeProcessingROI() functions. It is possible to servo the position of the camera ROI, the position of the processing
 * ROI, and the size of the processing ROI. See the documentation for these functions for details of how to override them.
 *
 * The ROIServo class is designed to be used and operated in conjunction with ComputerVisionUI which is built to accept
 * any pair of ROIServo classes, handle all signal connections automatically, and start/manage the servos through the user
 * interface. You should not have to do anything other than create two instances of ROIServo and hand them to ComputerVisionUI
 * in order to use this class. See the documentation for ComputerVisionUI for more details on how to use an ROIServo in the
 * interface. Also, since the signals and slots for this class are connected to/from by ComputerVisionUI using direct connections,
 * moving instances of this class to a different thread will have no effect. The functions of this class will be run in the
 * thread that the ComputerVisionTracker is assigned to. In other words, tracking and ROI servoing always occur in the same
 * thread and this thread is the one ComputerVisionTracker is assigned to.
 */
class ROIServo : public QObject {

	Q_OBJECT

	public:
		enum Mode { PROC_ROI, CAM_ROI, BOTH };
		ROIServo() {

			// Register signal types
			qRegisterMetaType<cv::Rect>();
			qRegisterMetaType<cv::Point>();

		}
		virtual ~ROIServo() = default;

	private:
		Mode mode = PROC_ROI;
		bool started = false;
		bool engaged = false;

		Camera* camera = nullptr;

		cv::Point camera_ROI_Position;
		cv::Point processing_ROI_Position;
		cv::Size camera_ROI_Size;
		cv::Size processing_ROI_Size;

		friend class ComputerVisionUI;

	private slots:
		void performServoing(int boundingBoxCenterX, int boundingBoxCenterY, int boundingBoxWidth, int boundingBoxHeight);

	signals:
		/**
		 * @brief A Qt signal emitted to the ComputerVisionTracker whenever a new processing ROI is computed. The signal
		 * contains the processing ROI expressed with respect to the camera ROI and the corresponding camera ROI position
		 * which is used by ComputerVisionTracker to delay the processing ROI from taking effect until the frame with the
		 * corresponding camera ROI position arrives.
		 *
		 * <b>Note for implementers</b>: You do not ever need to emit this signal. The ROIServo class will emit it for you.
		 * @param new_ROI The new processing ROI expressed with respect to the camera ROI
		 * @param corresponding_camera_ROI_position The position of the camera ROI that corresponds with the new processing
		 * ROI
		 */
		void newProcessingROI(const cv::Rect& new_ROI, cv::Point corresponding_camera_ROI_position);

	protected:
		virtual void reset();
		virtual void computeCameraROI(cv::Point boundingBoxCenter, cv::Size boundingBoxSize,
									  cv::Size camera_ROI_Size, cv::Point& camera_ROI_Position);
		virtual void computeProcessingROI(cv::Point boundingBoxCenter, cv::Size boundingBoxSize,
										  cv::Size camera_ROI_Size, cv::Point camera_ROI_Position,
										  cv::Size& processing_ROI_Size, cv::Point& processing_ROI_Position);

};

#endif // ROI_SERVO_H
