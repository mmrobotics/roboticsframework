#ifndef CHESSBOARD_CALIBRATION_PATTERN_H
#define CHESSBOARD_CALIBRATION_PATTERN_H


/**
 * @brief The ChessboardCalibrationPattern struct contains the relevant parameters which describe a
 * chessboard calibration pattern. The rows and columns should refer to number of "interior" corners.
 * The square size is in millimeters. Therefore, all three member variables should be larger than 0
 * for a given pattern.
 * See OpenCV documentation for more information.
 */
struct ChessboardCalibrationPattern {

	int pattern_rows;	// Number of "interior" corners along the height
	int pattern_cols;	// Number of "interior" corners along the width
	double square_size;	// Millimeters

	ChessboardCalibrationPattern(int pattern_rows = 0, int pattern_cols = 0, double square_size = 0.0)
		: pattern_rows(pattern_rows), pattern_cols(pattern_cols), square_size(square_size) {}

};

#endif // CHESSBOARD_CALIBRATION_PATTERN_H
