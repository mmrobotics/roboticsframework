#ifndef CAMERA_PAIR_H
#define CAMERA_PAIR_H

#include <opencv2/core.hpp>

#include <QObject>
#include <QMutex>

#include <CV/Camera.h>
#include <CV/OpenCVQtMetaTypeDeclarations.h>
#include <Utilities/Filters.h>


/**
 * @brief The CameraPair class represents a pair of cameras that can be used for computer vision. It is used by other computer
 * vision framework classes such as ComputerVisionUI.
 *
 * The camera pair is made up of two Camera objects. One camera acts as camera 1 while the other acts as camera 2. There is no
 * notion of left or right camera. When constructing the CameraPair object, if you only provide the two cameras, then this class
 * acts to group and control the cameras as a logical unit. This can be used with ComputerVisionUI to simply display the feed from
 * the cameras, record video, etc. In order to use this class for computer vision applications, you must also provide the paths to
 * calibration files for the camera pair intrinsic and extrinsic calibration information.
 * The camera pair can be in one of three states:
 *
 * 1. Uncalibrated and not localized - Occurs if you only provide two Camera objects
 * 2. Calibrated and not localized - Occurs if you provide two Camera objects and an intrinsic calibration file for each camera
 * 3. Calibrated and localized - Occurs if you provide two Camera objects, intrinsic calibration files for each camera, and at least
 * two of three rigid body transformation files which together fully define the pose of each camera in the world frame.
 *
 * Each intrinsic calibration file contains the intrinsic parameters of a camera (the camera matrix and the parameters for the distortion
 * model). Each rigid body transformation file simply contains the top three rows of a 4x4 rigid body transformation matrix that transforms
 * one frame to another. See the documentation for the CVCalibIO namespace for details on the format and contents of these files as well as
 * how you can create them yourself.
 *
 * There are three rigid body transformations that are relevant in the context of a pair of cameras: the transformation from one camera to
 * the other (where is one camera relative to the other), the transformation from camera 1 to the world frame (where is camera 1 relative to
 * the world frame), and the transformation from camera 2 to the world frame (where is camera 2 relative to the world frame). If two of these
 * three transformations are specified, the third is implicitly defined as well and can easily be computed from the other two. Thus it is only
 * necessary to provide transformation files for two of the three transformations.
 *
 * All calibration files are specified by setting the members of a CameraPair::Options object to contain the path to the relevant files. The
 * options object is then passed to the constructor of this class. See the documentation for each member of CameraPair::Options to know which
 * calibration information it will specify. The intrinsic calibration files can <b>and should</b> be created using ComputerVisionUI and a
 * CameraPair object that is uncalibrated and not localized. ComputerVisionUI can also produce a camera 1 to camera 2 transformation file. See
 * the documentation for ComputerVisionUI for more details. The transformation files can simply be the identity transformation if desired (which
 * sets each camera frame to be colocated with the world frame) or you must devise a method for determining these transformations yourself (e.g.
 * by using ComputerVisionUI and/or by using a robot arm to place an object in known locations to compare the known locations to computer vision
 * results).
 *
 * You can manipulate both cameras in the camera pair through functions that mirror the Camera interface. This class also provides many
 * functions for accessing various parts and forms of calibration information that is useful for writing computer vision tracking
 * algorithms. This class also automatically connects to the Camera::newFrame() signal of each camera, matches pairs of incoming frames in time,
 * and emits the frame pairs in the newFrames() signal. This way, you can treat the camera pair as an entity from which pairs of images are
 * emitted.
 */
class CameraPair : public QObject {

	Q_OBJECT

	public:
		/**
		 * @brief The Options struct is used to set the calibration files that the CameraPair should use.
		 */
		struct Options {

			Options() {}

			/**
			 * @brief Sets the path to the file containing intrinsic calibration data for camera 1.
			 */
			const char* camera_1_intrinsics_filename = nullptr;
			/**
			 * @brief Sets the path to the file containing intrinsic calibration data for camera 2.
			 */
			const char* camera_2_intrinsics_filename = nullptr;
			/**
			 * @brief Sets the path to the file containing the transformation from camera 1 to
			 * camera 2.
			 */
			const char* camera_1_to_camera_2_transform_filename = nullptr;
			/**
			 * @brief Sets the path to the file containing the transformation from camera 1 to
			 * the world frame.
			 */
			const char* camera_1_to_world_transform_filename = nullptr;
			/**
			 * @brief Sets the path to the file containing the transformation from camera 2 to
			 * the world frame.
			 */
			const char* camera_2_to_world_transform_filename = nullptr;

		};

		CameraPair(Camera& camera_1, Camera& camera_2, Options options = Options());
		~CameraPair();

		bool start(float desiredFrameRate);
		bool start(float desiredFrameRate, cv::Rect& ROI_cam_1, cv::Rect& ROI_cam_2);
		bool stop();
		bool isVideoStreaming() const;
		bool isInError() const;

		bool grabFrames(cv::Mat& frame_cam_1, cv::Mat& frame_cam_2) const;
		bool grabFrames(cv::Mat& frame_cam_1, cv::Mat& frame_cam_2, cv::Rect& ROI_cam_1, cv::Rect& ROI_cam_2) const;

		void getROIs(cv::Rect& ROI_cam_1, cv::Rect& ROI_cam_2) const;
		void getMaxROISizes(cv::Size& ROI_cam_1, cv::Size& ROI_cam_2) const;
		void getMinROISizes(cv::Size& ROI_cam_1, cv::Size& ROI_cam_2) const;
		bool setROIs(cv::Rect& ROI_cam_1, cv::Rect& ROI_cam_2);
		bool setROIsToMax();

		float getMaxFPS() const;
		float getAbsoluteMaxFPS() const;
		float getMinFPS() const;
		float getActualFPS() const;

		// Calibration related functions
		bool isPairCalibrated() const;
		bool hasCamera1ToCamera2FrameTransform() const;
		bool hasCamera1ToWorldFrameTransform() const;
		bool hasCamera2ToWorldFrameTransform() const;
		bool isPairLocalized() const;
		const cv::Matx33d& cameraMatrix1() const;
		const cv::Matx33d& cameraMatrix2() const;
		cv::Matx33d ROIAdjustedCameraMatrix1(const cv::Rect& roi) const;
		cv::Matx33d ROIAdjustedCameraMatrix2(const cv::Rect& roi) const;
		double fx1() const;
		double fy1() const;
		double cx1() const;
		double cy1() const;
		double fx2() const;
		double fy2() const;
		double cx2() const;
		double cy2() const;
		const cv::Mat& distortionParams1() const;
		const cv::Mat& distortionParams2() const;
		const cv::Matx33d& get_2R1() const;
		const cv::Matx33d& get_wR1() const;
		const cv::Matx33d& get_wR2() const;
		const cv::Vec3d& get_2T21() const;
		const cv::Vec3d& get_wTw1() const;
		const cv::Vec3d& get_wTw2() const;
		const cv::Matx34d& camera1ToCamera2FrameTransform() const;
		const cv::Matx34d& camera2ToCamera1FrameTransform() const;
		const cv::Matx34d& camera1ToWorldFrameTransform() const;
		const cv::Matx34d& worldToCamera1FrameTransform() const;
		const cv::Matx34d& camera2ToWorldFrameTransform() const;
		const cv::Matx34d& worldToCamera2FrameTransform() const;

	signals:
		/**
		 * @brief A Qt signal that is emitted by CameraPair for each pair of frames received by the cameras. CameraPair attempts to take the
		 * frames arriving from each camera on their respective Camera::newFrame() signals and pair them in time based on the timestamps. Each
		 * time a pair of frames arrive within half the frame interval of each other, this signal emits that pair.
		 * @param frame1 A cv::Mat header which references the last received frame for camera 1. <b>DO NOT MODIFY THIS PARAMETER IN YOUR SLOTS</b>
		 * @param frame2 A cv::Mat header which references the last received frame for camera 2. <b>DO NOT MODIFY THIS PARAMETER IN YOUR SLOTS</b>
		 * @param camera1ROIPosition The corresponding camera ROI position for camera 1
		 * @param camera2ROIPosition The corresponding camera ROI position for camera 2
		 */
		void newFrames(const cv::Mat& frame1, const cv::Mat& frame2, const cv::Point& camera1ROIPosition, const cv::Point& camera2ROIPosition);

	private:
		Camera& camera_1;
		Camera& camera_2;
		bool isCalibrated = false;	// Indicates whether BOTH cameras have intrinsic calibration
		bool have_1_to_2 = false;	// Indicates whether the camera_1_to_camera_2_frame_transform has been provided
		bool have_1_to_w = false;	// Indicates whether the camera_1_to_world_frame_transform has been provided
		bool have_2_to_w = false;	// Indicates whether the camera_2_to_world_frame_transform has been provided
		bool isLocalized = false;	// Indicates whether at least 2 of the 3 frame transforms have been provided

		// Camera pair calibration variables
		cv::Matx33d camera_matrix_1, camera_matrix_2;
		cv::Mat distortion_params_1, distortion_params_2;
		cv::Matx33d _2R1;	// Orientation of camera 1 frame expressed in camera 2 frame (i.e. Rotation from camera 1 frame to camera 2 frame)
		cv::Matx33d _wR1;	// Orientation of camera 1 expressed in world frame
		cv::Matx33d _wR2;	// Orientation of camera 2 expressed in world frame
		cv::Vec3d _2T21;	// Position of camera 1 origin relative to camera 2 origin expressed in the camera 2 frame
		cv::Vec3d _wTw1;	// Position of camera 1 expressed in world frame
		cv::Vec3d _wTw2;	// Position of camera 2 expressed in world frame
		cv::Matx34d camera_1_to_camera_2_frame_transform;	// transformation from camera 1 frame to camera 2 frame
		cv::Matx34d camera_2_to_camera_1_frame_transform;	// transformation from camera 2 frame to camera 1 frame
		cv::Matx34d camera_1_to_world_frame_transform;		// transformation from camera 1 frame to world frame
		cv::Matx34d world_to_camera_1_frame_transform;		// transformation from world frame to camera 2 frame
		cv::Matx34d camera_2_to_world_frame_transform;		// transformation from camera 2 frame to world frame
		cv::Matx34d world_to_camera_2_frame_transform;		// transformation from world frame to camera 2 frame

		// Variables for syncing frames in time from each camera
		Filters::MovingAverageFilter<float, 30> actualFPS;
		double lastFramePairTime;
		double halfFrameInterval;
		cv::Mat lastReceivedCamera1Frame;
		cv::Mat lastReceivedCamera2Frame;
		cv::Point lastReceivedCamera1ROIPosition;
		cv::Point lastReceivedCamera2ROIPosition;
		double lastReceivedCamera1Timestamp;
		double lastReceivedCamera2Timestamp;
		QMutex mutex;

		friend class ComputerVisionUI;

	private slots:
		void receiveCamera1Frame(const cv::Mat& frame, const cv::Point& cameraROIPosition, double timestamp);
		void receiveCamera2Frame(const cv::Mat& frame, const cv::Point& cameraROIPosition, double timestamp);



};

#endif // CAMERA_PAIR_H
