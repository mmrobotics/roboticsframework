#ifndef CVUTILS_H
#define CVUTILS_H

#include <opencv2/core.hpp>

/**
 * The CVUtils namespace is where computer vision utility functions that do not appear in OpenCV can be found.
 */
namespace CVUtils {

	void createCenterDistanceKernel(cv::Mat& matrix);
	bool computeRigidBodyTransform(const std::vector<cv::Point3d>& pointsFrame1, const std::vector<cv::Point3d>& pointsFrame2, cv::Matx34d& transform);

}

#endif // CVUTILS_H
