#include "ComputerVisionUI.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>

#include "CVCalibrationIO.h"
#include "Utilities/DateTime.h"


////
////************************************   ComputerVisionUI   ********************************************
////

/**
 * @brief Constructs a ComputerVisionUI object.
 * @param cameraPair The CameraPair this ComputerVisionUI will use
 * @param options An optional struct containing options for the ComputerVisionUI
 * @param parent
 */
ComputerVisionUI::ComputerVisionUI(CameraPair& cameraPair, Options options, QWidget *parent)
	: QWidget(parent),
	  cameraPair(cameraPair),
	  uiOpt(options),
	  pattern_size(cv::Size(uiOpt.chessboardPattern.pattern_cols, uiOpt.chessboardPattern.pattern_rows)),
	  camera_matrix_1(cv::Matx33d::zeros()),
	  camera_matrix_2(cv::Matx33d::zeros()),
	  R(cv::Matx33d::eye()),
	  T(cv::Vec3d::all(0)) {

	// Set the title of the user interface
	setWindowTitle("Computer Vision User Interface");

	// Check that provided options are valid
	if (!uiOpt.outputDirectory.exists())
		uiOpt.outputDirectory.mkpath(".");
	if (uiOpt.updateRate <= 0)
		uiOpt.updateRate = 24.0;
	if (uiOpt.projectionIdleMaxTimeout <= 0)
		uiOpt.projectionIdleMaxTimeout = 3000;
	if (uiOpt.projectionTimeIntervalMultiplier < 0)
		uiOpt.projectionTimeIntervalMultiplier = 5;
	if (uiOpt.projectionMinZCamera1_mm <= 0)
		uiOpt.projectionMinZCamera1_mm = 50;
	if (uiOpt.projectionMinZCamera2_mm <= 0)
		uiOpt.projectionMinZCamera2_mm = 50;

	// Set up the video feed section, this section is always present
	QHBoxLayout* videoFeedLabelsLayout = new QHBoxLayout();
	videoFeedLabelsLayout->addWidget(new QLabel("<b>Camera 1</b>"), 0, Qt::AlignCenter);
	videoFeedLabelsLayout->addWidget(new QLabel("<b>Camera 2</b>"), 0, Qt::AlignCenter);
	camera_display_1 = new VideoDisplayWidget(640, 480, this);
	camera_display_2 = new VideoDisplayWidget(640, 480, this);
	QHBoxLayout* videoFeedLayout = new QHBoxLayout();
	videoFeedLayout->addWidget(camera_display_1);
	videoFeedLayout->addWidget(camera_display_2);
	QVBoxLayout* videoFeedSectionLayout = new QVBoxLayout();
	videoFeedSectionLayout->addLayout(videoFeedLabelsLayout);
	videoFeedSectionLayout->addLayout(videoFeedLayout);

	// Set up the record and still frame buttons
	record_button = new SwitcherButton("START RECORDING", "STOP RECORDING");
	still_button = new StandardButton("TAKE STILL SHOT");
	connect(record_button, &SwitcherButton::clicked, this, &ComputerVisionUI::recordButtonClicked);
	connect(this, &ComputerVisionUI::recordingSizeMismatch, this, &ComputerVisionUI::recordingSizeMismatchHandler);
	connect(still_button, &StandardButton::clicked, this, &ComputerVisionUI::executeStillShot);

	QVBoxLayout* recordingButtonLayout = new QVBoxLayout();
	recordingButtonLayout->addWidget(record_button);
	recordingButtonLayout->addWidget(still_button);
	QGroupBox* recordingButtons = new QGroupBox("Recording Controls");
	recordingButtons->setLayout(recordingButtonLayout);

	// Set up the video feed mode radio buttons of the video feed controls section
	rawVideoFeedRadioButton = new QRadioButton("Raw");
	undistortedVideoFeedRadioButton = new QRadioButton("Undistorted");
	processedVideoFeedRadioButton = new QRadioButton("Processed");
	rawVideoFeedRadioButton->setChecked(true);
	undistortedVideoFeedRadioButton->setEnabled(false); // Will be enabled when it is confirmed that undistortion is possible
	processedVideoFeedRadioButton->setEnabled(false); // Will be enabled when it is confirmed that processed feed is possible
	connect(rawVideoFeedRadioButton, &QRadioButton::clicked, this, &ComputerVisionUI::switchToRawMode);
	connect(undistortedVideoFeedRadioButton, &QRadioButton::clicked, this, &ComputerVisionUI::switchToUndistortMode);
	connect(processedVideoFeedRadioButton, &QRadioButton::clicked, this, &ComputerVisionUI::switchToProcessedMode);

	QVBoxLayout* radioButtonLayout = new QVBoxLayout();
	radioButtonLayout->addWidget(rawVideoFeedRadioButton);
	radioButtonLayout->addWidget(undistortedVideoFeedRadioButton);
	radioButtonLayout->addWidget(processedVideoFeedRadioButton);

	// Set up the update rate part of the video feed controls section
	updateRateLabel = new QLabel("0.0");
	actualCameraFrameRateLabel = new QLabel("0.0");
	desiredCameraFrameRateLabel = new QLabel("0.0");
	QLabel* desiredUpdateRateLabel = new QLabel(QStringLiteral("/ %1").arg(uiOpt.updateRate, 0, 'f', 1));
	QGridLayout* updateRateLayout = new QGridLayout();
	updateRateLayout->addWidget(new QLabel("<b>Update Rate</b>"), 0, 0, 1, 2);
	updateRateLayout->addWidget(updateRateLabel, 1, 0, Qt::AlignRight);
	updateRateLayout->addWidget(desiredUpdateRateLabel, 1, 1, Qt::AlignLeft);
	updateRateLayout->addWidget(new QLabel("<b>Camera FPS</b>"), 2, 0, 1, 2);
	updateRateLayout->addWidget(actualCameraFrameRateLabel, 3, 0, Qt::AlignRight);
	updateRateLayout->addWidget(desiredCameraFrameRateLabel, 3, 1, Qt::AlignLeft);

	// Set up the video feed section
	QHBoxLayout* videoFeedControlsLayout = new QHBoxLayout();
	videoFeedControlsLayout->addLayout(radioButtonLayout);
	videoFeedControlsLayout->addSpacing(15);
	videoFeedControlsLayout->addLayout(updateRateLayout);

	QGroupBox* videoFeedControlsSection = new QGroupBox("Video Feed Controls");
	videoFeedControlsSection->setLayout(videoFeedControlsLayout);
	videoFeedControlsSection->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);

	// Put all video related items in a layout
	QHBoxLayout* videoRelatedControlsLayout = new QHBoxLayout();
	videoRelatedControlsLayout->addWidget(recordingButtons);
	videoRelatedControlsLayout->addWidget(videoFeedControlsSection);

	// Create the controls layout
	QHBoxLayout* controlsLayout = new QHBoxLayout();
	QHBoxLayout* modeSpecificControlsLayout = new QHBoxLayout();
	controlsLayout->addLayout(videoRelatedControlsLayout);
	controlsLayout->addLayout(modeSpecificControlsLayout);

	// Set up a text output display
	outputDisplay = new QTextEdit();
	outputDisplay->setReadOnly(true);
	outputDisplay->setMinimumHeight(150);
	bottomRowLayout = new QHBoxLayout();
	bottomRowLayout->addWidget(outputDisplay);

	// Set up the overall layout
	overallLayout = new QVBoxLayout();
	overallLayout->addLayout(videoFeedSectionLayout);
	overallLayout->addLayout(controlsLayout);
	overallLayout->addLayout(bottomRowLayout);
	setLayout(overallLayout);

	// Stop the cameras during the construction of this class in case they are running right now
	cameraPair.stop();

	// Grab the minimum and maximum sizes of the ROI's for each camera in the camera pair
	cameraPair.getMaxROISizes(maxCam1ROISize, maxCam2ROISize);
	cameraPair.getMinROISizes(minCam1ROISize, minCam2ROISize);

	if (!cameraPair.isPairCalibrated()) {

		if (uiOpt.chessboardPattern.pattern_cols <= 0 || uiOpt.chessboardPattern.pattern_rows <= 0 || uiOpt.chessboardPattern.square_size <= 0.0f) {

			// The provided camera pair is not calibrated and a chessboard pattern was NOT provided. Stay in video only mode.
			displayMsg(QStringLiteral("Uncalibrated camera pair detected"));
			displayMsg(QStringLiteral("Chessboard pattern provided . . . No"));
			displayMsg(QStringLiteral("    If you intend to calibrate the provided camera pair, you must provide a ChessboardCalibrationPattern "
									  "in the ComputerVisionUI::Options parameter of the constructor. Make sure the pattern provided matches the "
									  "chessboard pattern you are using. If you provided a ChessboardCalibrationPattern, make sure the member "
									  "variables are all positive and non zero."));
			displayMsg(QStringLiteral("Entering Video Only Mode"));

			// Use a QWidget in the mode specific controls layout for nice spacing
			modeSpecificControlsLayout->addWidget(new QWidget());

		}
		else {

			// The provided camera pair is not calibrated and a chessboard pattern was provided. Go into calibration mode.
			displayMsg(QStringLiteral("Uncalibrated camera pair detected"));
			displayMsg(QStringLiteral("Chessboard pattern provided . . . Yes"));

			displayMsg(QStringLiteral("Entering Calibration Mode"));

			uiMode = CALIBRATION;

			// Make sure a directory for calibration output exists
			QDir dir(uiOpt.outputDirectory.filePath("CVUICalibrationOutput"));
			if (!dir.exists())
				dir.mkpath(".");

			// Generate chessboard "ground truth" corner points (i.e. corner coordinates in the frame of the chessboard)
			for (int row = 0; row < pattern_size.height; ++row)
				for (int col = 0; col < pattern_size.width; ++col)
						object_points.push_back(cv::Point3d(col * uiOpt.chessboardPattern.square_size, row * uiOpt.chessboardPattern.square_size, 0.0));

			// Initialize flags for cv::calibrateCamera() function
			calibrationFlags = cv::CALIB_FIX_PRINCIPAL_POINT | cv::CALIB_ZERO_TANGENT_DIST;

			// Initialize camera distortion parameters based on requested number of params and add flags if necessary to enable
			// the corresponding parameters. Distortion parameters follow the OpenCV convention and therefore only certain numbers
			// of parameters are accepted.
			switch(uiOpt.numDistortionParams_cam1) {

				case 4:
				case 5:
					distortion_params_1 = cv::Mat(uiOpt.numDistortionParams_cam1, 1, CV_64F, cv::Scalar::all(0));
					break;
				case 8:
					calibrationFlags |= cv::CALIB_RATIONAL_MODEL;
					distortion_params_1 = cv::Mat(uiOpt.numDistortionParams_cam1, 1, CV_64F, cv::Scalar::all(0));
					break;
				case 12:
					calibrationFlags |= cv::CALIB_THIN_PRISM_MODEL;
					distortion_params_1 = cv::Mat(uiOpt.numDistortionParams_cam1, 1, CV_64F, cv::Scalar::all(0));
					break;
				case 14:
					calibrationFlags |= cv::CALIB_TILTED_MODEL;
					distortion_params_1 = cv::Mat(uiOpt.numDistortionParams_cam1, 1, CV_64F, cv::Scalar::all(0));
					break;

				default:
					distortion_params_1 = cv::Mat(5, 1, CV_64F, cv::Scalar::all(0));
					displayError(QStringLiteral("Only 4, 5, 8, 12, or 14 are options for the number of distortion parameters. Setting number of distortion parameters for Camera 1 to 5. See OpenCV documentation for details."));
					break;

			}
			switch(uiOpt.numDistortionParams_cam2) {

				case 4:
				case 5:
					distortion_params_2 = cv::Mat(uiOpt.numDistortionParams_cam2, 1, CV_64F, cv::Scalar::all(0));
					break;
				case 8:
					calibrationFlags |= cv::CALIB_RATIONAL_MODEL;
					distortion_params_2 = cv::Mat(uiOpt.numDistortionParams_cam2, 1, CV_64F, cv::Scalar::all(0));
					break;
				case 12:
					calibrationFlags |= cv::CALIB_THIN_PRISM_MODEL;
					distortion_params_2 = cv::Mat(uiOpt.numDistortionParams_cam2, 1, CV_64F, cv::Scalar::all(0));
					break;
				case 14:
					calibrationFlags |= cv::CALIB_TILTED_MODEL;
					distortion_params_2 = cv::Mat(uiOpt.numDistortionParams_cam2, 1, CV_64F, cv::Scalar::all(0));
					break;

				default:
					distortion_params_2 = cv::Mat(5, 1, CV_64F, cv::Scalar::all(0));
					displayError(QStringLiteral("Only 4, 5, 8, 12, or 14 are options for the number of distortion parameters. Setting number of distortion parameters for Camera 2 to 5. See OpenCV documentation for details."));
					break;

			}

			// Set up the calibration buttons
			StandardButton* sample_button = new StandardButton("TAKE SAMPLE");
			StandardButton* intrinsics_button = new StandardButton("CALIBRATE CAMERA INTRINSICS");
			StandardButton* calib_1_to_2_button = new StandardButton("CALIBRATE 1-TO-2 TRANSFORM");
			connect(sample_button, &StandardButton::clicked, this, &ComputerVisionUI::recordSample);
			connect(intrinsics_button, &StandardButton::clicked, this, &ComputerVisionUI::performIndividualIntrinsicsCalibration);
			connect(calib_1_to_2_button, &StandardButton::clicked, this, &ComputerVisionUI::performCamera1To2TransformCalibration);

			QVBoxLayout* calibrationButtonLayout = new QVBoxLayout();
			calibrationButtonLayout->addWidget(sample_button);
			calibrationButtonLayout->addWidget(intrinsics_button);
			calibrationButtonLayout->addWidget(calib_1_to_2_button);
			QGroupBox* calibrationButtons = new QGroupBox("Calibration Controls");
			calibrationButtons->setLayout(calibrationButtonLayout);

			// Use a QWidget in the mode specific controls layout for nice spacing
			modeSpecificControlsLayout->addWidget(new QWidget());
			// Add the calibration buttons to the controls layout
			modeSpecificControlsLayout->addWidget(calibrationButtons);

			// Set up the images list
			imagesList = new QListWidget();
			imagesList->setViewMode(QListWidget::IconMode);
			imagesList->setSelectionMode(QAbstractItemView::NoSelection);
			imagesList->setIconSize(QSize(150,150));
			imagesList->setResizeMode(QListWidget::Adjust);
			imagesList->setMinimumSize(QSize(645, 250));

			bottomRowLayout->addWidget(imagesList);

			// Set the cameras to their full frame (max ROI). Camera calibration must be done on the full frame of the camera.
			displayMsg(QStringLiteral("     Setting ROI's to full camera frames (maximum ROI) for calibration . . . "));
			if (cameraPair.setROIsToMax()) {
				isCameraPairAtFullFrame = true;
				displayMsg(QStringLiteral("Done"), false);
			}
			else
				displayError(QStringLiteral("Failed!"), false);

		}

	}
	else {

		if (!cameraPair.isPairLocalized()) {

			// The provided camera pair is calibrated but not localized. Stay in video only mode but allow undistorted video feed.
			displayMsg(QStringLiteral("Calibrated camera pair detected"));
			displayMsg(QStringLiteral("Camera pair not localized"));
			displayMsg(QStringLiteral("Entering Video Only Mode"));

			// Use a QWidget in the mode specific controls layout for nice spacing
			modeSpecificControlsLayout->addWidget(new QWidget());

		}
		else {

			// The provided camera pair is calibrated and localized. Move into computer vision mode
			displayMsg(QStringLiteral("Calibrated and localized camera pair detected"));
			displayMsg(QStringLiteral("Entering Computer Vision Mode"));

			uiMode = COMPUTER_VISION;

			if (!uiOpt.cvTracker) {

				// No computer vision tracker was provided. Let the user know and don't display computer vision controls
				displayMsg(QStringLiteral("Computer vision tracker provided . . . No"));
				displayMsg(QStringLiteral("    If you intend to track objects, you must provide a ComputerVisionTracker in the ComputerVisionUI::Options "
										  "parameter of the constructor."));

				// Use a QWidget in the mode specific controls layout for nice spacing
				modeSpecificControlsLayout->addWidget(new QWidget());

			}
			else {

				// A computer vision tracker was provided. Show computer vision controls
				displayMsg(QStringLiteral("    Computer vision tracker provided . . . Yes"));

				// Set up the main computer vision controls
				frameRateSpinBox = new QDoubleSpinBox();
				frameRateSpinBox->setMaximum(cameraPair.getAbsoluteMaxFPS());
				if (uiOpt.cvFrameRate > 0.0)
					frameRateSpinBox->setValue(uiOpt.cvFrameRate);
				else
					frameRateSpinBox->setValue(uiOpt.updateRate);
				cvUpdateRateLabel = new QLabel("0.0");
				startCVButton = new SwitcherButton("START", "STOP");
				startCVButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
				connect(startCVButton, &SwitcherButton::clicked, this, &ComputerVisionUI::enableComputerVision);

				QHBoxLayout* frameRateLayout = new QHBoxLayout();
				frameRateLayout->addWidget(new QLabel("<b>Frame Rate</b>"));
				frameRateLayout->addWidget(frameRateSpinBox);
				QHBoxLayout* cvUpdateRateLayout = new QHBoxLayout();
				cvUpdateRateLayout->addWidget(new QLabel("<b>Actual Rate </b>"));
				cvUpdateRateLayout->addWidget(cvUpdateRateLabel);
				QVBoxLayout* cvControlsLayoutLeft = new QVBoxLayout();
				cvControlsLayoutLeft->addLayout(frameRateLayout);
				cvControlsLayoutLeft->addLayout(cvUpdateRateLayout);
				cvControlsLayoutLeft->addWidget(startCVButton);

				// Set up ROI Mode controls
				QRadioButton* cameraROIOnlyRadioButton = new QRadioButton("Camera ROI Only");
				QRadioButton* processingROIOnlyRadioButton = new QRadioButton("Processing ROI Only");
				QRadioButton* bothROIRadioButton = new QRadioButton("Both");
				connect(cameraROIOnlyRadioButton, &QRadioButton::clicked, this, &ComputerVisionUI::switchToCameraOnlyROIMode);
				connect(processingROIOnlyRadioButton, &QRadioButton::clicked, this, &ComputerVisionUI::switchToProcessingOnlyROIMode);
				connect(bothROIRadioButton, &QRadioButton::clicked, this, &ComputerVisionUI::switchToBothROIMode);
				QVBoxLayout* ROIModeLayout = new QVBoxLayout();
				ROIModeLayout->addWidget(cameraROIOnlyRadioButton);
				ROIModeLayout->addWidget(processingROIOnlyRadioButton);
				ROIModeLayout->addWidget(bothROIRadioButton);
				ROIModeControls = new QGroupBox("ROI Mode");
				ROIModeControls->setLayout(ROIModeLayout);
				cvControlsLayoutLeft->addWidget(ROIModeControls);

				// Set up the ROI controls
				QRadioButton* fixedROIRadioButton = new QRadioButton("Fixed ROI");
				QRadioButton* ROIServoRadioButton = new QRadioButton("ROI Servo");
				connect(fixedROIRadioButton, &QRadioButton::clicked, this, &ComputerVisionUI::switchToFixedROIType);
				connect(ROIServoRadioButton, &QRadioButton::clicked, this, &ComputerVisionUI::switchToServoROIType);
				QVBoxLayout* ROITypeControlsLayout = new QVBoxLayout();
				ROITypeControlsLayout->addWidget(fixedROIRadioButton);
				ROITypeControlsLayout->addWidget(ROIServoRadioButton);
				ROITypeControls = new QGroupBox("ROI Type");
				ROITypeControls->setLayout(ROITypeControlsLayout);
				ROIFrameRateSpinBox = new QDoubleSpinBox();
				ROIFrameRateSpinBox->setMaximum(cameraPair.getAbsoluteMaxFPS());
				if (uiOpt.ROIFrameRate > 0.0)
					ROIFrameRateSpinBox->setValue(uiOpt.ROIFrameRate);
				else if (uiOpt.cvFrameRate > 0.0)
					ROIFrameRateSpinBox->setValue(uiOpt.cvFrameRate);
				else
					ROIFrameRateSpinBox->setValue(uiOpt.updateRate);
				hideROICheckBox = new QCheckBox("Hide ROIs");
				QVBoxLayout* ROIControlsLayoutLeft = new QVBoxLayout();
				ROIControlsLayoutLeft->addWidget(new QLabel("<b>ROI Frame Rate</b>"), 0, Qt::AlignBottom);
				ROIControlsLayoutLeft->addWidget(ROIFrameRateSpinBox, 0, Qt::AlignTop);
				ROIControlsLayoutLeft->addWidget(hideROICheckBox, 0, Qt::AlignVCenter);
				ROIControlsLayoutLeft->addWidget(ROITypeControls);

				cameraROI1_x = new QSpinBox();
				cameraROI1_y = new QSpinBox();
				cameraROI1_width = new QSpinBox();
				cameraROI1_height = new QSpinBox();
				processingROI1_x = new QSpinBox();
				processingROI1_y = new QSpinBox();
				processingROI1_width = new QSpinBox();
				processingROI1_height = new QSpinBox();
				cameraROI2_x = new QSpinBox();
				cameraROI2_y = new QSpinBox();
				cameraROI2_width = new QSpinBox();
				cameraROI2_height = new QSpinBox();
				processingROI2_x = new QSpinBox();
				processingROI2_y = new QSpinBox();
				processingROI2_width = new QSpinBox();
				processingROI2_height = new QSpinBox();

				// Disable keyboard tracking to prevent valueChanged() signals from being emitted while the
				// user is typing a new number into the box
				cameraROI1_x->setKeyboardTracking(false);
				cameraROI1_y->setKeyboardTracking(false);
				cameraROI1_width->setKeyboardTracking(false);
				cameraROI1_height->setKeyboardTracking(false);
				processingROI1_x->setKeyboardTracking(false);
				processingROI1_y->setKeyboardTracking(false);
				processingROI1_width->setKeyboardTracking(false);
				processingROI1_height->setKeyboardTracking(false);
				cameraROI2_x->setKeyboardTracking(false);
				cameraROI2_y->setKeyboardTracking(false);
				cameraROI2_width->setKeyboardTracking(false);
				cameraROI2_height->setKeyboardTracking(false);
				processingROI2_x->setKeyboardTracking(false);
				processingROI2_y->setKeyboardTracking(false);
				processingROI2_width->setKeyboardTracking(false);
				processingROI2_height->setKeyboardTracking(false);

				// Connect ROI shape controls to slots
				connect(cameraROI1_x, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::cameraROI1_x_valueChanged);
				connect(cameraROI1_y, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::cameraROI1_y_valueChanged);
				connect(cameraROI2_x, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::cameraROI2_x_valueChanged);
				connect(cameraROI2_y, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::cameraROI2_y_valueChanged);
				connect(cameraROI1_width, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::cameraROI1_width_valueChanged);
				connect(cameraROI1_height, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::cameraROI1_height_valueChanged);
				connect(cameraROI2_width, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::cameraROI2_width_valueChanged);
				connect(cameraROI2_height, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::cameraROI2_height_valueChanged);
				connect(processingROI1_width, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::processingROI1_width_valueChanged);
				connect(processingROI1_height, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::processingROI1_height_valueChanged);
				connect(processingROI2_width, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::processingROI2_width_valueChanged);
				connect(processingROI2_height, QOverload<int>::of(&QSpinBox::valueChanged), this, &ComputerVisionUI::processingROI2_height_valueChanged);

				// Set the ROI shape controls step sizes
				cameraROI1_x->setSingleStep(maxCam1ROISize.width / 100);
				cameraROI1_y->setSingleStep(maxCam1ROISize.height / 100);
				cameraROI1_width->setSingleStep(maxCam1ROISize.width / 100);
				cameraROI1_height->setSingleStep(maxCam1ROISize.height / 100);
				processingROI1_x->setSingleStep(maxCam1ROISize.width / 100);
				processingROI1_y->setSingleStep(maxCam1ROISize.height / 100);
				processingROI1_width->setSingleStep(maxCam1ROISize.width / 100);
				processingROI1_height->setSingleStep(maxCam1ROISize.height / 100);
				cameraROI2_x->setSingleStep(maxCam2ROISize.width / 100);
				cameraROI2_y->setSingleStep(maxCam2ROISize.height / 100);
				cameraROI2_width->setSingleStep(maxCam2ROISize.width / 100);
				cameraROI2_height->setSingleStep(maxCam2ROISize.height / 100);
				processingROI2_x->setSingleStep(maxCam2ROISize.width / 100);
				processingROI2_y->setSingleStep(maxCam2ROISize.height / 100);
				processingROI2_width->setSingleStep(maxCam2ROISize.width / 100);
				processingROI2_height->setSingleStep(maxCam2ROISize.height / 100);

				// Set the limits of the ROI controls which never change through the life of the program
				// (The rest of the limits are set whenever any ROI setting is changed for which it is
				//  relevant)
				cameraROI1_width->setRange(minCam1ROISize.width, maxCam1ROISize.width);
				cameraROI1_height->setRange(minCam1ROISize.height, maxCam1ROISize.height);
				cameraROI2_width->setRange(minCam2ROISize.width, maxCam2ROISize.width);
				cameraROI2_height->setRange(minCam2ROISize.height, maxCam2ROISize.height);
				processingROI1_width->setMinimum(1);
				processingROI1_height->setMinimum(1);
				processingROI2_width->setMinimum(1);
				processingROI2_height->setMinimum(1);

				// Set the ROIs size and position
				resetROIs();

				QGridLayout* cameraROIControlsLayout = new QGridLayout();
				cameraROIControlsLayout->addWidget(new QLabel("<b>x</b>"), 0, 1, Qt::AlignCenter | Qt::AlignBottom);
				cameraROIControlsLayout->addWidget(new QLabel("<b>y</b>"), 0, 2, Qt::AlignCenter | Qt::AlignBottom);
				cameraROIControlsLayout->addWidget(new QLabel("<b>width</b>"), 0, 3, Qt::AlignCenter | Qt::AlignBottom);
				cameraROIControlsLayout->addWidget(new QLabel("<b>height</b>"), 0, 4, Qt::AlignCenter | Qt::AlignBottom);
				cameraROIControlsLayout->addWidget(new QLabel("<b>Camera</b>"), 1, 0);
				cameraROIControlsLayout->addWidget(cameraROI1_x, 1, 1);
				cameraROIControlsLayout->addWidget(cameraROI1_y, 1, 2);
				cameraROIControlsLayout->addWidget(cameraROI1_width, 1, 3);
				cameraROIControlsLayout->addWidget(cameraROI1_height, 1, 4);
				cameraROIControlsLayout->addWidget(new QLabel("<b>Processing</b>"), 2, 0);
				cameraROIControlsLayout->addWidget(processingROI1_x, 2, 1);
				cameraROIControlsLayout->addWidget(processingROI1_y, 2, 2);
				cameraROIControlsLayout->addWidget(processingROI1_width, 2, 3);
				cameraROIControlsLayout->addWidget(processingROI1_height, 2, 4);
				camera1ROIcontrols = new QGroupBox("Camera 1 ROI Controls");
				camera1ROIcontrols->setLayout(cameraROIControlsLayout);

				cameraROIControlsLayout = new QGridLayout();
				cameraROIControlsLayout->addWidget(new QLabel("<b>x</b>"), 0, 1, Qt::AlignCenter | Qt::AlignBottom);
				cameraROIControlsLayout->addWidget(new QLabel("<b>y</b>"), 0, 2, Qt::AlignCenter | Qt::AlignBottom);
				cameraROIControlsLayout->addWidget(new QLabel("<b>width</b>"), 0, 3, Qt::AlignCenter | Qt::AlignBottom);
				cameraROIControlsLayout->addWidget(new QLabel("<b>height</b>"), 0, 4, Qt::AlignCenter | Qt::AlignBottom);
				cameraROIControlsLayout->addWidget(new QLabel("<b>Camera</b>"), 1, 0);
				cameraROIControlsLayout->addWidget(cameraROI2_x, 1, 1);
				cameraROIControlsLayout->addWidget(cameraROI2_y, 1, 2);
				cameraROIControlsLayout->addWidget(cameraROI2_width, 1, 3);
				cameraROIControlsLayout->addWidget(cameraROI2_height, 1, 4);
				cameraROIControlsLayout->addWidget(new QLabel("<b>Processing</b>"), 2, 0);
				cameraROIControlsLayout->addWidget(processingROI2_x, 2, 1);
				cameraROIControlsLayout->addWidget(processingROI2_y, 2, 2);
				cameraROIControlsLayout->addWidget(processingROI2_width, 2, 3);
				cameraROIControlsLayout->addWidget(processingROI2_height, 2, 4);
				camera2ROIcontrols = new QGroupBox("Camera 2 ROI Controls");
				camera2ROIcontrols->setLayout(cameraROIControlsLayout);

				QHBoxLayout* ROIShapeControlsLayout = new QHBoxLayout();
				ROIShapeControlsLayout->addWidget(camera1ROIcontrols);
				ROIShapeControlsLayout->addWidget(camera2ROIcontrols);

				startROIButton = new SwitcherButton("START ROI", "STOP ROI");
				engageROIButton = new SwitcherButton("ENGAGE ROI", "DISENGAGE ROI");
				connect(startROIButton, &SwitcherButton::clicked, this, &ComputerVisionUI::startROIs);
				connect(engageROIButton, &SwitcherButton::clicked, this, &ComputerVisionUI::engageROIs);
				QHBoxLayout* ROIButtonsLayout = new QHBoxLayout();
				ROIButtonsLayout->addWidget(startROIButton);
				ROIButtonsLayout->addWidget(engageROIButton);

				QVBoxLayout* ROIControlsLayoutRight = new QVBoxLayout();
				ROIControlsLayoutRight->addLayout(ROIShapeControlsLayout);
				ROIControlsLayoutRight->addLayout(ROIButtonsLayout);

				QHBoxLayout* ROIControlsLayout = new QHBoxLayout();
				ROIControlsLayout->addLayout(ROIControlsLayoutLeft);
				ROIControlsLayout->addLayout(ROIControlsLayoutRight);
				ROIControlsLayout->setSpacing(15);
				ROIControls = new QWidget();
				ROIControls->setLayout(ROIControlsLayout);

				QHBoxLayout* computerVisionControlsLayout = new QHBoxLayout();
				computerVisionControlsLayout->addLayout(cvControlsLayoutLeft);
				computerVisionControlsLayout->addWidget(ROIControls);

				QGroupBox* computerVisionControls = new QGroupBox("Computer Vision Controls");
				computerVisionControls->setLayout(computerVisionControlsLayout);
				modeSpecificControlsLayout->addWidget(computerVisionControls);

				// Rearrange the other controls for better spacing
				QVBoxLayout* videoRelatedControlsVLayout = new QVBoxLayout();
				videoRelatedControlsVLayout->addWidget(videoFeedControlsSection);
				videoRelatedControlsVLayout->addWidget(recordingButtons);
				controlsLayout->removeItem(controlsLayout->itemAt(0));
				delete videoRelatedControlsLayout;
				controlsLayout->insertLayout(0, videoRelatedControlsVLayout);

				// Place blank widgets on the left and right of the controls layout with larger stretch factor
				// This effectively makes the controls widgets stay centered
				controlsLayout->insertWidget(0, new QWidget(), 1);
				controlsLayout->addWidget(new QWidget(), 1);

				// Set the camera pair for the computer vision tracker
				uiOpt.cvTracker->camera_pair = &cameraPair;

				// Connect the camera pair to the computer vision tracker
				connect(&cameraPair, &CameraPair::newFrames, uiOpt.cvTracker, &ComputerVisionTracker::receiveNewFrames);

				// Start in CAMERA_ONLY ROI mode by default
				cameraROIOnlyRadioButton->click();

				// Handle ROI servos if they were provided
				if (uiOpt.ROIServoCamera1 || uiOpt.ROIServoCamera2) {

					if (uiOpt.ROIServoCamera1) {

						displayMsg(QStringLiteral("    ROI servo for Camera 1 detected"));

						// Set the camera for the servo to control
						uiOpt.ROIServoCamera1->camera = &(cameraPair.camera_1);

						connect(uiOpt.cvTracker, &ComputerVisionTracker::newTargetBoundingBoxCamera1,
								uiOpt.ROIServoCamera1, &ROIServo::performServoing, Qt::DirectConnection);
						connect(uiOpt.ROIServoCamera1, &ROIServo::newProcessingROI,
								uiOpt.cvTracker, &ComputerVisionTracker::setProcessingROICamera1, Qt::DirectConnection);

					}
					else {
						displayError(QStringLiteral("    No ROI servo detected for Camera 1. Its ROI will not respond when using the servo ROI type"));
					}

					if (uiOpt.ROIServoCamera2) {

						displayMsg(QStringLiteral("    ROI servo for Camera 2 detected"));

						// Set the camera for the servo to control
						uiOpt.ROIServoCamera2->camera = &(cameraPair.camera_2);

						connect(uiOpt.cvTracker, &ComputerVisionTracker::newTargetBoundingBoxCamera2,
								uiOpt.ROIServoCamera2, &ROIServo::performServoing, Qt::DirectConnection);
						connect(uiOpt.ROIServoCamera2, &ROIServo::newProcessingROI,
								uiOpt.cvTracker, &ComputerVisionTracker::setProcessingROICamera2, Qt::DirectConnection);

					}
					else {
						displayError(QStringLiteral("    No ROI servo detected for Camera 2. Its ROI will not respond when using the servo ROI type"));
					}

					ROIServoRadioButton->click();

				}
				else {

					displayMsg("    No ROI servos detected");

					fixedROIRadioButton->click();
					ROIServoRadioButton->setEnabled(false);

				}

				camera1ROIcontrols->setEnabled(false);
				camera2ROIcontrols->setEnabled(false);
				engageROIButton->setEnabled(false);
				ROIControls->setEnabled(false);
				ROIModeControls->setEnabled(false);

			}

		}

		undistortedVideoFeedRadioButton->setEnabled(true);
		displayMsg(QStringLiteral("Undistorted video feed mode available"));

	}

	// Start the camera pair at the update rate
	startCameras(uiOpt.updateRate, true);

	// Set up update rate computation variables
	lastUpdateTime = Timing::getSysTime();
	actualUpdateRate.seed(uiOpt.updateRate);

	// Start updating the UI
	update_timer = new QTimer(this);
	update_timer->setTimerType(Qt::PreciseTimer);
	connect(update_timer, &QTimer::timeout, this, &ComputerVisionUI::update);
	update_timer->start(static_cast<int>(1000.0 / uiOpt.updateRate));

	displayMsg(QLatin1String("")); // New line

}


/**
 * @brief Adds a ProjectionSocket to the ComputerVisionUI with the given name and returns a pointer to it. If a ProjectionSocket
 * with the given name already exists or if the ComputerVisionUI is not in computer vision mode, a dummy ProjectionSocket is
 * created instead. This dummy ProjectionSocket will effectively ignore all data it receives and won't be represented in the user
 * interface in any way. A message will be printed to the console warning that a ProjectionSocket already exists with the given
 * name. This operator is meant to be used directly in QObject::connect() function calls in your main() function. This function
 * is designed such that creating the projection socket and connecting to it is meant to be done at the same time in the
 * QObject::connect() function call.
 * @param socketName A unique name for the ProjectionSocket
 * @return A pointer to the ProjectionSocket referred to by `socketName`, or a pointer to a dummy socket if a socket with the
 * specified name already exists
 */
ProjectionSocket* ComputerVisionUI::operator[](const char* socketName) {

	if (uiMode != COMPUTER_VISION || projectionSockets.contains(socketName)) {
		ProjectionSocket* dummy = new ProjectionSocket("", -1);
		dummy->setParent(this); // Set this ComputerVisionUI as the parent object so this dummy can be properly accounted for
		if (projectionSockets.contains(socketName))
			std::cerr << "COMPUTER_VISION_UI: Projection socket with name \"" << socketName << "\" already exists. Cannot create multiple projection sockets with the same name. ERROR" << std::endl;
		return dummy;
	}

	// Build and add a socket table if this is the first socket created
	if (projectionSockets.empty()) {

		// Set up the socket table
		socketTable = new QWidget();
		socketTableLayout = new QGridLayout();
		socketTableLayout->setHorizontalSpacing(20);
		socketTable->setLayout(socketTableLayout);

		// Add the table titles to the socket table
		socketTableLayout->addWidget(new QLabel("<u><b>Socket</b></u>"), 0, 0);
		socketTableLayout->addWidget(new QLabel("<u><b>ID</b></u>"), 0, 1, Qt::AlignCenter);
		socketTableLayout->addWidget(new QLabel("<u><b>Type</b></u>"), 0, 2, Qt::AlignCenter);
		socketTableLayout->addWidget(new QLabel("<u><b>Display</b></u>"), 0, 3, Qt::AlignCenter);
		socketTableLayout->addWidget(new QLabel("<u><b>Show Label</b></u>"), 0, 4, Qt::AlignCenter);
		socketTableLayout->addWidget(new QLabel("<u><b>Arrow Length</b></u>"), 0, 5, Qt::AlignCenter);

		// Put the socket table in a scroll area to prevent the table from taking too much space if a lot of sockets are created
		scrollArea = new QScrollArea();
		scrollArea->setWidget(socketTable);
		scrollArea->setWidgetResizable(true);
		scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		scrollArea->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
		scrollArea->setBackgroundRole(QPalette::Light);

		// Add the socket table scroll area to the overall layout
		QVBoxLayout* scrollAreaLayout = new QVBoxLayout();
		scrollAreaLayout->addWidget(new QLabel("Projection Socket Table"));
		scrollAreaLayout->addWidget(scrollArea, 0, Qt::AlignTop);
		scrollAreaLayout->itemAt(0)->widget()->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		bottomRowLayout->addLayout(scrollAreaLayout);

	}

	// Create a new ProjectionSocket and add it to the QHash.
	ProjectionSocket* socket = new ProjectionSocket(socketName, nextSocketID);
	socket->setParent(this); // Set this ComputerVisionUI as the parent object so this socket can be properly accounted for
	projectionSockets[socketName] = socket;

	// Increment the next socket ID. This number now corresponds to the row number for the newly created ProjectionSocket
	// in the socket table (since row 0 is the title row).
	nextSocketID++;

	// Set up a row in the socket table for the new socket
	socketTableLayout->addWidget(new QLabel(socketName), nextSocketID, 0);

	socketTableLayout->addWidget(new QLabel(QStringLiteral("%1").arg(nextSocketID - 1)), nextSocketID, 1, Qt::AlignCenter);

	QLabel* typeLabel = new QLabel("PENDING");
	socketTableLayout->addWidget(typeLabel, nextSocketID, 2, Qt::AlignCenter);
	projectionSocketTypeLabels.append(typeLabel);

	QCheckBox* displayBox = new QCheckBox();
	displayBox->setCheckState(Qt::Checked);
	connect(displayBox, &QCheckBox::stateChanged, socket, &ProjectionSocket::enableDisplay);
	socketTableLayout->addWidget(displayBox, nextSocketID, 3, Qt::AlignCenter);

	QCheckBox* showLabelBox = new QCheckBox();
	showLabelBox->setCheckState(Qt::Checked);
	connect(showLabelBox, &QCheckBox::stateChanged, socket, &ProjectionSocket::enableLabel);
	socketTableLayout->addWidget(showLabelBox, nextSocketID, 4, Qt::AlignCenter);

	QSlider* arrowLengthSlider = new QSlider(Qt::Horizontal);
	arrowLengthSlider->setMinimumWidth(150);
	arrowLengthSlider->setMinimum(10);
	arrowLengthSlider->setMaximum(200);
	arrowLengthSlider->setValue(socket->arrowLength);
	connect(arrowLengthSlider, &QSlider::sliderMoved, socket, &ProjectionSocket::changeArrowLength);
	socketTableLayout->addWidget(arrowLengthSlider, nextSocketID, 5, Qt::AlignCenter);

	// Need to update the socket table scroll area minimum width because the QScrollArea has a bug that causes the
	// vertical scrollbar to be drawn on top of the right side of the main widget when the widget is at its smallest
	// size.
	scrollArea->setMinimumWidth(socketTable->minimumSizeHint().width() + scrollArea->verticalScrollBar()->width() - 85);

	return socket;

}


void ComputerVisionUI::closeEvent(QCloseEvent* event) {

	// "Click" the record button if recording is currently enabled
	if (record_button->getState())
		record_button->click();

	event->accept();

}


/**
 * @brief This function enables keyboard functionality for this widget. It will call recordSample() when
 * the Enter or Return is pressed. Note that Qt will execute the last button pressed when you press the
 * space bar too.
 * @param event
 */
void ComputerVisionUI::keyPressEvent(QKeyEvent *event) {

	int k = event->key();

	if (uiMode == CALIBRATION && (k == Qt::Key_Enter || k == Qt::Key_Return))
		recordSample();
	else
		QWidget::keyPressEvent(event); // Pass key events that are not handled to the base class handler

}


void ComputerVisionUI::displayMsg(const QString& text, bool newLine) {

	outputDisplay->moveCursor(QTextCursor::End);
	outputDisplay->setTextColor(Qt::black);
	if (newLine)
		outputDisplay->append(text);
	else
		outputDisplay->insertPlainText(text);

}


void ComputerVisionUI::displayError(const QString& text, bool newLine) {

	outputDisplay->moveCursor(QTextCursor::End);
	outputDisplay->setTextColor(Qt::red);
	if (newLine)
		outputDisplay->append(text);
	else
		outputDisplay->insertPlainText(text);

}


bool ComputerVisionUI::startCameras(double frameRate, bool throttle) {

	if(cameraPair.isInError()) {

		// Display a critical message that explains the cameras are in error state
		QMessageBox::critical(this, "Camera Pair Error",
							  "Cannot start camera pair because it is in the error state.",
							  QMessageBox::Ok);
		return false;

	}

	if (!cameraPair.isVideoStreaming()) {

		// Determine the frame rate at which to start the camera. If the desired frame rate is out of range, throttle it or fail based on
		// the value of the throttle variable
		float maxFrameRate = cameraPair.getMaxFPS();
		float minFrameRate = cameraPair.getMinFPS();
		if (frameRate > maxFrameRate) {

			if (throttle) {

				// Display an information message that explains the frame rate will be throttled
				QMessageBox::information(this, "Throttling Camera Pair Frame Rate",
										 QStringLiteral("Cannot set camera pair frame rate to the desired rate of %1 FPS because "
														"it is higher than the camera pair max frame rate. Throttling camera pair "
														"frame rate to its maximum value.").arg(frameRate),
										 QMessageBox::Ok);
				frameRate = maxFrameRate;

			}
			else {

				// Display a critical message that explains the cameras cannot start because the requested rate is too high
				QMessageBox::critical(this, "Desired Frame Rate Too High",
									  QStringLiteral("Cannot set camera pair frame rate to the desired rate of %1 FPS\n"
													 "Current maximum frame rate is: %2").arg(frameRate).arg(maxFrameRate),
									  QMessageBox::Ok);
				displayError(QStringLiteral("Cannot set camera pair frame rate to the desired rate of %1 FPS\n"
											"Current maximum frame rate is: %2").arg(frameRate).arg(maxFrameRate));
				return false;

			}

		}
		else if (frameRate < minFrameRate) {

			if (throttle) {

				// Display an information message that explains the frame rate will be throttled
				QMessageBox::information(this, "Throttling Camera Pair Frame Rate",
										 QStringLiteral("Cannot set camera pair frame rate to the desired rate of %1 FPS because "
														"it is lower than the camera pair min frame rate. Throttling camera pair "
														"frame rate to its minimum value.").arg(frameRate),
										 QMessageBox::Ok);
				frameRate = minFrameRate;

			}
			else {

				// Display a critical message that explains the cameras cannot start because the requested rate is too low
				QMessageBox::critical(this, "Desired Frame Rate Too Low",
									  QStringLiteral("Cannot set camera pair frame rate to the desired rate of %1 FPS\n"
													 "Current minimum frame rate is: %2").arg(frameRate).arg(minFrameRate),
									  QMessageBox::Ok);
				displayError(QStringLiteral("Cannot set camera pair frame rate to the desired rate of %1 FPS\n"
											"Current minimum frame rate is: %2").arg(frameRate).arg(minFrameRate));
				return false;

			}

		}

		displayMsg(QStringLiteral("Starting camera pair at %1 frames per second . . . ").arg(frameRate));
		if (cameraPair.start(frameRate)) {
			displayMsg(QStringLiteral("Done"), false);
			cameraPairFrameRate = frameRate;
			desiredCameraFrameRateLabel->setText(QStringLiteral("/ %1").arg(frameRate, 0, 'f', 1));
		}
		else {
			displayError(QStringLiteral("Failed!"), false);
			return false;
		}

	}

	return true;

}


void ComputerVisionUI::update() {

	// Compute and display the actual update rate
	long time = Timing::getSysTime();
	actualUpdateRate.update(1000.0 / (time - lastUpdateTime));
	lastUpdateTime = time;
	updateRateLabel->setText(QStringLiteral("%1").arg(actualUpdateRate.value(), 0, 'f', 1));
	if (actualUpdateRate.value() < uiOpt.updateRate)
		updateRateLabel->setStyleSheet("QLabel { color : red }");
	else
		updateRateLabel->setStyleSheet("QLabel { color : black }");

	// Display the actual camera pair frame rate
	float actualCameraFrameRate = cameraPair.getActualFPS();
	actualCameraFrameRateLabel->setText(QStringLiteral("%1").arg(actualCameraFrameRate, 0, 'f', 1));
	if (actualCameraFrameRate < cameraPairFrameRate)
		actualCameraFrameRateLabel->setStyleSheet("QLabel { color : red }");
	else
		actualCameraFrameRateLabel->setStyleSheet("QLabel { color : black }");

	if (uiMode == COMPUTER_VISION && uiOpt.cvTracker) {

		// Display the actual computer vision update rate
		double actualCVUpdateRate = uiOpt.cvTracker->getActualUpdateRate();
		cvUpdateRateLabel->setText(QStringLiteral("%1").arg(actualCVUpdateRate, 0, 'f', 1));
		if (startCVButton->getState() && actualCVUpdateRate < cameraPairFrameRate)
			cvUpdateRateLabel->setStyleSheet("QLabel { color : red }");
		else
			cvUpdateRateLabel->setStyleSheet("QLabel { color : black }");

		// Update the ROI positions and sizes if the ROI type is currently set to servo and the ROIs are started
		if (startROIButton->getState() && roiType == SERVO) {

			if (roiMode == CAMERA_ONLY || roiMode == BOTH) {

				if (uiOpt.ROIServoCamera1) {
					cameraROI1_x->setValue(uiOpt.ROIServoCamera1->camera_ROI_Position.x);
					cameraROI1_y->setValue(uiOpt.ROIServoCamera1->camera_ROI_Position.y);
					cameraROI1_width->setValue(uiOpt.ROIServoCamera1->camera_ROI_Size.width);
					cameraROI1_height->setValue(uiOpt.ROIServoCamera1->camera_ROI_Size.height);
				}
				if (uiOpt.ROIServoCamera2) {
					cameraROI2_x->setValue(uiOpt.ROIServoCamera2->camera_ROI_Position.x);
					cameraROI2_y->setValue(uiOpt.ROIServoCamera2->camera_ROI_Position.y);
					cameraROI2_width->setValue(uiOpt.ROIServoCamera2->camera_ROI_Size.width);
					cameraROI2_height->setValue(uiOpt.ROIServoCamera2->camera_ROI_Size.height);
				}

			}

			if (roiMode == PROCESSING_ONLY || roiMode == BOTH) {

				if (uiOpt.ROIServoCamera1) {
					processingROI1_x->setValue(uiOpt.ROIServoCamera1->processing_ROI_Position.x);
					processingROI1_y->setValue(uiOpt.ROIServoCamera1->processing_ROI_Position.y);
					processingROI1_width->setValue(uiOpt.ROIServoCamera1->processing_ROI_Size.width);
					processingROI1_height->setValue(uiOpt.ROIServoCamera1->processing_ROI_Size.height);
				}
				if (uiOpt.ROIServoCamera2) {
					processingROI2_x->setValue(uiOpt.ROIServoCamera2->processing_ROI_Position.x);
					processingROI2_y->setValue(uiOpt.ROIServoCamera2->processing_ROI_Position.y);
					processingROI2_width->setValue(uiOpt.ROIServoCamera2->processing_ROI_Size.width);
					processingROI2_height->setValue(uiOpt.ROIServoCamera2->processing_ROI_Size.height);
				}

			}

		}

	}

	// Grab images from the camera pair
	cv::Mat new_image_1, new_image_2;
	if ((feedMode == RAW || feedMode == UNDISTORTED) && cameraPair.isVideoStreaming() && cameraPair.grabFrames(new_image_1, new_image_2, ROIcam1, ROIcam2)) {

		if (feedMode == UNDISTORTED) {

			cv::Mat undistorted_image_1, undistorted_image_2;
			if (cameraPair.isPairCalibrated()) {

				// The principle point (cx, cy) must be adjusted in the camera matrix to correctly undistort the image if the ROI of
				// the frame is not the full frame ROI.
				// See https://stackoverflow.com/questions/22437737/opencv-camera-calibration-of-an-image-crop-roi-submatrix
				cv::undistort(new_image_1, undistorted_image_1, cameraPair.ROIAdjustedCameraMatrix1(ROIcam1), cameraPair.distortionParams1());
				cv::undistort(new_image_2, undistorted_image_2, cameraPair.ROIAdjustedCameraMatrix2(ROIcam2), cameraPair.distortionParams2());

			}
			else if (individualCameraIntrinsicsCalibrated) {

				// We are in calibration mode so the calibration parameters are found in the member variables, not in the CameraPair object
				cv::undistort(new_image_1, undistorted_image_1, camera_matrix_1, distortion_params_1);
				cv::undistort(new_image_2, undistorted_image_2, camera_matrix_2, distortion_params_2);

			}

			new_image_1 = undistorted_image_1;
			new_image_2 = undistorted_image_2;

		}

		// Draw the ROIs
		if (feedMode == RAW && uiMode == COMPUTER_VISION && uiOpt.cvTracker && startROIButton->getState() && !hideROICheckBox->isChecked())
			drawROIs(new_image_1, new_image_2);

		// Project projection data if any projection sockets exist.
		if (!projectionSockets.isEmpty())
			handleProjectionSockets(new_image_1, new_image_2);

		if (uiMode == COMPUTER_VISION && uiOpt.cvTracker && engageROIButton->getState() && roiType == SERVO && (roiMode == CAMERA_ONLY || roiMode == BOTH)) {

			// Initialize the new_image_when_servoing variables
			cv::Mat new_image_when_servoing_1(maxCam1ROISize, CV_8UC3, cv::Scalar(0, 0, 0));
			cv::Mat new_image_when_servoing_2(maxCam2ROISize, CV_8UC3, cv::Scalar(0, 0, 0));

			new_image_1.copyTo(new_image_when_servoing_1(ROIcam1));
			new_image_2.copyTo(new_image_when_servoing_2(ROIcam2));

			// Save the final image
			imageMutex.lock();
			current_image_1 = new_image_when_servoing_1;
			current_image_2 = new_image_when_servoing_2;
			imageMutex.unlock();

		}
		else {

			// Save the final image
			imageMutex.lock();
			current_image_1 = new_image_1;
			current_image_2 = new_image_2;
			imageMutex.unlock();

		}

		// Update the frames to the screen
		camera_display_1->updateImage(current_image_1);
		camera_display_2->updateImage(current_image_2);

		return;

	}
	else if (feedMode == PROCESSED) {

		uiOpt.cvTracker->getProcessedFrames(new_image_1, new_image_2);

		if (new_image_1.channels() == 3 && new_image_2.channels() == 3) {

			// Save the images
			imageMutex.lock();
			current_image_1 = new_image_1;
			current_image_2 = new_image_2;
			imageMutex.unlock();

			// Update the frames to the screen
			camera_display_1->updateImage(current_image_1);
			camera_display_2->updateImage(current_image_2);

			return;

		}

	}

	// Otherwise, show no image
	imageMutex.lock();
	current_image_1 = cv::Mat();
	current_image_2 = cv::Mat();
	imageMutex.unlock();

	camera_display_1->noImage();
	camera_display_2->noImage();

}


void ComputerVisionUI::handleProjectionSockets(cv::Mat& image_1, cv::Mat& image_2) {

	std::vector<ProjectionLabelInfo> projectionLabelInfo;
	for (ProjectionSocket* socket : projectionSockets) {

		// Check if the socket is idle. If so, change it to IDLE type.
		long interval = Timing::getSysTime() - socket->timeOfReceive;
		if (socket->type != ProjectionSocket::PENDING && ((interval > uiOpt.projectionIdleMaxTimeout) || (uiOpt.projectionTimeIntervalMultiplier > 0 && (interval > uiOpt.projectionTimeIntervalMultiplier * socket->receiveInterval + 1000.0 / actualUpdateRate.value()))))
				socket->type = ProjectionSocket::IDLE;

		// Update the type/status of the socket. Note the index for the corresponding type label matches the socket's ID
		switch(socket->type) {

			case ProjectionSocket::PENDING:
				projectionSocketTypeLabels[socket->ID]->setText("PENDING");
				break;
			case ProjectionSocket::IDLE:
				projectionSocketTypeLabels[socket->ID]->setText("IDLE");
				break;
			case ProjectionSocket::POSITION:
				projectionSocketTypeLabels[socket->ID]->setText("POSITION");
				break;
			case ProjectionSocket::POSE5DOF:
				projectionSocketTypeLabels[socket->ID]->setText("POSE5DOF");
				break;
			case ProjectionSocket::POSE6DOF:
				projectionSocketTypeLabels[socket->ID]->setText("POSE6DOF");
				break;

		}

		if (!socket->shouldDisplay)
			continue;

		// Get the projectable data. Note order of points received is first origin,
		// and then the position of tips of 1 or 3 vectors (heading or x,y,z axes)
		// If the socket is IDLE or PENDING, it will not return any points and we
		// continue to the next socket
		std::vector<cv::Point3d> worldPoints;
		if (!socket->getProjectableData(worldPoints))
			continue;

		// Transform all the 3D points from the world frame to each of the camera frames
		bool hasPointWithNegativeZ = false;
		std::vector<cv::Point3d> worldPointsCam1Frame;
		std::vector<cv::Point3d> worldPointsCam2Frame;
		for (unsigned long i = 0; i < worldPoints.size(); i++) {

			cv::Vec4d worldPoint;
			worldPoint << worldPoints.at(i).x, worldPoints.at(i).y, worldPoints.at(i).z, 1;
			worldPointsCam2Frame.push_back(cameraPair.worldToCamera2FrameTransform() * worldPoint);
			worldPointsCam1Frame.push_back(cameraPair.get_2R1().t() * (cv::Vec3d(worldPointsCam2Frame.at(i)) - cameraPair.get_2T21()));

			if (worldPointsCam1Frame.at(i).z <= uiOpt.projectionMinZCamera1_mm || worldPointsCam2Frame.at(i).z <= uiOpt.projectionMinZCamera1_mm)
				hasPointWithNegativeZ = true;

		}

		// If any of the points for this socket has a negative z component in either camera frame, don't project the socket
		if (hasPointWithNegativeZ)
			continue;

		// Project the world frame points, use the custom projection method if provided
		std::vector<cv::Point2d> pixelsCam1, pixelsCam2;
		if (feedMode == UNDISTORTED) {

			// Give empty distortion parameters because we want to project to the undistorted image
			if (uiOpt.customProjectionMethodCamera1)
				uiOpt.customProjectionMethodCamera1->projectPoints(worldPointsCam1Frame, cameraPair.ROIAdjustedCameraMatrix1(ROIcam1), cv::Mat(), pixelsCam1);
			else
				cv::projectPoints(worldPointsCam1Frame, cv::Vec3d::all(0), cv::Vec3d::all(0), cameraPair.ROIAdjustedCameraMatrix1(ROIcam1), cv::noArray(), pixelsCam1);

			if (uiOpt.customProjectionMethodCamera2)
				uiOpt.customProjectionMethodCamera2->projectPoints(worldPointsCam2Frame, cameraPair.ROIAdjustedCameraMatrix2(ROIcam2), cv::Mat(), pixelsCam2);
			else
				cv::projectPoints(worldPointsCam2Frame, cv::Vec3d::all(0), cv::Vec3d::all(0), cameraPair.ROIAdjustedCameraMatrix2(ROIcam2), cv::noArray(), pixelsCam2);

		}
		else {

			if (uiOpt.customProjectionMethodCamera1)
				uiOpt.customProjectionMethodCamera1->projectPoints(worldPointsCam1Frame, cameraPair.ROIAdjustedCameraMatrix1(ROIcam1), cameraPair.distortionParams1(), pixelsCam1);
			else
				cv::projectPoints(worldPointsCam1Frame, cv::Vec3d::all(0), cv::Vec3d::all(0), cameraPair.ROIAdjustedCameraMatrix1(ROIcam1), cameraPair.distortionParams1(), pixelsCam1);

			if (uiOpt.customProjectionMethodCamera2)
				uiOpt.customProjectionMethodCamera2->projectPoints(worldPointsCam2Frame, cameraPair.ROIAdjustedCameraMatrix2(ROIcam2), cameraPair.distortionParams2(), pixelsCam2);
			else
				cv::projectPoints(worldPointsCam2Frame, cv::Vec3d::all(0), cv::Vec3d::all(0), cameraPair.ROIAdjustedCameraMatrix2(ROIcam2), cameraPair.distortionParams2(), pixelsCam2);

		}

		// draw center point
		cv::circle(image_1, pixelsCam1.at(0), 5, cv::Scalar(255, 0, 0), 5);
		cv::circle(image_2, pixelsCam2.at(0), 5, cv::Scalar(255, 0, 0), 5);

		// Draw one arrow if the socket gave 2 world points
		if (worldPoints.size() == 2) {

			cv::arrowedLine(image_1, pixelsCam1.at(0), pixelsCam1.at(1), cv::Scalar(0, 0, 255), 3, 8, 0);
			cv::arrowedLine(image_2, pixelsCam2.at(0), pixelsCam2.at(1), cv::Scalar(0, 0, 255), 3, 8, 0);

		}
		// Draw three arrows if the socket gave 4 world points
		else if (worldPoints.size() == 4) {

			// Sort the tips of each axis based on its distance from each camera in the z direction
			// largest to smallest
			QVector<unsigned long> cam1Ordering;
			QVector<unsigned long> cam2Ordering;
			computeArrowDrawOrder(worldPointsCam1Frame, cam1Ordering);
			computeArrowDrawOrder(worldPointsCam2Frame, cam2Ordering);

			// Draw the arrows on camera 1
			for (int i = 0; i < 3; i++) {

				unsigned long j = cam1Ordering.at(i);

				cv::Scalar color;
				if (j == 1)
					color = cv::Scalar(0, 0, 255);
				if (j == 2)
					color = cv::Scalar(0, 255, 0);
				if (j == 3)
					color = cv::Scalar(255, 0, 0);
				cv::arrowedLine(image_1, pixelsCam1.at(0), pixelsCam1.at(j), color, 3, 8, 0);

			}

			// Draw the arrows on camera 2
			for (int i = 0; i < 3; i++) {

				unsigned long j = cam2Ordering.at(i);

				cv::Scalar color;
				if (j == 1)
					color = cv::Scalar(0, 0, 255);
				if (j == 2)
					color = cv::Scalar(0, 255, 0);
				if (j == 3)
					color = cv::Scalar(255, 0, 0);
				cv::arrowedLine(image_2, pixelsCam2.at(0), pixelsCam2.at(j), color, 3, 8, 0);

			}

		}

		// Populate the projection origins variable
		if (socket->shouldShowLabel)
			projectionLabelInfo.push_back({pixelsCam1.at(0), pixelsCam2.at(0), socket->ID});

	}

	// Draw the labels last
	for (ProjectionLabelInfo info : projectionLabelInfo) {

		std::string label = std::to_string(info.socketLabel);

		int baseline = 0;
		cv::Size textSize = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 1.1, 3, &baseline);

		info.originCam1.x += 10;
		info.originCam2.x += 10;
		// Draw background boxes
		cv::rectangle(image_1, info.originCam1 + cv::Point2d(-3, 4), info.originCam1 + cv::Point2d(textSize.width, -textSize.height - 3), cv::Scalar(255, 255, 255), cv::FILLED);
		cv::rectangle(image_2, info.originCam2 + cv::Point2d(-3, 4), info.originCam2 + cv::Point2d(textSize.width, -textSize.height - 3), cv::Scalar(255, 255, 255), cv::FILLED);
		// Draw the label text
		cv::putText(image_1, label, info.originCam1, cv::FONT_HERSHEY_SIMPLEX, 1.1, cv::Scalar(0, 0, 0), 3);
		cv::putText(image_2, label, info.originCam2, cv::FONT_HERSHEY_SIMPLEX, 1.1, cv::Scalar(0, 0, 0), 3);

	}

}


void ComputerVisionUI::computeArrowDrawOrder(const std::vector<cv::Point3d>& arrowTips, QVector<unsigned long>& order) {

	// Start with putting the first arrow into the order
	order.append(1);

	// Now insert 2 and 3 into the order based on their corresponding z coordinate
	for (unsigned long i = 2; i < 4; i++) {

		int size = order.size();
		for (int j = 0; j < size; j++) {

			if (arrowTips.at(i).z > arrowTips.at(order.at(j)).z) {
				order.insert(j, i);
				break;
			}
			else if (j == size - 1)
				order.append(i);

		}

	}

}


void ComputerVisionUI::drawROIs(cv::Mat& image_1, cv::Mat& image_2) {

	// Draw Camera ROI
	if (!engageROIButton->getState()) {

		cv::rectangle(image_1,
					  cv::Point(cameraROI1_x->value(), cameraROI1_y->value()),
					  cv::Point(cameraROI1_x->value() + cameraROI1_width->value(), cameraROI1_y->value() + cameraROI1_height->value()),
					  cv::Scalar(255, 0, 0), 5);
		cv::rectangle(image_2,
					  cv::Point(cameraROI2_x->value(), cameraROI2_y->value()),
					  cv::Point(cameraROI2_x->value() + cameraROI2_width->value(), cameraROI2_y->value() + cameraROI2_height->value()),
					  cv::Scalar(255, 0, 0), 5);

	}

	// Draw Processing ROI
	if ((roiMode == BOTH || roiMode == PROCESSING_ONLY)) {

		if (engageROIButton->getState()) {

			cv::rectangle(image_1,
						  cv::Point(processingROI1_x->value(), processingROI1_y->value()),
						  cv::Point(processingROI1_x->value() + processingROI1_width->value(), processingROI1_y->value() + processingROI1_height->value()),
						  cv::Scalar(0, 0, 255), 3);
			cv::rectangle(image_2,
						  cv::Point(processingROI2_x->value(), processingROI2_y->value()),
						  cv::Point(processingROI2_x->value() + processingROI2_width->value(), processingROI2_y->value() + processingROI2_height->value()),
						  cv::Scalar(0, 0, 255), 3);

		}
		else {

			cv::rectangle(image_1,
						  cv::Point(cameraROI1_x->value() + processingROI1_x->value(), cameraROI1_y->value() + processingROI1_y->value()),
						  cv::Point(cameraROI1_x->value() + processingROI1_x->value() + processingROI1_width->value(), cameraROI1_y->value() + processingROI1_y->value() + processingROI1_height->value()),
						  cv::Scalar(0, 0, 255), 3);
			cv::rectangle(image_2,
						  cv::Point(cameraROI2_x->value() + processingROI2_x->value(), cameraROI2_y->value() + processingROI2_y->value()),
						  cv::Point(cameraROI2_x->value() + processingROI2_x->value() + processingROI2_width->value(), cameraROI2_y->value() + processingROI2_y->value() + processingROI2_height->value()),
						  cv::Scalar(0, 0, 255), 3);

		}

	}

}


void ComputerVisionUI::recordButtonClicked() {

	if (record_button->getState()) { // Start recording

		if (cameraPair.isVideoStreaming()) {

			DateTime datetime;
			QString filename_camera_1 = uiOpt.outputDirectory.filePath("camera_1." + QString(datetime.MDYHMS().c_str()) + ".avi");
			QString filename_camera_2 = uiOpt.outputDirectory.filePath("camera_2." + QString(datetime.MDYHMS().c_str()) + ".avi");

			recording_size_1 = current_image_1.size();
			recording_size_2 = current_image_2.size();

			bool success1 = video_recorder_1.open(filename_camera_1.toStdString(), cv::VideoWriter::fourcc('D', 'I', 'V', 'X'), uiOpt.updateRate, recording_size_1, 1);
			bool success2 = video_recorder_2.open(filename_camera_2.toStdString(), cv::VideoWriter::fourcc('D', 'I', 'V', 'X'), uiOpt.updateRate, recording_size_2, 1);

			if (success1 && success2) {

				displayMsg(QStringLiteral("Recording video from camera 1 to %1").arg(filename_camera_1));
				displayMsg(QStringLiteral("Recording video from camera 2 to %1").arg(filename_camera_2));

				recordingThread.startTicking(*this, &ComputerVisionUI::recordFrames, 1000.0 / uiOpt.updateRate, "ComputerVisionUIRecordingThread");

			}
			else {

				video_recorder_1.release();
				video_recorder_2.release();
				record_button->setState(false);
				if (!success1)
					displayError(QStringLiteral("Could not open file (%1) to record video from camera 1!").arg(filename_camera_1));
				if (!success2)
					displayError(QStringLiteral("Could not open file (%1) to record video from camera 2!").arg(filename_camera_2));

			}

		}
		else {

			record_button->setState(false);
			displayError(QStringLiteral("The camera pair must be streaming in order to start recording"));

		}

	}
	else { // Stop recording and save videos

		recordingThread.stopTicking();

		video_recorder_1.release();
		video_recorder_2.release();

		displayMsg(QStringLiteral("Video from camera 1 written"));
		displayMsg(QStringLiteral("Video from camera 2 written"));

	}

}


bool ComputerVisionUI::recordFrames() {

	// Grab cv::Mat headers for the current images
	cv::Mat image_1, image_2;
	imageMutex.lock();
	image_1 = current_image_1;
	image_2 = current_image_2;
	imageMutex.unlock();

	// Check that the size hasn't changed. If it has, we need to stop recording
	if (image_1.size() != recording_size_1 || image_2.size() != recording_size_2) {

		// Emit a recording size mismatch signal and return false to stop recording
		emit recordingSizeMismatch();
		return true;

	}

	// Record the frames if we are currently recording
	if (video_recorder_1.isOpened())
		video_recorder_1.write(image_1);

	if (video_recorder_2.isOpened())
		video_recorder_2.write(image_2);

	return true;

}


void ComputerVisionUI::recordingSizeMismatchHandler() {

	if (recordingThread.isTicking()) {

		recordingThread.stopTicking();

		// Move to the not recording state
		if (record_button->getState())
			record_button->click();

		// Display a warning message that explains why the recording stopped
		QMessageBox::warning(this, "Recording Stopped Automatically",
							 "Recording stopped because the size of the video feed changed. "
							 "It is required that the size of the images do not change while "
							 "recording.", QMessageBox::Ok);

	}

}


void ComputerVisionUI::executeStillShot() {

	// Make sure cameras are streaming in order to have an image to undistort
	if (!cameraPair.isVideoStreaming()) {
		displayError(QStringLiteral("The camera pair must be streaming in order to capture still images"));
		return;
	}

	DateTime datetime;
	imwrite(uiOpt.outputDirectory.filePath("camera1_still_" + QString(datetime.MDYHMS().c_str()) + ".png").toStdString(), current_image_1);
	imwrite(uiOpt.outputDirectory.filePath("camera2_still_" + QString(datetime.MDYHMS().c_str()) + ".png").toStdString(), current_image_2);

	displayMsg(QStringLiteral("Still images captured"));

}


void ComputerVisionUI::switchToRawMode() {

	feedMode = RAW;

}


void ComputerVisionUI::switchToUndistortMode() {

	int choice = QMessageBox::question(this, "Are you sure?",
									   "Undistorted video feed mode requires heavy processing. If the current camera ROIs are large, the processing time "
									   "may cause a sluggish response in the video feed, and may contribute to starving anything currently running in the "
									   "GUI thread's event loop. The GUI thread is the one that is started by calling exec() on a QApplication or "
									   "QCoreApplication object since you cannot change the thread affinity of QWidgets. Any other QWidgets and QObjects "
									   "relying on the event loop of the GUI thread may be affected by the processing required for undistortion.\n\n"
									   "Make sure this will not affect any time critical components of your program before proceeding.",
									   QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

	if (choice == QMessageBox::Yes)
		feedMode = UNDISTORTED;
	else if (feedMode == RAW)
		rawVideoFeedRadioButton->setChecked(true);
	else
		processedVideoFeedRadioButton->setChecked(true);

}


void ComputerVisionUI::switchToProcessedMode() {

	feedMode = PROCESSED;

}


void ComputerVisionUI::recordSample() {

	// Make sure cameras are streaming since we need an image to find corners. We also cannot be recording since we don't
	// want race conditions for the current_image_x variables
	if (!cameraPair.isVideoStreaming() || record_button->getState()) {
		displayError(QStringLiteral("Cannot record sample. Camera pair is not streaming or you are recording. "
									"You cannot be recording when you take a sample."));
		return;
	}

	// Make sure the camera pair is using the full frame for calibration
	if(!isCameraPairAtFullFrame) {
		displayError(QStringLiteral("Cannot record sample. Camera pair is not using the full frame. "));
		return;
	}

	displayMsg(QStringLiteral("Capturing Sample . . ."));

	// Attempt to find the chessboard corner points for the current image from both cameras.
	// Note that the images are updated by the QTimer which does not run in a different thread
	// so there is no need for a mutex lock while we are finding the chessboard corners. The
	// QTimer will just stop getting images from the camera while we are busy and then resume
	// afterward.
	std::vector<cv::Point2f> corners_1;
	std::vector<cv::Point2f> corners_2;
	bool found_1 = findChessboardCorners(current_image_1, pattern_size, corners_1, cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK);
	bool found_2 = findChessboardCorners(current_image_2, pattern_size, corners_2, cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK);

	// If the chessboard was found in camera 1, perform further refinement on corner pixel coordinates, store them, and save the image
	if (found_1) {

		// Convert image to grayscale
		cv::Mat gray;
		cvtColor(current_image_1, gray, cv::COLOR_BGR2GRAY);

		// Refine the corner locations to subpixel accuracy
		cornerSubPix(gray, corners_1, cv::Size(5, 5), cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 30, 0.1));

		// Save the corner locations to the list of corner locations
		point_list_1.push_back(corners_1);

		displayMsg(QStringLiteral("    Camera 1 found chessboard -- %1 sample(s).").arg(point_list_1.size()));

		// draw a blue circle on the first corner
		circle(current_image_1, corners_1.front(), 4, cv::Scalar(255, 0, 0), 5);

		// draw a green circle on the last corner
		circle(current_image_1, corners_1.back(), 4, cv::Scalar(0, 255, 0), 5);

		// Draw the chessboard corners
		drawChessboardCorners(current_image_1, pattern_size, corners_1, true);

		// Write the image to disk
		QString filename = uiOpt.outputDirectory.filePath(QStringLiteral("CVUICalibrationOutput/camera1_image%1.png").arg(point_list_1.size()));
		imwrite(filename.toStdString(), current_image_1);

		// Put the image in the images list
		imagesList->addItem(new QListWidgetItem(QIcon(filename), QStringLiteral("Camera 1 - %1").arg(point_list_1.size())));
		imagesList->scrollToBottom(); // Keep the new image in view

	}
	else
		displayError(QStringLiteral("    Camera 1 could NOT find chessboard."));

	// If the chessboard was found in camera 2, perform further refinement on corner pixel coordinates, store them, and save the image
	if (found_2) {

		// Convert image to grayscale
		cv::Mat gray;
		cvtColor(current_image_2, gray, cv::COLOR_BGR2GRAY);

		// Refine the corner locations to subpixel accuracy
		cornerSubPix(gray, corners_2, cv::Size(5, 5), cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 30, 0.1));

		// Save the corner locations to the list of corner locations
		point_list_2.push_back(corners_2);

		displayMsg(QStringLiteral("    Camera 2 found chessboard -- %1 sample(s).").arg(point_list_2.size()));

		// draw a blue circle on the first corner
		circle(current_image_2, corners_2.front(), 4, cv::Scalar(255, 0, 0), 5);

		// draw a green circle on the last corner
		circle(current_image_2, corners_2.back(), 4, cv::Scalar(0, 255, 0), 5);

		// Draw the chessboard corners
		drawChessboardCorners(current_image_2, pattern_size, corners_2, true);

		// Write the image to disk
		QString filename = uiOpt.outputDirectory.filePath(QStringLiteral("CVUICalibrationOutput/camera2_image%1.png").arg(point_list_2.size()));
		imwrite(filename.toStdString(), current_image_2);

		// Put the image in the images list
		imagesList->addItem(new QListWidgetItem(QIcon(filename), QStringLiteral("Camera 2 - %1").arg(point_list_2.size())));
		imagesList->scrollToBottom(); // Keep the new image in view

	}
	else
		displayError(QStringLiteral("    Camera 2 could NOT find chessboard."));

	// If the chessboard was found for both cameras, store the corner points in lists for calibrating camera 1 to camera 2 transform
	if (found_1 && found_2) {

		point_list_found_in_both_1.push_back(corners_1);
		point_list_found_in_both_2.push_back(corners_2);

		displayMsg(QStringLiteral("    Chessboard found in both images -- %1 pair sample(s) taken.").arg(point_list_found_in_both_1.size()));

		// If individual cameras were previously calibrated, adding another pair sample voids the calibration because it would
		// cause calibrating camera 1 to camera 2 transform to fail. Force the user to calibrate individual cameras again before
		// calibrating camera 1 to camera 2 transform.
		individualCameraIntrinsicsCalibrated = false;

	}
	else
		displayMsg(QStringLiteral("    Chessboard not found in both images, no new pair sample -- %1 pair sample(s) taken.").arg(point_list_found_in_both_1.size()));


	if (!found_1 && !found_2)
		displayError(QStringLiteral("    Warning: No corner data was saved for this sample."));

}


void ComputerVisionUI::performIndividualIntrinsicsCalibration() {

	// Make sure cameras are streaming in order to access image size
	if (!cameraPair.isVideoStreaming()) {
		displayError(QStringLiteral("Both cameras must be streaming in order to calibrate intrinsics."));
		return;
	}

	// Make sure that at least 2 single camera samples have been taken for each camera
	if (point_list_1.size() < 2 || point_list_2.size() < 2) {
		displayError(QStringLiteral("You must take at least 2 good single camera samples on each camera to calibrate."));
		return;
	}

	// Perform calibration of each camera's intrinsic parameters (camera matrix and distortion).
	displayMsg(QStringLiteral("\nCalibrating Camera 1 intrinsics with %1 samples . . . ").arg(point_list_1.size()));

	object_points_arrayOfarrays = std::vector<std::vector<cv::Point3f>>(point_list_1.size(), object_points);
	std::vector<cv::Mat> rvecs_1, tvecs_1;
	double e1 = calibrateCamera(object_points_arrayOfarrays, point_list_1, current_image_1.size(), camera_matrix_1, distortion_params_1, rvecs_1, tvecs_1, calibrationFlags);

	displayMsg(QStringLiteral("Done"), false);
	displayMsg(QStringLiteral("    Calibration Complete - Camera 1 reprojection error: %1").arg(e1));

	displayMsg(QStringLiteral("Calibrating Camera 2 intrinsics with %1 samples . . . ").arg(point_list_2.size()));

	object_points_arrayOfarrays = std::vector<std::vector<cv::Point3f>>(point_list_2.size(), object_points);
	std::vector<cv::Mat> rvecs_2, tvecs_2;
	double e2 = calibrateCamera(object_points_arrayOfarrays, point_list_2, current_image_2.size(), camera_matrix_2, distortion_params_2, rvecs_2, tvecs_2, calibrationFlags);

	displayMsg(QStringLiteral("Done"), false);
	displayMsg(QStringLiteral("    Calibration Complete - Camera 2 reprojection error: %1").arg(e2));

	// Display the calibrated intrinsic parameters
	std::stringstream stream;
	stream << "\nCam 1 matrix:\n" << camera_matrix_1 << "\n\nCam 2 matrix:\n" << camera_matrix_2 << "\n\nCam 1 distortion:\n" << distortion_params_1 << "\n\nCam 2 distortion:\n" << distortion_params_2;
	displayMsg(stream.str().c_str());
	displayMsg(QStringLiteral("\nCamera Intrinsic Calibration Complete\n"));

	// Write the intrinsic calibration to file
	displayMsg(QStringLiteral("Writing intrinsic calibration data . . . "));
	bool success1 = CVCalibIO::writeCameraIntrinsics(uiOpt.outputDirectory.filePath("CVUICalibrationOutput/camera_1_intrinsic_calibration.dat").toStdString().c_str(),
													 camera_matrix_1, distortion_params_1);
	bool success2 = CVCalibIO::writeCameraIntrinsics(uiOpt.outputDirectory.filePath("CVUICalibrationOutput/camera_2_intrinsic_calibration.dat").toStdString().c_str(),
													 camera_matrix_2, distortion_params_2);

	// Write out the number of samples and reprojection error
	std::ofstream camera_1_metadata_file(uiOpt.outputDirectory.filePath("CVUICalibrationOutput/camera_1_intrinsic_calibration_error.txt").toStdString());
	std::ofstream camera_2_metadata_file(uiOpt.outputDirectory.filePath("CVUICalibrationOutput/camera_2_intrinsic_calibration_error.txt").toStdString());
	camera_1_metadata_file << "Num Samples: " << point_list_1.size() << "\nReprojection Error: " << e1 << " mm";
	camera_2_metadata_file << "Num Samples: " << point_list_2.size() << "\nReprojection Error: " << e2 << " mm";
	camera_1_metadata_file.close();
	camera_2_metadata_file.close();

	if (success1 && success2) {
		displayMsg(QStringLiteral("Done"), false);
		displayMsg(QStringLiteral("Intrinsic calibration data written to camera_1_intrinsic_calibration.dat and camera_1_intrinsic_calibration.dat in the %1/CVUICalibrationOutput directory.").arg(uiOpt.outputDirectory.path()));
		displayMsg(QStringLiteral("For information on the meaning of parameters, see OpenCV documentation and ComputerVisionUI class documentation."));
	}
	else
		displayError(QStringLiteral("Could not write intrinsic calibration data!"));

	individualCameraIntrinsicsCalibrated = true;
	undistortedVideoFeedRadioButton->setEnabled(true);
	displayMsg(QStringLiteral("\nUndistorted video feed mode now available\n"));



}


void ComputerVisionUI::performCamera1To2TransformCalibration() {

	// Make sure both cameras' intrinsics have been calculated first
	if (!individualCameraIntrinsicsCalibrated || !cameraPair.isVideoStreaming()) {
		displayError(QStringLiteral("Must perform camera intrinsic calibration before camera 1 to camera 2 transform calibration can occur."));
		return;
	}

	// Perform the calibration
	displayMsg(QStringLiteral("\nCalibrating Camera 1 to Camera 2 transform . . . "));
	object_points_arrayOfarrays = std::vector<std::vector<cv::Point3f>>(point_list_found_in_both_1.size(), object_points);
	double error = stereoCalibrate(object_points_arrayOfarrays, point_list_found_in_both_1, point_list_found_in_both_2, camera_matrix_1, distortion_params_1, camera_matrix_2, distortion_params_2, current_image_1.size(), R, T, E, F);
	displayMsg(QStringLiteral("Done"), false);
	displayMsg(QStringLiteral("\nCamera 1 to Camera 2 Transform Calibration Complete - Reprojection error: %1\n").arg(error));

	// Display the calibrated camera 1 to camera 2 transformation
	std::stringstream stream;
	stream << "\nR:\n" << R << "\n\nT:\n" << T;
	displayMsg(stream.str().c_str());

	// Write the transformation to file
	displayMsg(QStringLiteral("Writing Camera 1 to Camera 2 Transform . . . "));
	cv::Matx34d transform;
	cv::hconcat(R, T, transform);
	bool success1 = CVCalibIO::writeRigidTransform(uiOpt.outputDirectory.filePath("CVUICalibrationOutput/camera_1_to_camera_2_transform.dat").toStdString().c_str(), transform);
	bool success2 = CVCalibIO::writeFundamentalMatrix(uiOpt.outputDirectory.filePath("CVUICalibrationOutput/fundamentalMtx.dat").toStdString().c_str(), F);

	// Write out the number of samples and reprojection error
	std::ofstream camera_1_to_camera_2_transform_metadata_file(uiOpt.outputDirectory.filePath("CVUICalibrationOutput/camera_1_to_camera_2_transform_error.txt").toStdString());
	camera_1_to_camera_2_transform_metadata_file << "Num Samples: " << point_list_found_in_both_1.size() << "\nReprojection Error: " << error << " mm";
	camera_1_to_camera_2_transform_metadata_file.close();

	if (success1 && success2) {
		displayMsg(QStringLiteral("Done"), false);
		displayMsg(QStringLiteral("Camera 1 to Camera 2 Transform written to camera_1_to_camera_2_transform.dat and the fundamental matrix has been written to fundamentalMtx.dat in the %1/CVUICalibrationOutput directory.").arg(uiOpt.outputDirectory.path()));
		displayMsg(QStringLiteral("For information on the meaning of parameters, see OpenCV documentation and ComputerVisionUI class documentation."));
	}
	else
		displayError(QStringLiteral("Could not write calibration data!"));

}


void ComputerVisionUI::enableComputerVision() {

	if (startCVButton->getState()) {

		displayMsg(QStringLiteral("Starting computer vision"));

		// Stop the cameras if they are currently streaming
		if (!cameraPair.stop())
			displayError(QStringLiteral("Could not stop camera pair"));

		// Start with the cameras in the full frame ROI. This way the computer vision processing can acquire the initial location of an object before any
		// ROI gets applied
		displayMsg(QStringLiteral("    Setting ROI's to full camera frames (maximum ROI) for target acquisition . . . "));
		if (cameraPair.setROIsToMax()) {

			displayMsg(QStringLiteral("Done"), false);

			if(startCameras(frameRateSpinBox->value())) {

				// Set the processing ROI's for the computer vision trackers
				cv::Rect cam1ROI, cam2ROI;
				cameraPair.getROIs(cam1ROI, cam2ROI);
				uiOpt.cvTracker->processingROI1 = cam1ROI;
				uiOpt.cvTracker->processingROI2 = cam2ROI;

				// Enable the computer vision tracker. Use a single shot QTimer because we need to invoke the enable() function
				// in the thread that the tracker is running in. We enable 5 frames from now to give the tracker time to receive
				// at least one new set of frames with the new ROI's
				QTimer::singleShot(0, uiOpt.cvTracker, &ComputerVisionTracker::startTimer);
				QTimer::singleShot(5000 / frameRateSpinBox->value(), uiOpt.cvTracker, &ComputerVisionTracker::enable);

				frameRateSpinBox->setEnabled(false);
				processedVideoFeedRadioButton->setEnabled(true);
				ROIModeControls->setEnabled(true);
				ROIControls->setEnabled(true);
				return;

			}

		}
		else
			displayError(QStringLiteral("Failed!"), false);

		displayError(QStringLiteral("Failed to start computer vision"));
		startCVButton->setState(false);
		startCameras(uiOpt.updateRate, true);

	}
	else {

		displayMsg(QStringLiteral("Stopping Computer Vision"));

		// Stop the ROI's if they are currently running
		if (startROIButton->getState())
			startROIButton->click();

		// Stop the cameras if they are currently streaming
		if (!cameraPair.stop())
			displayError(QStringLiteral("Could not stop camera pair"));

		// disable the computer vision tracker. Use a single shot QTimer because we need to invoke the disable() function
		// in the thread that the tracker is running in.
		uiOpt.cvTracker->disable();

		startCameras(uiOpt.updateRate, true);

		// Move video feed to raw if we are in processed right now
		if (feedMode == PROCESSED)
			rawVideoFeedRadioButton->click();

		frameRateSpinBox->setEnabled(true);
		processedVideoFeedRadioButton->setEnabled(false);
		ROIModeControls->setEnabled(false);
		ROIControls->setEnabled(false);

		displayMsg(QLatin1String("")); // New line

	}

}


void ComputerVisionUI::startROIs() {

	if (startROIButton->getState()) {

		if (uiOpt.ROIServoCamera1) {
			uiOpt.ROIServoCamera1->reset();
			uiOpt.ROIServoCamera1->started = true;
		}
		if (uiOpt.ROIServoCamera2) {
			uiOpt.ROIServoCamera2->reset();
			uiOpt.ROIServoCamera2->started = true;
		}

		camera1ROIcontrols->setEnabled(true);
		camera2ROIcontrols->setEnabled(true);
		engageROIButton->setEnabled(true);

	}
	else {

		// Disengage the ROI's if they are currently engaged
		if (engageROIButton->getState()) {
			engageROIButton->click();
			// Wait for disengaging ROIs to complete
			QThread::msleep(6000 / frameRateSpinBox->value());
		}

		if (uiOpt.ROIServoCamera1)
			uiOpt.ROIServoCamera1->started = false;
		if (uiOpt.ROIServoCamera2)
			uiOpt.ROIServoCamera2->started = false;

		camera1ROIcontrols->setEnabled(false);
		camera2ROIcontrols->setEnabled(false);
		engageROIButton->setEnabled(false);

	}

}


void ComputerVisionUI::engageROIs() {

	if (engageROIButton->getState()) {

		displayMsg(QStringLiteral("Engaging ROIs"));

		// Disable the computer vision tracker and stop the camera pair so the ROI size can be adjusted
		uiOpt.cvTracker->disable();
		if (!cameraPair.stop())
			displayError(QStringLiteral("Could not stop camera pair"));

		// Set the camera ROIs
		cv::Rect cam1ROI(cameraROI1_x->value(), cameraROI1_y->value(), cameraROI1_width->value(), cameraROI1_height->value());
		cv::Rect cam2ROI(cameraROI2_x->value(), cameraROI2_y->value(), cameraROI2_width->value(), cameraROI2_height->value());
		if (cameraPair.setROIs(cam1ROI, cam2ROI)) {

			if (startCameras(ROIFrameRateSpinBox->value())) {

				// Update the camera ROI settings based on the results of setting the camera ROIs (this updates the ROI servos too)
				cameraROI1_x->setValue(cam1ROI.x);
				cameraROI1_y->setValue(cam1ROI.y);
				cameraROI1_width->setValue(cam1ROI.width);
				cameraROI1_height->setValue(cam1ROI.height);
				cameraROI2_x->setValue(cam2ROI.x);
				cameraROI2_y->setValue(cam2ROI.y);
				cameraROI2_width->setValue(cam2ROI.width);
				cameraROI2_height->setValue(cam2ROI.height);

				uiOpt.cvTracker->processingROI1 = cv::Rect(processingROI1_x->value(), processingROI1_y->value(), processingROI1_width->value(), processingROI1_height->value());
				uiOpt.cvTracker->processingROI2 = cv::Rect(processingROI2_x->value(), processingROI2_y->value(), processingROI2_width->value(), processingROI2_height->value());

				if (roiType == SERVO) {

					if (uiOpt.ROIServoCamera1)
						uiOpt.ROIServoCamera1->engaged = true;
					if (uiOpt.ROIServoCamera2)
						uiOpt.ROIServoCamera2->engaged = true;

				}

				// Enable the computer vision tracker. Use a single shot QTimer because we need to invoke the enable() function
				// in the thread that the tracker is running in. We enable 5 frames from now to give the tracker time to receive
				// at least one new set of frames with the new ROI's
				QTimer::singleShot(5000 / frameRateSpinBox->value(), uiOpt.cvTracker, &ComputerVisionTracker::enable);

				ROIModeControls->setEnabled(false);
				ROITypeControls->setEnabled(false);
				ROIFrameRateSpinBox->setEnabled(false);
				camera1ROIcontrols->setEnabled(false);
				camera2ROIcontrols->setEnabled(false);
				return;

			}
			else
				cameraPair.setROIsToMax();

		}

		displayError(QStringLiteral("Failed to engage ROIs"));
		engageROIButton->setState(false);
		startCameras(frameRateSpinBox->value());

		// Enable the computer vision tracker. Use a single shot QTimer because we need to invoke the enable() function
		// in the thread that the tracker is running in. We enable 5 frames from now to give the tracker time to receive
		// at least one new set of frames with the new ROI's
		QTimer::singleShot(5000 / frameRateSpinBox->value(), uiOpt.cvTracker, &ComputerVisionTracker::enable);

	}
	else {

		displayMsg(QStringLiteral("Disengaging ROIs"));

		uiOpt.cvTracker->disable();

		if (!cameraPair.stop())
			displayError(QStringLiteral("Could not stop camera pair"));

		if (uiOpt.ROIServoCamera1)
			uiOpt.ROIServoCamera1->engaged = false;
		if (uiOpt.ROIServoCamera2)
			uiOpt.ROIServoCamera2->engaged = false;

		cameraPair.setROIsToMax();

		// Set the processing ROI's for the computer vision trackers
		cv::Rect cam1ROI, cam2ROI;
		cameraPair.getROIs(cam1ROI, cam2ROI);
		uiOpt.cvTracker->processingROI1 = cam1ROI;
		uiOpt.cvTracker->processingROI2 = cam2ROI;

		startCameras(frameRateSpinBox->value());

		// Enable the computer vision tracker. Use a single shot QTimer because we need to invoke the enable() function
		// in the thread that the tracker is running in. We enable 5 frames from now to give the tracker time to receive
		// at least one new set of frames with the new ROI's
		QTimer::singleShot(5000 / frameRateSpinBox->value(), uiOpt.cvTracker, &ComputerVisionTracker::enable);

		ROIModeControls->setEnabled(true);
		ROITypeControls->setEnabled(true);
		ROIFrameRateSpinBox->setEnabled(true);
		camera1ROIcontrols->setEnabled(true);
		camera2ROIcontrols->setEnabled(true);

	}

}


void ComputerVisionUI::switchToCameraOnlyROIMode() {

	roiMode = CAMERA_ONLY;

	if (uiOpt.ROIServoCamera1)
		uiOpt.ROIServoCamera1->mode = ROIServo::CAM_ROI;
	if (uiOpt.ROIServoCamera2)
		uiOpt.ROIServoCamera2->mode = ROIServo::CAM_ROI;

	resetROIs();

	updateROIShapeControlsEnabledState();

}


void ComputerVisionUI::switchToProcessingOnlyROIMode() {

	roiMode = PROCESSING_ONLY;

	if (uiOpt.ROIServoCamera1)
		uiOpt.ROIServoCamera1->mode = ROIServo::PROC_ROI;
	if (uiOpt.ROIServoCamera2)
		uiOpt.ROIServoCamera2->mode = ROIServo::PROC_ROI;

	resetROIs();

	updateROIShapeControlsEnabledState();

}


void ComputerVisionUI::switchToBothROIMode() {

	roiMode = BOTH;

	if (uiOpt.ROIServoCamera1)
		uiOpt.ROIServoCamera1->mode = ROIServo::BOTH;
	if (uiOpt.ROIServoCamera2)
		uiOpt.ROIServoCamera2->mode = ROIServo::BOTH;

	resetROIs();

	updateROIShapeControlsEnabledState();

}


void ComputerVisionUI::switchToFixedROIType() {

	roiType = FIXED;

	resetROIs();

	updateROIShapeControlsEnabledState();

}


void ComputerVisionUI::switchToServoROIType() {

	roiType = SERVO;

	resetROIs();

	updateROIShapeControlsEnabledState();

}


void ComputerVisionUI::updateROIShapeControlsEnabledState() {

	bool enableProcessingROIControls = (roiMode == PROCESSING_ONLY || roiMode == BOTH);
	bool disableCameraROIControls = (roiType == FIXED && roiMode == PROCESSING_ONLY);

	cameraROI1_x->setEnabled(!disableCameraROIControls && (roiType == FIXED || roiMode == PROCESSING_ONLY));
	cameraROI1_y->setEnabled(!disableCameraROIControls && (roiType == FIXED || roiMode == PROCESSING_ONLY));
	cameraROI2_x->setEnabled(!disableCameraROIControls && (roiType == FIXED || roiMode == PROCESSING_ONLY));
	cameraROI2_y->setEnabled(!disableCameraROIControls && (roiType == FIXED || roiMode == PROCESSING_ONLY));
	cameraROI1_width->setEnabled(!disableCameraROIControls);
	cameraROI1_height->setEnabled(!disableCameraROIControls);
	cameraROI2_width->setEnabled(!disableCameraROIControls);
	cameraROI2_height->setEnabled(!disableCameraROIControls);

	processingROI1_x->setEnabled(enableProcessingROIControls && roiType == FIXED);
	processingROI1_y->setEnabled(enableProcessingROIControls && roiType == FIXED);
	processingROI1_width->setEnabled(enableProcessingROIControls);
	processingROI1_height->setEnabled(enableProcessingROIControls);
	processingROI2_x->setEnabled(enableProcessingROIControls && roiType == FIXED);
	processingROI2_y->setEnabled(enableProcessingROIControls && roiType == FIXED);
	processingROI2_width->setEnabled(enableProcessingROIControls);
	processingROI2_height->setEnabled(enableProcessingROIControls);

}


void ComputerVisionUI::resetROIs() {

	bool isFixedProcessingOnly = (roiMode == PROCESSING_ONLY && roiType == FIXED);

	// Set the camera ROI sizes
	if (uiOpt.defaultCameraROI_cam1.width > 0 && !isFixedProcessingOnly)
		cameraROI1_width->setValue(uiOpt.defaultCameraROI_cam1.width);
	else if (roiMode == PROCESSING_ONLY)
		cameraROI1_width->setValue(cameraROI1_width->maximum());
	else
		cameraROI1_width->setValue(cameraROI1_width->maximum() / 2);

	if (uiOpt.defaultCameraROI_cam1.height > 0 && !isFixedProcessingOnly)
		cameraROI1_height->setValue(uiOpt.defaultCameraROI_cam1.height);
	else if (roiMode == PROCESSING_ONLY)
		cameraROI1_height->setValue(cameraROI1_height->maximum());
	else
		cameraROI1_height->setValue(cameraROI1_height->maximum() / 2);

	if (uiOpt.defaultCameraROI_cam2.width > 0 && !isFixedProcessingOnly)
		cameraROI2_width->setValue(uiOpt.defaultCameraROI_cam2.width);
	else if (roiMode == PROCESSING_ONLY)
		cameraROI2_width->setValue(cameraROI2_width->maximum());
	else
		cameraROI2_width->setValue(cameraROI2_width->maximum() / 2);

	if (uiOpt.defaultCameraROI_cam2.height > 0 && !isFixedProcessingOnly)
		cameraROI2_height->setValue(uiOpt.defaultCameraROI_cam2.height);
	else if (roiMode == PROCESSING_ONLY)
		cameraROI2_height->setValue(cameraROI2_height->maximum());
	else
		cameraROI2_height->setValue(cameraROI2_height->maximum() / 2);

	// Set the camera ROI positions
	if (uiOpt.defaultCameraROI_cam1.x >= 0)
		cameraROI1_x->setValue(uiOpt.defaultCameraROI_cam1.x);
	else
		cameraROI1_x->setValue(cameraROI1_x->maximum() / 2);

	if (uiOpt.defaultCameraROI_cam1.y >= 0)
		cameraROI1_y->setValue(uiOpt.defaultCameraROI_cam1.y);
	else
		cameraROI1_y->setValue(cameraROI1_y->maximum() / 2);

	if (uiOpt.defaultCameraROI_cam2.x >= 0)
		cameraROI2_x->setValue(uiOpt.defaultCameraROI_cam2.x);
	else
		cameraROI2_x->setValue(cameraROI2_x->maximum() / 2);

	if (uiOpt.defaultCameraROI_cam2.y >= 0)
		cameraROI2_y->setValue(uiOpt.defaultCameraROI_cam2.y);
	else
		cameraROI2_y->setValue(cameraROI2_y->maximum() / 2);

	// Set the processing ROI sizes (if the ROI mode is camera only, then the processing ROI
	// sizes must be the same as the camera ROI sizes
	if (roiMode != CAMERA_ONLY) {

		if (uiOpt.defaultProcessingROI_cam1.width > 0)
			processingROI1_width->setValue(uiOpt.defaultProcessingROI_cam1.width);
		else
			processingROI1_width->setValue(processingROI1_width->maximum() / 2);

		if (uiOpt.defaultProcessingROI_cam1.height > 0)
			processingROI1_height->setValue(uiOpt.defaultProcessingROI_cam1.height);
		else
			processingROI1_height->setValue(processingROI1_height->maximum() / 2);

		if (uiOpt.defaultProcessingROI_cam2.width > 0)
			processingROI2_width->setValue(uiOpt.defaultProcessingROI_cam2.width);
		else
			processingROI2_width->setValue(processingROI2_width->maximum() / 2);

		if (uiOpt.defaultProcessingROI_cam2.height > 0)
			processingROI2_height->setValue(uiOpt.defaultProcessingROI_cam2.height);
		else
			processingROI2_height->setValue(processingROI2_height->maximum() / 2);

	}
	else {

		processingROI1_width->setValue(cameraROI1_width->value());
		processingROI1_height->setValue(cameraROI1_height->value());
		processingROI2_width->setValue(cameraROI2_width->value());
		processingROI2_height->setValue(cameraROI2_height->value());

	}

	// Set the processing ROI positions (if the ROI mode is camera only, then the processing ROI
	// positions must be 0)
	if (roiMode != CAMERA_ONLY) {

		if (uiOpt.defaultProcessingROI_cam1.x >= 0)
			processingROI1_x->setValue(uiOpt.defaultProcessingROI_cam1.x);
		else
			processingROI1_x->setValue(processingROI1_x->maximum() / 2);

		if (uiOpt.defaultProcessingROI_cam1.y >= 0)
			processingROI1_y->setValue(uiOpt.defaultProcessingROI_cam1.y);
		else
			processingROI1_y->setValue(processingROI1_y->maximum() / 2);

		if (uiOpt.defaultProcessingROI_cam2.x >= 0)
			processingROI2_x->setValue(uiOpt.defaultProcessingROI_cam2.x);
		else
			processingROI2_x->setValue(processingROI2_x->maximum() / 2);

		if (uiOpt.defaultProcessingROI_cam2.y >= 0)
			processingROI2_y->setValue(uiOpt.defaultProcessingROI_cam2.y);
		else
			processingROI2_y->setValue(processingROI2_y->maximum() / 2);

	}
	else {

		processingROI1_x->setValue(0);
		processingROI1_y->setValue(0);
		processingROI2_x->setValue(0);
		processingROI2_y->setValue(0);

	}

}


void ComputerVisionUI::cameraROI1_x_valueChanged(int value) {

	if (uiOpt.ROIServoCamera1 && roiMode == PROCESSING_ONLY)
		uiOpt.ROIServoCamera1->camera_ROI_Position.x = value;

}


void ComputerVisionUI::cameraROI1_y_valueChanged(int value) {

	if (uiOpt.ROIServoCamera1 && roiMode == PROCESSING_ONLY)
		uiOpt.ROIServoCamera1->camera_ROI_Position.y = value;

}


void ComputerVisionUI::cameraROI2_x_valueChanged(int value) {

	if (uiOpt.ROIServoCamera2 && roiMode == PROCESSING_ONLY)
		uiOpt.ROIServoCamera2->camera_ROI_Position.x = value;

}


void ComputerVisionUI::cameraROI2_y_valueChanged(int value) {

	if (uiOpt.ROIServoCamera2 && roiMode == PROCESSING_ONLY)
		uiOpt.ROIServoCamera2->camera_ROI_Position.y = value;

}


void ComputerVisionUI::cameraROI1_width_valueChanged(int value) {

	cameraROI1_x->setMaximum(cameraROI1_width->maximum() - value);

	processingROI1_width->setMaximum(value);
	processingROI1_x->setMaximum(processingROI1_width->maximum() - processingROI1_width->value());

	if (roiMode == CAMERA_ONLY)
		processingROI1_width->setValue(value);

	if (uiOpt.ROIServoCamera1)
		uiOpt.ROIServoCamera1->camera_ROI_Size.width = value;

}


void ComputerVisionUI::cameraROI1_height_valueChanged(int value) {

	cameraROI1_y->setMaximum(cameraROI1_height->maximum() - value);

	processingROI1_height->setMaximum(value);
	processingROI1_y->setMaximum(processingROI1_height->maximum() - processingROI1_height->value());

	if (roiMode == CAMERA_ONLY)
		processingROI1_height->setValue(value);

	if (uiOpt.ROIServoCamera1)
		uiOpt.ROIServoCamera1->camera_ROI_Size.height = value;

}


void ComputerVisionUI::cameraROI2_width_valueChanged(int value) {

	cameraROI2_x->setMaximum(cameraROI2_width->maximum() - value);

	processingROI2_width->setMaximum(value);
	processingROI2_x->setMaximum(processingROI2_width->maximum() - processingROI2_width->value());

	if (roiMode == CAMERA_ONLY)
		processingROI2_width->setValue(value);

	if (uiOpt.ROIServoCamera2)
		uiOpt.ROIServoCamera2->camera_ROI_Size.width = value;

}


void ComputerVisionUI::cameraROI2_height_valueChanged(int value) {

	cameraROI2_y->setMaximum(cameraROI2_height->maximum() - value);

	processingROI2_height->setMaximum(value);
	processingROI2_y->setMaximum(processingROI2_height->maximum() - processingROI2_height->value());

	if (roiMode == CAMERA_ONLY)
		processingROI2_height->setValue(value);

	if (uiOpt.ROIServoCamera2)
		uiOpt.ROIServoCamera2->camera_ROI_Size.height = value;

}


void ComputerVisionUI::processingROI1_width_valueChanged(int value) {

	processingROI1_x->setMaximum(processingROI1_width->maximum() - value);

	if (uiOpt.ROIServoCamera1)
		uiOpt.ROIServoCamera1->processing_ROI_Size.width = value;

}


void ComputerVisionUI::processingROI1_height_valueChanged(int value) {

	processingROI1_y->setMaximum(processingROI1_height->maximum() - value);

	if (uiOpt.ROIServoCamera1)
		uiOpt.ROIServoCamera1->processing_ROI_Size.height = value;

}


void ComputerVisionUI::processingROI2_width_valueChanged(int value) {

	processingROI2_x->setMaximum(processingROI2_width->maximum() - value);

	if (uiOpt.ROIServoCamera2)
		uiOpt.ROIServoCamera2->processing_ROI_Size.width = value;

}


void ComputerVisionUI::processingROI2_height_valueChanged(int value) {

	processingROI2_y->setMaximum(processingROI2_height->maximum() - value);

	if (uiOpt.ROIServoCamera2)
		uiOpt.ROIServoCamera2->processing_ROI_Size.height = value;

}


////
////************************************   ProjectionSocket   ********************************************
////


bool ProjectionSocket::getProjectableData(std::vector<cv::Point3d>& projData) {

	switch(type) {

		case POSITION:

			projData.push_back(cv::Point3d(position.position.x(), position.position.y(), position.position.z()));

			return true;

		case POSE5DOF:

			projData.push_back(cv::Point3d(pose5DOF.position.x(), pose5DOF.position.y(), pose5DOF.position.z()));
			headingTip = pose5DOF.position + arrowLength * pose5DOF.heading;
			projData.push_back(cv::Point3d(headingTip.x(), headingTip.y(), headingTip.z()));

			return true;

		case POSE6DOF:

			projData.push_back(cv::Point3d(pose6DOF.position.x(), pose6DOF.position.y(), pose6DOF.position.z()));
			xAxis = pose6DOF.position + arrowLength * pose6DOF.orientation.col(0);
			yAxis = pose6DOF.position + arrowLength * pose6DOF.orientation.col(1);
			zAxis = pose6DOF.position + arrowLength * pose6DOF.orientation.col(2);
			projData.push_back(cv::Point3d(xAxis.x(), xAxis.y(), xAxis.z()));
			projData.push_back(cv::Point3d(yAxis.x(), yAxis.y(), yAxis.z()));
			projData.push_back(cv::Point3d(zAxis.x(), zAxis.y(), zAxis.z()));

			return true;

		default:

			// Don't do anything. No projection data has been received yet, or this socket is idle
			return false;

	}

}


inline void ProjectionSocket::storeTime() {

	long timeOfLastReceive = timeOfReceive;
	timeOfReceive = Timing::getSysTime();
	receiveInterval = timeOfReceive - timeOfLastReceive;

}


void ProjectionSocket::receivePosition(const Robo::Position& pos) {

	// Set the type of the socket (the socket is no longer pending or idle)
	if (type == PENDING || type == IDLE)
		type = POSITION;

	// Save the position
	position = pos;

	// Save the current time
	storeTime();

}


void ProjectionSocket::receivePose5DOF(const Robo::Pose5DOF& pose) {

	// Set the type of the socket (the socket is no longer pending or idle)
	if (type == PENDING || type == IDLE)
		type = POSE5DOF;

	// Save the pose5DOF
	pose5DOF = pose;

	// Save the current time
	storeTime();

}


void ProjectionSocket::receivePose6DOF(const Robo::Pose6DOF& pose) {

	// Set the type of the socket (the socket is no longer pending or idle)
	if (type == PENDING || type == IDLE)
		type = POSE6DOF;

	// Save the pose6DOF
	pose6DOF = pose;

	// Save the current time
	storeTime();

}


void ProjectionSocket::enableDisplay(int state) {

	if (state == Qt::Checked)
		shouldDisplay = true;

	if (state == Qt::Unchecked)
		shouldDisplay = false;

}


void ProjectionSocket::enableLabel(int state) {

	if (state == Qt::Checked)
		shouldShowLabel = true;

	if (state == Qt::Unchecked)
		shouldShowLabel = false;

}


void ProjectionSocket::changeArrowLength(int length) {

	arrowLength = length;

}


////
////************************************   VideoDisplayWidget   ********************************************
////


VideoDisplayWidget::VideoDisplayWidget(int width, int height, ComputerVisionUI* ui, QString display_text, QWidget *parent)
	: QLabel(parent), width(width), height(height), ui(ui), display_text(display_text), image_displayed(false) {

	setFixedSize(width, height);

	setTextFormat(Qt::RichText);
	setAlignment(Qt::AlignCenter);

	final_image = cv::Mat(height, width, CV_8UC3);

}


void VideoDisplayWidget::updateImage(const cv::Mat& display_image) {

	// Resize the image so it fits within the size of the widget (maintain aspect ratio)
	double widthRatio = double(width) / display_image.cols;
	double heightRatio = double(height) / display_image.rows;
	double scale = 1.0;
	cv::Mat resized_image;
	if (widthRatio * display_image.cols <= width && widthRatio * display_image.rows <= height)
		scale = widthRatio;
	else
		scale = heightRatio;
	if (scale >= 1.0)
		cv::resize(display_image, resized_image, cv::Size(), scale, scale, cv::INTER_LINEAR);
	else
		cv::resize(display_image, resized_image, cv::Size(), scale, scale, cv::INTER_AREA);

	// Convert image to RGB (RGB is required for the QImage later since Qt does not support 24-bit BGR)
	cv::Mat rgb_image;
	cvtColor(resized_image, rgb_image, cv::COLOR_BGR2RGB);

	// Center the RGB image in the final image, use UI background color where the image does not cover
	QColor bgColor = ui->palette().color(ui->backgroundRole());
	final_image.setTo(cv::Scalar(bgColor.red(), bgColor.green(), bgColor.blue()));
	rgb_image.copyTo(final_image(cv::Rect((width - rgb_image.cols) / 2.0, (height - rgb_image.rows) / 2.0, rgb_image.cols, rgb_image.rows)));

	// Convert the OpenCV image to a QImage
	QImage image_store(final_image.data, final_image.cols, final_image.rows, QImage::Format_RGB888);

	// Draw text on the image
	QPainter painter(&image_store);
	if (display_text.size()) {
		painter.setPen(Qt::red);
		painter.setFont(QFont("Arial", 15));
		painter.drawText(5, 20, display_text);
	}

	// Convert the image to a pixmap and set it as the pixmap of this class (QLabel's pixmap)
	QPixmap pixmap;
	pixmap.convertFromImage(image_store);
	setPixmap(pixmap);

	image_displayed = true;

}


void VideoDisplayWidget::noImage() {

	clear();
	setText("<b>NO IMAGE</b>");
	image_displayed = false;

}


void VideoDisplayWidget::mousePressEvent(QMouseEvent *e) {

	// If the ctrl key is pressed while the widget was clicked
	if (e->modifiers() == Qt::ControlModifier) {

		if (!image_displayed) {
			ui->displayError(QStringLiteral("Cannot get pixel values - No image is displayed."));
			return;
		}

		// Get the location of the click
		int x = static_cast<int>(floor(e->x()));
		int y = static_cast<int>(floor(e->y()));

		// Extract the RGB of that pixel
		cv::Vec3b pixel_value_rgb = final_image.at<cv::Vec3b>(y, x);

		// Convert the RGB to HSV
		cv::Mat rgb_mat(1, 1, CV_8UC3);
		rgb_mat = cv::Scalar(pixel_value_rgb);
		cv::Mat hsv_mat;
		cvtColor(rgb_mat, hsv_mat, cv::COLOR_RGB2HSV);
		cv::Vec3b pixel_value_hsv = hsv_mat.at<cv::Vec3b>(0, 0);

		// Print the RGB and HSV values
		ui->displayMsg(QStringLiteral("Pixel color:"));
		ui->displayMsg(QStringLiteral("    rgb - (%1, %2, %3) hsv - (%4, %5, %6)").arg(pixel_value_rgb[0]).arg(pixel_value_rgb[1]).arg(pixel_value_rgb[2])
																				  .arg(pixel_value_hsv[0]).arg(pixel_value_hsv[1]).arg(pixel_value_hsv[2]));

	}

}
