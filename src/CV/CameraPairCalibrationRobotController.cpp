#include "CameraPairCalibrationRobotController.h"
#include <iostream>
#include <fstream>
#include <QMessageBox>
#include <QString>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <CV/CVUtils.h>
#include "CV/CVCalibrationIO.h"



CameraPairCalibrationRobotController::CameraPairCalibrationRobotController(WorkspaceControllableRobot& robot, CameraPair& camera_pair, ChessboardCalibrationPattern chessboardPattern, Options options)
	: WorkspaceWaypointController<Robo::Pose6DOF>(robot, 200),
	  robot(robot),
	  camera_pair(camera_pair),
	  opt(options),
	  pattern_size(cv::Size(chessboardPattern.pattern_cols, chessboardPattern.pattern_rows)),
	  pattern_step(chessboardPattern.square_size) {

	setName("CameraPair Calibration Controller");

	settings.translation_speed = opt.translation_speed;
	settings.rotation_speed = 5.0 * M_PI / 180.0;
	settings.wait_time = 0.5;

	// Make sure the output directory exists
	if (!opt.outputDirectory.exists())
		opt.outputDirectory.mkpath(".");

}


/**
 * @brief Resets the controller. This function must be called before starting the controller.
 * @return True if successful
 */
bool CameraPairCalibrationRobotController::reset() {

	// Cannot reset if the controller is running
	if (isRunning())
		return false;

	// Make sure the camera pair has calibrated intrinsic parameters and the transform between cameras. If not, we cannot calibrate
	if (!(camera_pair.isPairCalibrated() && camera_pair.hasCamera1ToCamera2FrameTransform())) {
		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: Either the intrinsic parameters or the transform between cameras of the provided camera pair have not been provided. Cannot calibrate. ERROR" << std::endl;
		return false;
	}

	// Make sure the provided options are valid
	if (opt.translation_speed <= 0) {
		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: The translation_speed option is not positive. ERROR" << std::endl;
		return false;
	}
	if (opt.orientation.determinant() != 1.0) {
		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: The orientation option is not an orthogonal matrix. ERROR" << std::endl;
		return false;
	}
	opt.dir1.normalize();
	opt.dir2.normalize();
	opt.dir3.normalize();
	if (opt.dir1.dot(opt.dir2) > (1 - 1e-3) || opt.dir1.dot(opt.dir3) > (1 - 1e-3) || opt.dir2.dot(opt.dir3) > (1 - 1e-3)) {
		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: The directions are not all linearly independent. ERROR" << std::endl;
		return false;
	}
	if (opt.dir1_steps < 0 || opt.dir2_steps < 0 || opt.dir3_steps < 0) {
		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: All dirX_steps options must be nonnegative. ERROR" << std::endl;
		return false;
	}
	if (opt.stepSize <= 0) {
		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: The stepSize option must be positive. ERROR" << std::endl;
		return false;
	}

	// Get the current robot position
	Robo::Pose6DOF beginning_config;
	if (!robot.getWorkspaceState(beginning_config)) {
		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: Cannot get the current workspace state of the robot. ERROR" << std::endl;
		return false;
	}

	// Build the grid of waypoints
	WaypointList waypoint_list;
	Robo::Position current_position = beginning_config.position;
	for (int currentDir3Steps = 0; currentDir3Steps <= opt.dir3_steps; currentDir3Steps++) {

		if (currentDir3Steps != 0)
			current_position.position += opt.stepSize * opt.dir3;

		for (int currentDir2Steps = 0; currentDir2Steps <= opt.dir2_steps; currentDir2Steps++) {

			if (currentDir2Steps != 0)
				current_position.position += opt.stepSize * opt.dir2;

			for (int currentDir1Steps = 0; currentDir1Steps <= opt.dir1_steps; currentDir1Steps++) {

				if (currentDir1Steps != 0)
					current_position.position += opt.stepSize * opt.dir1;

				waypoint_list.push_back(Robo::Pose6DOF(current_position, opt.orientation));

				if (currentDir1Steps == opt.dir1_steps)
					opt.dir1 *= -1;

			}

			if (currentDir2Steps == opt.dir2_steps)
				opt.dir2 *= -1;

		}

	}

	setWaypointList(waypoint_list);

	if (!camera_pair.isVideoStreaming()) {
		if (!camera_pair.setROIsToMax())
			return false;
		if (!camera_pair.start(24))
			return false;
	}

	// Clear the captured 3D point correspondences
	camera_2_frame_3D_points.clear();
	robot_frame_3D_points.clear();

	// Call the WorkspaceWaypointController reset() function
	return WorkspaceWaypointController<Robo::Pose6DOF>::reset();

}


/**
 * @brief Finds the 3D point corresponding to a pixel correspondence pair of the CameraPair in the camera 2
 * frame.
 * @param pixelCam1 Pixel in the correspondence pair from camera 1
 * @param pixelCam2 Pixel in the correspondence pair from camera 2
 * @return The 3D triangulated point in the camera 2 frame
 */
cv::Point3d CameraPairCalibrationRobotController::triangulate(cv::Point2d pixelCam1, cv::Point2d pixelCam2) {

	std::vector<cv::Point2d> input_array_1 = { pixelCam1 };
	std::vector<cv::Point2d> input_array_2 = { pixelCam2 };
	std::vector<cv::Point2d> output_array_1;
	std::vector<cv::Point2d> output_array_2;

	// Unroll the camera matrix and the lens distortion to get to the normalized points (i.e. the points on the image
	// plane that is parallel to the x-y plane of the camera frame and resides 1 unit in the z-direction. See OpenCV
	// documentation for details.)
	cv::undistortPoints(input_array_1, output_array_1, camera_pair.cameraMatrix1(), camera_pair.distortionParams1());
	cv::undistortPoints(input_array_2, output_array_2, camera_pair.cameraMatrix2(), camera_pair.distortionParams2());

	// Determine the projection line for both pixels in the Camera 2 frame
	cv::Vec3d u1 = camera_pair.get_2R1() * cv::Vec3d(output_array_1[0].x, output_array_1[0].y, 1.0);
	cv::Vec3d u2(output_array_2[0].x, output_array_2[0].y, 1.0);

	// Find the point where both lines are the closest
	cv::Matx22d A;
	A << u1.dot(u1), -u2.dot(u1), u1.dot(u2), -u2.dot(u2);
	cv::Vec2d b;
	b << -camera_pair.get_2T21().dot(u1), -camera_pair.get_2T21().dot(u2);
	cv::Vec2d soln = A.inv() * b;
	double z1 = soln(0);
	double z2 = soln(1);

	// Select the midpoint of the shortest segment connecting both lines
	cv::Vec3d position = 0.5 * (camera_pair.get_2T21() + z1*u1 + z2*u2);

	return position;

}


/**
 * @brief At each waypoint, the controller captures a pair of pictures from the camera pair, finds the chessboard corner
 * points in each image, and triangulates a corner to find the chessboard tool origin expressed in the camera 2 frame. It
 * then saves this point along with the corresponding point expressed in the robot base frame. This function tries to find
 * the chessboard 3 times before moving on to the next waypoint. If the chessboard is not found, it will just not add that
 * waypoint to the list of 3D point correspondences used to compute the transform.
 * @return
 */
bool CameraPairCalibrationRobotController::waypointReached() {

	// Try to find chessboard corners. Attempt several times if it fails.
	cv::Mat image_1, image_2;
	int try_counter = 0;
	while (try_counter < 3) {

		// Grab a pair of images from the camera pair
		if (!camera_pair.grabFrames(image_1, image_2)) {
			std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: Could not grab images from the camera pair. ERROR" << std::endl;
			return false;
		}

		// Attempt to find the chessboard corner points within the grabbed images
		std::vector<cv::Point2f> corners_1, corners_2;
		bool found_1 = cv::findChessboardCorners(image_1, pattern_size, corners_1, cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK);
		bool found_2 = cv::findChessboardCorners(image_2, pattern_size, corners_2, cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK);

		// If the chessboard was found for both cameras, perform further refinement on corner pixel coordinates, triangulate and store
		// 3D point correspondences, and save images
		if (found_1 && found_2) {

			// Convert images to grayscale
			cv::Mat gray_1, gray_2;
			cv::cvtColor(image_1, gray_1, cv::COLOR_BGR2GRAY);
			cv::cvtColor(image_2, gray_2, cv::COLOR_BGR2GRAY);

			// Refine the corner locations to subpixel accuracy
			cv::cornerSubPix(gray_1, corners_1, cv::Size(5, 5), cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 30, 0.1));
			cv::cornerSubPix(gray_2, corners_2, cv::Size(5, 5), cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 30, 0.1));

			// Grab the current workspace pose of the robot tool (checkerboard)
			Robo::Pose6DOF current_pose6DOF;
			if (!robot.getWorkspaceState(current_pose6DOF)) {
				std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: Cannot get the current workspace state of the robot. ERROR" << std::endl;
				return false;
			}

			// draw a blue circle on the first corner in both images
			cv::circle(image_1, corners_1.front(), 4, cv::Scalar(255, 0, 0), 5);
			cv::circle(image_2, corners_2.front(), 4, cv::Scalar(255, 0, 0), 5);

			// draw a green circle on the last corner in both images
			cv::circle(image_1, corners_1.back(), 4, cv::Scalar(0, 255, 0), 5);
			cv::circle(image_2, corners_2.back(), 4, cv::Scalar(0, 255, 0), 5);

			// Draw the chessboard corners in both images
			cv::drawChessboardCorners(image_1, pattern_size, corners_1, true);
			cv::drawChessboardCorners(image_2, pattern_size, corners_2, true);

			// Write each image to disk
			cv::imwrite(opt.outputDirectory.filePath(QStringLiteral("camera1_image%1.png").arg(robot_frame_3D_points.size())).toStdString(), image_1);
			cv::imwrite(opt.outputDirectory.filePath(QStringLiteral("camera2_image%1.png").arg(robot_frame_3D_points.size())).toStdString(), image_2);

			if (opt.pattern_origin_index >= corners_1.size() || opt.pattern_origin_index >= corners_2.size()) {
				std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: The pattern_origin_index option was set too large. There are only " << corners_1.size() << " corners on the chessboard. ERROR" << std::endl;
				return false;
			}

			/*
			 * Store the 3D point correspondence for this image. We only take one corner point (the origin) from the image
			 * to avoid errors that may occur from inaccuracies in hand measuring the orientation of the tool frame (chessboard
			 * plane) relative to the last link of the robot.
			 * Another alternative way is to perform an optimization which allows the pose of the camera pair and the pose of
			 * the tool frame (relative to the last frame of the robot) be free parameters. This way the hand measured tool
			 * transform does not need to be completely accurate and would just act as initial conditions.
			 */
			camera_2_frame_3D_points.push_back(triangulate(corners_1.at(opt.pattern_origin_index), corners_2.at(opt.pattern_origin_index)));
			robot_frame_3D_points.push_back(cv::Point3d(current_pose6DOF.position.x(),
														current_pose6DOF.position.y(),
														current_pose6DOF.position.z()));

			return true;

		}

		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: Failed to find corners in the current images, trying again. Failed Attempts = "
				  << try_counter+1 << ". Camera 1 found? " << found_1 << " Camera 2 found? " << found_2 << ". WARNING" << std::endl;
		try_counter += 1;

	}

	std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: Failed to find corners in current images after " << try_counter << " trys. Moving to next position. WARNING" << std::endl;

	return true;

}


/**
 * @brief When the waypoint list is done, this function takes the 3D point correspondences that have been gathered and uses them
 * to compute the transformation from camera 2 frame to robot frame. The average reprojection error is computed and the point
 * correspondences are recorded to file. The calibration information are also recorded to file.
 * @return
 */
bool CameraPairCalibrationRobotController::waypointsDone() {

	std::cout << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: Computing camera 2 to robot transformation..." << std::endl;

	// Find the transformation that aligns the camera 2 frame and robot frame point clouds (camera 2 frame to robot frame transformation)
	cv::Matx34d camera_to_robot_transform;
	if (!CVUtils::computeRigidBodyTransform(camera_2_frame_3D_points, robot_frame_3D_points, camera_to_robot_transform))
		return false;

	// Create files to write out the points that were recorded
	std::ofstream robot_points_file(opt.outputDirectory.filePath("robot_frame_3d_points.txt").toStdString());
	std::ofstream camera_2_points_file(opt.outputDirectory.filePath("camera_2_frame_3D_points.txt").toStdString());
	std::ofstream transformed_camera_2_points_file(opt.outputDirectory.filePath("camera_2_frame_3D_points_transformed_to_robot_frame.txt").toStdString());
	robot_points_file << "Robot Frame Points: X,Y,Z" << std::endl;
	camera_2_points_file << "Camera 2 Frame Points: X,Y,Z" << std::endl;
	transformed_camera_2_points_file << "Camera 2 Frame Points Transformed to Robot Frame: X,Y,Z" << std::endl;

	// Compute the average point correspondence error
	double avg_error = 0.0;
	cv::Vec4d camera_vector;
	cv::Vec3d transformed_camera_vector;
	for (unsigned long aa = 0; aa < robot_frame_3D_points.size(); ++aa) {

		camera_vector(0) = camera_2_frame_3D_points[aa].x;
		camera_vector(1) = camera_2_frame_3D_points[aa].y;
		camera_vector(2) = camera_2_frame_3D_points[aa].z;
		camera_vector(3) = 1.0;

		transformed_camera_vector =  camera_to_robot_transform * camera_vector;

		avg_error += cv::norm(cv::Vec3d(robot_frame_3D_points[aa]), transformed_camera_vector);

		robot_points_file << robot_frame_3D_points[aa].x << "," << robot_frame_3D_points[aa].y << "," << robot_frame_3D_points[aa].z << std::endl;
		camera_2_points_file << camera_vector(0)  << "," << camera_vector(1) << "," << camera_vector(2) << std::endl;
		transformed_camera_2_points_file << transformed_camera_vector(0) << "," << transformed_camera_vector(1) << "," << transformed_camera_vector(2) << std::endl;

	}
	avg_error /= robot_frame_3D_points.size();

	robot_points_file.close();
	camera_2_points_file.close();
	transformed_camera_2_points_file.close();

	// Write the reprojection error to a file
	std::ofstream reprojection_error_file(opt.outputDirectory.filePath("camera_2_to_robot_frame_transform_error.txt").toStdString());
	reprojection_error_file << "Average Reprojection Error: " << avg_error << " mm";
	reprojection_error_file.close();

	// Print results
	std::cout << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: " << robot_frame_3D_points.size() << " samples taken with an avg reprojection error of " << avg_error << " mm" << std::endl;
	std::cout << "Transform:\n" << camera_to_robot_transform << std::endl;

	// Write the transform to file
	if (!CVCalibIO::writeRigidTransform(opt.outputDirectory.filePath("camera_2_to_robot_frame_transform.dat").toStdString().c_str(), camera_to_robot_transform)) {
		std::cerr << "CAMERA_PAIR_CALIBRATION_ROBOT_CONTROLLER: Could not write the calibration to file. ERROR" << std::endl;
		return false;
	}

	return true;

}
