#include "StateDataUI.h"
#include <iostream>
#include <sstream>
#include "Utilities/Timing.h"


long StateDataUI::recordingStartTime_ms = 0;	// Default to zero in case user forgets to initialize (or does not want to)


/**
 * @brief Constructs the StateDataUI object with no receiver sockets.
 * @param recordOnStart Set to true if you want the StateDataUI to be constructed in the recording state. Defaults to false.
 * @param timestepUI_ms Update rate for plotting in milliseconds. Defaults to 50 ms (20 Hz).
 * @param plottingWindowSize Desired size of the rolling plotting window in seconds (i.e. how many seconds of plotting history
 * do you want to see on the screen?). Defaults to 40 seconds.
 * @param parent Pointer to a QWidget to act as parent.
 */
StateDataUI::StateDataUI(bool recordOnStart, int timestepUI_ms, int plottingWindowSize, QWidget* parent)
	: QWidget(parent),
	  defaultPenList({QPen(Qt::blue),
	  QPen(Qt::green),
	  QPen(Qt::red),
	  QPen(Qt::darkBlue),
	  QPen(Qt::darkGreen),
	  QPen(Qt::darkRed),
	  QPen(Qt::magenta),
	  QPen(Qt::cyan),
	  QPen(Qt::yellow),
	  QPen(Qt::black)}),
	  m_plottingWindowSize(plottingWindowSize),
	  nameIDMappingRecorder(Recorder::nextAvailableFilename("DataReceiverNameIDMappings", ".txt")),
	  typeIDMappingRecorder(Recorder::nextAvailableFilename("TypeIDMappings", ".txt")),
	  dataRecorder(Recorder::nextAvailableFilename("StateData", ".txt")),
	  recording(recordOnStart) {

	setWindowTitle("State Data User Interface");

	// Create the default widget parent. This QWidget acts as a parent to all the QWidgets that
	// that will only be shown in certain circumstances (e.g. the QTabWidget will only be shown
	// if at least one DataReceiver object receives data that is plottable. The DataReceivers
	// that receive unplottable data won't ever be shown.) These widgets start with the default
	// widget parent as their parent so they are not orphaned and can all be correctly deleted
	// by deleting the default parent widget in the destructor of StateDataUI. If one of these
	// widgets does end up being shown, its parent will automatically be reassigned by Qt when
	// it gets added to a layout.
	defaultWidgetParent = new QWidget();

	// Build the receiver summary (this is always visible)
	QWidget* receiverSummary = new QWidget();
	QHBoxLayout* receiverSummaryLayout = new QHBoxLayout();
	receiverSummary->setLayout(receiverSummaryLayout);

	// Build the receiver table
	receiverTable = new QWidget();
	receiverTableLayout = new QGridLayout();
	receiverTableLayout->setHorizontalSpacing(20);
	receiverTable->setLayout(receiverTableLayout);

	// Add the table titles to the receiver table
	receiverTableLayout->addWidget(new QLabel("<u><b>Receiver</b></u>"), 0, 0);
	receiverTableLayout->addWidget(new QLabel("<u><b>Messages Received</b></u>"), 0, 1, Qt::AlignCenter);
	receiverTableLayout->addWidget(new QLabel("<u><b>Record</b></u>"), 0, 2, Qt::AlignCenter);

	// Put the receiver table in a scroll area to prevent the table from taking too much space if a lot of DataReceiver objects are created
	receiverTableScrollArea = new QScrollArea();
	receiverTableScrollArea->setWidget(receiverTable);
	receiverTableScrollArea->setWidgetResizable(true);
	receiverTableScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	receiverTableScrollArea->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
	receiverTableScrollArea->setMinimumWidth(receiverTable->sizeHint().width() + receiverTableScrollArea->verticalScrollBar()->width());
	receiverTableScrollArea->setMaximumWidth(600);
	receiverTableScrollArea->setMaximumHeight(125);
	receiverTableScrollArea->setBackgroundRole(QPalette::Light);

	// Build the recording button section
	QWidget* recordingButtonSection = new QWidget();
	QVBoxLayout* recordingButtonSectionLayout = new QVBoxLayout();
	recordingButtonSection->setLayout(recordingButtonSectionLayout);
	recordButton = new QPushButton();
	if (recordOnStart)
		recordButton->setText("Stop Recording");
	else
		recordButton->setText("Start Recording");
	connect(recordButton, &QPushButton::clicked, this, &StateDataUI::recordButtonPressed);
	recordingButtonSectionLayout->addWidget(recordButton, 0, Qt::AlignLeft);

	recordingCount = new CountingLabel("Recording Items: ");
	if (!recordOnStart)
		recordingCount->setVisible(false);
	recordingButtonSectionLayout->addWidget(recordingCount, 0, Qt::AlignLeft);

	recordedCount = new CountingLabel("Items Recorded: ");
	recordedCount->setVisible(false);
	recordingButtonSectionLayout->addWidget(recordedCount, 0, Qt::AlignLeft);

	// Add the receiver summary widgets to the receiver summary layout
	receiverSummaryLayout->addWidget(receiverTableScrollArea, 0 , Qt::AlignTop);
	receiverSummaryLayout->addWidget(recordingButtonSection, 0, Qt::AlignTop);

	// Set up the overall layout. Leave adding the QTabWidget for later in case no
	// DataReceiver objects receive plottable data.
	overallLayout = new QVBoxLayout();
	overallLayout->addWidget(receiverSummary);
	setLayout(overallLayout);

	// Create the QTabWidget. The QTabWidget may never be shown so give it the default widget parent.
	plottingTabWidget = new QTabWidget(defaultWidgetParent);
	plottingTabWidget->setTabPosition(QTabWidget::South);

	// Store the current system time as a zero point for plotting (we always want a zero point for plotting)
	plottingStartTime_ms = Timing::getSysTime();

	// Start the plotting update timer
	updateTimer = new QTimer(this);
	updateTimer->setInterval(timestepUI_ms);
	updateTimer->start();
}


/**
 * @brief Registers the current system time as the zero timestamp to be used for recording. <b>IMPORTANT:</b>
 * Only call this function once in your program. This function should be one of the first things called in your
 * main() before any timestamps are created. If you do not want the recorded timestamps to be recorded
 * relative to this zero timestamp, then do not call this function and the timestamps will be recorded as
 * received.
 */
void StateDataUI::registerZeroTimestamp() {

	recordingStartTime_ms = Timing::getSysTime();

}


/**
 * @brief Adds a DataReceiver to the StateDataUI with the given name and returns a pointer to it. If a DataReceiver with
 * the given name already exists, a dummy DataReceiver is created instead. This dummy DataReceiver will ignore all data
 * it receives and won't be represented in the user interface in any way. A message will be printed to the command line
 * warning that a DataReceiver already exists with the given name. This operator is meant to be used directly in
 * QObject::connect() function calls in your main() function. This function is designed such that creating the receiver
 * socket and connecting to it is meant to be done at the same time in the QObject::connect() function call.
 * @param receiverName A unique name for the DataReceiver
 * @return A pointer to the DataReceiver referred to by receiverName, or a pointer to a dummy receiver if a receiver with
 * the specified name already exists
 */
DataReceiver* StateDataUI::operator[](const char* receiverName) {

	// Check if a DataReceiver with this name already exists. If so, return a dummy receiver. A dummy receiver is a DataReceiver that
	// simply does nothing in its receive() slot. It will not ever be shown and will never do anything. This is useful to prevent
	// multiple connections to the same receiver if the user accidentally uses the same receiver name twice in a QObject::connect()
	// function.
	if (receivers.contains(receiverName)) {
		DataReceiver* dummy = new DataReceiver("", -1, nullptr, true);
		dummy->setParent(defaultWidgetParent); // Give the default widget parent so this dummy can be properly accounted for
		std::cerr << "STATE_DATA_UI: Receiver socket with name \"" << receiverName << "\" already exists. Cannot create multiple receiver sockets with the same name. ERROR" << std::endl;
		return dummy;
	}

	// Write the receiver name-ID mapping to file
	std::ostringstream line;
	line << nextReceiverID << "=" << receiverName << "\n";
	nameIDMappingRecorder.record(line.str());
	nameIDMappingRecorder.flush(); // Flush the information in case the program crashes

	// Create a new DataReceiver and add it to the QHash. A pointer to this StateDataUI is given to the DataReceiver
	// so the DataReceiver can access relevant information and add itself as a plotting tab if the data it receives
	// is configured for plotting.
	DataReceiver* receiver = new DataReceiver(receiverName, nextReceiverID, this);
	receiver->setParent(defaultWidgetParent); // The receiver may not ever be shown so give it the default widget parent
	receivers[receiverName] = receiver;

	// Increment the next receiver ID. This number now corresponds to the row number for the newly created DataReceiver
	// in the receiver table (since row 0 is the title row).
	nextReceiverID++;

	// Set up a row in the receiver table for the new receiver
	receiverTableLayout->addWidget(new QLabel(receiverName), nextReceiverID, 0);

	CountingLabel* count = new CountingLabel();
	connect(receiver, &DataReceiver::messageReceived, count, &CountingLabel::increment);
	receiverTableLayout->addWidget(count, nextReceiverID, 1, Qt::AlignCenter);

	QCheckBox* recordBox = new QCheckBox();
	checkboxes.append(recordBox);
	recordBox->setCheckState(Qt::Checked);
	if (recording)
		recordBox->setEnabled(false);
	connect(recordBox, &QCheckBox::stateChanged, receiver, &DataReceiver::enableRecord);
	receiverTableLayout->addWidget(recordBox, nextReceiverID, 2, Qt::AlignCenter);

	// Need to update the receiver table scroll area minimum width because the QScrollArea has a bug that causes the vertical scrollbar
	// to be drawn on top of the right side of the main widget when the widget is at its smallest size.
	receiverTableScrollArea->setMinimumWidth(receiverTable->sizeHint().width() + receiverTableScrollArea->verticalScrollBar()->width());

	// Connect the receiver to the recording CountingLabels
	connect(receiver, &DataReceiver::recordedMessage, recordingCount, &CountingLabel::increment);
	connect(receiver, &DataReceiver::recordedMessage, recordedCount, &CountingLabel::increment);

	return receiver;

}


/**
 * @brief Takes care of starting and stopping recording. It is connected to the record button. It is meant to
 * be used by StateDataUI only. <b>Do not connect to this slot.</b>
 */
void StateDataUI::recordButtonPressed() {

	if (recording) { // Stop recording

		// Make recording count invisible and recorded count visible
		recordingCount->setVisible(false);
		recordedCount->setVisible(true);

		// Enable recording checkboxes
		for (auto box : checkboxes)
			box->setEnabled(true);

		// Change recording button text
		recordButton->setText("Start Recording");

		// Turn off recording
		recording = false;

	}
	else { // Start recording

		// Reset the recording labels. Make recording count visible and recorded count invisible
		recordingCount->reset();
		recordingCount->setVisible(true);
		recordedCount->reset();
		recordedCount->setVisible(false);

		// Disable recording checkboxes
		for (auto box : checkboxes)
			box->setEnabled(false);

		// Change recording button text
		recordButton->setText("Stop Recording");

		// Turn on recording
		recording = true;

	}

}


/**
 * @brief This event filter gets installed on all QCustomPlot objects for this DataReceiver in order to intercept
 * scroll wheel events from the mouse and send them to the scroll area's viewport. If this event filter is not
 * installed on QCustomPlot objects, then using the scroll wheel when the mouse is over plots will not scroll the
 * scroll area.
 * @param e QEvent to filter
 * @return Whether the event was handled or not
 */
bool DataReceiver::eventFilter(QObject*, QEvent* e) {

	// Send event to the scroll area's viewport if the event is a QEvent::Wheel type. Note that the event will
	// never make it to the widget it was intended for (which will be the QCustomPlot objects).
	if (e->type() == QEvent::Wheel)
		return QCoreApplication::sendEvent(scrollArea->viewport(), e);

	// Otherwise allow the event to propagate
	return false;

}


/**
 * @brief The receive() slot should be connected to a single signal which sends a const reference to a StateMessage
 * object and a timestamp. Again, <b>IT IS VERY IMPORTANT</b> that for any instance of DataReceiver, there is only <b>ONE</b>
 * signal connected to its receive() slot. This slot handles consuming received StateMessage objects for recording, plotting,
 * etc. This slot will automatically handle the StateMessage according to how it's configured. For example, if the StateMessage
 * is only configured for recording, it will record the data. If the StateMessage is configured for recording and plotting,
 * this slot will both record and plot the data within StateMessage.
 * @param data A StateMessage object representing data corresponding to a single timestamp
 * @param timestamp A timestamp corresponding to the data in milliseconds. This timestamp must be the raw system clock time when
 * the data was taken. Do not "zero" the timestamp relative to the time the program started or anything similar to it. This slot
 * will zero timestamps relative to the time the program was started.
 */
void DataReceiver::receive(const StateMessage& data, long timestamp) {

	// If this is a dummy receiver, do nothing.
	if (isDummy)
		return;

	// Notify the rest of the UI that a message has been received.
	emit messageReceived();

	// If this is the first piece of data received, then this DataReceiver needs to set up its
	// plotting tab (if the received data is configured for plotting) and may need to record
	// the MetaType ID mapping
	if (!receivedFirstData) {

		receivedFirstData = true;

		// Get the MetaType ID for the type of data this DataReceiver will receive and store it.
		// Also, write the MetaType ID mapping to file if it hasn't been written already.
		const char* typeName;
		if (data.getMetaTypeID(dataMetaTypeID) && (typeName = QMetaType::typeName(dataMetaTypeID))) {

			// Check if the MetaType ID hasn't been seen already
			if (!ui->seenMetaTypeIDs.contains(dataMetaTypeID)) {

				// Write the MetaType ID mapping to file
				std::ostringstream line;
				line << dataMetaTypeID << "=" << typeName << "\n";
				ui->typeIDMappingRecorder.record(line.str());
				ui->typeIDMappingRecorder.flush(); // Flush the information in case the program crashes

				// Record that the MetaType ID has been seen
				ui->seenMetaTypeIDs.insert(dataMetaTypeID);

			}

		}

		// Get the plotting attributes from the provided data, if the function returns false, then this type
		// of data is not configured for plotting so don't build a plotting tab.
		std::vector<PlotAttributes> attributes;
		if (data.getPlottingAttributes(attributes)) {

			// Create plots, add the graphs to the plots and put them in a layout
			QVBoxLayout* layout = new QVBoxLayout();
			setLayout(layout);
			for (unsigned long plotIndex = 0; plotIndex < attributes.size(); plotIndex++) {

				// Add a title to the plot
				QString title = "<b>";
				title.append(attributes[plotIndex].name.c_str());
				title.append("</b>");
				QLabel* plotTitle = new QLabel(title);
				plotTitle->setTextFormat(Qt::RichText);
				plotTitle->setAlignment(Qt::AlignCenter);
				plotTitle->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed));
				layout->addWidget(plotTitle);

				QCustomPlot* plot = new QCustomPlot();

				// Install the event filter so the scroll wheel will work properly. See DataReceiver::eventFilter() for details.
				plot->installEventFilter(this);

				// Configure the graphs for this plot
				unsigned long defaultPenIndex = 0;
				for (int graphIndex = 0; graphIndex < static_cast<int>(attributes[plotIndex].graphAttributes.size()); graphIndex++) {

					// Add the graph
					plot->addGraph();

					GraphAttributes graphAttributes = attributes[plotIndex].graphAttributes[static_cast<unsigned long>(graphIndex)];

					// Set the name of the graph for the legend
					plot->graph(graphIndex)->setName(graphAttributes.name.c_str());

					// Set the pen color for the graph
					plot->graph(graphIndex)->setPen(ui->defaultPenList[defaultPenIndex++]);

					// Start at the beginning of the default pen list if we are at the end
					if (defaultPenIndex >= ui->defaultPenList.size())
						defaultPenIndex = 0;

				}

				// Make the legend visible and place it in the top right of the plot if there is more than one graph for this plot
				if (attributes[plotIndex].graphAttributes.size() > 1) {
					QCPLayoutGrid* subLayout1 = new QCPLayoutGrid;
					plot->plotLayout()->addElement(0, 1, subLayout1);
					subLayout1->addElement(0, 0, new QCPLayoutElement);
					subLayout1->addElement(1, 0, plot->legend);
					subLayout1->addElement(2, 0, new QCPLayoutElement);
					plot->plotLayout()->setColumnStretchFactor(1, 0.001);
					plot->legend->setBorderPen(QPen(Qt::transparent));
					plot->legend->setVisible(true);
				}

				// Set the x-axis of the plot to use a time format (time data must be in seconds)
				QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
				timeTicker->setTimeFormat("%m:%s");
				plot->xAxis->setTicker(timeTicker);
				plot->xAxis->setLabel("Time");

				// Configure plot size policy
				plot->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));
				plot->setMinimumSize(QSize(800, plot->legend->minimumOuterSizeHint().height() + 100));

				plots.push_back(plot); // Store the plot for later access in updatePlots()
				layout->addWidget(plot); // Add the plot to the layout

			}

			// Add this DataReceiver to a QScrollArea. Note: Ownership of this DataReceiver will be transferred from
			// the default widget parent to the QScrollArea.
			scrollArea = new QScrollArea();
			scrollArea->setWidget(this);
			scrollArea->setWidgetResizable(true);
			scrollArea->setBackgroundRole(QPalette::Light);

			// Add the plottingTabWidget to the overall layout if this is the first receiver to build a new plotting tab.
			// Note: Ownership of the plotting tab widget will be transferred from the default widget parent to the UI's
			// overall layout.
			if (ui->plottingTabWidget->count() == 0)
				ui->overallLayout->insertWidget(0, ui->plottingTabWidget);

			// Find the index in the tab widget that this new receiver should have. This method of computing the index we
			// should use is necessary to avoid the tabs getting in an order that does not match the receiver table.
			int index;
			for (index = 0; index < ui->plottingTabWidget->count(); index++) {

				if (ui->receivers.value(ui->plottingTabWidget->tabText(index))->ID > ID)
					break;

			}
			// Add the scroll area as a tab of the plottingTabWidget in StateDataUI.
			ui->plottingTabWidget->insertTab(index, scrollArea, name);

			// connect the UI's update timer to the updatePlots slot of this DataReceiver
			connect(ui->updateTimer, &QTimer::timeout, this, &DataReceiver::updatePlots);

		}

	}

	// Convert the data to plotting format and store for later use in updatePlots(). If the received data type doesn't support
	// plotting, then this function will just return immediately.
	data.convertToPlottingFormat(plottingData);

	// Record the data if recording for this DataReceiver and overall recording are enabled and the data type is properly
	// registered as a StateMessage
	if (ui->recording && recordingEnabled && dataMetaTypeID) {

		std::ostringstream line;
		// Start with the ID of the DataReceiver to differentiate multiple DataReceivers that receive the same data type,
		// then follow with the MetaType ID, and finally the actual data
		line << ID << ":" << dataMetaTypeID << ":";
		data.convertToFileLineFormat(line);
		if (ui->dataRecorder.record(timestamp - ui->recordingStartTime_ms, line.str()))
			emit recordedMessage(); // emit only if the recording was successful

	}

}


/**
 * @brief Handles updating the plots for this instance of DataReceiver. Meant to be used by StateDataUI
 * <b>Do not connect to this slot.</b>
 */
void DataReceiver::updatePlots() {

	// Compute the "zeroed" time relative to the plotting start time. Use the current
	// system time rather than the received timestamps so the window continues to scroll
	// even when the DataReceiver is not receiving data (i.e. we don't want the plot to
	// "freeze" if the incoming data stops, we'd rather the graphs in the plots to "flatline".
	// You can determine if the incoming data has stopped by looking at the receiver table).
	double time = (Timing::getSysTime() - ui->plottingStartTime_ms) / 1000.0;
	int plottingWindowSize = ui->plottingWindowSize();
	double min_time = time - plottingWindowSize;

	// Iterate over each plot for this DataReceiver
	for (unsigned long plotIndex = 0; plotIndex < plottingData.stateValues.size(); plotIndex++) {

		// Update each graph for this plot with plottingData
		for (unsigned long graphIndex = 0; graphIndex < plottingData.stateValues[plotIndex].size(); graphIndex++) {
			auto data = plots[plotIndex]->graph(static_cast<int>(graphIndex))->data();

			// Remove older points (no need to keep in the plots what we cannot see)
			data->removeBefore(min_time);

			// Add the new value
			data->add({time, plottingData.stateValues[plotIndex][graphIndex]});
		}

		// Rescale the y-axis
		plots[plotIndex]->yAxis->rescale(true);

		// Update the range of the time axis to give window following behavior
		plots[plotIndex]->xAxis->setRange(time, plottingWindowSize, Qt::AlignRight);

		// Update the plot on the screen
		plots[plotIndex]->replot();
	}

}


/**
 * @brief Handles enabling recording for this instance of DataReceiver. This slot is connected to the corresponding
 * QCheckBox for this DataReceiver in the receiver table of StateDataUI. It is meant to be used by StateDataUI
 * <b>Do not connect to this slot.</b>
 * @param state New state of the QCheckBox
 */
void DataReceiver::enableRecord(int state) {

	if (state == Qt::Checked)
		recordingEnabled = true;

	if (state == Qt::Unchecked)
		recordingEnabled = false;

}
