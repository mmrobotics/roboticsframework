#ifndef SPACE_EXPLORER_FILE_POSE_PRODUCER_H
#define SPACE_EXPLORER_FILE_POSE_PRODUCER_H

#include <QtWidgets>

#include <Hardware/InputDevices/SpaceExplorer.h>
#include <QtGUIElements/Buttons.h>
#include <RoboticsToolbox.h>
#include <Utilities/Timing.h>


class DirectionDotDisplay; // Forward declaration to allow SpaceExplorerFilePoseProducer definition to appear first

/**
 * @brief The SpaceExplorerFilePoseProducer class is used to produce poses in time that can be used as a desired pose
 * source for controllers.
 *
 * The class primarily uses the SpaceExplorer 3D mouse as input to control the pose in real-time. You may also provide
 * a trajectory file that represents a trajectory for the pose to follow that can be played as desired, overriding the
 * input from the 3D mouse during the duration of trajectory playback. The class accepts an options class
 * (SpaceExplorerFilePoseProducer::Options) that allows customization of behavior.
 *
 * The SpaceExplorerFilePoseProducer keeps an internal Robo::Pose6DOF that represents the current pose. You may set the
 * starting pose with the `startingPose` option, otherwise, the default Robo::Pose6DOF constructor will be used. The
 * current pose is emitted from the two signals newPose5DOF() and newPose6DOF() at the rate specified by the `updateRate`
 * option. The newPose6DOF() signal simply emits the internal Robo::Pose6DOF. The newPose5DOF() signal emits a
 * Robo::Pose5DOF which is created from the position and one axis of the frame represented by the internal Robo::Pose6DOF.
 * The axis used defaults to the z-axis, but can be changed in the options class. Therefore, when setting the initial
 * pose for use cases that only require the Robo::Pose5DOF, you must set `startingPose` such that the chosen axis to serve
 * as the heading in the Robo::Pose5DOF is set as desired. Keep in mind the `orientation` of the Robo::Pose6DOF must still
 * be a valid rotation matrix.
 *
 * Once the producer is running, the SpaceExplorer can be used to manipulate the pose. The `translation_speed` option
 * sets the speed at which the mouse translates the desired pose while the `rotation_speed` option sets the speed at which
 * the mouse rotates the desired pose. The `counts_threshold` option can be set to determine how far the mouse must be
 * moved in order to produce motion (i.e. the sensitivity of the mouse). The class is a Qt widget which shows when
 * input from the mouse exceeds this threshold.
 *
 * If a trajectory file is provided in the `filename` option, a button will appear on the widget that can be used to
 * initiate (and stop) the trajectory. The trajectory is defined in a relative manner from a starting pose and this
 * starting pose is taken as the current internal pose. While the trajectory is playing, input from the mouse is
 * ignored. Once the trajectory finishes, input from the mouse resumes from the final pose of the trajectory.
 *
 * The trajectory file is a simple text file where the first line is:
 *
 *     rate <command rate>
 *
 * The `<command rate>` is simply a number representing the rate at which commands should be consumed in Hz. All following
 * lines are commands of the form:
 *
 *     <x-offset> <y-offset> <z-offset> <x-angle> <y-angle> <z-angle>
 *
 * The `<x-offset>`, `<y-offset>`, `<z-offset>` are numbers that represent the XYZ translational offset from the starting
 * pose in millimeters. The `<x-angle>`, `<y-angle>`, `<z-angle>` are numbers that represent the XYZ Euler angles in
 * degrees relative to the starting pose. For clarity, the orientation at each command is computed as
 * R<sub>z</sub>R<sub>y</sub>R<sub>x</sub>R<sub>start</sub>. Therefore the XYZ angles express rotation about the global
 * XYZ axes. Here is an example trajectory file that describes a simultaneous
 * translation along the x-axis of 10 millimeters and rotation about the y-axis by 90 degrees.
 *
 *     rate 25.0
 *     1 0 0 0 9 0
 *     2 0 0 0 18 0
 *     3 0 0 0 27 0
 *     4 0 0 0 36 0
 *     5 0 0 0 45 0
 *     6 0 0 0 54 0
 *     7 0 0 0 63 0
 *     8 0 0 0 72 0
 *     9 0 0 0 81 0
 *     10 0 0 0 90 0
 *
 * The rate of the trajectory can be overridden with the `trajectory_rate_multiplier` option. This option multiplies the
 * rate specified in the trajectory file. For example, setting `trajectory_rate_multiplier` to 2.0 makes the trajectory
 * play twice as fast.
 */
class SpaceExplorerFilePoseProducer : public QWidget {

	Q_OBJECT

	public:
		/**
		 * @brief The Options struct is used to set various options for the SpaceExplorerFilePoseProducer as well as
		 * set a trajectory file. See the documentation for each member variable for details of each setting as well
		 * as the SpaceExplorerFilePoseProducer documentation for more information.
		 */
		struct Options {

			Options() {}

			/**
			 * @brief Used to set the starting pose of the pose producer. The `orientation` component of this option
			 * must always be a valid rotation matrix, even if you are only interested in position or heading or both.
			 */
			Robo::Pose6DOF startingPose;

			/**
			 * @brief Used to set the translation speed in mm/s of the pose when controlling with the SpaceExplorer.
			 */
			double translation_speed = 10.0;

			/**
			 * @brief Used to set the rotation speed in deg/s of the pose when controlling with the SpaceExplorer.
			 */
			double rotation_speed = 5.0;

			/**
			 * @brief Used to set the translation speed in mm/s of the pose when controlling with the SpaceExplorer
			 * in low gear.
			 */
			double translation_speed_low_gear = 1.0;

			/**
			 * @brief Used to set the rotation speed in deg/s of the pose when controlling with the SpaceExplorer
			 * in low gear.
			 */
			double rotation_speed_low_gear = 1.0;

			/**
			 * @brief Used to control the rate at which the pose is emitted from this class in Hz.
			 */
			double updateRate = 25.0;

			/**
			 * @brief Used to control the threshold of the SpaceExplorer in counts.
			 */
			int counts_threshold = 200;

			/**
			 * @brief Used to choose which axis of the internal 6-DOF frame is used when emitting the newPose5DOF() signal.
			 * The value can be 0, 1, or 2 to represent the x, y, or z axis respectively. Defaults to 2.
			 */
			int axisForPose5DOF = 2;

			/**
			 * @brief Used to specify a trajectory file to use.
			 */
			const char* filename = nullptr;

			/**
			 * @brief Used to override the playback speed of the trajectory file. For example, a value of 2.0 means
			 * the trajectory will play back twice as fast.
			 */
			double trajectory_rate_multiplier = 1.0;

		};

		SpaceExplorerFilePoseProducer(const SpaceExplorer& explorer, Options options = Options(), QWidget* parent = nullptr);
		virtual ~SpaceExplorerFilePoseProducer() = default;

	private:
		Options opt;

		Timing::Timer updateThread;

		Robo::Vector3d translation_input;
		Robo::Vector3d rotation_input;
		double translation_stepsize;
		double rotation_stepsize;
		double translation_low_gear_stepsize;
		double rotation_low_gear_stepsize;
		double current_translation_stepsize;
		double current_rotation_stepsize;

		Robo::Pose6DOF currentPose;
		Robo::Pose6DOF trajectoryBasePose;

		struct Command {
			float x;
			float y;
			float z;
			float rx;
			float ry;
			float rz;
		};

		std::vector<Command> command_list;
		double trajectory_rate;
		bool trajectory_started = false;
		unsigned long command_index = 0;

		DirectionDotDisplay* translation_display;
		DirectionDotDisplay* rotation_display;
		QCheckBox* low_gear_checkbox;
		SwitcherButton* trajectory_button = nullptr;

		void receiveInput(int x, int y, int z, int rx, int ry, int rz);

	private slots:
		bool update();
		void lowGearChecked(bool isChecked);

	signals:
		/**
		 * @brief Emits the current pose as a Robo::Pose5DOF. The heading is taken as one of the axes of the internal
		 * Robo::Pose6DOF used to represent the current pose. The axis used is specified in the options class and
		 * defaults to the z-axis.
		 * @param pose The current pose
		 * @param timestamp The associated timestamp
		 */
		void newPose5DOF(const Robo::Pose5DOF& pose, long timestamp);
		/**
		 * @brief Emits the current pose as a Robo::Pose6DOF.
		 * @param pose The current pose
		 * @param timestamp The associated timestamp
		 */
		void newPose6DOF(const Robo::Pose6DOF& pose, long timestamp);

};


/**
 * @brief The DirectionDotDisplay class is a helper class used by SpaceExplorerFilePoseProducer to display when
 * input from the SpaceExplorer has exceeded the current threshold.
 */
class DirectionDotDisplay : public QGraphicsView {

	Q_OBJECT

	private:
		QGraphicsScene* scene;

		QGraphicsEllipseItem* x_up;
		QGraphicsEllipseItem* x_down;
		QGraphicsEllipseItem* y_up;
		QGraphicsEllipseItem* y_down;
		QGraphicsEllipseItem* z_up;
		QGraphicsEllipseItem* z_down;

		QFont pm_font;
		QFont d_font;

	public:
		DirectionDotDisplay();

		void setDirectionX(int val = 0);
		void setDirectionY(int val = 0);
		void setDirectionZ(int val = 0);

};

#endif // SPACE_EXPLORER_FILE_POSE_PRODUCER_H
