#include "SpaceExplorerFilePoseProducer.h"
#include <iostream>


/**
 * @brief Constructs a SpaceExplorerFilePoseProducer using the SpaceExplorer and Options provided. The SpaceExplorer is created
 * outside this class so it may be able to be used by other classes that may need it (there can only be one instance of
 * SpaceExplorer in a given program).
 * @param explorer
 * @param options
 * @param parent
 */
SpaceExplorerFilePoseProducer::SpaceExplorerFilePoseProducer(const SpaceExplorer& explorer, Options options, QWidget* parent)
    : QWidget(parent),
      opt(options),
	  translation_input(Robo::Vector3d::Zero()),
	  rotation_input(Robo::Vector3d::Zero()) {

	// Set the window title
    setWindowTitle("Pose Producer");

	// Register signal types
    qRegisterMetaType<Robo::Pose5DOF>();
    qRegisterMetaType<Robo::Pose6DOF>();

    QVBoxLayout* vlayout = new QVBoxLayout();

    translation_display = new DirectionDotDisplay();
    rotation_display = new DirectionDotDisplay();
    QVBoxLayout* translation_layout = new QVBoxLayout();
    QVBoxLayout* rotation_layout = new QVBoxLayout();
    translation_layout->addWidget(translation_display);
    rotation_layout->addWidget(rotation_display);

    QGroupBox* translation_box = new QGroupBox("Translation Input", this);
    translation_box->setLayout(translation_layout);

    QGroupBox* rotation_box = new QGroupBox("Rotation Input", this);
    rotation_box->setLayout(rotation_layout);

    vlayout->addWidget(translation_box);
    vlayout->addWidget(rotation_box);

	low_gear_checkbox = new QCheckBox("Engage Low Gear", this);
	connect(low_gear_checkbox, &QCheckBox::stateChanged, this, &SpaceExplorerFilePoseProducer::lowGearChecked);
	vlayout->addWidget(low_gear_checkbox);

    // Parse the file if provided
    if (opt.filename) {

        std::ifstream input_file(opt.filename);

        if (input_file.is_open()) {

            bool success = true;
            std::string tmp_string;

            std::getline(input_file, tmp_string);
			int read = std::sscanf(tmp_string.c_str(), "rate %lf\n", &trajectory_rate);
			if (read != 1 || trajectory_rate <= 0.0) {
				std::cerr << "SPACE_EXPLORER_FILE_POSE_PRODUCER: There was an error parsing the file - Make sure the first line is \"rate #\" where '#' is replaced "
							 "with a number larger than 0.0 which represents the rate of commands for the file. ERROR" << std::endl;
				success = false;
			}

			if (success) {

				// Make sure the multiplier is valid
				if (opt.trajectory_rate_multiplier <= 0.0) {
					opt.trajectory_rate_multiplier = 1.0;
					std::cerr << "SPACE_EXPLORER_FILE_POSE_PRODUCER: The provided trajectory rate multiplier must be larger than 0.0. Setting it to 1.0. WARNING" << std::endl;
				}

                while (std::getline(input_file, tmp_string)) {

                    Command new_command;
					int read = std::sscanf(tmp_string.c_str(), "%f %f %f %f %f %f\n",
                                                               &new_command.x, &new_command.y, &new_command.z,
                                                               &new_command.rx, &new_command.ry, &new_command.rz);

					if (read != 6) {
                        command_list.clear();
						std::cerr << "SPACE_EXPLORER_FILE_POSE_PRODUCER: There was an error parsing the file - make sure the elements are delimited by a space and there are at least six elements per line. ERROR" << std::endl;
                        success = false;
                        break;
                    }

                    command_list.push_back(new_command);

                }

                if (success) {
                    trajectory_button = new SwitcherButton("START Trajectory", "STOP Trajectory");
                    vlayout->addWidget(trajectory_button);
                }

            }

            input_file.close();

        }
		else
			std::cerr << "SPACE_EXPLORER_FILE_POSE_PRODUCER: Could not open provided trajectory file: " << opt.filename << " ERROR" << std::endl;

    }

    setLayout(vlayout);

    // Connect the SpaceExplorer
    connect(&explorer, &SpaceExplorer::newInput, this, &SpaceExplorerFilePoseProducer::receiveInput);

	// Set the starting pose
    currentPose = opt.startingPose;

    translation_stepsize = opt.translation_speed / opt.updateRate;
    rotation_stepsize = opt.rotation_speed / opt.updateRate;
	translation_low_gear_stepsize = opt.translation_speed_low_gear / opt.updateRate;
	rotation_low_gear_stepsize = opt.rotation_speed_low_gear / opt.updateRate;
	current_translation_stepsize = translation_stepsize;
	current_rotation_stepsize = rotation_stepsize;

	// Verify axis choice is valid
	if (opt.axisForPose5DOF < 0 || opt.axisForPose5DOF > 2) {
		std::cerr << "SPACE_EXPLORER_FILE_POSE_PRODUCER: axisForPose5DOF option is not 0, 1, or 2. Defaulting to 2. WARNING" << std::endl;
		opt.axisForPose5DOF = 2;
	}

	// Start the producer
    updateThread.startTicking(*this, &SpaceExplorerFilePoseProducer::update, 1000.0 / opt.updateRate, "SpaceExplorerFilePoseProducer");

}


/**
 * @brief This function gets connected to the SpaceExplorer::newInput() signal. It is responsible for parsing
 * the SpaceExplorer counts, checking thresholds, and updating the threshold indicators on the widget.
 * @param x
 * @param y
 * @param z
 * @param rx
 * @param ry
 * @param rz
 */
void SpaceExplorerFilePoseProducer::receiveInput(int x, int y, int z, int rx, int ry, int rz) {

	// Zero values that are not at or above the threshold
	if (abs(x) < opt.counts_threshold)
	x = 0;
	if (abs(y) < opt.counts_threshold)
	y = 0;
	if (abs(z) < opt.counts_threshold)
	z = 0;
	if (abs(rx) < opt.counts_threshold)
	rx = 0;
	if (abs(ry) < opt.counts_threshold)
	ry = 0;
	if (abs(rz) < opt.counts_threshold)
	rz = 0;

	// Store the values
	translation_input << x, y, z;
	rotation_input << rx, ry, rz;

	// Update the threshold indicators on the widget
	translation_display->setDirectionX(x);
	translation_display->setDirectionY(y);
	translation_display->setDirectionZ(z);
	rotation_display->setDirectionX(rx);
	rotation_display->setDirectionY(ry);
	rotation_display->setDirectionZ(rz);

}


/**
 * @brief This function is where the current pose is updated either by the most recent SpaceExplorer data or by the
 * trajectory file.
 * @return
 */
bool SpaceExplorerFilePoseProducer::update() {

    if (trajectory_button && trajectory_button->getState()) {

		// If the trajectory was just started, then initialize the relevant variables
		if (!trajectory_started) {

			command_index = 0;
			trajectoryBasePose = currentPose;
			updateThread.setTickingTimestep(1000.0 / (opt.trajectory_rate_multiplier * trajectory_rate));
			trajectory_started = true;

		}

		// Update the current pose based on the current command
		Command current_command = command_list[command_index];
		currentPose.position = trajectoryBasePose.position + Robo::Vector3d(current_command.x, current_command.y, current_command.z);
		currentPose.orientation = Robo::Rotation(current_command.rx, current_command.ry, current_command.rz) * trajectoryBasePose.orientation;

		// Increment the index
		command_index++;

		// If the index has reached the end of the command list, the trajectory is complete
		if (command_index >= command_list.size()) {

			trajectory_button->click();
			updateThread.setTickingTimestep(1000.0 / opt.updateRate);
			trajectory_started = false;

		}

    }
    else  {

		// If the trajectory was stopped early (via the button), make sure to revert to the main update rate
		if (trajectory_started) {

			updateThread.setTickingTimestep(1000.0 / opt.updateRate);
			trajectory_started = false;

		}

		// Determine the change in position from the Space Explorer input
		Robo::Vector3d translation;
		translation << Robo::sign(translation_input.x()), Robo::sign(translation_input.y()), Robo::sign(translation_input.z());
		translation *= current_translation_stepsize;

		// Determine the change in orientation from the Space Explorer input.
		Robo::Matrixd33 rotation = Robo::I33;
		if (rotation_input.x() != 0)
			rotation = Robo::Rotation(Robo::sign(rotation_input.x()) * current_rotation_stepsize, Robo::UnitX());
		else if (rotation_input.y() != 0)
			rotation = Robo::Rotation(Robo::sign(rotation_input.y()) * current_rotation_stepsize, Robo::UnitY());
		else if (rotation_input.z() != 0)
			rotation = Robo::Rotation(Robo::sign(rotation_input.z()) * current_rotation_stepsize, Robo::UnitZ());

		// Update the current pose
		currentPose.position += translation;
		currentPose.orientation = rotation * currentPose.orientation;

    }

	// Emit the current pose
    emit newPose5DOF(Robo::Pose5DOF(currentPose.position, currentPose.orientation.col(opt.axisForPose5DOF)), Timing::getSysTime());
    emit newPose6DOF(currentPose, Timing::getSysTime());

    return true;

}


void SpaceExplorerFilePoseProducer::lowGearChecked(bool isChecked) {

	if (isChecked) {
		current_translation_stepsize = translation_low_gear_stepsize;
		current_rotation_stepsize = rotation_low_gear_stepsize;
	}
	else {
		current_translation_stepsize = translation_stepsize;
		current_rotation_stepsize = rotation_stepsize;
	}

}


DirectionDotDisplay::DirectionDotDisplay() : QGraphicsView() {

	setFixedSize(190,95);
	setRenderHints(QPainter::Antialiasing);

	setStyleSheet("background: transparent; border-style: none;");

	// Set up the fonts.
	pm_font.setPixelSize(17);
	d_font.setPixelSize(17);
	d_font.setItalic(true);

	int x_zero = 0;
	int y_zero = 25;
	int v_step = 30;
	int h_step = 50;
	int diameter = 25;

	QGraphicsTextItem* tmp;

	scene = new QGraphicsScene(0, 0, 150, 90);

	x_up = scene->addEllipse(x_zero+15, y_zero+0, diameter, diameter);
	tmp = scene->addText("+", pm_font); tmp->setPos(x_zero, y_zero-1);

	x_down = scene->addEllipse(x_zero+15, y_zero+v_step, diameter, diameter);
	tmp = scene->addText("-", pm_font); tmp->setPos(x_zero,y_zero+v_step-1);

	y_up = scene->addEllipse(x_zero+15+h_step, y_zero, diameter, diameter);
	tmp = scene->addText("+", pm_font); tmp->setPos(x_zero+h_step,y_zero-1);

	y_down = scene->addEllipse(x_zero+15+h_step, y_zero+v_step, diameter, diameter);
	tmp = scene->addText("-", pm_font); tmp->setPos(x_zero+h_step,y_zero+v_step-1);

	z_up = scene->addEllipse(x_zero+15+2*h_step, y_zero, diameter, diameter);
	tmp = scene->addText("+", pm_font); tmp->setPos(x_zero+2*h_step,y_zero-1);

	z_down = scene->addEllipse(x_zero+15+2*h_step, y_zero+v_step, diameter, diameter);
	tmp = scene->addText("-", pm_font); tmp->setPos(x_zero+2*h_step,y_zero+v_step-1);

	tmp = scene->addText("X", d_font); tmp->setPos(19, 0);
	tmp = scene->addText("Y", d_font); tmp->setPos(19+h_step, 0);
	tmp = scene->addText("Z", d_font); tmp->setPos(19+2*h_step, 0);

	setDirectionX(0);
	setDirectionY(0);
	setDirectionZ(0);

	setScene(scene);

}


void DirectionDotDisplay::setDirectionX(int val) {

	QColor up_fill_color = Qt::lightGray;
	QColor down_fill_color = Qt::lightGray;
	QColor up_line_color = Qt::gray;
	QColor down_line_color = Qt::gray;

	if (val > 0) {
		up_fill_color = Qt::red;
		up_line_color = Qt::darkRed;
	}
	else if (val < 0) {
		down_fill_color = Qt::red;
		down_line_color = Qt::darkRed;
	}

	x_up->setBrush(QBrush(up_fill_color));
	x_up->setPen(QPen(up_line_color, 1.5));
	x_down->setBrush(QBrush(down_fill_color));
	x_down->setPen(QPen(down_line_color, 1.5));

}


void DirectionDotDisplay::setDirectionY(int val) {

	QColor up_fill_color = Qt::lightGray;
	QColor down_fill_color = Qt::lightGray;
	QColor up_line_color = Qt::gray;
	QColor down_line_color = Qt::gray;

	if (val > 0) {
		up_fill_color = Qt::red;
		up_line_color = Qt::darkRed;
	}
	else if (val < 0) {
		down_fill_color = Qt::red;
		down_line_color = Qt::darkRed;
	}

	y_up->setBrush(QBrush(up_fill_color));
	y_up->setPen(QPen(up_line_color, 1.5));
	y_down->setBrush(QBrush(down_fill_color));
	y_down->setPen(QPen(down_line_color, 1.5));

}


void DirectionDotDisplay::setDirectionZ(int val) {

	QColor up_fill_color = Qt::lightGray;
	QColor down_fill_color = Qt::lightGray;
	QColor up_line_color = Qt::gray;
	QColor down_line_color = Qt::gray;

	if (val > 0) {
		up_fill_color = Qt::red;
		up_line_color = Qt::darkRed;
	}
	else if (val < 0) {
		down_fill_color = Qt::red;
		down_line_color = Qt::darkRed;
	}

	z_up->setBrush(QBrush(up_fill_color));
	z_up->setPen(QPen(up_line_color, 1.5));
	z_down->setBrush(QBrush(down_fill_color));
	z_down->setPen(QPen(down_line_color, 1.5));

}
