#undef Success

#ifndef ROBOTICS_TOOLBOX_H
#define ROBOTICS_TOOLBOX_H

#include <Eigen/Dense>
#include <StateMessage.h>


/**
 * The Robo namespace is meant to contain mathematical structures and supporting functions commonly used in robotics.
 *
 * It contains for example the following functionality and data structures:
 *
 * - a simple signum function
 * - definitions for points (2D), vectors (3D), and matrices
 * - definitions for making skew matrices, rotation matrices, diagonal matrices, etc.
 * - definitions for converting between various representations of 3D orientation.
 * - definitions of 3D rigid body state and various combinations of the parts of such a state
 *
 * The rigid body state classes are designed in such a way that they can easily be cast to types that they contain.
 * For example, a Pose6DOF contains a Position part and an Orientation part. This means that wherever a Position or
 * an Orientation is required, a Pose6DOF can also be used (because it <b>is</b> a Position and it also <b>is</b> an
 * Orientation). This comes with a small awkward syntactical cost which is that when you have a rigid body state that
 * is its own unit (i.e. it does not inherit from other rigid body states, e.g. Position and Orientation), you still
 * have to use the member variable to access the actual data. In other words, a Position is <b>not</b> a Robo::Vector3d.
 * It <b>contains</b> a Robo::Vector3d and to access it, you need to use the `position` member variable. This can feel
 * redundant syntactically, but it is definitely worth the polymorphic advantages.
 */
namespace Robo {

	// Eigen library matrix type redefinitions
	using Vector2d  = Eigen::Matrix<double, 2,  1, Eigen::DontAlign>;
	using Vector3d  = Eigen::Matrix<double, 3,  1, Eigen::DontAlign>;
	using Vector3i  = Eigen::Matrix<int,    3,  1, Eigen::DontAlign>;
	using Vector4d  = Eigen::Matrix<double, 4,  1, Eigen::DontAlign>;
	using Vector5d  = Eigen::Matrix<double, 5,  1, Eigen::DontAlign>;
	using Vector6d  = Eigen::Matrix<double, 6,  1, Eigen::DontAlign>;
	using Vector7d  = Eigen::Matrix<double, 7,  1, Eigen::DontAlign>;
	using Vector8d  = Eigen::Matrix<double, 8,  1, Eigen::DontAlign>;
	using Vector9d  = Eigen::Matrix<double, 9,  1, Eigen::DontAlign>;
	using Vector10d = Eigen::Matrix<double, 10, 1, Eigen::DontAlign>;
	using Vector11d = Eigen::Matrix<double, 11, 1, Eigen::DontAlign>;
	using Vector12d = Eigen::Matrix<double, 12, 1, Eigen::DontAlign>;
	using Matrixd22 = Eigen::Matrix<double, 2,  2, Eigen::DontAlign>;
	using Matrixd33 = Eigen::Matrix<double, 3,  3, Eigen::DontAlign>;
	using Matrixd34 = Eigen::Matrix<double, 3,  4, Eigen::DontAlign>;
	using Matrixd36 = Eigen::Matrix<double, 3,  6, Eigen::DontAlign>;
	using Matrixd37 = Eigen::Matrix<double, 3,  7, Eigen::DontAlign>;
	using Matrixd43 = Eigen::Matrix<double, 4,  3, Eigen::DontAlign>;
	using Matrixd44 = Eigen::Matrix<double, 4,  4, Eigen::DontAlign>;
	using Matrixd66 = Eigen::Matrix<double, 6,  6, Eigen::DontAlign>;
	using Matrixd63 = Eigen::Matrix<double, 6,  3, Eigen::DontAlign>;
	using Matrixd64 = Eigen::Matrix<double, 6,  4, Eigen::DontAlign>;
	using Matrixd67 = Eigen::Matrix<double, 6,  7, Eigen::DontAlign>;
	using Matrixd69 = Eigen::Matrix<double, 6,  9, Eigen::DontAlign>;
	using Matrixd76 = Eigen::Matrix<double, 7,  6, Eigen::DontAlign>;
	using Matrixd77 = Eigen::Matrix<double, 7,  7, Eigen::DontAlign>;
	using Matrixd78 = Eigen::Matrix<double, 7,  8, Eigen::DontAlign>;
	using Matrixd96 = Eigen::Matrix<double, 9,  6, Eigen::DontAlign>;
	using Matrixd99 = Eigen::Matrix<double, 9,  9, Eigen::DontAlign>;

	// Convenience Identity matrices
	const Matrixd22 I22 = Matrixd22::Identity();
	const Matrixd33 I33 = Matrixd33::Identity();
	const Matrixd44 I44 = Matrixd44::Identity();
	const Matrixd66 I66 = Matrixd66::Identity();
	const Matrixd77 I77 = Matrixd77::Identity();
	const Matrixd99 I99 = Matrixd99::Identity();

	/**
	 * @brief The UnitVector3d class represents a 3D unit vector. When assigning the value
	 * of this vector, either through the constructor or the assignment operator, the vector
	 * is normalized such that this vector is always unit length. This class is an extension
	 * to Robo::Vector3d.
	 */
	class UnitVector3d : public Vector3d {

		public:
			UnitVector3d(const Vector3d& vec = Vector3d::Zero()) : Vector3d(vec.normalized()) {}
			UnitVector3d& operator=(const Vector3d& vec) { *static_cast<Vector3d*>(this) = vec.normalized(); return *this; }

	};

	Matrixd33 Skew(const Vector3d& v);
	Matrixd33 Rotation(double angle, const Vector3d& axis);
	Matrixd33 Rotation(double rx, double ry, double rz);
	Matrixd33 Diagonal(double m11, double m22, double m33);
	Vector3d Zero();
	Vector3d UnitX();
	Vector3d UnitY();
	Vector3d UnitZ();

	Vector3d RotationToAngleAxis(const Matrixd33& R);
	Matrixd33 AngleAxisToRotation(const Eigen::Vector3d& k, double angle);
	Vector4d AngleAxisToQuaternion(const Vector3d& k, double angle);
	Matrixd33 QuaternionToRotation(const Vector4d& q);
	Vector4d qMult(const Vector4d& q1, const Vector4d& q2);
	Vector3d qRotate(const Vector4d& q, const Vector3d& v);
	Vector3d qInvRotate(const Vector4d& q, const Vector3d& v);


	/**
	 * @brief A simple implementation of the signum function. Returns 1 if value > 0,
	 * returns -1 if value < 0, and returns 0 if value == 0.
	 * @param value Value for which to get the sign
	 * @return -1, 0, or 1 representing the sign of the value
	 */
	template<typename T>
	T sign(const T& value) {

		return (value > 0) - (value < 0);

	}


	/**
	 * @brief Returns the sign of a given value as a '+' or '-' character. If the value is
	 * 0, it returns '+'.
	 * @param value Value for which to get the sign as a character
	 * @return '+' or '-' depending on the sign of the value
	 */
	template<typename T>
	char signChar(const T& value) {

		if (((value > 0) - (value < 0)) < 0)
			return '-';
		else return '+';

	}


	/**
	 * @brief Saturates the given vector to the desired value. In other words, makes sure the
	 * intput vector has a magnitude no greater than the saturation value. If the
	 * magnitude is not larger than the saturation value, no change is made to the
	 * vector. This function expects a vector created from the Eigen library. It is assumed
	 * that the saturation value is positive. Also keep in mind that if you want to use an
	 * expression for the value of input_vector, you will need to call the eval() function
	 * on the expression (with parenthesis) or this template cannot be instantiated. See the
	 * information on "lazy evaluation" in the Eigen documentation for an explanation of why
	 * this is needed.
	 * @param input_vector Vector whose magnitude must be limited (saturated)
	 * @param saturation_value Maximum magnitude of the vector
	 * @return A vector whose magnitude has been limited by the saturation value
	 */
	template<typename T>
	T saturate(const T& input_vector, const typename T::Scalar& saturation_value) {

		if (input_vector.squaredNorm() > saturation_value * saturation_value)
			return input_vector.normalized() * saturation_value;
		return input_vector;

	}


	/**
	 * @brief Saturates the given value to the desired value. If the absolute value of the input
	 * value is larger than the saturation value, the saturation value is returned with the
	 * same sign as the input value. Otherwise, the input value is returned unchanged.
	 * It is assumed that the saturation value is positive.
	 * @param input_value
	 * @param saturation_value
	 * @return
	 */
	template<typename T>
	T saturate(const T& input_value, const T& saturation_value) {

		if (fabs(input_value) > saturation_value)
			return sign(input_value) * saturation_value;
		return input_value;

	}

	/**
	 * Template specialization of saturate()
	 */
	template<> Vector3d saturate(const Vector3d& input_vector, const Vector3d& saturation_vector);


	/**
	 * @brief Returns true if the input is NaN (Not a Number).
	 * @param value Value to check
	 * @return True of input is NaN
	 */
	template<typename T>
	bool isNaN(const T& value) {

		return !(value == value);

	}


	/**
	 * @brief The LinearlyInterpolatableState struct is an abstract struct that is inherited by robot workspace state types that are capable of linear interpolation.
	 * This struct uses the Curiously Recurring Template Pattern. That is, the derived struct should pass its own name as the template parameter when inheriting this
	 * struct.
	 */
	template<typename T>
	struct LinearlyInterpolatableState {

		LinearlyInterpolatableState() = default;
		LinearlyInterpolatableState(const LinearlyInterpolatableState&) = default;
		virtual ~LinearlyInterpolatableState() = default;

		/**
		 * @brief Returns the next LinearlyInterpolatableState after a single linear interpolation step of time_step toward the final LinearlyInterpolatableState given
		 * at the specified rate. If a particular rate does not make sense for the actual type, then it is ignored.
		 *
		 * Note that the parts of a LinearlyInterpolatableState may arrive at the final state at different times because the rate of change of each component can be
		 * specified independently as well as the fact that the "distance" each component needs to travel is likely not the same. In other words, this function does not
		 * synchronize the transition of components to take the same amount of time regardless of the magnitude of their respective errors.
		 * @param final Goal LinearlyInterpolatableState for the interpolation.
		 * @param translation_speed Constant translational speed in mm/s during interpolation in LinearlyInterpolatableState objects which have a position.
		 * If the LinearlyInterpolatableState does not have a positional component, this parameter is ignored.
		 * @param rotation_speed Constant rotation speed in rad/s during interpolation in LinearlyInterpolatableState objects which have an orientation component.
		 * If the LinearlyInterpolatableState does not have an orientation component, this parameter is ignored.
		 * @param acceleration Constant translational acceleration in mm/s^2 during interpolation in LinearlyInterpolatableState objects which have a translational velocity.
		 * If the LinearlyInterpolatableState does not have a translational velocity component, this parameter is ignored.
		 * @param angular_acceleration Constant angular acceleration in rad/s^2 during interpolation in LinearlyInterpolatableState objects which have an angular velocity.
		 * If the LinearlyInterpolatableState does not have an angular velocity component, this parameter is ignored.
		 * @param time_step Time step for the interpolation (non-negative)
		 * @return The new LinearlyInterpolatableState after a single step of linear interpolation toward the final LinearlyInterpolatableState.
		 */
		virtual T interpolate(const T& final,
							  double translation_speed /* mm/s */,
							  double rotation_speed /* rad/s */,
							  double acceleration /* mm/s^2 */,
							  double angular_acceleration /* rad/s^2 */,
							  double time_step /* s */) const = 0;

		/**
		 * @brief Returns `true` if this LinearlyInterpolatableState is near enough to the final LinearlyInterpolatableState as specified by the thresholds.
		 * If a particular threshold does not make sense for the actual type, then it is ignored.
		 * @param final Goal LinearlyInterpolatableState to compare this LinearlyInterpolatableState against
		 * @param translation_threshold Positional threshold in millimeters. If the LinearlyInterpolatableState does not have a positional component, this
		 * parameter is ignored.
		 * @param angular_threshold Angular threshold in radians. If the LinearlyInterpolatableState does not have an angular component, this
		 * parameter is ignored.
		 * @param vel_threshold Positional velocity threshold in mm/s. If the LinearlyInterpolatableState does not have a positional velocity component, this
		 * parameter is ignored.
		 * @param angularVel_threshold Angular velocity threshold in rad/s.  If the LinearlyInterpolatableState does not have an angular velocity component, this
		 * parameter is ignored.
		 * @return True if this LinearlyInterpolatableState is near the final LinearlyInterpolatableState given the thresholds
		 */
		virtual bool hasArrived(const T& final,
								double translation_threshold /* mm */,
								double angular_threshold /* rad */,
								double vel_threshold /* mm/s */,
								double angularVel_threshold /* rad/s */) const = 0;

	};


	/**
	 * @brief The Position struct represents a 3D position. Position components are in millimeters.
	 */
	struct Position : public LinearlyInterpolatableState<Position>, public virtual StateMessage {

		Vector3d position; // mm

		Position(const Vector3d& pos = Vector3d::Zero() /* mm */) : position(pos) {}
		Position(const Position&) = default;
		Position& operator=(const Position&) = default;

		/**
		 * @brief Computes the distance between two Position objects in millimeters.
		 * @param other The other Position.
		 * @return the distance between the two Position objects in millimeters (always positive).
		 */
		double /* mm */ distance(const Position& other) const { return (position - other.position).norm(); }

		Position interpolate(const Position& final,
							 double translation_speed /* mm/s */,
							 double rotation_speed /* rad/s */,
							 double acceleration /* mm/s^2 */,
							 double angular_acceleration /* rad/s^2 */,
							 double time_step /* s */) const override final;

		bool hasArrived(const Position& final,
						double translation_threshold /* mm */,
						double angular_threshold /* rad */,
						double vel_threshold /* mm/s */,
						double angularVel_threshold /* rad/s */) const override final;

		Position projectToLine(const Vector3d& line_offset, const Vector3d& line_direction) const;
		Position projectToPlane(const Vector3d& plane_offset, const Vector3d& plane_normal) const;

		Position& operator=(const Vector3d& other) { position = other; return *this;}

		friend std::ostream& operator<<(std::ostream& stream, const Position& position);

	private:
		STATE_MESSAGE

	};
	

	/**
	 * @brief The Heading struct represents a 3D vector heading. A heading is a 2-DOF vector even though it
	 * is described by a 3D vector. This is because the magnitude of the vector describing the heading does
	 * not change the heading.
	 */
	struct Heading : public LinearlyInterpolatableState<Heading>, public virtual StateMessage {

		UnitVector3d heading;

		Heading(const Vector3d& heading = Vector3d::UnitX()) : heading(heading) {}
		Heading(const Heading&) = default;
		Heading& operator=(const Heading&) = default;

		double /* rad */ distance(const Heading& other) const;

		Heading interpolate(const Heading& final,
							double translation_speed /* mm/s */,
							double rotation_speed /* rad/s */,
							double acceleration /* mm/s^2 */,
							double angular_acceleration /* rad/s^2 */,
							double time_step /* s */) const override final;

		bool hasArrived(const Heading& final,
						double translation_threshold /* mm */,
						double angular_threshold /* rad */,
						double vel_threshold /* mm/s */,
						double angularVel_threshold /* rad/s */) const override final;

		Heading& operator=(const Vector3d& other) { heading = other; return *this; }

	private:
		STATE_MESSAGE

	};


	/**
	 * @brief The Orientation struct represents a 3D orientation. This representation is kept in the form
	 * of a rotation matrix.
	 */
	struct Orientation : public LinearlyInterpolatableState<Orientation>, public virtual StateMessage {

		Matrixd33 orientation;

		Orientation(const Matrixd33& matrix = I33) : orientation(matrix) {}
		Orientation(const Orientation&) = default;
		Orientation& operator=(const Orientation&) = default;

		double /* rad */ distance(const Orientation& other) const;

		Orientation interpolate(const Orientation& final,
								double translation_speed /* mm/s */,
								double rotation_speed /* rad/s */,
								double acceleration /* mm/s^2 */,
								double angular_acceleration /* rad/s^2 */,
								double time_step /* s */) const override final;

		bool hasArrived(const Orientation& final,
						double translation_threshold /* mm */,
						double angular_threshold /* rad */,
						double vel_threshold /* mm/s */,
						double angularVel_threshold /* rad/s */) const override final;

		Orientation& operator=(const Matrixd33& other) { orientation = other; return *this; }

	private:
		STATE_MESSAGE_RECORD_ONLY
		
	};


	/**
	 * @brief The AngularVelocity struct represents a 3D angular velocity vector. Units are in radians per second.
	 */
	struct AngularVelocity : public LinearlyInterpolatableState<AngularVelocity>, public virtual StateMessage {

		Vector3d angularVelocity; // rad/s

		AngularVelocity(const Vector3d& angular_velocity = Vector3d::Zero()) : angularVelocity(angular_velocity) {}
		AngularVelocity(const AngularVelocity&) = default;
		AngularVelocity& operator=(const AngularVelocity&) = default;

		AngularVelocity interpolate(const AngularVelocity& final,
									double translation_speed /* mm/s */,
									double rotation_speed /* rad/s */,
									double acceleration /* mm/s^2 */,
									double angular_acceleration /* rad/s^2 */,
									double time_step /* s */) const override final;

		bool hasArrived(const AngularVelocity& final,
						double translation_threshold /* mm */,
						double angular_threshold /* rad */,
						double vel_threshold /* mm/s */,
						double angularVel_threshold /* rad/s */) const override final;

		AngularVelocity& operator=(const Vector3d& other) { angularVelocity = other; return *this; }

	private:
		STATE_MESSAGE

	};

	
	/**
	 * @brief The Pose5DOF struct represents a 5 degree of freedom pose with a Position and a Heading. Position is in
	 * millimeters.
	 */
	struct Pose5DOF : public virtual Position, public virtual Heading, public LinearlyInterpolatableState<Pose5DOF>, public virtual StateMessage {

		Pose5DOF() = default;
		Pose5DOF(const Vector3d& pos /* mm */, const Vector3d& direction)
			: Position(pos), Heading(direction) {}
		Pose5DOF(const Position& pos /* mm */, const Heading& direction)
			: Pose5DOF(pos.position, direction.heading) {}

		using Position::distance;
		using Heading::distance;

		using Position::interpolate;
		using Heading::interpolate;
		Pose5DOF interpolate(const Pose5DOF& final,
							 double translation_speed /* mm/s */,
							 double rotation_speed /* rad/s */,
							 double acceleration /* mm/s^2 */,
							 double angular_acceleration /* rad/s^2 */,
							 double time_step /* s */) const override final;

		using Position::hasArrived;
		using Heading::hasArrived;
		bool hasArrived(const Pose5DOF& final,
						double translation_threshold /* mm */,
						double angular_threshold /* rad */,
						double vel_threshold /* mm/s */,
						double angularVel_threshold /* rad/s */) const override final;

		friend std::ostream& operator<<(std::ostream& stream, const Pose5DOF& pose5DOF);

	private:
		STATE_MESSAGE

	};


	/**
	 * @brief The Pose6DOF struct represents a full 6 degree of freedom pose with a Position and an Orientation. Position is in
	 * millimeters.
	 */
	struct Pose6DOF : public virtual Position, public virtual Orientation, public LinearlyInterpolatableState<Pose6DOF>, public virtual StateMessage {

		Pose6DOF() = default;
		Pose6DOF(const Vector3d& pos /* mm */, const Matrixd33& orientMatrix)
			: Position(pos), Orientation(orientMatrix) {}
		Pose6DOF(const Position& pos /* mm */, const Orientation& orientMatrix)
			: Pose6DOF(pos.position, orientMatrix.orientation) {}

		using Position::distance;
		using Orientation::distance;

		using Position::interpolate;
		using Orientation::interpolate;
		Pose6DOF interpolate(const Pose6DOF& final,
							 double translation_speed /* mm/s */,
							 double rotation_speed /* rad/s */,
							 double acceleration /* mm/s^2 */,
							 double angular_acceleration /* rad/s^2 */,
							 double time_step /* s */) const override final;

		using Position::hasArrived;
		using Orientation::hasArrived;
		bool hasArrived(const Pose6DOF& final,
						double translation_threshold /* mm */,
						double angular_threshold /* rad */,
						double vel_threshold /* mm/s */,
						double angularVel_threshold /* rad/s */) const override final;

		friend std::ostream& operator<<(std::ostream& stream, const Pose6DOF& pose6DOF);

	private:
		STATE_MESSAGE

	};


	/**
	 * @brief The RotatingPosition struct represents a 6 degree of freedom rigid body state with a position and a 3 DOF angular velocity.
	 * Position is in millimeters and angular velocity is in rad/s.
	 */
	struct RotatingPosition : public virtual Position, public virtual AngularVelocity, public LinearlyInterpolatableState<RotatingPosition>, public virtual StateMessage {

		RotatingPosition() = default;
		RotatingPosition(const Vector3d& pos /* mm */, const Vector3d& angular_velocity)
			: Position(pos), AngularVelocity(angular_velocity) {}
		RotatingPosition(const Position& pos /* mm */, const AngularVelocity& angular_velocity)
			: RotatingPosition(pos.position, angular_velocity.angularVelocity) {}

		using Position::interpolate;
		using AngularVelocity::interpolate;
		RotatingPosition interpolate(const RotatingPosition& final,
									 double translation_speed /* mm/s */,
									 double rotation_speed /* rad/s */,
									 double acceleration /* mm/s^2 */,
									 double angular_acceleration /* rad/s^2 */,
									 double time_step /* s */) const override final;

		using Position::hasArrived;
		using AngularVelocity::hasArrived;
		bool hasArrived(const RotatingPosition& final,
						double translation_threshold /* mm */,
						double angular_threshold /* rad */,
						double vel_threshold /* mm/s */,
						double angularVel_threshold /* rad/s */) const override final;

		friend std::ostream& operator<<(std::ostream& stream, const RotatingPosition& rotatingPosition);

	private:
		STATE_MESSAGE

	};


	/**
	 * @brief The RotatingPose5DOF struct represents an 8 degree of freedom rigid body state with a position, a 3 DOF angular velocity, and a
	 * 2 DOF heading. Position is in millimeters and angular velocity is in rad/s.
	 */
	struct RotatingPose5DOF : public virtual Pose5DOF, public virtual RotatingPosition, public virtual StateMessage {

		RotatingPose5DOF() = default;
		RotatingPose5DOF(const Vector3d& pos /* mm */, const Vector3d& direction, const Vector3d& angular_vel)
			: Position(pos), Heading(direction), AngularVelocity(angular_vel) {}
		RotatingPose5DOF(const Position& pos /* mm */, const Heading& direction, const AngularVelocity& angular_vel)
			: RotatingPose5DOF(pos.position, direction.heading, angular_vel.angularVelocity) {}

		using Position::interpolate;
		using Heading::interpolate;
		using AngularVelocity::interpolate;
		using Pose5DOF::interpolate;
		using RotatingPosition::interpolate;

		using Position::hasArrived;
		using Heading::hasArrived;
		using AngularVelocity::hasArrived;
		using Pose5DOF::hasArrived;
		using RotatingPosition::hasArrived;

	private:
		STATE_MESSAGE

	};


} // End Robo namespace

// Declare Robo types for the Qt metatype system
DECLARE_STATE_MESSAGE_TYPE(Robo::Position)
DECLARE_STATE_MESSAGE_TYPE(Robo::Heading)
DECLARE_STATE_MESSAGE_TYPE(Robo::Orientation)
DECLARE_STATE_MESSAGE_TYPE(Robo::AngularVelocity)
DECLARE_STATE_MESSAGE_TYPE(Robo::Pose6DOF)
DECLARE_STATE_MESSAGE_TYPE(Robo::Pose5DOF)
DECLARE_STATE_MESSAGE_TYPE(Robo::RotatingPosition)
DECLARE_STATE_MESSAGE_TYPE(Robo::RotatingPose5DOF)

#endif // ROBOTICS_TOOLBOX_H
