#ifndef STATE_DATA_UI_H
#define STATE_DATA_UI_H

#include <vector>

#include <QtCore>
#include <QtWidgets>

#include <QtGUIElements/qcustomplot.h>
#include <QtGUIElements/Labels.h>
#include <StateMessage.h>
#include <Utilities/Recorder.h>


class DataReceiver; // Declaration needed to resolve circular dependency

/**
 * @brief The StateDataUI class is a dynamic user interface for recording and visualization of data flowing through your program. It is designed
 * to accept messages through the Qt signal-slot paradigm.
 *
 * This class is a user interface which can record and plot user data in real-time as it is flowing through your program. The messages it receives
 * are simple structs (or classes) that must implement the StateMessage interface (i.e. they must inherit from StateMessage). The StateMessage
 * interface repackages the internal data of the message in a standard format such that it can be consumed for recording and visualization. Messages
 * can be configured according to what makes sense for the underlying data. For example, if it doesn't make sense to plot the data, then the message
 * can be configured for recording only. StateDataUI will dynamically adjust the user interface based on how the messages it receives are configured.
 * The user only needs to worry about implementing the StateMessage interface (i.e. configuring their custom message) and connecting signals which
 * produce these messages to StateDataUI. StateDataUI will handle the rest automatically. Please see the documentation of StateMessage for details on
 * how you can create your own custom StateMessage classes.
 *
 * Qt signals that send StateMessage messages are connected to StateDataUI through receiver sockets implemented by the DataReceiver class. Each
 * StateMessage signal can be connected to a single receiver socket (i.e. DataReceiver instance) by connecting the signal to its DataReceiver::receive()
 * slot. As such, the first parameter of a StateMessage signal must be a const reference to a struct or class that inherits from StateMessage
 * (StateMessage is an abstract class and therefore cannot be instantiated). The const reference is important because the type of the message will be
 * implicitly converted to a StateMessage to call the DataReceiver::receive() slot and Qt only allows implicit conversion of parameters when they are
 * const references. This also means that you must use the functor-based form of QObject::connect() (as in the example below) and not the string-based
 * one. See Qt documentation for details. The second parameter must be of type long and represents the timestamp in milliseconds associated with the
 * message. This timestamp must correspond to the raw system clock time when the data was "captured". Do not "zero" the timestamp relative to the time
 * the program started or anything similar to it. StateDataUI has a mechanism of recording "zeroed" timestamps relative to the start time of the program
 * if you desire. This is done with the registerZeroTimestamp() function. The signal can have more parameters in addition to these first two, if you
 * so desire, because Qt allows connecting signals to slots with fewer parameters than the signal.
 *
 * When a StateDataUI is first created, it has no receiver sockets (DataReceiver objects). The DataReceiver class is <b>not</b> meant to be constructed
 * by anyone other than StateDataUI. In fact, its constructor is private. In order to connect your StateMessage signal to StateDataUI, you must first add
 * a receiver socket using operator[](). Then you can connect your signal to the new socket's DataReceiver::receive() slot. The operator[]() function is
 * designed such that creating the receiver socket and connecting to it is done at the same time in a single line. Below is an example of a class called
 * `Sender` that has a StateMessage signal and how that signal gets connected to StateDataUI.
 *
 * Sender.h file:
 *
 * ~~~{.cpp}
 * // Sender is a QObject which has a StateMessage signal that sends a custom StateMessage called MyCustomStateMessage
 * // in a Qt signal every time it reads new data. See the documentation for StateMessage for the example implementation
 * // of MyCustomStateMessage.
 * class Sender : QObject {
 *
 *		Q_OBJECT
 *
 *	private:
 *		QTimer* timer;
 *		void readData();
 *
 *	public:
 *		Sender() {
 *			// Register MyCustomStateMessage for queued Qt signal-slot connections
 *			qRegisterMetaType<MyCustomStateMessage>();
 *		}
 *
 *	signals:
 *		void sendStateMessage(const MyCustomStateMessage& msg, long timestamp); // signal follows guidelines above
 *
 * };
 * ~~~
 *
 * Sender.cpp file:
 *
 * ~~~{.cpp}
 * Sender::Sender() {
 *
 *		timer = new QTimer(this);
 *		connect(timer, &QTimer::timeout, this, Sender::readData); // Can connect to any function, not just slots
 *		timer->start(100); // Read data every 100 ms
 *
 * }
 *
 * void Sender::readData() {
 *
 *		MyCustomStateMessage msg;
 *
 *		// Read the data and put the data into the MyCustomStateMessage object...
 *
 *		emit sendStateMessage(msg, Timing::getSysTime());
 *
 * }
 * ~~~
 *
 * In main():
 *
 * ~~~{.cpp}
 * Sender s;
 *
 * StateDataUI stateUI;
 *
 * // Connect Sender::sendStateMessage() to the StateDataUI on a new receiver socket named "Receiver Name"
 * QObject::connect(&s, &Sender::sendStateMessage, stateUI["Receiver Name"], &DataReceiver::receive);
 * ~~~
 *
 * This is how you should connect every StateMessage signal to StateDataUI. The parameter passed to operator[]() is a name for the receiver socket and is
 * used to identify it within the user interface. If a receiver socket has already been created with a given name, operator[]() returns a pointer to a
 * dummy DataReceiver object, not a pointer to the existing DataReceiver with that name. This is important to prevent accidental connection of multiple
 * signals to a single receiver socket (which can easily happen with careless copy and pasting). You will receive a message on the command line if you
 * attempt to connect multiple signals to the same receiver socket.
 *
 * The user interface allows you to start and stop recording via the recording button and control which receiver sockets participate in recording via their
 * respective recording checkboxes. All data is recorded to StateData.txt in the master recording directory of the Recorder class. See Recorder documentation
 * for details. Each line of StateData.txt corresponds to a single received message and has the following format:
 *
 *     <timestamp>:<receiver socket ID>:<data metatype ID>:<state message line as implemented by the message creator>
 *
 * The receiver socket ID and the data metatype ID are numbers automatically assigned to each receiver socket and each custom StateMessage involved in the
 * recording. These mappings are logged in DataReceiverNameIDMappings.txt and TypeIDMappings.txt respectively. These files are also written to the master
 * recording directory.
 *
 * It is possible to adjust the length of time shown in the rolling plotting window at runtime using setPlottingWindowSize(). It is also possible to
 * adjust the timestep used to update the UI, which affects the sampling rate for plotting, at runtime using setTimestepUI(). Note that this timestep has
 * no effect on data recording whatsoever.
 *
 */
class StateDataUI : public QWidget {

	Q_OBJECT

	public:
		StateDataUI(bool recordOnStart = false, int timestepUI_ms = 50, int plottingWindowSize = 40, QWidget* parent = nullptr);
		~StateDataUI() { delete defaultWidgetParent; }

		static void registerZeroTimestamp();

		DataReceiver* operator[](const char* receiverName);

		/**
		 * @brief Returns the size of the rolling plotting window in seconds (i.e. how many seconds of plotting history
		 * are currently being shown?).
		 * @return
		 */
		int plottingWindowSize() const { return m_plottingWindowSize; }
		/**
		 * @brief Returns the timestep currently used to update the UI in milliseconds.
		 * @return
		 */
		int timestepUI() const { return updateTimer->interval(); }
	public slots:
		/**
		 * @brief Sets the size of the rolling plotting window in seconds (i.e. how many seconds of plotting history do
		 * you want to see on the screen?). This function is not thread-safe. To safely call this function from another
		 * thread, a `Qt::QueuedConnection` or `Qt::AutoConnection` can be used.
		 * @param size The desired size of the plotting window in seconds
		 */
		void setPlottingWindowSize(int size) { m_plottingWindowSize = size; }
		/**
		 * @brief Sets the timestep used to update the UI. This timestep affects the sampling rate for plotting, it does
		 * not affect data recording whatsoever. This function is not thread-safe. To safely call this function from
		 * another thread, a `Qt::QueuedConnection` or `Qt::AutoConnection` can be used.
		 * @param timestepUI_ms The desired timestep in milliseconds
		 */
		void setTimestepUI(int timestepUI_ms) { updateTimer->setInterval(timestepUI_ms); }

	private:
		QHash<QString, DataReceiver*> receivers;
		QWidget* defaultWidgetParent;			// See comment in constructor for details
		int nextReceiverID = 0;					// ID to use for the next created DataReceiver
		QSet<int> seenMetaTypeIDs;				// Keeps track of data MetaType IDs that have been seen

		// Plotting related variables
		const std::vector<QPen> defaultPenList; // Default order of graph colors to cycle through
		int m_plottingWindowSize;
		long plottingStartTime_ms;
		static long recordingStartTime_ms;
		QTimer* updateTimer;

		// Recording related variables
		Recorder nameIDMappingRecorder;
		Recorder typeIDMappingRecorder;
		Recorder dataRecorder;
		bool recording;

		// QWidgets
		QTabWidget* plottingTabWidget;
		QVBoxLayout* overallLayout;
		QWidget* receiverTable;
		QGridLayout* receiverTableLayout;
		QScrollArea* receiverTableScrollArea;
		QList<QCheckBox*> checkboxes;
		QPushButton* recordButton;
		CountingLabel* recordingCount;
		CountingLabel* recordedCount;

		friend class DataReceiver; // Allow DataReceiver to access private members of StateDataUI

	private slots:
		void recordButtonPressed();

};


/**
 * @brief The DataReceiver class should be thought of as a socket in the StateDataUI class for receiving StateMessage messages.
 * This class is closely intertwined with StateDataUI and is not meant to be instantiated by anyone other than StateDataUI. The
 * constructor for this class is private because it is only meant to be created by StateDataUI which is a friend to this class.
 * Instances of this class are created indirectly through the use of StateDataUI::operator[].
 *
 * The DataReceiver class accepts StateMessage objects and their associated timestamps through the receive() slot. See StateDataUI
 * for details of how this class is used in conjunction with StateDataUI. Ideally this class would be nested inside StateDataUI to
 * prevent its instantiation, but Qt does not allow nested classes in QObjects.
 */
class DataReceiver : public QWidget {

	Q_OBJECT

	private:
		const char* name;
		const int ID;
		StateDataUI* const ui;
		bool receivedFirstData = false;
		int dataMetaTypeID = 0;				// MetaType ID for the data this DataReceiver receives
		PlottingFormat plottingData;
		std::vector<QCustomPlot*> plots;
		QScrollArea* scrollArea;
		bool recordingEnabled = true;		// Default to enable recording for individual DataReceivers
		const bool isDummy;					// Whether this is a dummy receiver or not

		/**
		 * @brief Constructs a DataReceiver. The constructor for this class is private because it is only meant to be created by
		 * StateDataUI which is a friend to this class. Create DataReceiver objects indirectly through the use of
		 * StateDataUI::operator[].
		 * @param name Unique name for the DataReceiver
		 * @param ID Unique ID for the DataReceiver
		 * @param ui Pointer to the StateDataUI that owns this DataReceiver
		 * @param isDummy Whether the DataReceiver is a dummy receiver or not
		 */
		DataReceiver(const char* name, const int ID, StateDataUI* ui, bool isDummy = false) : name(name), ID(ID), ui(ui), isDummy(isDummy) {}

		friend class StateDataUI;

	protected:
		bool eventFilter(QObject*, QEvent* e) override;

	public slots:
		void receive(const StateMessage& data, long timestamp);
		void updatePlots();
		void enableRecord(int state);

	signals:
		/**
		 * @brief Signal emitted every time the receive() slot is invoked.
		 */
		void messageReceived();
		/**
		 * @brief Signal emitted every time the receive() slot records a message.
		 */
		void recordedMessage();

};

#endif // STATE_DATA_UI_H
