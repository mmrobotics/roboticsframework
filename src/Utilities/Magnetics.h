#ifndef MAGNETICS_H
#define MAGNETICS_H

#include <RoboticsToolbox.h>


namespace Magnetics {

	constexpr double MU_0 = 4 * M_PI * 1e-7; // Permeability of free space (vacuum permeability) [H/m]

	class CapsuleDipolePair {

		private:
			// M refers to the actuator magnet dipole, m refers to the capsule magnet dipole, H = (3*p_hat*p_hat^T - I)
			const double M_norm; // A m^2
			Robo::Vector3d M_hat;
			const double m_norm; // A m^2
			Robo::Vector3d m_hat;

			Robo::Vector3d M_position; // m
			Robo::Vector3d m_position; // m

			double capsule_weight; // N
			double capsule_buoyancy; // N

			static Robo::Matrixd33 computeFMatrix(const Robo::Vector3d& M_hat, const Robo::Vector3d& m_hat, const Robo::Vector3d& p_hat);
			static Robo::Matrixd33 computeHMatrix(const Robo::Vector3d& p_hat);
			static Robo::Matrixd33 computeHInverseMatrix(const Robo::Vector3d& p_hat);

		public:
			CapsuleDipolePair(double M_norm /* A m^2 */, double m_norm  /* A m^2 */);
			CapsuleDipolePair(double capsule_weight, double capsule_buoyancy, double M_norm /* A m^2 */, double m_norm = 0.0 /* A m^2 */);
			CapsuleDipolePair(double capsule_weight, double capsule_buoyancy, const Robo::Vector3d& M_hat, double M_norm /* A m^2 */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero(), double m_norm = 0.0 /* A m^2 */);

			Robo::Vector3d weight() { return -capsule_weight*Robo::UnitZ(); }
			Robo::Vector3d buoyancy() { return capsule_buoyancy*Robo::UnitZ(); }

			Robo::Vector3d M(); /* A m^2 */
			Robo::Vector3d MHat();
			double MNorm(); /* A m^2 */
			Robo::Vector3d MPosition() { return M_position; } /* m */
			void setMHat(const Robo::Vector3d& M_hat);
			void setMPosition(const Robo::Vector3d& M_p /* m */);
			Robo::Vector3d m(); /* A m^2 */
			Robo::Vector3d mHat();
			double mNorm(); /* A m^2 */
			Robo::Vector3d mPosition() { return m_position; }
			void setmHat(const Robo::Vector3d& m_hat);
			void setmPosition(const Robo::Vector3d& m_p /* m */);

			Robo::Vector3d force(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero(), double m_norm = 0.0); // force attracting m toward M
			Robo::Vector3d torque(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero(), double m_norm = 0.0); // torque rotating m toward the field at p (NOT IMPLEMENTED)
			Robo::Vector3d field(const Robo::Vector3d& p /* m */); // the field produced by M at p
			Robo::Vector3d fieldInv(const Robo::Vector3d& p_hat, const Robo::Vector3d& desired); // necessary M to point the field in the desired direction at position p
			Robo::Vector3d axisFwd(const Robo::Vector3d& magnet_axs_hat); // the rotation axis of the field produced by M at p (NOT IMPLEMENTED)
			Robo::Vector3d axisInv(const Robo::Vector3d& field_axis_hat); // compute the rotation axis of M to produce the desired rotation axis of the field (NOT IMPLEMENTED)

			Robo::Matrixd33 force_p_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero(), double m_norm = 0.0); // partial force / partial p
			Robo::Matrixd33 force_m_hat_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero(), double m_norm = 0.0); // partial force / partial m_hat
			Robo::Matrixd33 force_M_hat_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero(), double m_norm = 0.0); // partial force / partial M_hat

			Robo::Matrixd33 field_hat_p_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero()); // partial field_hat / partial p
			Robo::Matrixd33 field_hat_M_hat_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero()); // partial field_hat / partial M_hat

			Robo::Matrixd33 M_hat_field_hat_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero()); // partial M_hat / partial field_hat

			Robo::Matrixd33 M_hat_p_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat = Robo::Vector3d::Zero()); // partial M_hat / partial p

	};

}

#endif // MAGNETICS_H
