#ifndef TIMING_H
#define TIMING_H

#include <ctime>
#include <pthread.h>
#include <iostream>
#include <cstring>
#include <string>
#include <errno.h>

#include <Utilities/Recorder.h>


/**
 * The Timing namespace contains functionality for timing on Linux platforms. Does not currently work on Windows.
 */
namespace Timing {

	long getSysTime();
	double getSysTimeHighPrecision();
	long long getSysTimeNanos();
	bool wait(long milliseconds);


	/**
	 * @brief The CodeStopwatch class is used to profile the runtime of a section of code.
	 *
	 * The intention is that the section of code being profiled is one which will be run repeatedly during the lifetime of your
	 * program such as the contents of a control loop. However, it can be used to profile the runtime of any piece of code provided
	 * you place it in a loop temporarily for profiling purposes. This class will automatically compute runtime statistics (average,
	 * minimum and maximum times) of the section of code being profiled. The resulting times are given in milliseconds with nanosecond
	 * precision. Usage consists of three simple steps:
	 *
	 * 1) Create a CodeStopwatch object somewhere where it will maintain its life during multiple iterations of the code to profile
	 *    (i.e. Don't create the object inside the function you are profiling unless you make it static. If the function is a member
	 *    of a class, then a static variable won't work if you are creating many instances of the class because they will all share
	 *    the instance of CodeStopwatch. In that case, make it a normal member variable of the class).\n
	 * 2) Call start() just before the first line of the section of code.\n
	 * 3) Call stop() just after the last line of the section of code.
	 *
	 * To be clear, the design is that start(), stop() and the code in between get run over and over and CodeStopwatch will profile the
	 * duration of the code between the start() and stop() calls. Therefore, these function calls should only appear once in your code
	 * for a particular instance of CodeStopwatch. You may put whatever code you want between the function calls (including loops,
	 * provided they are not infinite), but you need to provide some mechanism to run the code from start() through to stop() multiple
	 * times.
	 *
	 * This class is NOT thread-safe. Therefore, if multiple threads are running the code from start() to stop() then the profiling
	 * <b>will</b> be incorrect.
	 *
	 * The class will automatically keep track of runtimes of the section of code. Once the section has been run `numSamples` times, then
	 * statistics are computed for that set of samples and will either be printed to the terminal, or written to file. The decision is
	 * made based on whether you provide a filename in the constructor of CodeStopwatch. If a filename is provided, the terminal output is
	 * suppressed. This class uses the Recorder class, so the filename given will be prepended with the master Recorder path. Each set of
	 * statistics is independent (i.e. the average, min, and max are only for the last `numSamples` samples, they are not lifetime stats).
	 * The class also needs a name. This name will be used when printing to the terminal to make it easy to differentiate between instances
	 * of CodeStopwatch if you are profiling multiple sections of code simultaneously.
	 */
	class CodeStopwatch {

		public:
			CodeStopwatch(const std::string& name, int numSamples = 100, const std::string& filename = "");

			void start();
			void stop();

		private:
			std::string name;
			int numSamples;
			int count;
			double prevTime;
			double sum;
			double maxTime;
			double minTime;
			Recorder file;

	};


	/**
	 * @brief The Timer class is a versatile class used for timing with nanosecond precision and can play one of several
	 * different roles which require precise timing.
	 *
	 * The Timer class can play 3 major roles which require precise timing:
	 *
	 * <b>Role 1)</b> It can be used to call a provided function at regular and modifiable time intervals.\n
	 * <b>Role 2)</b> It can be used for measuring the length of time between time instants.\n
	 * <b>Role 3)</b> It can be used to block (or deschedule) the current thread for a specified amount of time.
	 *
	 * If an instance of Timer is being used for Role 1, then <b>DO NOT</b> use it for either of the other two roles. If you
	 * do, you <b>WILL</b> ruin the timing of the calls to the provided function. If you need to use one of the other roles,
	 * create a new instance of Timer for it.
	 *
	 * This class references the system monotonic clock. The system monotonic clock is a nanosecond precision clock that is always
	 * increasing and represents time elapsed since a certain point (usually the computer boot-time). Type `man clock_gettime`
	 * in a terminal for more information on the system monotonic clock.
	 *
	 * Since this class uses the pthread library, you must link the pthread library when compiling with the `-pthread` linker flag.
	 * If you are using an older GCC compiler, then you must also link the real-time library with `-lrt`.
	 *
	 * <h3>Role 1</h3>
	 *
	 * In this form of usage, the class will create a thread which repeatedly calls the provided function at the given time
	 * interval. The interval can be changed while the thread is running. The scheduling priority of the thread itself is
	 * configurable in case the function is important for real-time computation. The scheduling priorities use the Linux Real-Time
	 * priorities. This means you can give your Timer a priority between 0 and 99 where 0 is the lowest and 99 is the highest. The
	 * scheduling policy for any priority other than 0 is SCHED_FIFO. While a higher priority will help your function execute at
	 * more precise intervals by preempting other processes and threads on your computer, there is still some overhead before the
	 * operating system gets around to scheduling the thread calling your function. Type `man sched` in a terminal for documentation
	 * on Linux scheduling policies. To ensure that you have operating system permission to change the thread priority (normal users
	 * don't have the permission by default), you need to add the line:
	 *
	 * \verbatim
		@USERNAME	-	rtprio	99 \endverbatim
	 *
	 * to the `/etc/security/limits.conf` file (you need root access in order to do this) where `USERNAME` is replaced with your username.
	 * Once the line has been added, reboot the machine and you should be good to go. For more information on this, type `man ulimit` or
	 * `man limits.conf` into a terminal and look at the documentation about `rtprio`.
	 *
	 * The user defined function declaration can take on two forms:
	 *
	 * 1) `bool functionName()`\n
	 * 2) `bool functionName(double)`
	 *
	 * where `functionName` is any valid function name you choose. For form 2, the double parameter will contain the elapsed time in
	 * milliseconds (with nanosecond precision) from the moment the startTicking() function was called. If you need to access the current
	 * system time within your function, then use the Timing::getSysTime() function. The boolean return value is used by Timer to recognize
	 * an error in your user defined function and automatically stop the timer. If you encounter an error in your function, then return
	 * `false` and the Timer will safely stop calling your function. You can use the isTicking() function (from elsewhere in your code) to
	 * recognize if an error has stopped the Timer and deal with it accordingly. Make sure to return `true` at the end of your function to
	 * indicate to the Timer class that your function finished properly. The user defined function can be a stand-alone function or a member
	 * function of a class. Below are some examples of how to use the Timer class for this role:
	 *
	 * 1) Calling a function that is <b>not</b> a member of a class at a constant time interval:
	 *
	 * ~~~{.cpp}
	 * bool myfunction(double elapsed_time) { // print out the elapsed time since the function started getting called
	 *		cout << "Hello World " << elapsed_time << endl;
	 *		return true;
	 * }
	 *
	 * // somewhere else in your code...
	 * Timer my_timer;
	 * my_timer.startTicking(&myfunction, 1000); // call myfunction() every 1000 milliseconds
	 * // do other things here...
	 * my_timer.stopTicking(); // stop calling myfunction()
	 * ~~~
	 *
	 * 2) Calling a function that <b>is</b> a member of a class at a constant time interval:
	 *
	 * ~~~{.cpp}
	 * class MyClass {
	 *		bool mymember(double elapsed_time) { // print out the elapsed time since the function started getting called
	 *			cout << "Hello World " << elapsed_time << endl;
	 *			return true;
	 *		}
	 * };
	 *
	 * // somewhere else in your code...
	 * MyClass instance_of_myclass;
	 * Timer my_timer;
	 * my_timer.startTicking(instance_of_myclass, &MyClass::mymember, 1000); // call mymember() every 1000 milliseconds
	 * // do other things here...
	 * my_timer.stopTicking(); // stop calling myfunction()
	 * ~~~
	 *
	 * For convenience, one of the parameters of the startTicking() function is a name/identifier string that will be used
	 * in any output to the console that may occur in association with this role. This is very useful, for example, to
	 * determine which Timer instance generated an error on the console. If you do not provide a name, the default name
	 * is "Unknown".
	 *
	 * The following member functions are the only ones safe to use with a Timer instance that is performing this role:
	 *
	 * - startTicking()\n
	 * - stopTicking()\n
	 * - isTicking()\n
	 * - getTickingTimestep()\n
	 * - setTickingTimestep()\n
	 *
	 *
	 * <h3>Roles 2 and 3</h3>
	 *
	 * In this usage, the class basically becomes a stopwatch and a structure to store time. Time can be added(subtracted)
	 * to(from) the time stored in the class using the increment() and decrement() functions or any of the overloaded
	 * mathematical operators. Note that all of these modifying functions use time in milliseconds or the time contained in
	 * another instance of Timer. There are also functions to find elapsed time or wait until time has passed. Also note that
	 * a Timer instance can either contain a time point, or a time interval which can be positive or negative. You must recognize
	 * the difference based on the context of use. The following illustrates this difference:
	 *
	 * ~~~{.cpp}
	 * Timer t1;                     // t1 contains a time point which corresponds to the time when it was created
	 * Timer t2 = t1.elapsedTime();  // t2 contains a time interval (interval from t1 to the moment t1.elapsedTime() was called
	 *
	 * Timer t3 = t1 + t2;           // t3 contains a time point which is t2 after t1
	 * Timer t4 = t1 - t3;           // t4 contains a time interval which corresponds to the negative interval of t2
	 * Timer t5 = t2 + t4;           // t5 contains a 0 time interval since t2 and t4 are intervals of equal length but opposite direction
	 *
	 * t1 += 500;                    // t1 is still a time point, but it has been moved 500 milliseconds later
	 * t1 += t2;                     // t1 is still a time point, but it has been moved t2 time later
	 * t2.getTime();                 // t2 is now a time point corresponding to the time when this line was executed
	 * t1 += t3;                     // DON'T ADD TIMEPOINTS - while this will work, t1 is now a timepoint with an undefined value
	 * ~~~
	 *
	 * Here are some additional examples of using Timer with Roles 2 and 3:
	 *
	 * 1) %Timing a section of code (The CodeStopwatch class is better suited to profile runtime of code. Consider using it instead of
	 * Timer if that is your goal):
	 *
	 * ~~~{.cpp}
	 * Timer my_timer;
	 * // do code here
	 * cout << my_timer.elapsedTime().milliseconds() << endl;  // report milliseconds elapsed
	 * // or...
	 * cout << my_timer.elapsedTime().seconds() << endl; // if you prefer the seconds elapsed to be reported instead
	 * ~~~
	 *
	 *
	 * 2) Performing accurate frequency timing in a control loop of some sort.  The following code will run at a
	 *    given frequency regardless of how long the code inside the loop takes to execute. This is similar to the
	 *    built in functionality in Role 1, except it happens in the current thread rather than creating a dedicated
	 *    thread:
	 *
	 * ~~~{.cpp}
	 * Timer my_timer;
	 * while (true) {
	 *		my_timer += 10; // millisecond increment time, makes the loop run at 100 Hz
	 *		// do stuff
	 *		my_timer.waitUntilPassed(); // waits until the time has been reached, if your code in the loop
	 *									// takes more than 10 ms, then it won't wait at all
	 * }
	 * ~~~
	 *
	 * Note that you must be careful when using Timer in the way outlined in the previous example to not accidentally decrement the
	 * time stored in the instance of Timer (especially if the interval is the result of a computation). If you decrement, the
	 * waitUntilPassed() function will return immediately. If you continue to decrement such that the time representation in the Timer
	 * becomes negative, waitUntilPassed() will still return immediately and begin to print error messages.
	 *
	 * If you are tempted to create your own version of this class tailored to your specific needs, <b>STOP IT!</b> I
	 * promise you that this class will cover 99.999% of your needs in robotics. Often times it may even be better to
	 * use a professional library to get what you want rather than using or modifying this class. The Qt event system
	 * is an excellent option that's cross-platform. If you really are that stubborn, try to add the functionality to
	 * this class <b>without modifying the current functionality</b> instead of creating a
	 * completely separate class that is almost exactly the same except for your one seemingly nonnegotiable addition.
	 * I've spent more time than I care to admit combining this class with several others which the authors thought
	 * needed to be their own classes but were perfectly capable of coexisting in a single class.
	 *
	 */
	class Timer {

		private:
			timespec _time;

			pthread_t thread_handle;
			bool ticking = false;
			double ticking_timestep_ms;
			const char* threadName = "Unknown";

			void adjustForRollover();

			// These structures store data that is passed to the timing thread when an ordinary function is
			// provided as a user defined function.
			struct NormalFunctionThreadArgumentsWithElapsedTime {
				bool (*user_function)(double elapsed_ms);
				Timer* timer;
			};
			struct NormalFunctionThreadArguments {
				bool (*user_function)();
				Timer* timer;
			};

			// These structures store data that is passed to the timing thread when executing a user defined
			// function that is a member of a class at regular intervals.
			template<typename T>
			struct ClassSubmemberThreadArgumentsWithElapsedTime {
				T* object;
				bool (T::*user_function)(double elapsed_ms);
				Timer* timer;
			};
			template<typename T>
			struct ClassSubmemberThreadArguments {
				T* object;
				bool (T::*user_function)();
				Timer* timer;
			};

			static void* runNormalFunctionWithElapsedTime(void* args);
			static void* runNormalFunction(void* args);
			template<typename T>
			static void* runClassSubmemberWithElapsedTime(void* args);
			template<typename T>
			static void* runClassSubmember(void* args);

		public:
			Timer();
			Timer(double milliseconds, int seconds = 0);
			Timer(const Timer& other);
			~Timer();

			bool startTicking(bool (*user_function)(), double timestep_ms, const char* name = "Unknown", int priority = 0);
			bool startTicking(bool (*user_function)(double), double timestep_ms, const char* name = "Unknown", int priority = 0);
			template<typename T>
			bool startTicking(T& object, bool (T::*user_function)(), double timestep_ms, const char* name = "Unknown", int priority = 0);
			template<typename T>
			bool startTicking(T& object, bool (T::*user_function)(double), double timestep_ms, const char* name = "Unknown", int priority = 0);

			bool stopTicking();
			bool isTicking() const;
			double getTickingTimestep() const;
			void setTickingTimestep(double timestep_ms);

			bool getTime();
			void setTime(double milliseconds, int seconds = 0);
			void increment(double milliseconds);
			void decrement(double milliseconds);
			Timer elapsedTime() const;
			Timer updateTime();

			double milliseconds() const;
			double seconds() const;

			bool waitUntilPassed();

			Timer& operator+=(double milliseconds);
			Timer& operator-=(double milliseconds);
			Timer& operator+=(const Timer& timer);
			Timer& operator-=(const Timer& timer);
			Timer operator+(double milliseconds);
			Timer operator-(double milliseconds);
			Timer operator+(const Timer& timer);
			Timer operator-(const Timer& timer);

			Timer& operator=(const Timer& other_timer);
			Timer operator=(double milliseconds);

			friend std::ostream& operator<<(std::ostream& stream, const Timer& time);

	};


	/**
	 * @brief Begins calling user_function at a specified interval in a separate thread with the
	 * specified priority. The user_function is a class member function and does not take any
	 * parameters. It is repeatedly called until stopTicking() is called or the provided function
	 * returns false. See the full description of this class for more details and examples of usage.
	 * @param object An instance of the class which contains user_function as a member function and
	 * for which you want to call user_function at regular intervals.
	 * @param user_function A function pointer to a user defined class member function to call at
	 * regular intervals. The function must return a boolean and take no parameters.
	 * @param timestep_ms Initial time interval between calls to user_function in milliseconds. If
	 * negative, ticking does not start and this function returns false.
	 * @param name A string used as an identifier in any output to the console related to this role of
	 * Timer.
	 * @param priority Scheduling priority for the thread which calls user_function. This can be from
	 * 0 to 99 where 0 is the lowest priority and 99 is the highest.
	 * @return True if the ticking has been successfully started
	 */
	template<typename T>
	bool Timer::startTicking(T& object, bool (T::*user_function)(), double timestep_ms, const char* name, int priority) {

		threadName = name;

		if (ticking)
			return true;

		if (timestep_ms < 0) {
			std::cerr << "TIMER[" << threadName << "]: Tried to start ticking with a negative timestep. ERROR" << std::endl;
			return false;
		}

		ticking_timestep_ms = timestep_ms;

		// Create a new instance of the thread argument structure and populate it with information.
		ClassSubmemberThreadArguments<T> args = {&object, user_function, this};

		// Allocate and copy the argument structure to the private member 'function_arguments' (the space will be deleted by runClassSubmember).
		void* function_arguments = new unsigned char[sizeof(ClassSubmemberThreadArguments<T>)];
		memcpy(function_arguments, &args, sizeof(ClassSubmemberThreadArguments<T>));

		// create the thread attribute and initialize it
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);

		// set the subthread priority and policy
		if (priority > 0)
			pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
		else
			pthread_attr_setschedpolicy(&attr, SCHED_OTHER);

		struct sched_param param;
		param.sched_priority = priority;
		pthread_attr_setschedparam(&attr, &param);

		// Need to set this true before we create the thread because the new thread might be able to get past the while loop before
		// this thread can set ticking to true.
		ticking = true;

		// Start up a new thread which executes the runClassSubmember() function with the provided arguments.
		int ret = 0;
		if ((ret = pthread_create(&thread_handle, &attr, &Timer::runClassSubmember<T>, function_arguments)) != 0) {
			ticking = false;
			std::cerr << "TIMER[" << threadName << "]: could not create the thread for the timer: " << strerror(ret) << " ERROR" << std::endl;
			if (ret == EPERM)
				std::cerr << "   *** Does your user have the appropriate permissions in the /etc/security/limits.conf file?  (see the Timer class documentation for details) ***" << std::endl;
			return false;
		}

		pthread_attr_destroy(&attr);

		return true;

	}


	/**
	 * @brief This is the function that startTicking() passes to the pthread_create() function which the
	 * new thread will execute. It defines how and when the user defined function will get called for
	 * the case of a class member function with no parameters.
	 */
	template<typename T>
	void* Timer::runClassSubmember(void* args) {

		ClassSubmemberThreadArguments<T>* my_args = static_cast<ClassSubmemberThreadArguments<T>*>(args);
		T* object = my_args->object;
		bool (T::*user_function)() = my_args->user_function;
		Timer* timer = my_args->timer;

		delete[] static_cast<unsigned char*>(args); // delete the place in memory where the function arguments were stored

		// Store the current time.
		timer->getTime();

		// Keep looping until boolean ticking variable is set to false, then stop.
		while (timer->ticking && (object->*user_function)() ) {
			timer->increment(timer->getTickingTimestep()); // increment the time to the time interval

			if (!timer->waitUntilPassed()) // wait until the time interval has passed
				break;
		}

		// If the ticking variable is still true, then an error occurred (i.e. stopTicking() was not called) and ticking should be stopped
		if (timer->ticking) {
			std::cerr << "TIMER[" << timer->threadName << "]: an error caused the timer to stop ticking. Likely the user function returned false. ERROR" << std::endl;
			// Since stopTicking() was not called, then pthread_join() is not expected to be called. Therefore, we detach this thread so
			// that resources will be cleaned up when this thread terminates. We also set the ticking variable to false so that it is safe
			// to call startTicking() or stopTicking() (which will happen when the program closes).
			pthread_detach(pthread_self()); // An error occurred so no one will be calling pthread_join() for this thread
			timer->ticking = false;
		}

		pthread_exit(nullptr);

	}


	/**
	 * @brief Begins calling user_function at a specified interval in a separate thread with the
	 * specified priority. The user_function is a class member function and takes a double parameter
	 * which will contain the elapsed time in milliseconds. It is repeatedly called until stopTicking()
	 * is called or the provided function returns false. See the full description of this class for
	 * more details and examples of usage.
	 * @param object An instance of the class which contains user_function as a member function and
	 * for which you want to call user_function at regular intervals.
	 * @param user_function A function pointer to a user defined class member function to call at
	 * regular intervals. The function must return a boolean and take a double parameter which will
	 * contain the elapsed time from the time startTicking() was called in milliseconds.
	 * @param timestep_ms Initial time interval between calls to user_function in milliseconds. If
	 * negative, ticking does not start and this function returns false.
	 * @param name A string used as an identifier in any output to the console related to this role of
	 * Timer.
	 * @param priority Scheduling priority for the thread which calls user_function. This can be from
	 * 0 to 99 where 0 is the lowest priority and 99 is the highest.
	 * @return True if the ticking has been successfully started
	 */
	template<typename T>
	bool Timer::startTicking(T& object, bool (T::*user_function)(double), double timestep_ms, const char* name, int priority) {

		threadName = name;

		if (ticking)
			return true;

		if (timestep_ms < 0) {
			std::cerr << "TIMER[" << threadName << "]: Tried to start ticking with a negative timestep. ERROR" << std::endl;
			return false;
		}

		ticking_timestep_ms = timestep_ms;

		// Create a new instance of the thread argument structure and populate it with information.
		ClassSubmemberThreadArgumentsWithElapsedTime<T> args = {&object, user_function, this};

		// Allocate and copy the argument structure to the private member 'function_arguments' (the space will be deleted by runClassSubmember).
		void* function_arguments = new unsigned char[sizeof(ClassSubmemberThreadArgumentsWithElapsedTime<T>)];
		memcpy(function_arguments, &args, sizeof(ClassSubmemberThreadArgumentsWithElapsedTime<T>));

		// create the thread attribute and initialize it
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);

		// set the subthread priority and policy
		if (priority > 0)
			pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
		else
			pthread_attr_setschedpolicy(&attr, SCHED_OTHER);

		struct sched_param param;
		param.sched_priority = priority;
		pthread_attr_setschedparam(&attr, &param);

		// Need to set this true before we create the thread because the new thread might be able to get past the while loop before
		// this thread can set ticking to true.
		ticking = true;

		// Start up a new thread which executes the runClassSubmemberWithElapsedTime() function with the provided arguments.
		int ret = 0;
		if ((ret = pthread_create(&thread_handle, &attr, &Timer::runClassSubmemberWithElapsedTime<T>, function_arguments)) != 0) {
			ticking = false;
			std::cerr << "TIMER[" << threadName << "]: could not create the thread for the timer: " << strerror(ret) << " ERROR" << std::endl;
			if (ret == EPERM)
				std::cerr << "   *** Does your user have the appropriate permissions in the /etc/security/limits.conf file?  (see the Timer class documentation for details) ***" << std::endl;
			return false;
		}

		pthread_attr_destroy(&attr);

		return true;
	}


	/**
	 * @brief This is the function that startTicking() passes to the pthread_create() function which the
	 * new thread will execute. It defines how and when the user defined function will get called for
	 * the case of a class member function with the elapsed time parameter.
	 */
	template<typename T>
	void* Timer::runClassSubmemberWithElapsedTime(void* args) {

		ClassSubmemberThreadArgumentsWithElapsedTime<T>* my_args = static_cast<ClassSubmemberThreadArgumentsWithElapsedTime<T>*>(args);
		T* object = my_args->object;
		bool (T::*user_function)(double) = my_args->user_function;
		Timer* timer = my_args->timer;

		delete[] static_cast<unsigned char*>(args); // delete the place in memory where the function arguments were stored

		// Store the current time.
		Timer start_time;
		timer->getTime();

		// Keep looping until boolean ticking variable is set to false, then stop.
		while (timer->ticking && (object->*user_function)(start_time.elapsedTime().milliseconds()) ) {
			timer->increment(timer->getTickingTimestep()); // increment the time to the time interval

			if (!timer->waitUntilPassed()) // wait until the time interval has passed
				break;
		}

		// If the ticking variable is still true, then an error occurred (i.e. stopTicking() was not called) and ticking should be stopped
		if (timer->ticking) {
			std::cerr << "TIMER[" << timer->threadName << "]: an error caused the timer to stop ticking. Likely the user function returned false. ERROR" << std::endl;
			// Since stopTicking() was not called, then pthread_join() is not expected to be called. Therefore, we detach this thread so
			// that resources will be cleaned up when this thread terminates. We also set the ticking variable to false so that it is safe
			// to call startTicking() or stopTicking() (which will happen when the program closes).
			pthread_detach(pthread_self()); // An error occurred so no one will be calling pthread_join() for this thread
			timer->ticking = false;
		}

		pthread_exit(nullptr);

	}

} // end Timing namespace

#endif // TIMING_H
