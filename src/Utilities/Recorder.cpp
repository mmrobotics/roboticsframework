#include "Recorder.h"


bool Recorder::isRecordingMaster = true;
QDir Recorder::masterDirectoryPath = QDir("Output");


//
// Static Methods
//


/**
 * @brief Returns the next available filename that does not already exist
 *
 * If `filebase` + `extension` does not exist, then return that.  Otherwise,
 * try `filebase` + "-1" + `extension`, then the same with "-2", and so on
 * until we find a filename that does not already exist.
 *
 * Example:
 *
 *     Recorder state_recorder(Recorder::nextAvailableFilename("StateData", ".txt"));
 *
 * @param filebase: Beginning of the filename
 * @param extension: Usually the file extension (including the dot), but
 *   just the suffix to append after the "-#" piece
 */
QString Recorder::nextAvailableFilename(const QString& filebase, const QString& extension) {
	const auto& dir = masterDirectoryPath;
	auto filename = filebase + extension;
	for (int i = 1; true; ++i) {
		if (!dir.exists(filename)) {
			return filename; // this file does not yet exist, return it
		}
		filename = filebase + "-" + QString::number(i) + extension;
	}
}


//
// Non-Static Methods
//

/**
 * @brief Constructs a Recorder object and opens the file specified by `filename`.
 * @param filename The file to which data should be written
 */
Recorder::Recorder(const QString& filename) {

	open(filename);

}


/**
 * @brief Closes the file and destroys the Recorder.
 */
Recorder::~Recorder() {

	close();

}


/**
 * @brief Opens the file specified by `filename`. If the filename is specified
 * in the constructor, this function is not necessary.
 * @param filename The file to which data should be written
 */
void Recorder::open(const QString& filename) {

	if (filename == "") {
		std::cerr << "RECORDER: file name cannot be an empty string. ERROR" << std::endl;
		return;
	}

	// Make sure the directory exists, create it if it doesn't
	QDir dir = QFileInfo(masterDirectoryPath, filename).dir();
	if (!dir.exists())
		dir.mkpath(".");

	this->filename = masterDirectoryPath.filePath(filename);
	count = 0;
	usedInsertionOperator = false;

	file_stream.open(masterDirectoryPath.filePath(filename).toStdString());
	
}


/**
 * @brief Returns `true` if the file is open.
 * @return
 */
bool Recorder::isOpened() {

	return file_stream.is_open();

}


/**
 * @brief Closes the file this Recorder object is writing to.
 */
void Recorder::close() {

	if (file_stream.is_open()) {

		file_stream.close();

		if (usedInsertionOperator && count == 0)
			std::cout << "RECORDER: wrote data to the file " << filename.toStdString() << "." << std::endl;
		else
			std::cout << "RECORDER: wrote " << count << " items to the file " << filename.toStdString() << "." << std::endl;

		count = 0;
		usedInsertionOperator = false;

	}

}


/**
 * @brief Forces all previously recorded data to be sent to disk (i.e. the file buffer is
 * flushed).
 */
void Recorder::flush() {

	file_stream << std::flush;

}
