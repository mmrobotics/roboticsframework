#include "Optimization.h"

// TODO: Clean up and document these classes

namespace Optimization {

	/**
	 * Class constructor for the RootFinder class.
	 *
	 */
	RootFinder::RootFinder(Function1D* function, double tolerance /* how accurate the root location should be */, unsigned int max_iterations /* max number of iterations to perform, leave empty if it doesn't matter */)
		: function(function),
		  tolerance(tolerance),
		  max_iterations(max_iterations) {

		gsl_function.f = &functionValue;
		gsl_function.df = &derivativeValue;
		gsl_function.fdf = &functionDerivativeValue;
		gsl_function.params = function;

		gsl_finder = gsl_root_fdfsolver_alloc(gsl_root_fdfsolver_newton);

	}


	/**
	 * Class destructor for the RootFinder class.
	 *
	 */
	RootFinder::~RootFinder() {

		gsl_root_fdfsolver_free(gsl_finder);

	}


	/**
	 * Returns the root of the given function using an initial guess of the root.
	 *
	 */
	double RootFinder::find(double initial_x) {

		int status;
		unsigned int iteration = 0;
		double current_x = initial_x;
		double new_x;

		gsl_root_fdfsolver_set (gsl_finder, &gsl_function, current_x);

		do {
			iteration += 1;

			gsl_root_fdfsolver_iterate(gsl_finder);

			new_x = gsl_root_fdfsolver_root(gsl_finder);

			status = gsl_root_test_delta(new_x, current_x, 0, tolerance);

			current_x = new_x;
		}
		while ((status == GSL_CONTINUE) && (!max_iterations || (iteration < max_iterations)));

		return current_x;

	}



	/**
	 * Returns the function value (used by GSL functions).
	 *
	 */
	double RootFinder::functionValue(double x, void* function) {

		Function1D* tmp = (Function1D*)function;
		return tmp->value(x);

	}


	/**
	 * Returns the derivative value (used by GSL functions).
	 *
	 */
	double RootFinder::derivativeValue(double x, void* function) {

		Function1D* tmp = (Function1D*)function;
		return tmp->derivative(x);

	}


	/**
	 * Gives the function and derivative values (used by GSL functions).
	 *
	 */
	void RootFinder::functionDerivativeValue(double x, void* function, double* value, double* derivative) {

		Function1D* tmp = (Function1D*)function;

		*value = tmp->value(x);
		*derivative = tmp->derivative(x);

	}

} // End Optimization namespace
