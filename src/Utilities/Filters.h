#ifndef FILTERS_H
#define FILTERS_H

#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>
#include <RoboticsToolbox.h>
#include <Utilities/Timing.h>


/**
 * The Filters namespace contains various filters such as a low-pass filter, moving average filter,
 * Kalman filter, etc. Other non-filter signal processing utilities can be found in the SignalProc namespace.
 */
namespace Filters {

	/**
	 * @brief The LowPassFilter class implements a first order, low-pass filter.
	 *
	 * The cut-off frequency is specified in the constructor in Hz and must be larger than zero. If you attempt
	 * to use a zero or negative cut-off frequency, the class will defer to the default of using an infinite
	 * cut-off frequency (which means that the filter will simply not filter at all). Using the default
	 * constructor will also set the cut-off frequency to be infinite.
	 *
	 * Once the filter has been constructed, the filter needs to be seeded with an initial value. This is done
	 * with the seed() function. New data points are passed through the filter using the update() function. If
	 * the filter has not been seeded when the update() function is called, the provided value will be used to
	 * seed the filter first (for convenience) and then the value will be used to update the filter.
	 *
	 * The time interval between data points can be specified in a number of ways. First, it can be specified
	 * in the constructor as a frequency in Hz. This will cause the update() function that does not take a
	 * `delta_t` parameter to use the specified frequency. If you do not specify an update frequency in the
	 * constructor, then the update() function that does not take a `delta_t` parameter will compute the time
	 * interval based on the real-world time between calls to that update() function. The last way to specify
	 * the time interval is to use the update() function that takes a `delta_t` parameter. In this function,
	 * the provided time interval (the `delta_t` parameter in seconds) will take precedence. Note that if you are using
	 * the real-world time approach, it is recommended that you <b>do not</b> use the update() function that
	 * takes a `delta_t` parameter because the real-world time is only computed between calls to the update()
	 * function that does <b>not</b> have a `delta_t` parameter.
	 *
	 * The current filtered value can be obtained with the value() function.
	 *
	 * @tparam T The type of the value to be filtered, for which addition and multiplication by a `double`
	 * are defined. Multiplication by a `double` must be implemented with an operator overload of the form:
	 * `T& operator*(double)`. The type must also be default constructable and the default value should be
	 * some representation of zero (primitive types except `bool` fit this specification and the default
	 * value is 0).
	 */
	template<typename T>
	class LowPassFilter {

		private:
			bool seeded = false;
			bool auto_delta_t = true;

			double RC = 0.0;
			double alpha = 0.0;

			Timing::Timer time_keeper;

			T filtered_value{};

		public:
			LowPassFilter() = default;
			LowPassFilter(double cutoff_frequency /* Hz */)
				: RC(cutoff_frequency > 0 ? 1.0 / (2.0 * M_PI * cutoff_frequency) : 0.0) {}
			LowPassFilter(double cutoff_frequency /* Hz */, double update_frequency /* Hz */)
				: auto_delta_t(false),
				  RC(cutoff_frequency > 0 ? 1.0 / (2.0 * M_PI * cutoff_frequency) : 0.0),
				  alpha((1.0 / update_frequency) / (RC + (1.0 / update_frequency))) {}

			void seed(T initial_value) {

				filtered_value = initial_value;

				if (auto_delta_t)
					time_keeper.getTime();

				seeded = true;

			}

			void update(const T& new_value) {

				if (!seeded) {

					seed(new_value);

				}
				else {

					if (auto_delta_t) {
						double delta_t = time_keeper.updateTime().seconds(); // get the elapsed time in seconds
						alpha = delta_t / (RC + delta_t);
					}

					filtered_value = new_value * alpha + filtered_value * (1.0 - alpha);

				}

			}

			/**
			 * Adds another data point to the filtered value. Allows direct specification of the time interval so it
			 * can be used with batch data (i.e. data that was produced at intervals, but given to you all at once in
			 * batches), in an offline manner, or with data that has an inconsistent time interval.
			 *
			 */
			void update(const T& new_value, double delta_t  /* s */) {

				if (!seeded) {

					seed(new_value);

				}
				else {

					alpha = delta_t / (RC + delta_t);
					filtered_value = new_value * alpha + filtered_value * (1.0 - alpha);

				}

			}

			T& value() { return filtered_value; }

	};


	/**
	 * @brief The MovingAverageFilter class implements a moving average filter.
	 *
	 * The usage is simple. First, seed the filter with an initial value. Then for each new unfiltered value, call the
	 * update() function. You obtain the current filtered value with the value() function. For convenience, the update()
	 * function also returns the current filtered value that results from the update so the value() function is really
	 * only useful if you need to access the current filtered value multiple times between updates.
	 *
	 * The filter can be seeded in three ways:
	 *
	 * 1. In the constructor
	 * 2. Using the seed() function. This is useful if you do not have an initial value when the filter is constructed.
	 * 3. If the filter is not seeded when you call update(), update() will call seed() for you out of convenience.
	 *
	 * If you call seed() when the filter is already seeded, it effectively resets the filter with the given initial value.
	 *
	 * <b>Note:</b> This class stores the current total sum internally and on each call to update() it updates the
	 * total sum and performs a single division to compute the next filtered value. This is necessary to minimize truncation
	 * error that occurs for integer types (e.g. `int`, `long`, etc.). Truncation error is always a possibility for these
	 * types, but it will not accumulate between updates because the current filtered value is not used to compute the next
	 * filtered value on each update. In other words, the filtered value you receive from value() will always be the rounded
	 * down (to the next whole integer) version of the correct filtered value.
	 *
	 * @tparam T The type of the value to be filtered, for which addition, subtraction, and division by an `int` are
	 * defined. The type must also be default constructable and the default value should be some representation of zero
	 * (primitive types except `bool` fit this specification and the default value is 0).
	 * @tparam length The length of the moving average (i.e. how many recent values should be considered in the average?)
	 */
	template<typename T, int length>
	class MovingAverageFilter {

		static_assert(length > 0, "The length of the MovingAverageFilter must be positive non-zero.");

		private:
			T circularBuffer[length];
			unsigned int currentIndex = 0;
			T currentSum{};		// Default construct (for primitive types this means set to zero)
			T currentValue{};	// Default construct (for primitive types this means set to zero)
			bool seeded = false;

		public:
			MovingAverageFilter() = default;
			MovingAverageFilter(const T& startingValue) {

				seed(startingValue); // Seed the filter

			}

			void seed(const T& startingValue) {

				// Bring the current sum back to zero
				currentSum = currentSum - currentSum;

				// Fill the buffer with the starting value
				for (unsigned int i = 0; i < length; ++i) {
					circularBuffer[i] = startingValue;
					currentSum += startingValue;
				}

				currentValue = startingValue;
				currentIndex = 0;
				seeded = true;

			}

			const T& update(const T& newValue) {

				if (!seeded) {

					seed(newValue);

				}
				else {

					// Update the sum
					currentSum += newValue - circularBuffer[currentIndex];

					// Update the average
					currentValue = currentSum / length;

					// Store the new value
					circularBuffer[currentIndex] = newValue;

					// Move the index of the circular buffer to the next (oldest) value
					currentIndex++;
					if (currentIndex >= length)
						currentIndex = 0;

				}

				return currentValue;

			}

			const T& value() const {

				return currentValue;

			}

			void reset() {

				currentValue = currentValue - currentValue;
				seeded = false;

			}

	};


	/**
	 * @brief The ExponentialMovingAverageFilter class implements an exponentially weighted moving average filter. An
	 * exponentially weighted moving average filter uses the entire history where samples further in the past get a
	 * smaller weight than newer ones. It is slightly less computationally expensive and requires less memory than
	 * MovingAverageFilter.
	 *
	 * The usage is simple. First, seed the filter with an initial value. Then for each new unfiltered value, call the
	 * update() function. You obtain the current filtered value with the value() function. For convenience, the update()
	 * function also returns the current filtered value that results from the update so the value() function is really
	 * only useful if you need to access the current filtered value multiple times between updates.
	 *
	 * The filter can be seeded in three ways:
	 *
	 * 1. In the constructor
	 * 2. Using the seed() function. This is useful if you do not have an initial value when the filter is constructed.
	 * 3. If the filter is not seeded when you call update(), update() will call seed() for you out of convenience.
	 *
	 * If you call seed() when the filter is already seeded, it effectively resets the filter with the given initial value.
	 *
	 * The `alpha` parameter is a number in the range [0, 1] that tells how much new samples will affect the current
	 * filtered value. If `alpha` is closer to 1, new samples will have a large effect. An `alpha` of 1 means the filtered
	 * value will always be equal to the most recent update. If `alpha` is closer to 0, new samples will have a smaller effect.
	 * An `alpha` of 0 means new samples will be completely ignored (i.e. the filtered value will never change from the seeded
	 * value).
	 *
	 * <b>Beware</b> that if you are using an integer type (e.g. `int`, `long`, etc.) truncation will happen on each update.
	 * This could result in strange behavior where the true effect of `alpha` is dependent on the value of new samples. It is
	 * recommended to only use floating point numbers with this filter.
	 *
	 * @tparam T The type of the value to be filtered, for which addition and multiplication by a `double` are defined.
	 * Multiplication by a `double` must be implemented with an operator overload of the form: `T& operator*(double)`.
	 * The type must also be default constructable and the default value should be some representation of zero (primitive
	 * types except `bool` fit this specification and the default value is 0).
	 */
	template<typename T>
	class ExponentialMovingAverageFilter {

		private:
			T currentValue{}; // Default construct (for primitive types this means set to zero)
			bool seeded = false;
			const double alpha;

		public:
			ExponentialMovingAverageFilter(double alpha) : alpha(alpha) {}
			ExponentialMovingAverageFilter(double alpha, const T& startingValue) : alpha(alpha) {

				seed(startingValue); // Seed the filter

			}

			void seed(const T& startingValue) {

				currentValue = startingValue;
				seeded = true;

			}

			const T& update(const T& newValue) {

				if (!seeded) {

					seed(newValue);

				}
				else {

					currentValue = (newValue * alpha) + (currentValue * (1.0 - alpha));

				}

				return currentValue;

			}

			const T& value() const {

				return currentValue;

			}

			void reset() {

				currentValue = currentValue - currentValue;
				seeded = false;

			}

	};

	/**
	 * @brief The KalmanFilter class implements a Linear Kalman Filter algorithm in discrete-time. If you have
	 * a continuous time state-space model, this class can automatically convert it to a discrete-time system
	 * for you.
	 *
	 * This filter is designed to work with linear state-space systems of the form
	 *
	 *     x[k+1] = A*x[k] + B*u[k] + v[k]
	 *     y[k+1] = C*x[k] + w[k]
	 *
	 * where `v[k]` and `w[k]` are the process and sensor noise terms with covariance V and W respectively.
	 * This filter can also deal with a linear state-space system that is described in continuous time
	 *
	 *     x'(t) = Ax(t) + Bu(t) + v(t)
	 *     y(t)  = Cx(t) + w(t)
	 *
	 * where `v(t)` and `w(t)` are the process and sensor noise terms with covariance V<SUB>c</SUB> = Q/T
	 * and W<SUB>c</SUB> = WT respectively. In other words, the continuous time covariances are computed from
	 * the discrete time covariances V and W using the timestep T used to convert between discrete and
	 * continuous time. If you provide the continuous form of A, B, and C, as well as the timestep (in seconds),
	 * your system will automatically be converted to discrete time (you must use the constructor or setModel()
	 * function that accepts a `timestep` parameter). Note that in this class, the covariances are always
	 * expected to be the discrete covariances even when providing the continuous time system model.
	 *
	 * The system model can be provided either in the the constructor or by using the setModel() function. Once
	 * the model is set, the filter needs to be seeded with the initial state vector and state error covariance
	 * matrix using the seed() function. You may verify whether the filter has been seeded using the isSeeded()
	 * function, but be aware that the filter does not check that it is seeded before performing any
	 * calculations. It is your job to make sure the filter is seeded before you use it.
	 *
	 * The filter is used by calling the predict() function with the input vector at the previous timestep
	 * followed by the update() function with the current sensor vector. Make sure to always call these two
	 * functions in that order and in pairs. The predict() function will return the predicted state and the
	 * update() function will return the Kalman filter estimate for the current time step. At any time, you
	 * may access the current estimate of the state and the state error covariance using the state() and
	 * covariance() functions.
	 *
	 * You may change the model at any time using setModel() (e.g. to change the timestep) as long as you do
	 * not do so in between the predict and update steps (i.e. only change the model after a full predict/
	 * update pair has completed).
	 *
	 * @tparam stateDim The dimension of the state space for the Kalman filter. This number must be larger than
	 * zero.
	 * @tparam inputDim The dimension of the input vector for the Kalman filter. This number must be larger
	 * than zero. If your system does not have an input vector, then simply set B to a zero matrix and pass
	 * a zero vector to the predict() function.
	 * @tparam sensorDim The dimension of the sensor vector for the Kalman filter. This number must be larger
	 * than zero.
	 */
	template<int stateDim, int inputDim, int sensorDim>
	class KalmanFilter {

		static_assert(stateDim > 0, "The state dimension must be larger than 0.");
		static_assert(inputDim > 0, "The input dimension must be larger than 0.");
		static_assert(sensorDim > 0, "The sensor dimension must be larger than 0.");

		public:
			typedef Eigen::Matrix<double, stateDim, 1, Eigen::DontAlign> StateVector;
			typedef Eigen::Matrix<double, inputDim, 1, Eigen::DontAlign> InputVector;
			typedef Eigen::Matrix<double, sensorDim, 1, Eigen::DontAlign> SensorVector;
			
			typedef Eigen::Matrix<double, stateDim, stateDim, Eigen::DontAlign> StateMatrix;
			typedef Eigen::Matrix<double, stateDim, inputDim, Eigen::DontAlign> InputMatrix;
			typedef Eigen::Matrix<double, sensorDim, stateDim, Eigen::DontAlign> SensorMatrix;
			
			typedef Eigen::Matrix<double, stateDim, stateDim, Eigen::DontAlign> StateCovarianceMatrix;
			typedef Eigen::Matrix<double, sensorDim, sensorDim, Eigen::DontAlign> SensorCovarianceMatrix;
			
		private:
			bool seeded = false;

			// Discrete state-space model
			StateMatrix _Ad;
			InputMatrix _Bd;
			SensorMatrix _Cd;
			StateCovarianceMatrix _V;
			SensorCovarianceMatrix _W;
		
			// Predicted and updated state and covariance
			StateVector predicted_state = StateVector::Zero();
			StateCovarianceMatrix predicted_covariance = StateCovarianceMatrix::Zero();
			StateVector updated_state = StateVector::Zero();
			StateCovarianceMatrix updated_covariance = StateCovarianceMatrix::Zero();
			
		public:
			KalmanFilter() = default;
			KalmanFilter(const StateMatrix& A, const InputMatrix& B, const SensorMatrix& C, const StateCovarianceMatrix& V, const SensorCovarianceMatrix& W, double timestep /* s */) {

				setModel(A, B, C, V, W, timestep);

			}
			KalmanFilter(const StateMatrix& Ad, const InputMatrix& Bd, const SensorMatrix& Cd, const StateCovarianceMatrix& V, const SensorCovarianceMatrix& W) {

				setModel(Ad, Bd, Cd, V, W);

			}
			
			void setModel(const StateMatrix& A, const InputMatrix& B, const SensorMatrix& C, const StateCovarianceMatrix& V, const SensorCovarianceMatrix& W, double timestep /* s */) {

				// Compute the discrete form of the continuous state-space matrices with the given timestep
				Eigen::Matrix<double, stateDim + inputDim, stateDim + inputDim, Eigen::DontAlign> continuous_matrices = Eigen::Matrix<double, stateDim + inputDim, stateDim + inputDim, Eigen::DontAlign>::Zero();
				continuous_matrices.topLeftCorner(stateDim, stateDim) = A * timestep;
				continuous_matrices.topRightCorner(stateDim, inputDim) = B * timestep;
				Eigen::Matrix<double, stateDim + inputDim, stateDim + inputDim, Eigen::DontAlign> discrete_matrices = continuous_matrices.exp();

				setModel(discrete_matrices.topLeftCorner(stateDim, stateDim), discrete_matrices.topRightCorner(stateDim, inputDim), C, V, W);

			}
			void setModel(const StateMatrix& Ad, const InputMatrix& Bd, const SensorMatrix& Cd, const StateCovarianceMatrix& V, const SensorCovarianceMatrix& W) {

				_Ad = Ad; _Bd = Bd; _Cd = Cd; _V = V; _W = W;

			}
			
			void seed(const StateVector& seed_vector, const StateCovarianceMatrix& seed_covariance) {

				predicted_covariance = updated_covariance = seed_covariance;
				predicted_state = updated_state = seed_vector;

				seeded = true;

			}
			
			bool isSeeded() { return seeded; }
			
			StateVector predict(const InputVector& current_input) {

				predicted_state = _Ad * updated_state + _Bd * current_input;
				predicted_covariance = _Ad * updated_covariance * _Ad.transpose() + _V;

				return predicted_state;

			}

			StateVector update(const SensorVector& sensor_measurement) {

				SensorCovarianceMatrix S = _Cd * predicted_covariance * _Cd.transpose() + _W;
				Eigen::Matrix<double, stateDim, sensorDim, Eigen::DontAlign> K = predicted_covariance * _Cd.transpose() * S.inverse();

				updated_state = predicted_state + K * (sensor_measurement - (_Cd * predicted_state));
				updated_covariance = predicted_covariance - K * _Cd * predicted_covariance;

				// Force the covariance matrix to be symmetric since we are using a covariance measurement update equation
				// that is prone to producing a non-symmetric matrix but is computationally more efficient (see Optimal
				// State Estimation, Simon 2006 Section 5.5)
				updated_covariance = (updated_covariance + updated_covariance.transpose()) / 2;

				return updated_state;

			}

			StateVector state() { return updated_state; }
			StateCovarianceMatrix covariance() { return updated_covariance; }

	};


/*
	template<int x, int y>
	class ExtendedKalmanFilter {
			public:
				typedef Eigen::Matrix<double, x, 1, Eigen::DontAlign> StateVector;
				typedef Eigen::Matrix<double, y, 1, Eigen::DontAlign> SensorVector;
				typedef Eigen::Matrix<double, x, x, Eigen::DontAlign> StateCovarianceMatrix;
				typedef Eigen::Matrix<double, y, y, Eigen::DontAlign> SensorCovarianceMatrix;
				typedef Eigen::Matrix<double, x, x, Eigen::DontAlign> StateJacobian;
				typedef Eigen::Matrix<double, y, x, Eigen::DontAlign> SensorJacobian;

			private:
				bool initialized;
				bool seeded;

				StateCovarianceMatrix V;
				SensorCovarianceMatrix W;

				StateVector predicted_state;
				SensorVector predicted_sensor_reading;
				StateCovarianceMatrix predicted_covariance;

				StateVector updated_state;
				StateCovarianceMatrix updated_covariance;

			public:
				ExtendedKalmanFilter();
				ExtendedKalmanFilter(const StateCovarianceMatrix &V, const SensorCovarianceMatrix &W);

				void seed(const StateCovarianceMatrix &seed_covariance, const StateVector &seed_vector);
				void seed(const StateVector &seed_vector);

				StateVector state() { return updated_state; }
				StateCovarianceMatrix covariance() { return updated_covariance; }

				// Functions for extended Kalman filter: x_dot = f(x,u) + v and y = g(x) + w
				void predict(const StateJacobian &Jf, const StateVector &predicted_state, const SensorVector &predicted_sensor_reading); // Jf = df/dx
				void update(const SensorJacobian &Jg, const SensorVector &new_sensor_reading); // Jg = dg/dx
		};


	template<int x, int y>
	ExtendedKalmanFilter<x, y>::ExtendedKalmanFilter() {

		this->initialized = false;
		this->seeded = false;

		return;
	}


	template<int x, int y>
	ExtendedKalmanFilter<x, y>::ExtendedKalmanFilter(const StateCovarianceMatrix &V, const SensorCovarianceMatrix &W) {

		this->initialized = true;
		this->seeded = false;

		this->V = V;
		this->W = W;

		return;
	}




	template<int x, int y>
	void ExtendedKalmanFilter<x, y>::seed(const StateCovarianceMatrix &seed_covariance, const StateVector &seed_vector) {

		this->predicted_state = this->updated_state = seed_vector;

		this->predicted_covariance = this->updated_covariance = seed_covariance;

		this->seeded = true;

		return;
	}


	template<int x, int y>
	void ExtendedKalmanFilter<x, y>::seed(const StateVector &seed_vector) {
		if (!this->initialized) {
			cout << "KALMAN_FILTER: this kalman filter needs to be initialized with covariance matrices before seeding without a seed state covariance matrix provided. ERROR" << endl;
			return;
		}

		this->predicted_state = this->updated_state = seed_vector;

		this->predicted_covariance = this->updated_covariance = V;

		this->seeded = true;

		return;
	}


	// intended for use in an Extended Kalman Filter
	template<int x, int y>
	void ExtendedKalmanFilter<x, y>::predict(const StateJacobian &Jf, const StateVector &predicted_state, const SensorVector &predicted_sensor_reading) {

		if (!this->initialized) {
			cout << "KALMAN_FILTER: this kalman filter needs to be initialized with covariance matrices before performing the prediction step. ERROR" << endl;
			return;
		}
		if (!this->seeded) {
			cout << "KALMAN_FILTER: this kalman filter needs to be seeded before performing the prediction step. ERROR" << endl;
			return;
		}


		this->predicted_state = predicted_state;

		this->predicted_sensor_reading = predicted_sensor_reading;

		this->predicted_covariance = Jf*updated_covariance*Jf.transpose() + V;

		return;
	}


	// intended for use in an Extended Kalman Filter
	template<int x, int y>
	void ExtendedKalmanFilter<x, y>::update(const SensorJacobian &Jg, const SensorVector &sensor_reading) {

		if (!this->initialized) {
			cout << "KALMAN_FILTER: this kalman filter needs to be initialized with covariance matrices before performing the update step. ERROR" << endl;
			return;
		}
		if (!this->seeded) {
			cout << "KALMAN_FILTER: this kalman filter needs to be seeded before performing the update step. ERROR" << endl;
			return;
		}



		SensorCovarianceMatrix S = Jg*this->predicted_covariance*Jg.transpose() + this->W;

		Eigen::Matrix<double, x, y, Eigen::DontAlign> R = this->predicted_covariance*Jg.transpose()*S.inverse();

		SensorVector v = sensor_reading - this->predicted_sensor_reading;

		this->updated_state = this->predicted_state + R*v;

		this->updated_covariance = this->predicted_covariance - R*Jg*this->predicted_covariance;


		return;
	}
*/

} // End Filters namespace

#endif // FILTERS_H
