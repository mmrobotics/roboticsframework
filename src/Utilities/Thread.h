#ifndef THREAD_H
#define THREAD_H

#include <pthread.h>


/**
 * @brief The Thread class implements the functionality of a thread. This class is designed for
 * the Linux platform.
 *
 * To use it, derive your class from the Thread class and reimplement the run() member function.
 * The code that you place in your implementation of the run() function is the code that the
 * thread will execute when it starts.
 *
 * To start the thread, execute the start() member function.
 *
 * Stopping the thread is a little bit in your hands. If you implement a continuous loop in your
 * run() implementation, then you should repeatedly call the isRunning() function to see if the
 * stop() function has been called (the isRunning() function will return `false` if the stop()
 * function has been called). Note that the stop() function waits for the thread to terminate
 * before returning so if your implementation does not return from the run() function when the
 * isRunning() function returns `false`, the thread that called stop() will block indefinitely.
 * This allows the opportunity to put clean-up code in your run() function if desired before
 * returning.
 *
 * Make sure to set the Thread class as a private base (or at least a protected base) if your new
 * class is not strictly a sub-type of Thread. Inheriting from Thread is more of an implementation
 * detail and unless you can say "My new class IS a Thread" (which you probably wouldn't say if
 * your new class represents a robot device for example) then you should be using private or at
 * least protected inheritance. This will prevent other code from type-casting your new class to
 * a Thread class (which usually won't make sense) or using functions inherited from the Thread
 * class at all. In this case, it is better to provide an interface that has sematic meaning to
 * your class. For example, since start() and stop() would not be public to the general user
 * anymore, you could call start() in the constructor and stop() in the destructor, or provide
 * start and stop functions which make sense for the meaning of your new class.
 *
 * Here is an example of a proper implementation of a thread using this class, it continuously
 * prints out the string "Thread: Hello World!" until the stop() member function is called:
 *
 * ~~~{.cpp}
 * class MyThreadImplementation : public Thread {
 *
 *		public:
 *			void run() {
 *				while (isRunning()) {
 *					cout << "Thread: Hello World!" << endl;
 *				}
 *			}
 *
 * };
 * ~~~
 *
 * If `my_thread` is an instance of `MyThreadImplementation`, then calling `my_thread.start()`
 * will start the thread execution, and calling `my_thread.stop()` will stop the thread. Notice
 * that `MyThreadImplementation` is using public inheritance from Thread. Presumably I can say
 * `MyThreadImplementation` IS a Thread since `MyThreadImplementation` is a class that only
 * reimplements run() (it is just a thread doing some generic work). If `MyThreadImplementation`
 * contained other data and functions that gives it more semantic meaning than just a thread,
 * then as mentioned before, I should use private or protected inheritance since it is just
 * an implementation detail. I would then write some public functions in `MyThreadImplementation`
 * which in turn start and stop the thread (e.g. startTracking() and stopTracking()).
 *
 * This thread implementation also gives you the ability to set the scheduling style and
 * priority.  By default, the priority is set to `NORMAL`.  If you wish to use the Linux real-time
 * scheduling system (it is more of a "soft" real-time), then set the priority in the argument
 * of the constructor to be one of `REALTIME_LOW`, `REALTIME_MEDIUM`, or `REALTIME_HIGH`. Naturally,
 * the `*_HIGH` will get priority over the `*_LOW` priority, but any of the real-time priorities
 * have priority over the `NORMAL` thread priority.  To ensure that you have operating system
 * permission to change the thread priority (normal users don't have the permission by
 * default), you need to add the line:
 *
 * \verbatim
	@USERNAME	-	rtprio	99 \endverbatim
 *
 * to the `/etc/security/limits.conf` file (you need root access in order to do this) where
 * `USERNAME` is replaced with your username. Once the line has been added, reboot the machine
 * and you should be good to go. For more information on this, type `man ulimit` or
 * `man limits.conf` into a terminal an look at the documentation about `rtprio`.
 *
 * You may also specify which processor(s) on your computer will be allowed to run the thread.
 * This is done in the constructor by specifying the Processor parameter. This parameter can
 * take any of the following values: `PROCESSOR_ANY`, `PROCESSOR_1`, `PROCESSOR_2`,
 * `PROCESSOR_3`, `PROCESSOR_4`, `PROCESSOR_5`, `PROCESSOR_6`, `PROCESSOR_7`, and `PROCESSOR_8`.
 * These values can be combined using bitwise OR to specify multiple processors that are
 * allowed to run the thread.
 */
class Thread {

	public:
		enum ThreadPriority { NORMAL, REALTIME_LOW, REALTIME_MEDIUM, REALTIME_HIGH };
		using Processor = unsigned int;
		static const Processor PROCESSOR_ANY	= 0xFFFFFFFF;
		static const Processor PROCESSOR_0		= (1 << 0);
		static const Processor PROCESSOR_1		= (1 << 1);
		static const Processor PROCESSOR_2		= (1 << 2);
		static const Processor PROCESSOR_3		= (1 << 3);
		static const Processor PROCESSOR_4		= (1 << 4);
		static const Processor PROCESSOR_5		= (1 << 5);
		static const Processor PROCESSOR_6		= (1 << 6);
		static const Processor PROCESSOR_7		= (1 << 7);

		Thread(ThreadPriority priority = NORMAL, Processor runnable_processors = PROCESSOR_ANY);
		virtual ~Thread();

		bool isRunning() const;	// returns true if the thread is running, false otherwise
		bool start();		// starts the thread's execution
		bool stop();		// stops the thread's execution
		unsigned int id() const;	// returns the pthread id of the thread (can be typecast as a pthread_t type)
		int currentCPU() const; 	// returns the CPU id that the thread is currently executing on

	protected:
		virtual void run() = 0; // this function is executed when the thread is started, this needs to be implemented by **you**

	private:
		bool running = false;

		ThreadPriority thread_priority;
		Processor thread_processors;

		pthread_t thread_handle;
		pthread_attr_t thread_attributes;
		sched_param scheduler_parameters;

		static void* threadKickstarter(void *args);

};

#endif // THREAD_H
