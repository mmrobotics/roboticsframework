#ifndef SIGNAL_PROCESSING_H
#define SIGNAL_PROCESSING_H

#include <vector>
#include <algorithm>
#include <functional>
#include <RoboticsToolbox.h>


/**
 * The SignalProc namespace contains utilities for processing a signal (other than filters). Filters can be
 * found in the Filters namespace.
 */
namespace SignalProc {

	/**
	 * @brief The Difference class is a simple class that finds the difference between consecutive values
	 * of a signal. It is meant to be used as an "online" form of keeping track of the difference between
	 * the current signal value and the immediately previous value.
	 *
	 * Each time a new signal measurement is taken, give this class the new data point with the update()
	 * function. The difference value is returned by the value() member function. This class only keeps
	 * track of the last two data points it was given and has no memory beyond these points. If you desire
	 * differences for an entire "pre-recorded" signal, use the findDifferenceSignal() function.
	 *
	 * @tparam T Type of the data (e.g. `int`, `double`, etc.)
	 */
	template<typename T>
	class Difference {

		private:
			bool initialized;
			T previous;
			T difference;

		public:
			/**
			 * @brief Constructor for the Difference class
			 */
			Difference() : initialized(false) {}

			/**
			 * @brief Constructor for the Difference class
			 * @param previous Initial value for the previous value.
			 */
			Difference(const T& previous) : initialized(true), previous(previous) {}

			/**
			 * @brief Updates the difference with a new data point.
			 * @param next The next data point received
			 */
			void update(const T& next) {

				if (!initialized) {
					previous = next;
					difference = next - previous; // aka, zero but I can't write just 0 because it's a template
					initialized = true;
				}
				else {
					difference = next - previous;
					previous = next;
				}

			}

			/**
			 * @brief Returns the current value of the difference
			 * @return Value of the difference
			 */
			T value() const { return difference; }

			/**
			 * @brief Resets the Difference class
			 */
			void clear() { initialized = false; }

	};


	/**
	 * @brief Computes an std::vector that contains the difference between consecutive data points
	 * of an input std::vector.
	 *
	 * The resulting std::vector will be one data point shorter than the input std::vector. The
	 * first data point in the result will be the difference between the second and first data
	 * points of a. The last data point will be the difference between the last and penultimate
	 * data points of a.
	 * @param a An std::vector for which to find the difference signal.
	 * @param d An empty std::vector into which the difference signal is placed. If d is not empty,
	 * this function will just append the difference signal to d.
	 * @tparam T Type of the data (e.g. `int`, `double`, etc.)
	 */
	template<typename T>
	void findDifferenceSignal(const std::vector<T>& a, std::vector<T> d) {

		double difference = 0;
		double eps = 2.22e-16;

		typename std::vector<T>::const_iterator iPrev = a.begin();
		for (typename std::vector<T>::const_iterator i = a.begin() + 1; i != a.end(); ++i) {

			difference = *i - *iPrev;

			if (difference == 0.0)
				d.push_back(-eps);
			else
				d.push_back(difference);

			iPrev++;

		}

	}


	/**
	 * @brief WARNING! THIS FUNCTION HAS NOT BEEN VALIDATED AND MAY NEED MODIFICATION. ONCE IT HAS BEEN VALIDATED, PLEASE COMPLETE THIS DOXYGEN COMMENT
	 *
	 * Finds the zero crossings using the derivative -- so collects all possible points instead of just the highest as findpeaks() does
	 * @param s
	 * @param isPeak
	 * @param peakLocations
	 * @param peakMagnitudes
	 * @tparam T Type of the data (e.g. `int`, `double`, etc.)
	 */
	template<typename T>
	void findZeroCrossings(std::vector<T> s, int isPeak, std::vector<T>& peakLocations, std::vector<T>& peakMagnitudes) {

		// Wanting to find the valleys, so negate data
		if (isPeak == -1)
			std::transform(s.begin(), s.end(), s.begin(), std::bind1st(std::multiplies<T>(), -1));

		std::vector<T> sDiff;
		findDifferenceSignal(s, sDiff);

		// Find the points where the derivative changes sign
		int count = 0;
		typename std::vector<T>::iterator i2 = sDiff.begin();
		for (typename std::vector<T>::iterator i = sDiff.begin() + 1; i != (sDiff.end()); ++i) {

			if ((*i) * (*i2) < 0) {
				peakLocations.push_back(count + 1);
				peakMagnitudes.push_back(s.at(count + 1));
			}

			count++;
			i2++;

		}

	}


	/**
	 * @brief WARNING! THIS FUNCTION HAS NOT BEEN VALIDATED AND MAY NEED MODIFICATION. ONCE IT HAS BEEN VALIDATED, PLEASE COMPLETE/FIX THIS DOXYGEN COMMENT
	 *
	 *  Finds the peaks or the valleys of a signal based on the derivative -
	 *  based on the Matlab code peakfinder.m from the Matlab exchange
	 * @param v vector to find the peaks/valleys
	 * @param isPeak boolean that determines if function searches for peaks (1) or valleys (-1)
	 * @param peakLocations saves the indexes of the peaks to this memory location
	 * @param peakMagnitudes saves the magnitudes of the peaks to this memory location
	 * @tparam T Type of the data (e.g. `int`, `double`, etc.)
	 */
	template<typename T>

	void findPeaks(std::vector<T> v, int isPeak, std::vector<T>& peakLocations, std::vector<T>& peakMagnitudes) {

		std::vector<T> peakLoc;
		std::vector<T> peakMag;
		bool foundPeak = false;
		bool firstPass = true;

		if(isPeak == -1){//wanting to find the valleys, so negate data
			std::transform(v.begin(), v.end(), v.begin(), std::bind1st(std::multiplies<T>(), -1));
		}

		std::vector<T> der_v;
		findDifferenceSignal(v, der_v);

		std::vector<int> v_ind;
		std::vector<T> v_peaks;
		int count = 0;
		int s = v.size();
		T threshMax;
		T max = *std::max_element(v.begin(), v.end());
		T min = *std::min_element(v.begin(), v.end());
		if(max > 0){
			threshMax = .9*max;
		}else{
			threshMax = 1.1*max;
		}

		T minMag = min;
		T tempLoc = 0;
		T leftMin = minMag;
		T tempMag = min;
		T sel = (max-min)/4;

		//add the first point to possible peaks
		v_ind.push_back(0);
		v_peaks.push_back(v.at(0));

		typename std::vector<T>::iterator i2 = der_v.begin();

		T temp1 = 0;
		T temp2 = 0;
		//find the points where the derivative changes sign
		for (typename std::vector<T>::iterator i = der_v.begin(); i!=(der_v.end()); ++i) {
			if(count == 0){//first time through need to increment the iterator, so that looking at adjacent points
				i++;
			}
			temp1 = *i;
			temp2 = *i2;
			if(temp1*temp2 < 0){
				v_ind.push_back(count+1);
				v_peaks.push_back(v.at(count+1));
			}

			count++;
			i2++;
		}

		//and the last points add them as possible peaks
		v_ind.push_back(s-1);
		v_peaks.push_back(v.at(s-1));


		unsigned long len = v_ind.size();


		if(len > 2){

			typename std::vector<T>::iterator peakIter = v_peaks.begin();
			unsigned int peakCount = 0;
			int sign1 = Robo::sign((*(peakIter+1) - *peakIter));
			int sign2 = Robo::sign(*(peakIter+2) - *(peakIter+1));
			if( sign1 <= 0){
				peakCount = 0;
				if(sign1 == sign2){//signs dont alternate - get rid of second point
					v.erase(peakIter+1);
					v_ind.erase(v_ind.begin()+1);
					len--;
				}

			}else{
				peakCount = 1;
				peakIter++;
				if(sign1 == sign2){//signs dont alternate
					v.erase(peakIter);
					v_ind.erase(v_ind.begin());
					len--;
				}
			}


			//go through possible peak/valleys and decide which is which
			while (peakCount < len){
				if(!firstPass){
					peakIter++;//matlab is 1 based we are 0 based, this is the reason we are skipping over
					//the peaks see if there is a better way to do this...
					//also need to test on the other array but it seems to be finding the peaks currently
					peakCount++;

				}
				firstPass = false;
				if(foundPeak){
					tempMag = minMag;
					foundPeak = false;
				}
				if(peakCount != len){
					if(*peakIter > tempMag && *peakIter > (leftMin + sel)){
						tempLoc = peakCount;
						tempMag = *peakIter;
					}


					//move onto valley
					peakIter++;
					peakCount++;
					//must be at least sel less than the peak
					if(!foundPeak && tempMag > (sel + *peakIter)){
						foundPeak = true;
						leftMin = *peakIter;
						peakLoc.push_back(v_ind.at(tempLoc));
						peakMag.push_back(tempMag);

					}else if(*peakIter < leftMin){
						leftMin = *peakIter;
					}
				}
			}
			//checking endpoint
			peakIter = v.end();
			peakIter--;//go to last point in vector
			if(*peakIter > tempMag && *peakIter > leftMin + sel){
				peakLoc.push_back(v_ind.at(len-1));
				peakMag.push_back(*peakIter);

			}else if(!foundPeak && tempMag > minMag){
				peakLoc.push_back(v_ind.at(tempLoc));
				peakMag.push_back(tempMag);
			}
			int p_cnt = 0;
			for (typename std::vector<T>::iterator p = peakMag.begin(); p!=(peakMag.end()); ++p) {
				if(*p > threshMax){
					peakLocations.push_back(peakLoc.at(p_cnt));
					peakMagnitudes.push_back(*p);

				}
				//peakLocations = peakLoc;
				//peakMagnitudes = peakMag;
				p_cnt++;
			}
		}

	}

} // End SignalProc namespace

#endif // SIGNAL_PROCESSING_H
