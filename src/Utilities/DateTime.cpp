#include "DateTime.h"

#include <ctime>
#include <errno.h>
#include <cstring>
#include <string>
#include <iostream>


/**
 * @brief Creates a DateTime object with the current time.
 */
DateTime::DateTime() {

	update();

}


/**
 * @brief Updates this DateTime object to the current time.
 * @return True if successful
 */
bool DateTime::update() {

	// Get the total number of seconds since 1970-01-01 00:00:00 +0000 (UTC)
	_rawTime = time(nullptr);

	if (_rawTime < 0) {
		std::cout << "TIMER: an error occurred when getting the current time: " + std::string(strerror(errno)) + " ERROR";
		return false;
	}

	// Get the broken-down time struture used for calendar dates with second precision
	tzset(); // POSIX documentation says to call this function before calling localtime_r() for portability
	localtime_r(&_rawTime, &_brokenDownTime);

	return true;

}


/**
 * @brief Returns a string of the form "hours.minutes.seconds_AM/PM". Hours are 1-12.
 * @return
 */
std::string DateTime::HMS() const{

	char timeString[100];
	strftime(timeString, 100, "%I.%M.%S_%p", &_brokenDownTime);
	return timeString;

}


/**
 * @brief Returns a string of the form "hours.minutes.seconds". Hours are 0-23.
 * @return
 */
std::string DateTime::HMS24() const {

	char timeString[100];
	strftime(timeString, 100, "%H.%M.%S", &_brokenDownTime);
	return timeString;

}


/**
 * @brief Returns a string of the form "MM-DD-YYYY".
 * @return
 */
std::string DateTime::MDY() const {

	char timeString[100];
	strftime(timeString, 100, "%m-%d-%Y", &_brokenDownTime);
	return timeString;

}


/**
 * @brief Returns a string of the form "YYYY-MM-DD".
 * @return
 */
std::string DateTime::YMD() const {

	char timeString[100];
	strftime(timeString, 100, "%Y-%m-%d", &_brokenDownTime);
	return timeString;

}


/**
 * @brief Returns a string of the form "MM-DD-YYYY_hours.minutes.seconds_AM/PM". Hours are 1-12.
 * @return
 */
std::string DateTime::MDYHMS() const {

	char timeString[100];
	strftime(timeString, 100, "%m-%d-%Y_%I.%M.%S_%p", &_brokenDownTime);
	return timeString;

}


/**
 * @brief Returns a string of the form "YYYY-MM-DD_hours.minutes.seconds_AM/PM". Hours are 1-12.
 * @return
 */
std::string DateTime::YMDHMS() const{

	char timeString[100];
	strftime(timeString, 100, "%Y-%m-%d_%I.%M.%S_%p", &_brokenDownTime);
	return timeString;

}


/**
 * @brief Returns a string of the form "MM-DD-YYYY_hours.minutes.seconds". Hours are 0-23.
 * @return
 */
std::string DateTime::MDYHMS24() const {

	char timeString[100];
	strftime(timeString, 100, "%m-%d-%Y_%H.%M.%S", &_brokenDownTime);
	return timeString;

}


/**
 * @brief Returns a string of the form "YYYY-MM-DD_hours.minutes.seconds". Hours are 0-23.
 * @return
 */
std::string DateTime::YMDHMS24() const {

	char timeString[100];
	strftime(timeString, 100, "%Y-%m-%d_%H.%M.%S", &_brokenDownTime);
	return timeString;

}


/**
 * @brief Returns a string as returned by the asctime() function in the time.h C library. This format
 * is roughly "<Day of Week> <Month abbreviation> <Day> HH:MM:SS YYYY\n"
 * @return
 */
std::string DateTime::toString() const {

	return asctime(&_brokenDownTime);

}

/**
 * @brief Overloaded cast which converts the DateTime to an std::string using toString().
 */
DateTime::operator std::string()const {

	return toString();

}
