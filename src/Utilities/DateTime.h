#ifndef DATE_TIME_H
#define DATE_TIME_H

#include <ctime>
#include <string>

/**
 * @brief The DateTime class is used to get the current real-world date and time with second precision which can be expressed as std::strings
 * of various formats.
 *
 * All formats are suitable to be used in filenames as they do not contain whitespace or "/" characters. This class uses standard C/C++
 * functions in its implementation which in turn use the standard Epoch [1970-01-01 00:00:00 +0000 (UTC)] as the reference point
 * (0 seconds). Assuming you are using a system which uses 64 bits to represent a variable of type 'time_t' (which is the case for most
 * 64-bit Linux systems), then this class will correctly retrieve the date and time for 292 billion years after the 1970 reference point;
 * at which point the underlying 64-bit number representing seconds since the Epoch will overflow.
 */
class DateTime {

	private:
		time_t _rawTime;
		tm _brokenDownTime;

	public:
		DateTime();

		bool update();

		std::string HMS() const;
		std::string HMS24() const;
		std::string MDY() const;
		std::string YMD() const;
		std::string MDYHMS() const;
		std::string YMDHMS() const;
		std::string MDYHMS24() const;
		std::string YMDHMS24() const;
		std::string toString() const;

		operator std::string() const;

};

#endif // DATE_TIME_H
