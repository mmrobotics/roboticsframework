#include "Thread.h"
#include <iostream>
#include <unistd.h>
#include <string.h>


Thread::Thread(ThreadPriority priority, Processor runnable_processors) {

	thread_priority = priority;
	thread_processors = runnable_processors;

}


Thread::~Thread() {

	if (isRunning())
		stop();

}


/**
 * @brief Starts the thread. This function can be called from any thread.
 * @return True if successful
 */
bool Thread::start() {

	int ret = 0;

	if (isRunning()) {
		std::cerr << "THREAD: can't start thread... thread already started. (thread " << id() << ") WARNING" << std::endl;
		return false;
	}

	running = true;

	// Start the thread
	if ((ret = pthread_create(&thread_handle, nullptr, &threadKickstarter, (void*)this)) != 0) {
		std::cerr << "THREAD: there was an error starting the thread - " << strerror(ret) << ". (thread " << id() << ") ERROR" << std::endl;
		return false;
	}

	return true;

}


/**
 * @brief Performs all the necessary tasks to get the thread up and running such as setting the priority and processor affinity.
 */
void *Thread::threadKickstarter(void* arg) {

	int ret = 0;

	Thread* thread_object = (Thread*)arg;

	// Set the realtime scheduling policy and priority
	int policy;
	int priority; // 0 is the lowest, 99 is the highest	
	switch (thread_object->thread_priority) {

		case NORMAL:
			policy = SCHED_OTHER; // use the default scheduler
			priority = 0;
			break;
		case REALTIME_LOW:
			policy = SCHED_FIFO;
			priority = 33;
			std::cout << "THREAD: setting realtime priority to low. (thread " << thread_object->id() << ")" << std::endl;
			break;
		case REALTIME_MEDIUM:
			policy = SCHED_FIFO;
			priority = 66;
			std::cout << "THREAD: setting realtime priority to medium. (thread " << thread_object->id() << ")" << std::endl;
			break;
		case REALTIME_HIGH:
			policy = SCHED_FIFO;
			priority = 99;
			std::cout << "THREAD: setting realtime priority to high. (thread " << thread_object->id() << ")" << std::endl;
			break;
		default:
			policy = SCHED_OTHER; // use the default scheduler
			priority = 0;
			break;		

	}

	struct sched_param param;
	param.sched_priority = priority;	
	
	if ((ret = pthread_setschedparam(thread_object->thread_handle, policy, &param)) != 0) { // set the Linux scheduler and priority
		std::cerr << "THREAD: could not set the desired scheduler and priority. " << strerror(ret) << " Warning! (thread " << thread_object->id() << ")" << std::endl;
		std::cerr << "   *** Does your user have the appropriate permissions in the /etc/security/limits.conf file?  (see the Thread class documentation for details) ***" << std::endl;
	}

	// Set the processor affinity
	int cpu_count = sysconf(_SC_NPROCESSORS_ONLN); // get the number of cpus on the system
	
	cpu_set_t* processor_set = CPU_ALLOC(cpu_count);
	CPU_ZERO_S(CPU_ALLOC_SIZE(cpu_count), processor_set);
	Processor mask = 1;
	for (int aa = 0; aa < std::min(cpu_count, 32); ++aa) {
		if (mask & thread_object->thread_processors) {
			CPU_SET_S(aa, CPU_ALLOC_SIZE(cpu_count), processor_set);
		}
		mask = (mask << 1);
	}
	
	if ((ret = pthread_setaffinity_np(thread_object->thread_handle, CPU_ALLOC_SIZE(cpu_count), processor_set)) != 0) { // set the affinity
		std::cerr << "THREAD: could not set the desired processor affinity. " << strerror(ret) << " Warning! (thread " << thread_object->id() << ")" << std::endl;
	}
	
	CPU_FREE(processor_set);
	
	// Run the user's code
	thread_object->run();
	
	thread_object->running = false;

	std::cout << "THREAD: thread has finished." << std::endl;
	
	pthread_exit(nullptr);

}


/**
 * @brief Stops the thread and waits until the thread has terminated before returning.
 * @return
 */
bool Thread::stop() {

	if (!isRunning())
		return true;

	running = false;
	
	// Get the calling thread id, don't join() if the stop function is called within the running thread
	if (pthread_equal(pthread_self(), thread_handle)) {
		return true;
	}

	// Wait until the thread has quit before moving on
	int ret = 0;
	if ((ret = pthread_join(thread_handle, nullptr)) != 0) {
		std::cerr << "THREAD: there was an error stopping the thread: " << strerror(ret) << ". (thread " << id() << ") ERROR" << std::endl;
		return false;
	}

	return true;

}


/**
 * @brief Returns true if this thread is running.
 * @return
 */
bool Thread::isRunning() const {

	return running;

}


/**
 * @brief Returns the thread ID which is the `pthread_t` handle cast to an `unsigned int`.
 * @return
 */
unsigned int Thread::id() const {

	return (unsigned int)thread_handle;

}


/**
 * @brief Returns the CPU ID that the thread is currently executing on.
 * @return
 */
int Thread::currentCPU() const {

	return sched_getcpu();

}
