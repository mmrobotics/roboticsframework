#ifndef RECORDER_H
#define RECORDER_H

#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QDir>


/**
 * @brief The Recorder class is a convenience class that can be used to record data to a file.
 * A master directory can be set so all files created by Recorder objects are placed in the
 * same base directory.
 *
 * This class is well suited to recording data that is produced at a regular interval or
 * infrequently/once (e.g. the initial conditions of a controller, etc.). However, if you want
 * to record data that is produced at a regular interval, then you should consider formatting
 * the data into a StateMessage and record it with StateDataUI (which uses Recorder
 * internally). Using StateDataUI instead of Recorder directly provides the advantages of being
 * able to visualize the data in real-time and write the data to file in a standard way. This
 * standard way uses only a few files for the data and the lab has a repository called `libmatlab`
 * with MATLAB code capable of reading that data into MATLAB easily.
 *
 * Each Recorder object is associated with a single file. The filename is specified either in
 * the constructor or in the open() function. If you specify the filename in the constructor,
 * the open() function is called automatically. The filename is appended to the master directory
 * before it is used. This way, all files created by Recorder objects can be found in the same
 * place. The master directory is set using setMasterRecordingDirectory() and should be set at
 * the beginning of your program before any Recorder objects are created (i.e. on one of the
 * first lines of main()). If you change the master directory after a Recorder object has been
 * opened (by use of the open() function) the new directory will have no affect on the already
 * opened Recorder object. The default master directory is `Output/`.
 *
 * The Recorder class will overwrite the file if it already exists. If this is not the desired
 * behavior, the Recorder class offers the static function nextAvailableFilename() for
 * convenience which can be used to automatically append a number to a base filename if the
 * file already exists such that the Recorder class will always create a new file.
 *
 * It is also possible to toggle whether Recorder objects will actually record data using the
 * setIsRecordingMaster() function. If this function is given `false`, all Recorder objects will
 * ignore data that they are asked to record. If the function is given a `true`, then Recorder
 * objects will behave normally. This allows you to turn off all recording if desired, or even
 * briefly disable recording during some portion of your program. The default state is for
 * recording to occur.
 *
 * Data can be recorded using one of the record() functions or by using the insertion operator
 * (the operator<<() function). See the documentation for each of these functions to see how
 * the data is recorded for each. The flush() function can be used to make sure all the
 * currently recorded data actually gets written to file. Use this function when you are worried
 * that your program may crash and you want to make sure that data is recorded as close as possible
 * to the crash.
 *
 * Recorder keeps track of the number of times record() has been called. When the file is closed,
 * this number is reported on the console.
 */
class Recorder {

	private:
		std::ofstream file_stream;
		QString filename;
		unsigned int count;
		bool usedInsertionOperator;
		static QDir masterDirectoryPath;
		static bool isRecordingMaster;

	public:
		Recorder() = default;
		Recorder(const QString& filename);
		~Recorder();

		/**
		 * @brief Sets whether all Recorder objects should record or not. The default state is for all
		 * Recorder objects to record.
		 * @param shouldRecord True if you want all Recorder objects to record, false otherwise
		 */
		static void setIsRecordingMaster(bool shouldRecord) { isRecordingMaster = shouldRecord; }
		/**
		 * @brief Sets the master directory for all files created by Recorder objects. This directory is prepended
		 * to the filename provided either in the constructor or the open() function. The default directory is
		 * `Output/`.
		 * @param masterDirectory The directory you wish as the master directory
		 */
		static void setMasterRecordingDirectory(const QString& masterDirectory) { masterDirectoryPath = QDir(QDir::cleanPath(masterDirectory)); }

		static QString nextAvailableFilename(const QString& filebase, const QString& extension);

		void open(const QString& filename);
		bool isOpened();
		void close();
		void flush();

		template<typename T>
		bool record(double time, const T& data);
		template<typename T>
		bool record(long time, const T& data);
		template<typename T>
		bool record(const T& data);

		template<typename T>
		Recorder& operator<<(const T& data);
	
};


/**
 * @brief This function uses the insertion operator to insert the `time` and `data` parameters into
 * the std::ofstream object that represents the file. The format is: \<time\>:\<data\>\\n.
 *
 * You must make sure that the type of `data` is capable of being used with the insertion operator
 * of std::ofstream. If this is not the case, your code will not compile.
 * @param time A timestamp for the line of data
 * @param data The data to record (which is compatible with the std::ofstream insertion operator)
 * @return True if successful, false if unsuccessful or the master recording is turned off
 */
template<typename T>
bool Recorder::record(double time, const T& data) {

	if (isRecordingMaster) {

		if (!file_stream.is_open()) {
			std::cerr << "RECORDER: can't record data, file not opened. ERROR" << std::endl;
			return false;
		}

		file_stream << time << ":";
		// When inserting "data" it needs to be the first one so the proper operator overload gets called.
		file_stream << data << "\n";
		count += 1;

		return true;

	}

	return false;

}


/**
 * @brief This function uses the insertion operator to insert the `time` and `data` parameters into
 * the std::ofstream object that represents the file. The format is: \<time\>:\<data\>\\n.
 *
 * You must make sure that the type of `data` is capable of being used with the insertion operator
 * of std::ofstream. If this is not the case, your code will not compile.
 * @param time A timestamp for the line of data
 * @param data The data to record (which is compatible with the std::ofstream insertion operator)
 * @return True if successful, false if unsuccessful or the master recording is turned off
 */
template<typename T>
bool Recorder::record(long time, const T& data) {

	if (isRecordingMaster) {

		if (!file_stream.is_open()) {
			std::cerr << "RECORDER: can't record data, file not opened. ERROR" << std::endl;
			return false;
		}

		file_stream << time << ":";
		// When inserting "data" it needs to be the first one so the proper operator overload gets called.
		file_stream << data << "\n";
		count += 1;

		return true;

	}

	return false;

}


/**
 * @brief This function uses the insertion operator to insert the `data` parameter into the
 * std::ofstream object that represents the file. There is no special format, the `data` parameter
 * is simply inserted and nothing else is done.
 *
 * You must make sure that the type of `data` is capable of being used with the insertion operator
 * of std::ofstream. If this is not the case, your code will not compile.
 * @param data The data to record (which is compatible with the std::ofstream insertion operator)
 * @return True if successful, false if unsuccessful or the master recording is turned off
 */
template<typename T>
bool Recorder::record(const T& data) {

	if (isRecordingMaster) {

		if (!file_stream.is_open()) {
			std::cerr << "RECORDER: can't record data, file not opened. ERROR" << std::endl;
			return false;
		}

		file_stream << data;
		count += 1;

		return true;

	}

	return false;

}


template<typename T>
/**
 * @brief This function uses the insertion operator to insert the `data` parameter into the
 * std::ofstream object that represents the file. There is no special format, the `data` parameter
 * is simply inserted and nothing else is done.
 *
 * You must make sure that the type of `data` is capable of being used with the insertion operator
 * of std::ofstream. If this is not the case, your code will not compile.
 * @param data The data to record (which is compatible with the std::ofstream insertion operator)
 * @return The Recorder object
 */
Recorder& Recorder::operator<<(const T& data) {

	if (isRecordingMaster) {

		if (!file_stream.is_open()) {
			std::cerr << "RECORDER: can't record data, file not opened. ERROR" << std::endl;
			return *this;
		}

		file_stream << data;
		usedInsertionOperator = true;

	}

	return *this;

}

#endif // RECORDER_H
