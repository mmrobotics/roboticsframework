#include "Magnetics.h"

// TODO: Clean up this namespace

namespace Magnetics {

	CapsuleDipolePair::CapsuleDipolePair(double M_norm, double m_norm)
		: M_norm(M_norm),
		  m_norm(m_norm) {

		this->capsule_weight = 0;
		this->capsule_buoyancy = 0;
		this->M_hat = Robo::Vector3d::Zero();
		this->M_hat = Robo::Vector3d::Zero();
		this->M_position = Robo::Vector3d::Zero();
		this->m_position = Robo::Vector3d::Zero();

	}


	CapsuleDipolePair::CapsuleDipolePair(double capsule_weight /* N */, double capsule_buoyancy /* N */, double M_norm, double m_norm)
		: M_norm(M_norm),
		  m_norm(m_norm) {

		this->capsule_weight = capsule_weight;
		this->capsule_buoyancy = capsule_buoyancy;
		this->M_hat = Robo::Vector3d::Zero();
		this->M_hat = Robo::Vector3d::Zero();
		this->M_position = Robo::Vector3d::Zero();
		this->m_position = Robo::Vector3d::Zero();

	}


	CapsuleDipolePair::CapsuleDipolePair(double capsule_weight /* N */, double capsule_buoyancy /* N */, const Robo::Vector3d& M_hat, double M_norm, const Robo::Vector3d& m_hat, double m_norm)
		: M_norm(M_norm),
		  m_norm(m_norm) {

		this->capsule_weight = capsule_weight;
		this->capsule_buoyancy = capsule_buoyancy;
		this->M_hat = M_hat.normalized();
		this->M_position = Robo::Vector3d::Zero();
		this->m_position = Robo::Vector3d::Zero();

		if (m_hat.norm() > 0.0)
			this->m_hat = m_hat.normalized();

	}


	Robo::Vector3d CapsuleDipolePair::M() {

		if (this->M_hat.norm() < .01) {
			//cout << "DIPOLE: the M vector has not been set yet. WARNING" << endl;
		}

		return this->M_norm*this->M_hat;

	}


	Robo::Vector3d CapsuleDipolePair::MHat() {

		if (this->M_hat.norm() < .01) {
			//cout << "DIPOLE: the M vector has not been set yet. WARNING" << endl;
		}

		return this->M_hat;

	}


	double CapsuleDipolePair::MNorm() {

		return M_norm;

	}


	void CapsuleDipolePair::setMHat(const Robo::Vector3d& M_hat) {

		this->M_hat = M_hat.normalized();

	}


	void CapsuleDipolePair::setMPosition(const Robo::Vector3d& M_p /* m */) {

		this->M_position = M_p;

	}


	Robo::Vector3d CapsuleDipolePair::m() {

		if (this->m_hat.norm() < .01) {
			//cout << "DIPOLE: the m vector has not been set yet. WARNING" << endl;
		}

		return this->m_norm*this->m_hat;

	}


	Robo::Vector3d CapsuleDipolePair::mHat() {

		if (this->m_hat.norm() < .01) {
			//cout << "DIPOLE: the m vector has not been set yet. WARNING" << endl;
		}

		return this->m_hat;

	}


	double CapsuleDipolePair::mNorm() {

		return m_norm;

	}


	void CapsuleDipolePair::setmHat(const Robo::Vector3d& m_hat) {

		this->m_hat = m_hat.normalized();

	}


	void CapsuleDipolePair::setmPosition(const Robo::Vector3d& m_p /* m */) {

		this->m_position = m_p;

	}


	Robo::Vector3d CapsuleDipolePair::force(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat, double m_norm /* A m^2 */) {

		if (this->M_hat.norm() < .5) {
			//cout << "DIPOLE: the M vector has not been set yet, can't compute force. ERROR" << endl;
			return Robo::Vector3d::Zero();
		}

		Robo::Vector3d m_hat_tmp;
		if (m_hat.norm() > 0.0)
			m_hat_tmp = m_hat.normalized();
		else
			m_hat_tmp = this->m_hat;

		double force_scalar;
		if (m_norm > 0.0)
			force_scalar = 3.0*MU_0*m_norm*this->M_norm/(4.0*M_PI*pow(p.norm(), 4.0));
		else
			force_scalar = 3.0*MU_0*this->m_norm*this->M_norm/(4.0*M_PI*pow(p.norm(), 4.0));

		Robo::Vector3d p_hat = p.normalized();

		Robo::Matrixd33 F = computeFMatrix(this->M_hat, m_hat_tmp, p_hat);

		return force_scalar*F*p_hat;

	}


	Robo::Vector3d CapsuleDipolePair::field(const Robo::Vector3d& p /* m */) {

		Robo::Matrixd33 H = computeHMatrix(p.normalized());

		double field_scalar = M_norm/(4.0*M_PI*pow(p.norm(), 3.0));

		return field_scalar*H*M_hat;

	}


	Robo::Matrixd33 CapsuleDipolePair::computeFMatrix(const Robo::Vector3d& M_hat, const Robo::Vector3d& m_hat, const Robo::Vector3d& p_hat) {

		Robo::Vector3d M_hat_tmp = M_hat.normalized();
		Robo::Vector3d m_hat_tmp = m_hat.normalized();
		Robo::Vector3d p_hat_tmp = p_hat.normalized();

		return M_hat_tmp*m_hat_tmp.transpose() + m_hat_tmp*M_hat_tmp.transpose() + m_hat_tmp.dot((Robo::I33-5.0*p_hat_tmp*p_hat_tmp.transpose())*M_hat_tmp)*Robo::I33;

	}


	Robo::Matrixd33 CapsuleDipolePair::computeHMatrix(const Robo::Vector3d& p_hat) {

		Robo::Vector3d p_hat_tmp = p_hat.normalized();

		return 3.0*p_hat_tmp*p_hat_tmp.transpose() - Robo::I33;

	}


	Robo::Matrixd33 CapsuleDipolePair::computeHInverseMatrix(const Robo::Vector3d& p_hat) {

		Robo::Vector3d p_hat_tmp = p_hat.normalized();

		return 1.5*p_hat_tmp*p_hat_tmp.transpose() - Robo::I33;

	}


	Robo::Matrixd33 CapsuleDipolePair::force_p_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat, double m_norm) {

		Robo::Vector3d p_hat = p.normalized();
		Robo::Matrixd33 p_hat_times_p_hat_transpose = p_hat*p_hat.transpose();
		Robo::Matrixd33 I_minus_five_p_hat_times_p_hat_transpose = (Robo::I33-5.0*p_hat_times_p_hat_transpose);
		Robo::Matrixd33 H = computeHMatrix(p_hat);

		Robo::Vector3d m_hat_tmp;
		if (m_hat.norm() > 0.0)
			m_hat_tmp = m_hat.normalized();
		else
			m_hat_tmp = this->m_hat;

		Robo::Matrixd33 F = computeFMatrix(this->M_hat, m_hat_tmp, p_hat);

		double jacobian_scalar;
		if (m_norm > 0.0)
			jacobian_scalar = 3.0*MU_0*this->M_norm*m_norm/(4.0*M_PI*pow(p.norm(), 5.0));
		else
			jacobian_scalar = 3.0*MU_0*this->M_norm*this->m_norm/(4.0*M_PI*pow(p.norm(), 5.0));

		return jacobian_scalar*(F*I_minus_five_p_hat_times_p_hat_transpose - 5.0*p_hat_times_p_hat_transpose*(F+m_hat_tmp.dot(H*this->M_hat)*Robo::I33));

	}


	Robo::Matrixd33 CapsuleDipolePair::force_m_hat_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat, double m_norm) {

		Robo::Vector3d p_hat = p.normalized();
		Robo::Matrixd33 p_hat_times_p_hat_transpose = p_hat*p_hat.transpose();
		Robo::Matrixd33 I_minus_five_p_hat_times_p_hat_transpose = (Robo::I33-5.0*p_hat_times_p_hat_transpose);

		double jacobian_scalar;
		if (m_norm > 0.0)
			jacobian_scalar = 3.0*MU_0*this->M_norm*m_norm/(4.0*M_PI*pow(p.norm(), 4.0));
		else
			jacobian_scalar = 3.0*MU_0*this->M_norm*this->m_norm/(4.0*M_PI*pow(p.norm(), 4.0));

		Robo::Vector3d m_hat_tmp;
		if (m_hat.norm() > 0.0)
			m_hat_tmp = m_hat.normalized();
		else
			m_hat_tmp = this->m_hat;

		Robo::Matrixd33 I_minus_m_hat_times_m_hat_transpose = (Robo::I33 - m_hat_tmp*m_hat_tmp.transpose());

		return jacobian_scalar*(this->M_hat.dot(p_hat)*I_minus_five_p_hat_times_p_hat_transpose + this->M_hat*p_hat.transpose() + p_hat*this->M_hat.transpose())*I_minus_m_hat_times_m_hat_transpose;

	}


	Robo::Matrixd33 CapsuleDipolePair::force_M_hat_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat, double m_norm) {

		Robo::Vector3d p_hat = p.normalized();
		Robo::Matrixd33 p_hat_times_p_hat_transpose = p_hat*p_hat.transpose();
		Robo::Matrixd33 I_minus_five_p_hat_times_p_hat_transpose = (Robo::I33-5.0*p_hat_times_p_hat_transpose);
		Robo::Matrixd33 I_minus_M_hat_times_M_hat_transpose = (Robo::I33 - this->M_hat*this->M_hat.transpose());

		double jacobian_scalar;
		if (m_norm > 0.0)
			jacobian_scalar = 3.0*MU_0*this->M_norm*m_norm/(4.0*M_PI*pow(p.norm(), 4.0));
		else
			jacobian_scalar = 3.0*MU_0*this->M_norm*this->m_norm/(4.0*M_PI*pow(p.norm(), 4.0));

		Robo::Vector3d m_hat_tmp;
		if (m_hat.norm() > 0.0)
			m_hat_tmp = m_hat.normalized();
		else
			m_hat_tmp = this->m_hat;

		return jacobian_scalar*(m_hat_tmp.dot(p_hat)*I_minus_five_p_hat_times_p_hat_transpose + m_hat_tmp*p_hat.transpose() + p_hat*m_hat_tmp.transpose())*I_minus_M_hat_times_M_hat_transpose;

	}


	Robo::Matrixd33 CapsuleDipolePair::field_hat_p_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat) {

		Robo::Vector3d p_hat = p.normalized();
		Robo::Matrixd33 p_hat_times_p_hat_transpose = p_hat*p_hat.transpose();
		Robo::Matrixd33 H = computeHMatrix(p.normalized());
		double norm_H_times_M_hat = (H*this->M_hat).norm();

		Robo::Vector3d m_hat_tmp;
		if (m_hat.norm() > 0.0)
			m_hat_tmp = m_hat.normalized();
		else
			m_hat_tmp = this->m_hat;

		Robo::Matrixd33 I_minus_m_hat_times_m_hat_transpose = (Robo::I33 - m_hat_tmp*m_hat_tmp.transpose());

		return 3.0*I_minus_m_hat_times_m_hat_transpose*(this->M_hat.dot(p_hat)*Robo::I33 + p_hat*this->M_hat.transpose())*(Robo::I33 - p_hat_times_p_hat_transpose)/(norm_H_times_M_hat*p.norm());

	}


	Robo::Matrixd33 CapsuleDipolePair::field_hat_M_hat_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat) {

		Robo::Matrixd33 H = computeHMatrix(p.normalized());
		double norm_H_times_M_hat = (H*this->M_hat).norm();

		Robo::Vector3d m_hat_tmp;
		if (m_hat.norm() > 0.0)
			m_hat_tmp = m_hat.normalized();
		else
			m_hat_tmp = this->m_hat;

		Robo::Matrixd33 I_minus_m_hat_times_m_hat_transpose = (Robo::I33 - m_hat_tmp*m_hat_tmp.transpose());


		return I_minus_m_hat_times_m_hat_transpose*H/norm_H_times_M_hat;

	}


	Robo::Matrixd33 CapsuleDipolePair::M_hat_field_hat_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat) {

		Robo::Matrixd33 H_inverse = computeHInverseMatrix(p.normalized());


		Robo::Vector3d m_hat_tmp;
		if (m_hat.norm() > 0.0)
			m_hat_tmp = m_hat.normalized();
		else
			m_hat_tmp = this->m_hat;

		double norm_H_inverse_times_m_hat = (H_inverse*m_hat_tmp).norm();

		Robo::Matrixd33 I_minus_M_hat_times_M_hat_transpose = (Robo::I33 - this->M_hat*this->M_hat.transpose());

		return I_minus_M_hat_times_M_hat_transpose*H_inverse/norm_H_inverse_times_m_hat;

	}


	Robo::Matrixd33 CapsuleDipolePair::M_hat_p_jacobian(const Robo::Vector3d& p /* m */, const Robo::Vector3d& m_hat) {

		Robo::Vector3d p_hat = p.normalized();
		Robo::Matrixd33 p_hat_times_p_hat_transpose = p_hat*p_hat.transpose();
		Robo::Matrixd33 H_inverse = computeHInverseMatrix(p_hat);


		Robo::Vector3d m_hat_tmp;
		if (m_hat.norm() > 0.0)
			m_hat_tmp = m_hat.normalized();
		else
			m_hat_tmp = this->m_hat;

		double norm_H_inverse_times_m_hat = (H_inverse*m_hat_tmp).norm();

		Robo::Matrixd33 I_minus_M_hat_times_M_hat_transpose = (Robo::I33 - this->M_hat*this->M_hat.transpose());

		return 3.0*I_minus_M_hat_times_M_hat_transpose*(m_hat_tmp.dot(p_hat)*Robo::I33 + p_hat*m_hat_tmp.transpose())*(Robo::I33 - p_hat_times_p_hat_transpose)/(2.0*norm_H_inverse_times_m_hat*p.norm());

	}

}
