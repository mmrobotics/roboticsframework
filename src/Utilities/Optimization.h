#ifndef OPTIMIZATION_H
#define OPTIMIZATION_H

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <cmath>
#include <RoboticsToolbox.h>


/**
 * The Optimization namespace requires the GNU Scientific Library for root finding.  Make sure it's installed,
 * then add -lgsl -lgslcbblas -lm to your linker flags.  This also requires the Eigen linear algebra library.
 */
namespace Optimization {

	/**
	 * @brief The RootFinder class implements a derivative-based method for finding zeros of the given
	 * Function1D function. It is basically a wrapper for the GSL Newton method. It takes as input a
	 * pointer to a Function1D type class. You should derive your own class from Function1D and
	 * implement the value() and derivative() functions.
	 */
	class RootFinder {

		public:
			// Base class to that must return the value and derivative of a 1D function at the
			// given value of x.
			class Function1D {
				public:
					virtual double value(double x) = 0;
					virtual double derivative(double x) = 0;
			};

		private:
			Function1D* function;
			double tolerance; // how accurate the result is
			unsigned int max_iterations;
			gsl_function_fdf gsl_function;
			gsl_root_fdfsolver* gsl_finder;

			static double functionValue(double x, void* function);
			static double derivativeValue(double x, void* function);
			static void functionDerivativeValue(double x, void* function, double* value, double* derivative);

		public:
			RootFinder(Function1D* function /* a pointer to your 1D function */,
				   double tolerance = 1e-4 /* how accurate the root location should be */,
				   unsigned int max_iterations = 0 /* max number of iterations to perform, leave empty if it doesn't matter */);
			~RootFinder();
			double find(double initial_x /* an initial guess of the root */);

	};
	
	
	/**
	 * @brief The PseudoInverse class generates the Moore-Penrose pseudoinverse of a given matrix using the singular
	 * value decomposition. In other words, this class can be used to solve the problem: min ||Ax-b||+||x||, which
	 * minimizes a least squares error while minimizing ||x|| if possible.
	 *
	 * The class is a template class whose parameter must be of type Eigen::Matrix from the Eigen Linear Algebra
	 * Library (Note the Matrix types in the Robo namespace are simply type redefinitions of Eigen::Matrix and
	 * can be used). The pseudoinverse is generated in the constructor of the class. Simply provide the matrix for
	 * which you wish to compute the pseudoinverse and the optional tolerance used to determine whether a particular
	 * singular value is considered to be zero. If this tolerance is less than or equal to 0, or is not provided, then
	 * the same default tolerance used by MATLAB is used. The pseudoinverse is accessed via the matrixPInv() function
	 * and if you wish to solve the problem described above for `x`, then use the solve() function and provide the
	 * `b` for which you wish to solve. The rank of the pseudoinverse can also be accessed via the rank() function.
	 * Since this class uses the singular value decomposition to compute the pseudoinverse, the elements of the singular
	 * decomposition of A are available via the matrixU(), matrixS(), and matrixV() functions where A = U*S*V<sup>T</sup>.
	 */
	template<typename T>
	class PseudoInverse {

		public:
			typedef Eigen::Matrix<typename T::Scalar, T::RowsAtCompileTime, 1, Eigen::DontAlign> InputVector;
			typedef Eigen::Matrix<typename T::Scalar, T::ColsAtCompileTime, 1, Eigen::DontAlign> OutputVector;
			typedef Eigen::Matrix<typename T::Scalar, T::ColsAtCompileTime, T::RowsAtCompileTime, Eigen::DontAlign> InverseMatrix;
			typedef Eigen::Matrix<typename T::Scalar, T::RowsAtCompileTime, T::RowsAtCompileTime, Eigen::DontAlign> UMatrix;
			typedef Eigen::Matrix<typename T::Scalar, T::RowsAtCompileTime, T::ColsAtCompileTime, Eigen::DontAlign> SMatrix;
			typedef Eigen::Matrix<typename T::Scalar, T::ColsAtCompileTime, T::ColsAtCompileTime, Eigen::DontAlign> VMatrix;

		private:
			InverseMatrix PInv;
			UMatrix U;
			SMatrix S;
			VMatrix V;
			int matrix_rank = 0;

		public:
			PseudoInverse(const T& A, double tolerance = -1.0) {

				Eigen::JacobiSVD<T> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
				U = svd.matrixU();
				S = SMatrix::Zero(A.rows(), A.cols());
				S.diagonal() = svd.singularValues();
				V = svd.matrixV();
				SMatrix pinvS = SMatrix::Zero(A.rows(), A.cols());

				if (tolerance <= 0.0)
					tolerance = std::max(A.cols(), A.rows()) * svd.singularValues()[0] * DBL_EPSILON; // same zero threshold that Matlab uses by default

				// Loop through the singular values and invert those that don't fall below the zero threshold
				for (int aa = 0; aa < std::min(A.rows(), A.cols()); aa++) {

					typename T::Scalar singularValue = svd.singularValues()[aa];
					if (singularValue > tolerance) {
						pinvS(aa, aa) = 1.0 / singularValue;
						matrix_rank += 1; // increment the matrix rank
					}
					else // we can break here because the singular values are sorted in decreasing order
						break;

				}

				PInv = V * pinvS.transpose() * U.transpose();

			}

			InverseMatrix matrixPInv() { return PInv; }
			int rank() { return matrix_rank; }
			OutputVector solve(const InputVector& b) { return PInv * b; }

			UMatrix matrixU() { return U; }
			SMatrix matrixS() { return S; }
			VMatrix matrixV() { return V; }

	};


	/**
	 * Solves the problem: min ||A*x-b|| s.t. ||x|| = r.
	 *
	 * The implemented solution is called "complete diagonalization" and is described
	 * in W.W. Hager. "Minimizing a quadratic over a sphere."  SIAM J. Optim.,
	 * 12(1):188-208, 2001.  The references to equations in the comments of this code
	 * refer to equations in this paper.
	 *
	 */
	template<typename T>
	class QuadraticMinimizationWithNormEqualityConstraint {

		public:
			typedef Eigen::Matrix<typename T::Scalar, T::RowsAtCompileTime, T::RowsAtCompileTime, Eigen::DontAlign> QuadraticMatrix; // A'*A
			typedef Eigen::Matrix<typename T::Scalar, T::RowsAtCompileTime, 1, Eigen::DontAlign> QuadraticVector; // vector b

		private:
			static constexpr double epsilon = 1e-10; // how close something can be to 0.0 and still be considered 0.0

			T A;
			QuadraticMatrix Q;
			QuadraticVector d;
			double r; // norm constraint
			int smallest_eigenvalue_multiplicity = 1; // multiplicity of the smallest eigenvalue (at least one)
			bool degenerate = true;
			QuadraticVector eigenvalues;
			QuadraticMatrix eigenvectors;
			QuadraticVector beta;

			/**
			 * @brief The NonDegenerateFunction class implements Eqn. 2.3.
			 */
			class NonDegenerateFunction : public RootFinder::Function1D {

				private:
					QuadraticVector eigenvalues;
					QuadraticVector beta;
					double r;
					double r_squared;

				public:
					NonDegenerateFunction(const QuadraticVector& eigenvalues, const QuadraticVector& beta, double r)
						: eigenvalues(eigenvalues), beta(beta), r(r), r_squared(std::pow(r, 2.0)) {}

					/**
					 * @brief Returns the value of Eqn. 2.3 for a given mu.
					 * @param mu
					 * @return
					 */
					double value(double mu) {

						double sum = 0.0;

						for (int aa = 0; aa < eigenvalues.rows(); ++aa)
							sum += std::pow(beta(aa), 2.0) / std::pow(eigenvalues(aa) + mu, 2.0);

						return sum - r_squared;

					}

					/**
					 * @brief Returns the derivative of Eqn. 2.3 for a given mu.
					 * @param mu
					 * @return
					 */
					double derivative(double mu) {

						double sum = 0.0;

						for (int aa = 0; aa < eigenvalues.rows(); ++aa)
							sum += std::pow(beta(aa), 2.0) / std::pow(eigenvalues(aa) + mu, 3.0);

						return -2.0 * sum;

					}

			};

		public:
			QuadraticMinimizationWithNormEqualityConstraint(const T& A, double r /* constraint */)
				: A(A),
				  Q(A.transpose() * A),
				  r(r) {

				// Find the eigenvalues and eigenvectors.
				Eigen::SelfAdjointEigenSolver<QuadraticMatrix> eigensolver(Q);
				eigenvalues = eigensolver.eigenvalues();
				eigenvectors = eigensolver.eigenvectors();

				// Find the multiplicity of the first (smallest in magnitude) eigenvalue.
				for (int aa = 1; aa < eigenvalues.rows(); ++aa) {
					if (std::abs(eigenvalues(aa) - eigenvalues(0)) < epsilon)
						smallest_eigenvalue_multiplicity += 1;
					else
						break;
				}

			}

			/**
			 * Solves the constrained problem for the vector b.
			 *
			 */
			QuadraticVector solve(const QuadraticVector& b) {

				QuadraticVector solution = QuadraticVector::Zero(A.cols(), 1);
				double degenerate_sum = 0.0;
				double r_squared = pow(this->r, 2.0);

				this->d = this->A.transpose()*b;

				this->beta = this->eigenvectors.transpose()*d;

				this->degenerate = true;

				// Check to see if we're in the degenerate condition by first checking if any component of
				// Beta is 0, which corresponds to the eigenvectors of the smallest eigenvalue (there may
				// be several if the multiplicity is larger than one).
				for (int aa=0; aa<this->smallest_eigenvalue_multiplicity; ++aa) {
					if (abs(this->beta(aa)) > this->epsilon) {
						this->degenerate = false;
						break;
					}
				}
				// Then check to see if we're in the degenerate condition by looking at the squared sum of
				// Eqn 2.2.
				if (this->degenerate) {
					for (int bb=this->smallest_eigenvalue_multiplicity; bb<this->eigenvalues.rows(); ++bb) { // compute the sum of Eqn 2.2
						degenerate_sum += pow(this->beta(bb), 2.0)/pow(this->eigenvalues(bb)-this->eigenvalues(0), 2.0);

						if (degenerate_sum > r_squared) { // if the sum ever gets above r^2, then not degenerate
							this->degenerate = false;
							break;
						}
					}
				}

				// Handle the degenerate case, if necessary.
				if (this->degenerate) { // degenerate, there are an infinite number of solutions
					// Construct the solution.
					for (int cc=this->smallest_eigenvalue_multiplicity; cc<this->eigenvalues.rows(); ++cc)
						solution += (this->beta(cc)/(this->eigenvalues(cc)-this->eigenvalues(0)))*this->eigenvectors.col(cc);
					solution += sqrt(r_squared - degenerate_sum)*this->eigenvectors.col(0);
				}
				else { // not degenerate, there is one solution
					// Determine the lower bound for mu that solves Eqn. 2.3.
					double low_bound_sum = 0.0;
					double low_bound = 0.0;
					for (int dd=0; dd<this->smallest_eigenvalue_multiplicity; ++dd)
						low_bound_sum += pow(beta(dd), 2.0);
					low_bound = sqrt(low_bound_sum)/r - this->eigenvalues(0);

					// Construct a function for Eqn 2.3.
					NonDegenerateFunction nondegenerate_function(this->eigenvalues, this->beta, this->r);

					// Solve Eqn 2.3.
					RootFinder root_finder(&nondegenerate_function);
					double mu = root_finder.find(low_bound);

					// Construct the solution.
					for (int ee=0; ee<this->eigenvalues.rows(); ++ee)
						solution += (this->beta(ee)/(this->eigenvalues(ee)+mu))*this->eigenvectors.col(ee);
				}

				return solution;

			}

	};


	/**
	 * @brief The QuadraticMinimizationWithLinearEqualityAndNormInequalityConstraints class solves the
	 * problem: min ||A*x-b|| s.t. B*x = c and ||x||<=r.
	 *
	 * TypeA and TypeB both need to be Eigen matrices and have the same number of columns
	 */
	template<typename TypeA, typename TypeB>
	class QuadraticMinimizationWithLinearEqualityAndNormInequalityConstraints {

		public:
			typedef Eigen::Matrix<typename TypeA::Scalar, TypeA::RowsAtCompileTime, 1, Eigen::DontAlign> QuadraticVector;
			typedef Eigen::Matrix<typename TypeB::Scalar, TypeB::RowsAtCompileTime, 1, Eigen::DontAlign> ConstraintVector;
			typedef Eigen::Matrix<typename TypeA::Scalar, TypeA::ColsAtCompileTime, 1, Eigen::DontAlign> SolutionVector;

		private:
			TypeA A;
			TypeB B;
			ConstraintVector c;
			double r;

		public:
			/**
			 * @brief Constructs a QuadraticMinimizationWithLinearEqualityAndNormInequalityConstraints objects.
			 * Solving is done in the solve() function.
			 * @param A
			 * @param B must have the same # of columns as A
			 * @param c must have the same # of rows as B
			 * @param r must be a positive scalar
			 */
			QuadraticMinimizationWithLinearEqualityAndNormInequalityConstraints(const TypeA& A, const TypeB& B, const ConstraintVector& c, double r)
				: A(A), B(B), c(c), r(r) {}

			/**
			 * @brief Solves the minimization problem for the given vector `b`, returns `false` if the two constraints
			 * are mutually exclusive.
			 * @param solution The solution found by this method
			 * @param b Must have the same rows as A
			 * @return
			 */
			bool solve(SolutionVector& solution, const QuadraticVector& b) {

				unsigned int null_space_dimension;
				SolutionVector solution_left_term;
				SolutionVector solution_right_term;

				// Compute the pseudoinverse solution that satisfies the constraint and produce the svd decomposition of B.
				PseudoInverse<TypeB> B_pinv(B);
				solution_left_term = B_pinv.solve(c);

				// Test to see if the norm inequality constraint can be satisfied, if not then quit.
				if ((r - solution_left_term.norm()) < 0.0)
					return false;

				// Determine the dimensionality of the nullspace of B.
				null_space_dimension = B.cols() - B_pinv.rank();

				// Generate the matrix N whose columns span the nullspace of B.
				Eigen::MatrixXd N = B_pinv.matrixV().rightCols(null_space_dimension);

				// Minimize the error while satisfying the constraint.
				Eigen::MatrixXd AN = A * N;
				Eigen::MatrixXd tmp = b - A * solution_left_term; // move in the direction that minimizes the error while staying in the nullspace of B
				PseudoInverse<Eigen::MatrixXd> AN_pinv(AN);
				Eigen::JacobiSVD<Eigen::MatrixXd> AN_svd(AN, Eigen::ComputeFullU | Eigen::ComputeFullV);
				solution_right_term = N * AN_pinv.solve(tmp);


				// Combine the constraint satisfaction term and the error minimization term.
				solution = solution_left_term + solution_right_term;

				// Test to see if the solution satisfies the norm inequality constraint, if so then return.
				if (solution.norm() <= r)
					return true;

				// If the solution doesn't satisfy the norm inequality constraint, then keep going.

				// Compute the minimizing term so that it satisfies the norm inequality constraint
				double new_constraint = sqrt(pow(r, 2.0) - pow(solution_left_term.norm(), 2.0));
				QuadraticMinimizationWithNormEqualityConstraint<Eigen::MatrixXd> equality_constrained_minimizer(AN, new_constraint);
				solution_right_term = N * equality_constrained_minimizer.solve(tmp);

				// Combine the constraint satisfaction term and the error minimization term.
				solution = solution_left_term + solution_right_term;

				return true;

			}

	};


} // End Optimization namespace

#endif // OPTIMIZATION_H
