#include "NumericalMethods.h"
#include <cmath>
#include <iostream>


namespace Numerical {

	/**
	 * @brief This constructor accepts two `std::vector` objects to function as the lookup table for the function.
	 * The `x` object contains the inputs to the function and the `f` object contains the corresponding function
	 * outputs such that the first entry in `f` is the output corresponding to the first entry in `x` and so on.
	 * The space between entries in `x` must be some fixed step size and the entries must be in increasing order.
	 * The table must have at least 2 input-output correspondences. This class will store its own copy of `x` and
	 * `f` so the user does not need to keep the provided `std::vector` objects in memory.
	 * @param x An `std::vector` containing the inputs to the function
	 * @param f An `std::vector` containing the outputs of the function that correspond to the entries of `x`
	 */
	ScalarFunctionFixedStep::ScalarFunctionFixedStep(const std::vector<double>& x, const std::vector<double>& f)
		: x(x),
		  f(f) {

		// Make sure the sizes of the tables are the same
		if (x.size() != f.size()) {
			std::cerr << "SCALAR_FUNCTION_FIXED_STEP: The input and output tables do not have the same number of entries! ERROR" << std::endl;
			return;
		}

		// Make sure the tables have more than 1 entry
		if (x.size() <= 1) {
			std::cerr << "SCALAR_FUNCTION_FIXED_STEP: The input and output tables must have more than one entry! ERROR" << std::endl;
			return;
		}

		// Find the step size from the table and the ending index
		step = x[1] - x[0];
		size = x.size();

	}


	/**
	 * @brief Evaluates the function at the specified value `input` using linear interpolation. If the value of
	 * `input` is outside the lookup table, that is, if the value of `input` is smaller than the smallest value
	 * in `x` or larger than the largest value in `x`, this function will use the two nearest entries in the table
	 * to linearly extrapolate a function value (this assumes the function is linear outside the definition of the
	 * lookup table). It is not recommended that you rely on this. The provided lookup table should include every
	 * expected input value.
	 * @param input The input at which to evaluate the function
	 * @return The value of the function evaluated at the specified input
	 */
	double ScalarFunctionFixedStep::linear_interp(double input) {

		// If step = 0 then there is a problem with the provided tables
		if (step == 0)
			return 0;

		// Determine the index of the input that is just smaller than the input to evaluate
		int i = std::floor((input - x[0]) / step);

		// If index is negative, the input to evaluate is smaller than the smallest input in the table
		if (i < 0)
			i = 0;
		// If index is larger than the (size - 2) of the table, the input is larger than the largest input in the table
		if (i > size - 2)
			i = size - 2;

		// Perform linear interpolation and return result
		return (f[i + 1] - f[i]) * (input - x[i]) / step + f[i];

	}

} // End Numerical namespace
