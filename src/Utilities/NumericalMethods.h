#ifndef NUMERICAL_METHODS_H
#define NUMERICAL_METHODS_H

#include <vector>


/**
 * The Numerical namespace contains utilities for miscellaneous numerical methods and representations such as numerical representations of
 * arbitrary functions as lookup tables with interpolation between entries, etc.
 */
namespace Numerical {

	/**
	 * @brief The ScalarFunctionFixedStep class is used to create a numerical representation of an arbitrary scalar-valued function of a
	 * scalar variable (i.e. scalar input, scalar output). The function is provided to the constructor as a lookup table. The lookup table
	 * consists of two `std::vector` objects which define the input-output relationship for the function over a range of input values. The
	 * first entry in the input array corresponds to the first entry in the output array and so on.
	 *
	 * This class assumes that the step between inputs in the table is constant. This allows the evaluation of the function to be performed
	 * in O(1) (constant) time (since there is no need to search the table for the specifed input to evaluate). Currently only linear
	 * interpolation is supported for evaluating the function.
	 */
	class ScalarFunctionFixedStep {

		private:
			std::vector<double> x;	// The array of inputs for the function acting as part of the lookup table
			std::vector<double> f;	// The array of outputs for the function acting as part of the lookup table
			double step = 0;		// The constant step between inputs in the lookup table
			int size;				// The number of entries (rows) of the lookup table

		public:
			ScalarFunctionFixedStep(const std::vector<double>& x, const std::vector<double>& f);
			double linear_interp(double input);

	};

} // End Numerical namespace

#endif // NUMERICAL_METHODS_H
