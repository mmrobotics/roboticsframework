#include "RoboticsToolbox.h"
#include <vector>
#include <cmath>
#include <iostream>
#include <iomanip> // std::setprecision
#include <sstream>


namespace Robo {

	/**
	 * @brief Returns a skew symmetric matrix packed with the given vector.
	 * @param v 3x1 vector to be packed into the skew symmetric matrix
	 * @return The skew symmetric matrix
	 */
	Matrixd33 Skew(const Vector3d& v) {

		Matrixd33 skew_matrix;

		skew_matrix <<   0.0, -v(2),  v(1),
						v(2),   0.0, -v(0),
					   -v(1),  v(0),   0.0;

		return skew_matrix;

	}


	/**
	 * @brief Returns the rotation matrix that corresponds to a rotation of `angle` degrees about
	 * the given axis.
	 * @param angle Angle of rotation in degrees
	 * @param axis Axis of rotation
	 * @return Matrix representation of the rotation
	 */
	Matrixd33 Rotation(double angle, const Vector3d& axis) {

		return Eigen::AngleAxisd(angle * M_PI / 180.0, axis.normalized()).toRotationMatrix();

	}


	/**
	 * @brief Returns a rotation matrix specified with XYZ Euler angles in degrees.
	 * @param rx X Euler angle in degrees
	 * @param ry Y Euler angle in degrees
	 * @param rz Z Euler angle in degrees
	 * @return Rotation matrix representing XYZ Euler angles
	 */
	Matrixd33 Rotation(double rx, double ry, double rz) {

		Matrixd33 Rx = Rotation(rx, UnitX());
		Matrixd33 Ry = Rotation(ry, UnitY());
		Matrixd33 Rz = Rotation(rz, UnitZ());

		return Rz * Ry * Rx;

	}


	/**
	 * @brief Returns a 3x3 matrix with the given coefficients on the diagonal in the order in
	 * which they are specified from top-left to bottom-right and zeros elsewhere. If you have
	 * a Robo::Vector or an Eigen::Vector, then just use the asDiagonal() member function.
	 * @param m11 Top-left coefficient
	 * @param m22 Center coefficient
	 * @param m33 Bottom-right coefficient
	 * @return Matrix with coefficients on the diagonal and zeros elsewhere
	 */
	Matrixd33 Diagonal(double m11, double m22, double m33) {

		return Vector3d(m11, m22, m33).asDiagonal();

	}


	/**
	 * @brief Returns a 3x1 zero vector.
	 * @return
	 */
	Vector3d Zero() {

		return Vector3d::Zero();

	}


	/**
	 * @brief Returns the 3x1 unit vector in the 'x' direction [1 0 0]^T.
	 * @return
	 */
	Vector3d UnitX() {

		return Vector3d::UnitX();

	}


	/**
	 * @brief Returns the 3x1 unit vector in the 'y' direction [0 1 0]^T.
	 * @return
	 */
	Vector3d UnitY() {

		return Vector3d::UnitY();

	}


	/**
	 * @brief Returns the 3x1 unit vector in the 'z' direction [0 0 1]^T.
	 * @return
	 */
	Vector3d UnitZ() {

		return Vector3d::UnitZ();

	}


	/**
	 * @brief Converts a rotation matrix to an angle-axis vector where direction is the unitized vector and angle (in radians) is the magnitude of the vector
	 * @param R Rotation matrix
	 * @return Angle-axis vector described by the input rotation matrix (magnitude is angle in radians)
	 */
	Vector3d RotationToAngleAxis(const Matrixd33& R) {

		Eigen::AngleAxisd angle_axis(R);

		return angle_axis.angle() * angle_axis.axis(); // Note: the angle calculated by Eigen::AngleAxis when constructed with a rotation matrix is always in the interval [0, pi].
													   // However, this is not really important in this case since we are multiplying the angle and axis together.

	}


	/**
	 * @brief Converts an angle-axis representation (in radians) to a rotation matrix.
	 * @param k Axis of rotation
	 * @param angle Angle of rotation in radians
	 * @return Rotation matrix which represents the angle-axis input
	 */
	Matrixd33 AngleAxisToRotation(const Eigen::Vector3d& k, double angle) {

		return Eigen::AngleAxisd(angle, k.normalized()).toRotationMatrix();

	}


	/**
	 * @brief Converts an angle-axis representation of a rotation to a quaternion representation.
	 * @param k Axis of rotation
	 * @param angle Angle of rotation in radians
	 * @return Quaternion representation of the rotation
	 */
	Vector4d AngleAxisToQuaternion(const Vector3d& k, double angle) {

		Robo::Vector4d Q;
		Q << std::cos(angle / 2), k.normalized() * std::sin(angle / 2);

		return Q.normalized();

	}


	/**
	 * @brief Returns a rotation matrix specified with the quaternion `q`.
	 * @param q The quaternion to convert to a rotation matrix representation
	 * @return Rotation matrix representing the quaternion
	 */
	Matrixd33 QuaternionToRotation(const Vector4d& q) {

		Vector4d unit_q = q.normalized();
		double q0 = unit_q(0);
		double q1 = unit_q(1);
		double q2 = unit_q(2);
		double q3 = unit_q(3);
		Matrixd33 R;
		R << 1 - 2*(q2*q2 + q3*q3), 2*(q1*q2 - q0*q3),		2*(q1*q3 + q0*q2),
			 2*(q1*q2 + q0*q3),		1 - 2*(q1*q1 + q3*q3),	2*(q2*q3 - q0*q1),
			 2*(q1*q3 - q0*q2),		2*(q2*q3 + q0*q1),		1 - 2*(q1*q1 + q2*q2);
		return R;

	}


	/**
	 * @brief Returns the quaternion product of `q1` and `q2` with `q1` on the
	 * left (i.e. Returns `result = q1 * q2`). Note the quaternion product does
	 * not commute.
	 * @param q1 Left quaternion
	 * @param q2 Right quaternion
	 * @return Quaternion product `q1 * q2`
	 */
	Vector4d qMult(const Vector4d& q1, const Vector4d& q2) {

		Matrixd44 qMat;
		qMat << q1(0),      -q1.tail(3).transpose(),
				q1.tail(3), q1(0) * Robo::I33 + Robo::Skew(q1.tail(3));

		return qMat * q2;

	}


	/**
	 * @brief Rotates the 3D vector `v` using the quaternion `q` as a rotation
	 * (Implements `q*p*q_conj` where `q_conj` is the conjugate of `q` and `p`
	 * is a quaternion with 0 scalar part and the vector part is `v`).
	 * @param q Quaternion rotation
	 * @param v 3D vector to rotate
	 * @return Vector `v` rotated by `q`
	 */
	Vector3d qRotate(const Vector4d& q, const Vector3d& v) {

		double q0 = q(0) / q.norm();		// scalar part of q (normalized)
		Vector3d qv = q.tail(3) / q.norm();	// vector part of q (normalized)
		return (q0*q0 - qv.dot(qv))*v + 2*q0*qv.cross(v) + 2*qv*qv.dot(v);

	}


	/**
	 * @brief Inversely rotates the 3D vector `v` using the quaternion `q` as a
	 * rotation (Implements `q_conj*p*q` where `q_conj` is the conjugate of `q`
	 * and `p` is a quaternion with 0 scalar part and the vector part is `v`).
	 * @param q Quaternion rotation
	 * @param v 3D vector to rotate
	 * @return Vector `v` inversely rotated by `q`
	 */
	Vector3d qInvRotate(const Vector4d& q, const Vector3d& v) {

		double q0 = q(0) / q.norm();		// scalar part of q (normalized)
		Vector3d qv = q.tail(3) / q.norm();	// vector part of q (normalized)
		return (q0*q0 - qv.dot(qv))*v + 2*q0*v.cross(qv) + 2*qv*qv.dot(v);

	}


	/**
	 * @brief Saturates the components of a 3D vector to the given vector of saturation values. Returns the saturated
	 * vector. In other words, saturation is done for each component of the input vector independently based
	 * upon the corresponding saturation value in the saturation vector.
	 * @param input_vector Vector whose components must be limited (saturated)
	 * @param saturation_vector Vector of saturation values for each component
	 * @return
	 */
	template<>
	Vector3d saturate(const Vector3d& input_vector, const Vector3d& saturation_vector) {

		return Vector3d(saturate(input_vector(0), saturation_vector(0)), saturate(input_vector(1), saturation_vector(1)), saturate(input_vector(2), saturation_vector(2)));

	}


	Position /* mm */ Position::interpolate(const Position& final, double translation_speed, double rotation_speed, double acceleration, double angular_acceleration, double time_step) const {

		Q_UNUSED(rotation_speed)
		Q_UNUSED(acceleration)
		Q_UNUSED(angular_acceleration)

		// Measure the difference in position between the current and desired positions.
		Vector3d delta_position = final.position - position;

		if (delta_position.norm() <= translation_speed * time_step)
			return final; // Return the final position if the interpolation will send us past the final position.
		else
			return Position(position + translation_speed * time_step * delta_position.normalized());

	}


	bool Position::hasArrived(const Position& final, double translation_threshold, double angular_threshold, double vel_threshold, double angularVel_threshold) const {

		Q_UNUSED(angular_threshold)
		Q_UNUSED(vel_threshold)
		Q_UNUSED(angularVel_threshold)

		return distance(final) <= translation_threshold;

	}


	Position Position::projectToLine(const Vector3d& line_offset, const Vector3d& line_direction) const {

		Vector3d normalized_dir = line_direction.normalized();

		return Position(line_offset + normalized_dir.dot(position - line_offset)*normalized_dir);

	}


	Position Position::projectToPlane(const Vector3d& plane_offset, const Vector3d& plane_normal) const {

		Vector3d normalized_normal = plane_normal.normalized();

		return Position(position + normalized_normal.dot(plane_offset - position) * normalized_normal);

	}


	/**
	 * @brief Prints out a Position object to the given stream on one line.
	 */
	std::ostream& operator<<(std::ostream& stream, const Position& position) {

		stream << "( " << position.position.x() << ", " << position.position.y() << ", " << position.position.z() << " )";

		return stream;
	}


	bool Position::getPlottingAttributes(std::vector<PlotAttributes>& attributes) const {

		GraphAttributes xGraph = {"x"};
		GraphAttributes yGraph = {"y"};
		GraphAttributes zGraph = {"z"};

		attributes = {{"Position", {xGraph, yGraph, zGraph}}};

		return true;

	}


	bool Position::convertToPlottingFormat(PlottingFormat& data) const {

		data.stateValues = {{position.x(), position.y(), position.z()}};

		return true;

	}


	bool Position::convertToFileLineFormat(std::ostringstream& lineStream) const {

		lineStream << std::setprecision(12) << position.x() << "," << position.y() << "," << position.z();

		return true;

	}


	/**
	 * @brief Computes the absolute value of the minimum angle between two Heading objects in radians.
	 * @param other The other Heading.
	 * @return Minimum absolute value of angle between the two Heading objects in radians.
	 */
	double /* rad */ Heading::distance(const Heading& other) const {

		// We want to avoid using asin or acos due to the numerical inaccuracies. Therefore we get the sine and
		// the cosine of the angle from the cross and dot products respectively and then use atan2.
		double dot_product = heading.dot(other.heading);

		double cross_product = (heading.cross(other.heading)).norm();

		double inverse_tangent = std::fabs(std::atan2(cross_product, dot_product));

		return inverse_tangent;

	}


	Heading Heading::interpolate(const Heading& final, double translation_speed, double rotation_speed, double acceleration, double angular_acceleration, double time_step) const {

		Q_UNUSED(translation_speed)
		Q_UNUSED(acceleration)
		Q_UNUSED(angular_acceleration)

		if (distance(final) <= rotation_speed * time_step)
			return final;

		Eigen::Matrix<double, 2, 3> tmp;
		tmp << heading.transpose(), final.heading.transpose();

		Eigen::FullPivLU<Eigen::Matrix<double, 2, 3>> lu_decomp(tmp);

		Vector3d rotation_axis = lu_decomp.kernel().col(0);

		// We cannot control the direction of the rotation axis. Therefore, we need to
		// try both a positive and a negative rotation and use whichever one moves us
		// closer to the final Heading.
		Matrixd33 r1 = Rotation(rotation_speed * time_step * 180 / M_PI, rotation_axis);
		Matrixd33 r2 = Rotation(-rotation_speed * time_step * 180 / M_PI, rotation_axis);

		Vector3d v1 = r1 * heading;
		Vector3d v2 = r2 * heading;

		if (final.distance(v1) < final.distance(v2))
			return v1;
		else
			return v2;

	}


	bool Heading::hasArrived(const Heading& final, double translation_threshold, double angular_threshold, double vel_threshold, double angularVel_threshold) const {

		Q_UNUSED(translation_threshold)
		Q_UNUSED(vel_threshold)
		Q_UNUSED(angularVel_threshold)

		return distance(final) <= angular_threshold;

	}


	bool Heading::getPlottingAttributes(std::vector<PlotAttributes>& attributes) const {

		GraphAttributes xGraph = {"x"};
		GraphAttributes yGraph = {"y"};
		GraphAttributes zGraph = {"z"};

		attributes = {{"Heading", {xGraph, yGraph, zGraph}}};

		return true;

	}


	bool Heading::convertToPlottingFormat(PlottingFormat& data) const {

		data.stateValues = {{heading.x(), heading.y(), heading.z()}};

		return true;

	}


	bool Heading::convertToFileLineFormat(std::ostringstream& lineStream) const {

		lineStream << std::setprecision(12) << heading.x() << "," << heading.y() << "," << heading.z();

		return true;

	}


	/**
	 * @brief Computes the positive rotation angle about the angle-axis between this Orientation and another in radians.
	 * @param other The other Orientation.
	 * @return The positive rotation angle (about the angle-axis) in radians.
	 */
	double /* rad */ Orientation::distance(const Orientation& other) const {

		// Rotation which takes the current Orientation to the other Orientation (i.e. multiplying this matrix on the left of the current orientation will result in the other orientation.
		// The corresponding angle-axis representation of this matrix is expressed in the world frame. This is in contrast to using the other orientation expressed in the current
		// frame/orientation which is a matrix that will produce the other orientation if multiplied on the right of the current orientation and whose corresponding angle-axis representation
		// is expressed in the current frame.)
		Matrixd33 delta_rotation = other.orientation * orientation.transpose();

		Eigen::AngleAxisd angle_axis(delta_rotation);

		return angle_axis.angle(); // Note: the angle calculated by Eigen::AngleAxis when constructed with a rotation matrix is always in the interval [0, pi].

	}


	Orientation Orientation::interpolate(const Orientation& final, double translation_speed, double rotation_speed, double acceleration, double angular_acceleration, double time_step) const {

		Q_UNUSED(translation_speed)
		Q_UNUSED(acceleration)
		Q_UNUSED(angular_acceleration)

		// Rotation which takes the current Orientation to the desired Orientation (i.e. multiplying this matrix on the left of the current orientation will result in the desired orientation.
		// The corresponding angle-axis representation of this matrix is expressed in the world frame. This is in contrast to using the desired orientation expressed in the current
		// frame/orientation which is a matrix that will produce the desired orientation if multiplied on the right of the current orientation and whose corresponding angle-axis representation
		// is expressed in the current frame.)
		Matrixd33 delta_rotation = final.orientation * orientation.transpose();

		// Get the angle between the current and desired Orientations. Note: the angle calculated by Eigen::AngleAxis when constructed with a rotation matrix is always in the interval [0, pi].
		Eigen::AngleAxisd angle_axis(delta_rotation);

		if (std::fabs(angle_axis.angle()) <= rotation_speed * time_step)
			return final;
		else {
			Eigen::AngleAxis<double> orientation_change(rotation_speed * time_step, angle_axis.axis());
			return Orientation(orientation_change.toRotationMatrix() * orientation); // Multiply on the left of the current orientation (see above comments)
		}

	}


	bool Orientation::hasArrived(const Orientation& final, double translation_threshold, double angular_threshold, double vel_threshold, double angularVel_threshold) const {

		Q_UNUSED(translation_threshold)
		Q_UNUSED(vel_threshold)
		Q_UNUSED(angularVel_threshold)

		return distance(final) <= angular_threshold;

	}


	bool Orientation::convertToFileLineFormat(std::ostringstream& lineStream) const {

		lineStream << std::setprecision(12) <<
					  orientation(0,0) << "," << orientation(0,1) << "," << orientation(0,2) << ";" <<
					  orientation(1,0) << "," << orientation(1,1) << "," << orientation(1,2) << ";" <<
					  orientation(2,0) << "," << orientation(2,1) << "," << orientation(2,2);

		return true;

	}


	AngularVelocity AngularVelocity::interpolate(const AngularVelocity& final, double translation_speed, double rotation_speed, double acceleration, double angular_acceleration, double time_step) const {

		Q_UNUSED(translation_speed)
		Q_UNUSED(rotation_speed)
		Q_UNUSED(acceleration)

		// Compute difference between current and desired angular velocities
		Vector3d delta_angular_vel = final.angularVelocity - angularVelocity;

		if (delta_angular_vel.norm() <= angular_acceleration * time_step)
			return final; // Return the final angular velocity if the interpolation will send us past the final position.
		else
			return AngularVelocity(angularVelocity + angular_acceleration * time_step * delta_angular_vel.normalized());

	}

	bool AngularVelocity::hasArrived(const AngularVelocity& final, double translation_threshold, double angular_threshold, double vel_threshold, double angularVel_threshold) const {

		Q_UNUSED(translation_threshold)
		Q_UNUSED(angular_threshold)
		Q_UNUSED(vel_threshold)

		return (final.angularVelocity - angularVelocity).norm() <= angularVel_threshold;

	}


	bool AngularVelocity::getPlottingAttributes(std::vector<PlotAttributes>& attributes) const {

		GraphAttributes xGraph = {"x"};
		GraphAttributes yGraph = {"y"};
		GraphAttributes zGraph = {"z"};

		attributes = {{"Angular Velocity", {xGraph, yGraph, zGraph}}};

		return true;

	}


	bool AngularVelocity::convertToPlottingFormat(PlottingFormat& data) const {

		data.stateValues = {{angularVelocity.x(), angularVelocity.y(), angularVelocity.z()}};

		return true;

	}


	bool AngularVelocity::convertToFileLineFormat(std::ostringstream& lineStream) const {

		lineStream << std::setprecision(12) << angularVelocity.x() << "," << angularVelocity.y() << "," << angularVelocity.z();

		return true;

	}


	Pose5DOF Pose5DOF::interpolate(const Pose5DOF& final, double translation_speed, double rotation_speed, double acceleration, double angular_acceleration, double time_step) const {

		Position next_position = Position::interpolate(final, translation_speed, rotation_speed, acceleration, angular_acceleration, time_step);
		Heading next_heading = Heading::interpolate(final, translation_speed, rotation_speed, acceleration, angular_acceleration, time_step);

		return Pose5DOF(next_position, next_heading);

	}


	bool Pose5DOF::hasArrived(const Pose5DOF& final, double translation_threshold, double angular_threshold, double vel_threshold, double angularVel_threshold) const {

		return Position::hasArrived(final, translation_threshold, angular_threshold, vel_threshold, angularVel_threshold) &&
				Heading::hasArrived(final, translation_threshold, angular_threshold, vel_threshold, angularVel_threshold);

	}


	/**
	 * @brief Prints out a Pose5DOF object to the given stream.
	 */
	std::ostream& operator<<(std::ostream& stream, const Pose5DOF& pose5DOF) {

		stream << "( " << pose5DOF.position.x() << " " << pose5DOF.position.y() << " " << pose5DOF.position.z() << " )" << std::endl;
		stream << "| " << pose5DOF.heading.x() << " " << pose5DOF.heading.y() << " " << pose5DOF.heading.z() << " |";

		return stream;

	}


	bool Pose5DOF::getPlottingAttributes(std::vector<PlotAttributes>& attributes) const {

		GraphAttributes xPosGraph = {"x"};
		GraphAttributes yPosGraph = {"y"};
		GraphAttributes zPosGraph = {"z"};
		GraphAttributes xHeadGraph = {"x"};
		GraphAttributes yHeadGraph = {"y"};
		GraphAttributes zHeadGraph = {"z"};

		attributes = {{"Position", {xPosGraph, yPosGraph, zPosGraph}},
					  {"Heading", {xHeadGraph, yHeadGraph, zHeadGraph}}};

		return true;

	}


	bool Pose5DOF::convertToPlottingFormat(PlottingFormat& data) const {

		data.stateValues= {{position.x(), position.y(), position.z()},
						   {heading.x(), heading.y(), heading.z()}};

		return true;

	}


	bool Pose5DOF::convertToFileLineFormat(std::ostringstream& lineStream) const {

		lineStream << std::setprecision(12) <<
					  position.x() << "," << position.y() << "," << position.z() << ":" <<
					  heading.x() << "," << heading.y() << "," << heading.z();

		return true;

	}


	Pose6DOF Pose6DOF::interpolate(const Pose6DOF& final, double translation_speed, double rotation_speed, double acceleration, double angular_acceleration, double time_step) const {

		Position next_position = Position::interpolate(final, translation_speed, rotation_speed, acceleration, angular_acceleration, time_step);
		Orientation next_orientation = Orientation::interpolate(final, translation_speed, rotation_speed, acceleration, angular_acceleration, time_step);

		return Pose6DOF(next_position, next_orientation);

	}


	bool Pose6DOF::hasArrived(const Pose6DOF& final, double translation_threshold, double angular_threshold, double vel_threshold, double angularVel_threshold) const {

		return Position::hasArrived(final, translation_threshold, angular_threshold, vel_threshold, angularVel_threshold) &&
			   Orientation::hasArrived(final, translation_threshold, angular_threshold, vel_threshold, angularVel_threshold);

	}


	/**
	 * @brief Prints out a Pose6DOF object to the given stream.
	 */
	std::ostream& operator<<(std::ostream& stream, const Pose6DOF& pose6DOF) {

		stream << "( " << pose6DOF.position.x() << " " << pose6DOF.position.y() << " " << pose6DOF.position.z() << " )" << std::endl;

		for (int aa = 0; aa < 3; ++aa) {
			stream << "| ";
			for (int bb = 0; bb < 3; ++bb) {
				stream << pose6DOF.orientation(aa, bb) << " ";
			}

			if (aa < 2)
				stream << "|" << std::endl;
			else
				stream << "|";
		}

		return stream;
	}


	bool Pose6DOF::getPlottingAttributes(std::vector<PlotAttributes>& attributes) const {

		GraphAttributes xGraph = {"x"};
		GraphAttributes yGraph = {"y"};
		GraphAttributes zGraph = {"z"};

		attributes = {{"Position", {xGraph, yGraph, zGraph}}};

		return true;

	}


	bool Pose6DOF::convertToPlottingFormat(PlottingFormat& data) const {

		data.stateValues = {{position.x(), position.y(), position.z()}};

		return true;

	}


	bool Pose6DOF::convertToFileLineFormat(std::ostringstream& lineStream) const {

		lineStream << std::setprecision(12) <<
					  position.x() << "," << position.y() << "," << position.z() << ":" <<
					  orientation(0,0) << "," << orientation(0,1) << "," << orientation(0,2) << ";" <<
					  orientation(1,0) << "," << orientation(1,1) << "," << orientation(1,2) << ";" <<
					  orientation(2,0) << "," << orientation(2,1) << "," << orientation(2,2);

		return true;

	}


	RotatingPosition RotatingPosition::interpolate(const RotatingPosition& final, double translation_speed, double rotation_speed, double acceleration, double angular_acceleration, double time_step) const {

			// Measure the difference in position between the current and desired positions.
			Position next_position = Position::interpolate(final, translation_speed, rotation_speed, acceleration, angular_acceleration, time_step);
			AngularVelocity next_angular_vel = AngularVelocity::interpolate(final, translation_speed, rotation_speed, acceleration, angular_acceleration, time_step);

			return RotatingPosition(next_position, next_angular_vel);

	}


	bool RotatingPosition::hasArrived(const RotatingPosition& final, double translation_threshold, double angular_threshold, double vel_threshold, double angularVel_threshold) const {

		return Position::hasArrived(final, translation_threshold, angular_threshold, vel_threshold, angularVel_threshold) &&
			   AngularVelocity::hasArrived(final, translation_threshold, angular_threshold, vel_threshold, angularVel_threshold);

	}


	/**
	 * @brief Prints a RotatingPosition to the given stream.
	 * @param stream Stream into which the RotatingPosition should be printed
	 * @param rotatingPosition The RotatingPosition to print
	 * @return
	 */
	std::ostream& operator<<(std::ostream& stream, const RotatingPosition& rotatingPosition) {

		stream << "( " << rotatingPosition.position.x() << " " << rotatingPosition.position.y() << " " << rotatingPosition.position.z() << " )" << std::endl;
		stream << "( " << rotatingPosition.angularVelocity.x() << " " << rotatingPosition.angularVelocity.y() << " " << rotatingPosition.angularVelocity.z() << " )" << std::endl;

		return stream;

	}


	bool RotatingPosition::getPlottingAttributes(std::vector<PlotAttributes>& attributes) const {

		GraphAttributes xPosGraph = {"x"};
		GraphAttributes yPosGraph = {"y"};
		GraphAttributes zPosGraph = {"z"};
		GraphAttributes xHeadGraph = {"x"};
		GraphAttributes yHeadGraph = {"y"};
		GraphAttributes zHeadGraph = {"z"};

		attributes = {{"Position", {xPosGraph, yPosGraph, zPosGraph}},
					  {"Angular Velocity", {xHeadGraph, yHeadGraph, zHeadGraph}}};

		return true;

	}


	bool RotatingPosition::convertToPlottingFormat(PlottingFormat& data) const {

		data.stateValues = {{position.x(), position.y(), position.z()},
							{angularVelocity.x(), angularVelocity.y(), angularVelocity.z()}};

		return true;

	}


	bool RotatingPosition::convertToFileLineFormat(std::ostringstream& lineStream) const {

		lineStream << std::setprecision(12) <<
					  position.x() << "," << position.y() << "," << position.z() << ":" <<
					  angularVelocity.x() << "," << angularVelocity.y() << "," << angularVelocity.z();

		return true;

	}


	bool RotatingPose5DOF::getPlottingAttributes(std::vector<PlotAttributes>& attributes) const {

		GraphAttributes xPosGraph = {"x"};
		GraphAttributes yPosGraph = {"y"};
		GraphAttributes zPosGraph = {"z"};
		GraphAttributes xMomGraph = {"x"};
		GraphAttributes yMomGraph = {"y"};
		GraphAttributes zMomGraph = {"z"};
		GraphAttributes xAngVelGraph = {"x"};
		GraphAttributes yAngVelGraph = {"y"};
		GraphAttributes zAngVelGraph = {"z"};

		attributes = {{"Position", {xPosGraph, yPosGraph, zPosGraph}},
					  {"Heading", {xMomGraph, yMomGraph, zMomGraph}},
					  {"Angular Velocity", {xAngVelGraph, yAngVelGraph, zAngVelGraph}}};

		return true;

	}


	bool RotatingPose5DOF::convertToPlottingFormat(PlottingFormat& data) const {

		data.stateValues = {{position.x(), position.y(), position.z()},
							{heading.x(), heading.y(), heading.z()},
							{angularVelocity.x(), angularVelocity.y(), angularVelocity.z()}};

		return true;

	}


	bool RotatingPose5DOF::convertToFileLineFormat(std::ostringstream& lineStream) const {

		lineStream << std::setprecision(12) <<
					  position.x() << "," << position.y() << "," << position.z() << ":" <<
					  heading.x() << "," << heading.y() << "," << heading.z() << ":" <<
					  angularVelocity.x() << "," << angularVelocity.y() << "," << angularVelocity.z();

		return true;

	}

} // End Robo namespace
