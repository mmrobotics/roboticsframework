#ifndef SPACE_EXPLORER_H
#define SPACE_EXPLORER_H

#include <QObject>
#include <Utilities/Timing.h>

/**
 * @brief The SpaceExplorer class is used to connect to the SpaceExplorer 3D mouse from 3DConnexion.
 * This class requires additional libraries to function, see the Robotics Framework README.md for
 * more information.
 *
 * The class emits a signal with the x, y, z and rotational rx, ry, rz counts from the mouse. The
 * polling rate defaults to 1000 Hz but is able to be specified in the constructor. There can only be
 * one instance of SpaceExplorer in a program because the driver only allows one client.
 *
 * The axes of the 3D mouse are as follows:
 *
 * - Up is the +z axis
 * - Left is the +y axis
 * - Forward is the +x axis
 *
 *        ___          x
 *       ( O )         |
 *        ) (          |
 *       (___)  y _____|
 *
 */
class SpaceExplorer : public QObject {

	Q_OBJECT

	private:
		Timing::Timer readThread;

		bool readSpaceExplorer();

	public:
		SpaceExplorer(double poll_rate = 1000.0);
		~SpaceExplorer();

	signals:
		/**
		 * @brief Signal with the readings from the SpaceExplorer.
		 * @param x x-direction counts
		 * @param y y-direction counts
		 * @param z z-direction counts
		 * @param rx Rotation about x-axis counts
		 * @param ry Rotation about y-axis counts
		 * @param rz Rotation about z-axis counts
		 */
		void newInput(int x, int y, int z, int rx, int ry, int rz);

};

#endif // SPACE_EXPLORER_H
