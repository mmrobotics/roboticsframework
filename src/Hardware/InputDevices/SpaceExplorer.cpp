#include "SpaceExplorer.h"
#include <iostream>
#include <spnav.h>


/**
 * @brief Constructs a SpaceExplorer object.
 * @param poll_rate Desired polling rate in Hz. Defaults to 1000 Hz.
 */
SpaceExplorer::SpaceExplorer(double poll_rate) {

	if (spnav_open() < 0) {
		std::cerr << "SPACE_EXPLORER: could not connect to the Space Explorer daemon. ERROR" << std::endl;
		return;
	}

	readThread.startTicking(*this, &SpaceExplorer::readSpaceExplorer, 1000.0 / poll_rate, "SpaceExplorerReadThread");

}


SpaceExplorer::~SpaceExplorer() {

	readThread.stopTicking();

	if (spnav_close() < 0)
		std::cerr << "SPACE_EXPLORER: could not close the connection with the Space Explorer daemon. ERROR" << std::endl;

}


/**
 * @brief Polls the readings from the SpaceExplorer and emits a signal with the readings.
 * @return
 */
bool SpaceExplorer::readSpaceExplorer() {

	static spnav_event explorer_event;

	if (spnav_poll_event(&explorer_event) == SPNAV_EVENT_MOTION) {

		emit newInput(explorer_event.motion.z,
					  -explorer_event.motion.x,
					  explorer_event.motion.y,
					  explorer_event.motion.rz,
					  -explorer_event.motion.rx,
					  explorer_event.motion.ry);

	}

	return true;

}
