#ifndef STATE_MESSAGE_H
#define STATE_MESSAGE_H

#include <string>
#include <vector>

#include <QtCore>


#define STATE_MESSAGE																\
	inline bool getMetaTypeID(int&) const override;									\
	bool getPlottingAttributes(std::vector<PlotAttributes>&) const override;		\
	bool convertToPlottingFormat(PlottingFormat&) const override;					\
	bool convertToFileLineFormat(std::ostringstream&) const override;

#define STATE_MESSAGE_RECORD_ONLY																\
	inline bool getMetaTypeID(int&) const override;												\
	bool getPlottingAttributes(std::vector<PlotAttributes>&) const override { return false; }	\
	bool convertToPlottingFormat(PlottingFormat&) const override { return false; }				\
	bool convertToFileLineFormat(std::ostringstream& lineStream) const override;

#define DECLARE_STATE_MESSAGE_TYPE(TYPE)										\
	Q_DECLARE_METATYPE(TYPE)													\
	inline bool TYPE::getMetaTypeID(int& typeID) const							\
	{																			\
		typeID = qMetaTypeId<TYPE>();											\
		return true;															\
	}


/**
 * @brief The GraphAttributes struct is used to describe the attributes of a single graph within a plot. A graph is
 * a single line in a plot representing a single numeric value over time. A plot can contain multiple graphs.
 */
struct GraphAttributes {

	/**
	 * @brief The name of the numerical value this graph represents. This std::string will be used in the legend
	 * of a plot if there is more than one graph in a single plot.
	 */
	std::string name;

};


/**
 * @brief The PlotAttributes struct is used to describe the attributes of a single plot. A plot has a title
 * (represented as an std::string) and one or more graphs described using the GraphAttributes struct.
 */
struct PlotAttributes {

	/**
	 * @brief The name of the plot. This std::string is used as the title of the plot in StateDataUI
	 */
	std::string name;
	/**
	 * @brief A list of graphs that this plot will have. Each element is a GraphAttributes object
	 * that describes that particular graph.
	 */
	std::vector<GraphAttributes> graphAttributes;

};


/**
 * @brief The PlottingFormat struct is used to repackage the internal data of a StateMessage object such that it can
 * be consumed by StateDataUI for plotting.
 */
struct PlottingFormat {

	/**
	 * @brief A list of lists of doubles. The doubles in the list of lists should correspond exactly (in
	 * order and size) to the structure of the return value from StateMessage::getPlottingAttributes().
	 * (i.e. For each plot, form a list of doubles corresponding to the numeric value of each graph in the
	 * plot. The order should match the corresponding order of graphs in the PlotAttributes object for each
	 * plot. Then form a list of all of these lists in the same order as the list of PlotAttributes objects
	 * that is returned from StateMessage::getPlottingAttributes() for the StateMessage object.
	 */
	std::vector<std::vector<double>> stateValues;

};


/**
 * @brief The StateMessage struct is an interface used by StateDataUI to consume data for recording and visualization.
 * It is meant to be used when you want to create your own data types that will work with StateDataUI. See documentation
 * of StateDataUI for more details.
 *
 * In order to create your own StateMessage data type, you must complete the following steps:
 *
 * 1. Derive your class publicly from StateMessage.
 * 2. Select and include a state message type definition (explained below) in the private section of your data type.
 * 3. Call DECLARE_STATE_MESSAGE_TYPE() at global scope in the header file after your data type declaration. The
 *    argument to the DECLARE_STATE_MESSAGE_TYPE() macro is the name of your custom data type (not a string). Make
 *    sure to use the fully qualified name of your type if it is in a namespace.
 * 4. Override the relevant functions associated with your choice of state message type definition from Step 2. Do not
 *    declare these functions in your class declaration. That is done already for you in the state message type
 *    definition. You only need to provide the definitions and these must reside outside your class declaration (e.g.
 *    in a .cpp file).
 *
 * If you are going to use a queued connection (which you should) to connect your StateMessage Qt signal (see StateDataUI
 * for how to format a StateMessage Qt signal) to StateDataUI, then you must also call qRegisterMetaType() before you make
 * the connection involving your custom type and after you create the QApplication object. The recommended place to call
 * qRegisterMetaType() is in the constructor of any class that sends a Qt signal of that type. The function should be called
 * for each non-primitive type that is sent in a Qt signal (except for those Qt types that are already registered, see Qt
 * documentation for details). An example is provided in the documentation for StateDataUI.
 *
 * If only one class will ever be producing messages for a particular custom StateMessage data type (e.g. A class that reads
 * data from a specific piece of hardware might have a message that is unique to the hardware it reads from), then it is
 * recommended that you make your custom StateMessage type a nested class inside the class that will be emitting Qt signals
 * with that type. This effectively defines the custom type in a namespace (the namespace being the emitting class itself).
 * When using a nested StateMessage type, you must make sure that the type in the Qt signal declaration uses the fully scoped
 * name. Since the type is defined in the emitting class, it is possible to omit the emitting class as part of the type in
 * the Qt signal declaration, but doing so will result in a run-time error when you attempt to connect that signal in a queued
 * connection. The rest of the steps above remain the same.
 *
 * The currently available state message type definitions are as follows:
 *
 * - STATE_MESSAGE - This state message type is recordable and plottable. You must override the following functions:
 *	- getPlottingAttributes()
 *	- convertToPlottingFormat()
 *	- convertToFileLineFormat()
 *
 * - STATE_MESSAGE_RECORD_ONLY - This state message type is only recordable. You must override the following function:
 *	- convertToFileLineFormat()
 *
 * Please see the documentation of each function for details on how to correctly provide their definitions in your class.
 *
 * Here is an example of a custom StateMessage type that represents 3 measurements:
 *
 * ~~~{.cpp}
 * struct MyCustomStateMessageType : public StateMessage {
 *
 *		int measurement1 = 0;
 *		int measurement2 = 0;
 *		int measurement3 = 0;
 *
 *		private:
 *			STATE_MESSAGE
 *
 * };
 * DECLARE_STATE_MESSAGE_TYPE(MyCustomStateMessageType)
 * ~~~
 *
 * Examples of how to correctly implement the definitions of StateMessage functions for `MyCustomStateMessageType` are
 * found in the documentation for each function.
 *
 */
struct StateMessage {

	virtual ~StateMessage() = default;

	/**
	 * @brief This function is used to automatically generate the metatype ID for your custom type (see StateDataUI).
	 * <b>DO NOT</b> provide a definition for this function. You can ignore it. A definition is provided by the
	 * DECLARE_STATE_MESSAGE_TYPE() macro so if you are getting a compiler error about this function, you have not
	 * used the DECLARE_STATE_MESSAGE_TYPE() macro correctly.
	 * @return Metatype ID
	 */
	virtual bool getMetaTypeID(int&) const = 0;

	/**
	 * @brief In this function, you provide a structure that describes the plots and graphs your custom data type will
	 * have. For each custom StateMessage type, you are able to create any number of plots. A plot contains any number
	 * of graphs where a graph is a "line" representing a single numerical value over time. In this function you must
	 * populate the `attributes` parameter with a PlotAttributes object for each plot you want your type to have. See
	 * documentation for PlotAttributes for details.
	 *
	 * Here is an example implementation for the `MyCustomStateMessageType` above where we want two plots. The first
	 * plot contains one graph of `measurement1` and the second plot contains two graphs, one of `measurement2` and
	 * one of `measurement3`:
	 *
	 * ~~~{.cpp}
	 * bool MyCustomStateMessageType::getPlottingAttributes(std::vector<PlotAttributes>& attributes) const {
	 *
	 *		GraphAttributes measurement1Graph = {"Measurement 1"};
	 *		GraphAttributes measurement2Graph = {"Measurement 2"};
	 *		GraphAttributes measurement3Graph = {"Measurement 3"};
	 *
	 *		attributes = {{"Plot 1 Title", {measurement1Graph}},
	 *					  {"Plot 2 Title", {measurement2Graph, measurement3Graph}}};
	 *
	 *		return true;
	 *
	 * }
	 * ~~~
	 *
	 * It is important to remember to return true from this function or your plots will not appear in StateDataUI.
	 *
	 * @param attributes
	 * @return True if this function has been overriden, false otherwise
	 */
	virtual bool getPlottingAttributes(std::vector<PlotAttributes>& attributes) const = 0;

	/**
	 * @brief In this function, you must repackage the data in a format that matches the description of plots and
	 * graphs you gave in getPlottingAttributes(). This is done by assigning the value of the `data` parameter
	 * which is of type PlottingFormat. See PlottingFormat for details on this format.
	 *
	 * Here is an example implementation that works with the example implementation of getPlottingAttributes():
	 *
	 * ~~~{.cpp}
	 * bool MyCustomStateMessageType::convertToPlottingFormat(PlottingFormat& data) const {
	 *
	 *		data.stateValues = {{measurement1},
	 *							{measurement2, measurement3}};
	 *
	 *		return true;
	 *
	 * }
	 * ~~~
	 *
	 * It is important to remember to return true from this function or your program may crash.
	 *
	 * @param data A PlottingFormat object into which you place the data to be plotted.
	 * @return True if this function has been overriden, false otherwise
	 */
	virtual bool convertToPlottingFormat(PlottingFormat& data) const = 0;

	/**
	 * @brief This function allows you to format the data into a single line of text for recording to file. You should
	 * choose a format so you can parse the data later. The format can be whatever you like but cannot include any new
	 * line or carriage return characters (you must form a <b>single</b> line). Don't worry about making a unique format
	 * to distinguish your type from others as the line will be prepended with a unique metatype ID that you can use to
	 * later identify the type that generated the line. See StateDataUI for details of what gets prepended to the line
	 * you create.
	 *
	 * The line is generated by using the insertion operator on the provided `lineStream` parameter.
	 *
	 * Here is an example implementation for the `MyCustomStateMessageType` above:
	 *
	 * ~~~{.cpp}
	 * bool MyCustomStateMessageType::convertToFileLineFormat(std::ostringstream& lineStream) const {
	 *
	 *		lineStream << measurement1 << "," << measurement2 << "," << measurement3;
	 *
	 *		return true;
	 *
	 * }
	 * ~~~
	 *
	 * @param lineStream std::stringstream into which you should insert your line of text
	 * @return True if this function has been overriden, false otherwise
	 */
	virtual bool convertToFileLineFormat(std::ostringstream& lineStream) const = 0;

};

#endif // STATE_MESSAGE_H
