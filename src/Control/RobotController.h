#ifndef ROBOT_CONTROLLER_H
#define ROBOT_CONTROLLER_H

#include <QObject>
#include <QWidget>
#include <QLabel>

#include <Utilities/Timing.h>
#include <Utilities/Filters.h>


/**
 * @brief The RobotController class is the base class for any robot controller you wish to implement.
 *
 * This class is designed to abstract away the overhead work of managing a controller that runs in its
 * own thread and is designed to work seamlessly with RobotControlUI. In order to implement your own
 * controller, create a class that inherits from RobotController and override the control() function
 * with the implementation of your control algorithm. The control() function will be called in its own
 * thread on a fixed interval once the start() function is called. The control() function will stop
 * being called once stop() is called. The control interval as well as the thread priority (see
 * Timing::Timer) are specified in the constructor of RobotController.
 *
 * This class allows for resetting the control algorithm (e.g. to zero out integrator wind-up) just
 * before starting the controller by overriding the reset() function. RobotControlUI always calls
 * reset() just before it calls start(). Since the reset() function is meant to be called just before
 * starting the controller, the class also allows for clean-up actions to be performed just after
 * stopping the controller if desired. This is done by overriding the stopCleanup() function.
 *
 * Your implementation class must have some way of sending commands from the control() function to
 * whatever robot you wish the controller to control. This can be done by requiring a pointer or
 * reference to a robot in the constructor, by use of the Qt signal-slot mechanism, or by some other
 * method. The most common method is to require a reference to the robot you wish to control in the
 * constructor. This usually allows you to directly ask the robot for state information that may be
 * needed by your control algorithm. If you need additional state information (e.g. the location of
 * an object in the workspace), it is recommended that you use the Qt signal-slot mechanism to accept
 * this information from other classes that produce it. RobotController inherits from QObject so your
 * implementation class will already be set up to have signals and slots. You just need to add the
 * Q_OBJECT macro to your class in order to enable the Qt signal-slot mechanism.
 *
 * The RobotController class also allows for implementation classes to create their own user interfaces
 * to interact with the controller (e.g. to set/adjust gains) if desired. This is done by overriding the
 * buildControlUI() function. This function is used by buildUI() and RobotControlUI to automatically
 * show the UI. Every RobotController has a default UI that displays the desired and actual execution
 * rates of the controller as well as the thread priority. The buildUI() function creates this default
 * UI and then adds your custom QWidget from buildControlUI(). If you do not want/need a UI for your
 * controller, then simply do not override the buildControlUI() function. You will always have the
 * default UI even if you do not override the buildControlUI() function.
 *
 * Each RobotController has a name which is used to identify the controller in user interfaces. This
 * name is meant to be set by all child classes of RobotController. The name should be set in the
 * constructor of the class using the setName() function (which has protected access). A derived class
 * will override the name of a parent class since the body of a class constructor is executed after the
 * constructors for parent classes. If a controller implementation does not set its name, the name will
 * be that of its nearest ancestor that has set a name. If no ancestors have set a name, then the name
 * of the controller will be "Unknown Controller".
 *
 * It is important that you do not perform blocking operations in the control() function due to the fact
 * that it will be called at a fixed interval. If the operations in the control() function take longer
 * than the desired interval, then the control() function will run immediately after each completion.
 * You can determine whether this is occurring by looking at the actual rate in the RobotController UI.
 *
 * If an unrecoverable error occurs in your control() function, return `false` and RobotController will
 * emit the stoppedDueToError() Qt signal. RobotControlUI listens for this signal and will automatically
 * stop the controller and disable the robot if it receives the signal.
 *
 * It is also easy to create implementations of RobotController that act as "meta" controllers. A meta
 * controller is a controller that manages other RobotControllers. When implementing a meta controller,
 * simply implement the algorithm that manages the sub controllers in the control() function. You will
 * need some way to manage each sub controller and the most common way is for the meta controller to
 * either create instances of each sub controller as member variables or to require references to each
 * sub controller in the constructor. This allows you to call the necessary member functions of each
 * sub controller (e.g. start(), stop(), etc). Make sure you connect to the stoppedDueToError() signal
 * of each sub controller and return `false` from the meta controller's control() function if this signal
 * is received. This effectively propagates the stoppedDueToError() signal all the way up to
 * RobotControlUI. You can alternatively connect the sub controller's stoppedDueToError() signals to the
 * meta controller's stoppedDueToError() signal (because Qt allows a signal to be connected to another
 * signal) and this will have the same propagation effect, but you will not be able to recognize an error
 * occurred (and handle it) in the meta controller unless you connect a slot in the meta controller to this
 * signal. Also, make sure to override the stopCleanup() function so sub controllers can be stopped by the
 * meta controller when it is stopped. In other words, stoppedDueToError() can be used to propagate stops
 * that initiate within the controller tree up to the top and stopCleanup() can be used to propagate stops
 * that initiate at the top of the tree to the bottom. See the documentation of stoppedDueToError() and
 * stopCleanup() for more details.
 */
class RobotController : public QObject {

	Q_OBJECT

	private:
		const char* name = "Unknown Controller";
		double timestep_ms;
		int priority;
		Timing::Timer robotControllerThread;

		QLabel* rateLabel;
		Filters::MovingAverageFilter<double, 15> actualControlRate;
		double lastControlTimestamp;

		bool runController();

	protected:
		/**
		 * @brief Sets the name of the controller which is used to identify it in user interfaces. This
		 * function should be called at the beginning of the constructor of any class that inherits from
		 * RobotController. If a controller implementation does not set its name, the name will be that
		 * of its nearest ancestor that has set a name. If no ancestors have set a name, then the name of
		 * the controller will be "Unknown Controller".
		 * @param name A name for the specific controller implementation
		 */
		void setName(const char* name) { this->name = name; }
		/**
		 * @brief This function is where the control algorithm is implemented. Override this function to
		 * implement your own control algorithm. This function will be called at a fixed interval set in
		 * the constructor of this class. the operations in this function should be designed to take less
		 * time than the control interval. If the operations are taking longer than the control interval,
		 * this can be seen in the actual execution rate of the default UI provided by RobotController.
		 * At the end of each control iteration, return `true` if the control algorithm is working normally.
		 * If an unrecoverable error occurs in the control algorithm, return `false` and RobotController will
		 * automatically emit the stoppedDueToError() signal for you.
		 * @return True if control is operating normally, false if an unrecoverable error has occurred.
		 */
		virtual bool control() = 0;
		/**
		 * @brief This function should be overridden when a controller needs to perform some clean-up steps
		 * as part of stopping the controller. This function will be called in the stop() function after the
		 * controller has been stopped (i.e. the control() function is guaranteed to NOT be running). An
		 * example use case of this function is in the case of a controller acting as a meta controller.
		 * Since a sub controller could be running independently of the meta controller (i.e. they are being
		 * started and stopped by the meta controller), when the meta controller is stopped, it may need to
		 * propagate this stop action to the active sub controller. Since stopping the meta controller means
		 * the control() function will no longer run, it cannot know it has been stopped from within the
		 * control function. This function provides a way for the meta controller to know it has been stopped
		 * and perform any necessary actions (e.g. stopping sub controllers).
		 */
		virtual void stopCleanup() { /* Do nothing by default */ }
		/**
		 * @brief This function is used by RobotControlUI to get a widget from the controller that is meant
		 * to act as a manual control panel for the specific type of controller. By default, this function
		 * returns a `nullptr` which means that the controller does not have a user interface to display.
		 * If desired, each RobotController implementation can override this function to display manual
		 * controls specific to the type of controller. The UI is meant to allow users to access and/or
		 * manipulate parameters that affect how the controller behaves. The UI will be available at all
		 * times (contrast this with the GenericRobot UI that RobotControlUI will only enable at certain
		 * times). As examples, a PID controller could allow the user to modify gains while the controller
		 * is running, or a waypoint controller could display the current progress through a list of
		 * waypoints. The QWidget returned by this function will always be included with a default
		 * RobotController UI that displays the actual control rate and the thead priority. This is done in
		 * the buildUI() function that wraps buildControlUI().
		 *
		 * <b>IMPLEMENTATION:</b> You must be aware of a few things when implementing this function:
		 *
		 * 1. Treat this function as if it were the constructor for a class that inherits from QWidget,
		 * but instead of inheriting from QWidget, create a QWidget object in the function and build
		 * the UI into that widget. Then return the widget you created.
		 * 2. Qt widgets have a parent-child relationship called the "object tree" that is used to destroy
		 * widgets when the parent is destroyed. Therefore, all of the QWidget elements created in
		 * buildControlUI() should be created with the `new` keyword, and they should all be part of the object
		 * tree of the QWidget that gets returned by this function. Do not make your implementation
		 * class be the parent of any of the QWidgets that make up the UI and do not delete them in
		 * your implementation class's destructor. The RobotControlUI will take responsibility for
		 * cleaning up these resources. A QWidget is usually added to the object tree of another QWidget
		 * by Qt automatically when you call the addWidget(), addLayout(), setLayout(), etc. functions,
		 * so most of the time you will not need to worry about explicitly setting up the object tree
		 * as long as each widget is added to the UI through Qt functions that reassign parent-child
		 * relationships.
		 * 3. Non QWidget elements of the UI such as a QTimer are not automatically added to the object
		 * tree, but they should be added so RobotControlUI can take responsibility for these classes.
		 * For example, if your UI uses the timeout signal from a QTimer to update the UI, then you
		 * should explicitly set the parent of that QTimer as the QWidget returned by this function.
		 * <b>You should familiarize yourself with the Qt object tree before implementing this function.</b>
		 * 4. If you are inheriting from a class that overrides this function, you can call the parent
		 * class implementation of buildControlUI() to get the parent QWidget and incorporate it into the
		 * QWidget created in your implementation. If you do not do this, the parent widget will simply
		 * not appear.
		 * 5. Signals and slots can be defined in your implementation class to interact with the UI.
		 * To do this, you need to include the Q_OBJECT macro in your class (the class already inherits
		 * from QObject since RobotController inherits from QObject). The best place to connect signals to
		 * slots would be in the implementation of this function before you return the QWidget representing
		 * the UI. Keep in mind that if you use Qt::AutoConnection or Qt::QueuedConnection, then it is
		 * always safe to emit a signal from any thread (e.g. from the control() function because that is
		 * always running in its own thread), but you should pay attention to which thread will
		 * be executing the slots in your class (usually the UI/main thread). <b>If a slot will be accessing
		 * data that a different thread also needs to access, you need to make sure the access to that
		 * data is thread-safe.</b>
		 * 6. QWidget objects can only be interacted with from the UI/main thread. This means, for example,
		 * that if you attempt to modify anything about the UI widget classes directly from the control()
		 * function, the program <b>will</b> crash. If you need to interact with the widgets directly, this
		 * should be done in a Qt slot that will be connected to a slot that will only be run by the UI/main
		 * thread. Since your slot will most likely be a member function of your controller implementation
		 * class, this means that all signals connected to that slot must be emitted from the UI/main thread.
		 * If you make yourself familiar with the Qt signal slot mechanism, you should have no trouble
		 * verifying that these rules are being met. If you need to update the UI based on logic performed
		 * in the control() function, this can be done indirectly by, for example, changing a member variable
		 * that a UI update slot will reference later when it updates the UI.
		 * 7. The widget that you return will be placed inside a QGroupBox with the name of the controller.
		 * Therefore, you do not need to do anything to label or outline your widget visually to
		 * distinguish it from other parts of the RobotControlUI.
		 * @return A QWidget with no parent that represents the UI for this particular controller
		 */
		virtual QWidget* buildControlUI() { return nullptr; }

	public:
		RobotController(double timestep_ms = 40.0, int priority = 0);
		virtual ~RobotController();

		/**
		 * @brief Returns the name of the specific RobotController implementation. If an implementation
		 * does not set its name, this function will return "Unknown Controller".
		 * @return
		 */
		const char* getName() const { return name; }
		/**
		 * @brief Resets the controller. Override this function if your controller needs to be reset just
		 * before starting the controller with start() (e.g. to zero out integrator wind-up). This function
		 * is meant to be called just before starting the controller and the RobotControlUI always calls
		 * this function just before calling start(). If a controller needs to perform some clean-up
		 * actions immediately after it stops, this should be done by overriding the stopCleanup() function.
		 * @return True if successful
		 */
		virtual bool reset() { return true; }
		bool start();
		bool stop();
		bool isRunning();

		QWidget* buildUI();

	signals:
		/**
		 * @brief This signal is emitted when an unrecoverable error occurs in the controller. Controller
		 * implementations can trigger this signal simply be returning `false` in the control() function.
		 * The RobotControlUI class listens for this signal.
		 */
		void stoppedDueToError();

};

#endif // ROBOT_CONTROLLER_H
