#include "RobotControlUI.h"
#include <QtWidgets>


/**
 * @brief Constructs a RobotControlUI with the provided robot and controller.
 * @param robot A GenericRobot object to control
 * @param controller An optional RobotController object to control the robot
 */
RobotControlUI::RobotControlUI(GenericRobot& robot, RobotController* controller)
	: robot(robot),
	  controller(controller) {

	setWindowTitle("Robot Control UI");

	QVBoxLayout* layout = new QVBoxLayout();

	// If the robot provides a user interface, then add it to the interface
	robotWidget = robot.buildUI();
	if (robotWidget) {

		QGroupBox* robotSection = new QGroupBox(QString(robot.getName()) + " UI");
		QVBoxLayout* v = new QVBoxLayout();
		v->addWidget(robotWidget);
		robotSection->setLayout(v);
		layout->addWidget(robotSection);
		robotWidget->setEnabled(false);

	}

	// Add the enable robot button
	enableRobotButton = new SwitcherButton("ENABLE ROBOT", "DISABLE ROBOT", robot.isEnabled());
	connect(enableRobotButton, &SwitcherButton::clicked, this, &RobotControlUI::enableRobot);
	layout->addWidget(enableRobotButton);

	// If a robot controller was provided, get set up to allow usage of the controller
	if (controller) {

		// Connect the controller error signal to the error handling slot
		connect(controller, &RobotController::stoppedDueToError,
				this, &RobotControlUI::handleControllerErrorEvent);

		// If the robot controller provides a user interface, then add it
		controllerWidget = controller->buildUI();
		if (controllerWidget) {

			QGroupBox* controllerSection = new QGroupBox(QString(controller->getName()) + " UI");
			QVBoxLayout* v = new QVBoxLayout();
			v->addWidget(controllerWidget);
			controllerSection->setLayout(v);
			layout->addWidget(controllerSection);

		}

		// Add the start controller button
		startControllerButton = new SwitcherButton("START CONTROLLER", "STOP CONTROLLER");
		startControllerButton->setEnabled(false);
		connect(startControllerButton, &SwitcherButton::clicked, this, &RobotControlUI::startController);
		layout->addWidget(startControllerButton);

	}

	setLayout(layout);

	setMinimumSize(QSize(sizeHint().width(), sizeHint().height()));

}


/**
 * @brief Handles when the enable robot button is pressed.
 */
void RobotControlUI::enableRobot() {

	// Enable the robot
	if (enableRobotButton->getState()) {

		if (robot.enable()) {
			if (robotWidget)
				robotWidget->setEnabled(true);
			if (controller)
				startControllerButton->setEnabled(true);
		}
		else {
			enableRobotButton->setState(false);
			if (robotWidget)
				robotWidget->setEnabled(false);
			QMessageBox::warning(this, QStringLiteral("Robot Enable Error"),
								 QStringLiteral("An error occurred when enabling the robot!"),
								 QMessageBox::Ok);
		}

	}
	// Disable the robot
	else {

		if (controller) {

			// Stop the controller first if it is running
			if (controller->isRunning()) {

				if (!controller->stop())
					QMessageBox::warning(this, QStringLiteral("Controller Failed to Stop"),
										 QStringLiteral("Could not stop the controller!"),
										 QMessageBox::Ok);

				startControllerButton->setState(false);

			}

			// Always disable the start controller button
			startControllerButton->setEnabled(false);

		}

		if (robotWidget)
			robotWidget->setEnabled(false);
		if (!robot.disable())
			QMessageBox::warning(this, QStringLiteral("Robot Disable Error"),
								 QStringLiteral("An error occurred when disabling the robot!"),
								 QMessageBox::Ok);

	}

}


/**
 * @brief Handles when the start controller button is pressed.
 */
void RobotControlUI::startController() {

	// Start the controller
	if (startControllerButton->getState()) {

		if (controller->reset()) {

			if (!controller->start()) {
				startControllerButton->setState(false);
				QMessageBox::warning(this, QStringLiteral("Controller Failed to Start"),
									 QStringLiteral("Could not start the controller!"),
									 QMessageBox::Ok);
			}
			else if (robotWidget)
				robotWidget->setEnabled(false);

		}
		else {

			startControllerButton->setState(false);
			QMessageBox::warning(this, QStringLiteral("Controller Failed to Start"),
								 QStringLiteral("Could not start the controller because it failed to be reset!"),
								 QMessageBox::Ok);

		}

	}
	// Stop the controller
	else {

		if (!controller->stop())
			QMessageBox::warning(this, QStringLiteral("Controller Failed to Stop"),
								 QStringLiteral("Could not stop the controller!"),
								 QMessageBox::Ok);
		else if (robotWidget)
			robotWidget->setEnabled(robot.isEnabled());

	}

}


/**
 * @brief Handles when a RobotController::stoppedDueToError() signal occurs from the RobotController. This slot simply
 * turns everything off in response to the error signal.
 */
void RobotControlUI::handleControllerErrorEvent() {

	// Turn off the controller
	controller->stop();
	startControllerButton->setState(false);
	startControllerButton->setEnabled(false);

	// Disable the robot
	robot.disable();
	if (robotWidget)
		robotWidget->setEnabled(false);
	enableRobotButton->setState(false);

	// Notify user that an error occurred
	QMessageBox::warning(this, QStringLiteral("Robot Controller Encountered an Error"),
						 QStringLiteral("The robot controller has encountered an error from "
										"which it could not recover. The controller has been "
										"stopped and the robot has been disabled for safety."),
						 QMessageBox::Ok);

}
