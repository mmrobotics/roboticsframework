#ifndef ROBOT_CONTROL_UI_H
#define ROBOT_CONTROL_UI_H

#include <QWidget>
#include <Robots/GenericRobot.h>
#include <Control/RobotController.h>
#include <QtGUIElements/Buttons.h>


/**
 * @brief The RobotControlUI class is meant to be the main graphical user interface for interaction with
 * a robot and a controller that can control the robot. It is designed to work in conjunction with
 * GenericRobot and RobotController.
 *
 * The user interface has two parts: the robot part and an optional controller part. You must always
 * provide a GenericRobot. The interface will always show a button for enabling the robot. If the robot
 * provided is capable of presenting a manual control interface, then this will also be shown (see
 * GenericRobot::buildUI() for details of how you can make a robot capable of presenting a manual control
 * interface).
 *
 * If a RobotController is also provided, a button will be available for starting and stopping the controller.
 * The controller is always reset before it is started and the controller must be one which is set up to
 * control the robot provided. In other words, this class does not tell the controller which robot to control.
 * The controller must already be configured with the robot it is meant to control (see the documentation for
 * RobotController for more information). This means that it is possible to provide this class with a controller
 * that is meant to control a different robot from the robot provided. This is not recommended as this class
 * will not be able to enable the robot that the controller is meant to control. Make sure to always provide
 * a controller that is configured to control the provided robot.
 *
 * If the RobotController provided is capable of presenting a unique interface for interacting with the
 * controller, then this will also be shown (see RobotController::buildControlUI() for details of how you can
 * make a controller capable of presenting a unique interface).
 *
 * This class connects to the RobotController::stoppedDueToError() signal and will automatically stop the
 * controller and disable the robot if this signal is received.
 *
 * It is not required that you use this class to interact with robots and controllers, but it is recommended as
 * this class is already set up to help with workflow and deal with errors.
 */
class RobotControlUI : public QWidget {

	Q_OBJECT

	private:
		GenericRobot& robot;
		RobotController* controller;

		// Widgets
		SwitcherButton* enableRobotButton;
		SwitcherButton* startControllerButton;
		QWidget* robotWidget;
		QWidget* controllerWidget;

	private slots:
		void enableRobot();
		void startController();
		void handleControllerErrorEvent();

	public:
		RobotControlUI(GenericRobot& robot, RobotController* controller = nullptr);

};

#endif // ROBOT_CONTROL_UI_H
