#ifndef WORKSPACE_WAYPOINT_CONTROLLER_H
#define WORKSPACE_WAYPOINT_CONTROLLER_H

#include <vector>
#include <iostream>
#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <Control/RobotController.h>
#include <Robots/WorkspaceControllableRobot.h>


/**
 * @brief The WorkspaceWaypointController class is a RobotController used to control a WorkspaceControllableRobot
 * by specifying a series of workspace state waypoints and how long to pause before moving between waypoints. It
 * is also possible to insert actions you wish the controller to take at each waypoint and at the end of the list
 * of waypoints by creating a class that derives from WorkspaceWaypointController and overriding the
 * waypointReached() and waypointsDone() functions.
 *
 * The WorkspaceWaypointController is a template class where the template parameter is the type of the workspace
 * state waypoints used. The chosen workspace state type must implement the Robo::LinearlyInterpolatableState
 * interface and must be accepted by WorkspaceControllableRobot::getWorkspaceState() and
 * WorkspaceControllableRobot::setWorkspaceState() (e.g. Robo::Position, Robo::Pose6DOF, etc). The waypoint list
 * can either be set in the constructor or by use of the setWaypointList() function. The status of the controller
 * can also be determined with waypointCount(), waypointsRemaining(), and isFinished(). The reset() function will
 * set cause the controller to start at the beginning of the waypoint list again.
 *
 * The wait time between waypoints and the interpolation behavior can be specified with the Settings struct. See
 * the Settings struct for details of what each option can control. The Settings struct can be set in the
 * constructor of the class or by using the setSettings() function. Classes that derive from
 * WorkspaceWaypointController have direct access to the Settings struct named `settings`. This makes it possible
 * for derived classes to set the settings in their constructors and to even change the settings between waypoints
 * if desired.
 *
 * This controller implements the buildControlUI() function to show the status of the waypoint controller. The
 * status that is shown is: number of waypoints completed, total number of waypoints, and current waypoint status
 * (e.g. moving to waypoint, waiting at waypoint, etc.). If you derive from this class, be aware that if you
 * override buildControlUI(), the UI of this class will be hidden unless you manually include it in your UI by
 * calling WorkspaceWaypointController::buildControlUI().
 *
 * @tparam WorkspaceStateType A type that implements the Robo::LinearlyInterpolatableState interface and is accepted
 * by WorkspaceControllableRobot::getWorkspaceState() and WorkspaceControllableRobot::setWorkspaceState()
 * (e.g. Robo::Position, Robo::Heading, Robo::Pose6DOF, etc).
 */
template<typename WorkspaceStateType>
class WorkspaceWaypointController : public RobotController {

	public:
		/**
		 * @brief WaypointList is the type of the waypoint list used by WorkspaceWaypointController. It is simply
		 * a typedef for std::vector<WorkspaceStateType>.
		 */
		typedef std::vector<WorkspaceStateType> WaypointList;

		/**
		 * @brief The Settings struct is used to set the wait time between waypoints as well as the interpolation
		 * behavior of WorkspaceWaypointController.
		 */
		struct Settings {

			/**
			 * @brief The time to wait between waypoints in seconds. Defaults to 0.
			 */
			double wait_time = 0;
			/**
			 * @brief The speed at which translation will occur in mm/s. If the workspace state type does not have
			 * a translational component, this setting is ignored. Defaults to 0 mm/s.
			 */
			double translation_speed = 0;
			/**
			 * @brief The speed at which rotation will occur in rad/s. If the workspace state type does not have a
			 * rotational component, this setting is ignored. Defaults to 0 rad/s.
			 */
			double rotation_speed = 0;
			/**
			 * @brief The acceleration at which changes in translational velocity will occur in mm/s^2. If the
			 * workspace state type does not have a translational velocity component, this setting is ignored.
			 * Defaults to 0 mm/s^2.
			 */
			double acceleration = 0;
			/**
			 * @brief The angular acceleration at which changes in angular velocity will occur in rad/s^2. If the
			 * workspace state type does not have an angular velocity component, this setting is ignored. Defaults
			 * to 0 rad/s^2.
			 */
			double angular_acceleration = 0;
			/**
			 * @brief The threshold for translation in millimeters used to determine whether the robot has reached
			 * a waypoint with a translational component. If the workspace state type does not have a translational
			 * component, this setting is ignored. Defaults to 0 millimeters.
			 */
			double translation_threshold = 0;
			/**
			 * @brief The threshold for rotation in radians used to determine whether the robot has reached a waypoint
			 * with a rotational component. If the workspace state type does not have a rotational component, this
			 * setting is ignored. Defaults to 0 radians.
			 */
			double angular_threshold = 0;
			/**
			 * @brief The threshold for translational velocity in mm/s used to determine whether the robot has reached
			 * a waypoint with a translational velocity component. If the workspace state type does not have a
			 * translational velocity component, this setting is ignored. Defaults to 0 mm/s.
			 */
			double vel_threshold = 0;
			/**
			 * @brief The threshold for angular velocity in rad/s used to determine whether the robot has reached a
			 * waypoint with an angular velocity component. If the workspace state type does not have an angular
			 * velocity component, this setting is ignored. Defaults to 0 rad/s.
			 */
			double angularVel_threshold = 0;

		};

		WorkspaceWaypointController(WorkspaceControllableRobot& robot, double timestep_ms, int priority = 0);
		WorkspaceWaypointController(WorkspaceControllableRobot& robot, double timestep_ms, Settings settings, int priority = 0);
		WorkspaceWaypointController(WorkspaceControllableRobot& robot, const WaypointList& waypoint_list, double timestep_ms, Settings settings = Settings(), int priority = 0);
		~WorkspaceWaypointController() = default;

		bool setSettings(Settings settings);
		bool setWaypointList(const WaypointList& waypoint_list);
		/**
		 * @brief Returns the current total number of waypoints in the waypoint list.
		 * @return
		 */
		int waypointCount() const { return waypoint_list.size(); }
		/**
		 * @brief Returns the current number of waypoints remaining in the waypoint list.
		 * @return
		 */
		int waypointsRemaining() const { return waypoint_list.size() - num_waypoints_completed; }
		/**
		 * @brief Returns true when the controller has completed all waypoints and after it has executed
		 * waypointsDone().
		 * @return
		 */
		bool isFinished() const { return waypoints_done; }

		bool reset() override;

	private:
		WorkspaceControllableRobot& robot;
		double timestep;	// seconds

		WaypointList waypoint_list;
		typename WaypointList::iterator current_waypoint;

		int num_waypoints_completed = 0;
		bool waypoint_reached = false;
		bool waypoints_done = false;

		Timing::Timer wait_timer;

		QLabel* currentWaypointLabel;
		QLabel* totalWaypointsLabel;
		QLabel* statusLabel;

		bool control() override final;
		bool waypointReachedMain();
		bool waypointsDoneMain();
		void updateUI();

	protected:
		Settings settings;

		QWidget* buildControlUI() override;
		/**
		 * @brief This function should be overriden if you want the controller to perform some custom action at each waypoint. This function runs as soon as
		 * a waypoint is reached (as opposed to after the waypoint wait time has expired, see waitTimeCompleted()). This function is run in the RobotController
		 * thread. This function must return `true` to indicate normal operation to WorkspaceWaypointController. If an unrecoverable error occurs in your
		 * implementation, return `false`. Returning `false` causes this controller to return `false` in the RobotController::control() function.
		 * @return
		 */
		virtual bool waypointReached() { return true; }
		/**
		 * @brief This function should be overriden if you want the controller to perform some custom action after the wait time has completed after reaching
		 * each waypoint. This function is run in the RobotController thread. This function must return `true` to indicate normal operation to
		 * WorkspaceWaypointController. If an unrecoverable error occurs in your implementation, return `false`. Returning `false` causes this controller to
		 * return `false` in the RobotController::control() function.
		 */
		virtual bool waitTimeCompleted() { return true; }
		/**
		 * @brief This function should be overriden if you want the controller to perform some custom action after all waypoints are completed. This function runs
		 * after the wait time has expired on the last waypoint. This function is run in the RobotController thread. This function must return `true` to indicate
		 * normal operation to WorkspaceWaypointController. If an unrecoverable error occurs in your implementation, return `false`. Returning `false` causes this
		 * controller to return `false` in the RobotController::control() function.
		 * @return
		 */
		virtual bool waypointsDone() { return true; }

};


template<typename WorkspaceStateType>
WorkspaceWaypointController<WorkspaceStateType>::WorkspaceWaypointController(WorkspaceControllableRobot& robot, double timestep_ms, int priority)
	: WorkspaceWaypointController<WorkspaceStateType>(robot, timestep_ms, Settings(), priority) {}


template<typename WorkspaceStateType>
WorkspaceWaypointController<WorkspaceStateType>::WorkspaceWaypointController(WorkspaceControllableRobot& robot, double timestep_ms, Settings settings, int priority)
	: RobotController(timestep_ms, priority),
	  robot(robot),
	  timestep(timestep_ms / 1000),
	  settings(settings) {

	setName("Workspace Waypoint Controller");

	// Make sure the iterator is tied to the waypoint list in case it ends up being empty
	current_waypoint = waypoint_list.begin();

}


template<typename WorkspaceStateType>
WorkspaceWaypointController<WorkspaceStateType>::WorkspaceWaypointController(WorkspaceControllableRobot& robot, const WaypointList& waypoint_list, double timestep_ms, Settings settings, int priority)
	: RobotController(timestep_ms, priority),
	  robot(robot),
	  timestep(timestep_ms / 1000),
	  waypoint_list(waypoint_list),
	  settings(settings) {

	setName("Workspace Waypoint Controller");

	reset();

}


/**
 * @brief Sets the settings of the WorkspaceWaypointController. This function can only be used when the controller is not
 * running (i.e. when isRunning() returns false).
 * @param settings New settings struct
 * @return True if success, false if the controller is running
 */
template<typename WorkspaceStateType>
bool WorkspaceWaypointController<WorkspaceStateType>::setSettings(Settings settings) {

	if (isRunning())
		return false;

	this->settings = settings;

	return true;

}


/**
 * @brief Sets the waypoint list of the WorkspaceWaypointController. This function can only be used when the controller is not
 * running (i.e. when isRunning() returns false). It is not necessary to call reset() after this function. The controller will
 * start at the beginning of the provided list.
 * @param waypoint_list New waypoint list
 * @return True if success, false if the controller is running
 */
template<typename WorkspaceStateType>
bool WorkspaceWaypointController<WorkspaceStateType>::setWaypointList(const WaypointList& waypoint_list) {

	if (isRunning())
		return false;

	this->waypoint_list = waypoint_list;

	// Need to call the WorkspaceWaypointController version of reset in case setWaypointList() is called from
	// an overridden reset() function which would cause infinite recursion
	WorkspaceWaypointController<WorkspaceStateType>::reset();

	return true;

}


template<typename WorkspaceStateType>
bool WorkspaceWaypointController<WorkspaceStateType>::reset() {

	// Cannot reset if the controller is running
	if (isRunning())
		return false;

	num_waypoints_completed = 0;
	waypoint_reached = false;
	waypoints_done = false;

	// Check to see if there are no waypoints given
	if (waypoint_list.size() == 0) {
		std::cout << "WORKSPACE_WAYPOINT_CONTROLLER: No waypoints have been given! Cannot move robot. ERROR" << std::endl;
		return false;
	}

	// Start at the beginning of the waypoint list
	current_waypoint = waypoint_list.begin();

	return true;

}


template<typename WorkspaceStateType>
bool WorkspaceWaypointController<WorkspaceStateType>::control() {

	// Check whether the list is completed
	if (current_waypoint == waypoint_list.end())
		return waypointsDoneMain();

	// Check if the waypoint has been reached and trigger the wait period
	if (waypoint_reached) {

		// Check if the wait period has elapsed
		if (wait_timer.elapsedTime().seconds() > settings.wait_time) {
			bool result = waitTimeCompleted();
			waypoint_reached = false;
			current_waypoint++; // Increment to the next waypoint
			return result;
		}

		return true;

	}

	// Get current robot workspace state
	WorkspaceStateType current_state;
	if (!robot.getWorkspaceState(current_state))
		return false;

	// Check if the waypoint has been reached
	if (current_state.hasArrived(*current_waypoint, settings.translation_threshold, settings.angular_threshold, settings.vel_threshold, settings.angularVel_threshold))
		return waypointReachedMain();

	WorkspaceControllableRobot::RatesAndFailStatus rates = {settings.translation_speed, settings.rotation_speed, settings.acceleration, settings.angular_acceleration};
	return robot.setWorkspaceState(*current_waypoint, rates);

}


template<typename WorkspaceStateType>
bool WorkspaceWaypointController<WorkspaceStateType>::waypointReachedMain() {

	waypoint_reached = true;
	num_waypoints_completed++;
	wait_timer.updateTime(); // reset the timer so that the robot arm will wait at this waypoint for the specified time

	std::cout << "WORKSPACE_WAYPOINT_CONTROLLER: waypoint reached (" << waypointsRemaining() << " waypoints remaining)" << std::endl;
	return waypointReached();

}


template<typename WorkspaceStateType>
bool WorkspaceWaypointController<WorkspaceStateType>::waypointsDoneMain() {

	if (waypoints_done)
		return true;

	bool result = waypointsDone();

	waypoints_done = true;
	std::cout << "WORKSPACE_WAYPOINT_CONTROLLER: waypoints completed" << std::endl;

	return result;

}


template<typename WorkspaceStateType>
void WorkspaceWaypointController<WorkspaceStateType>::updateUI() {

	currentWaypointLabel->setText(QString::number(num_waypoints_completed));
	totalWaypointsLabel->setText(QString::number(waypoint_list.size()));

	// Set the status label based on the current state of the controller
	if (!isRunning())
		statusLabel->setText("<b>Off</b>");
	else {

		if (waypoints_done)
			statusLabel->setText("<b>Idle</b>");
		else if (waypoint_reached)
			statusLabel->setText("<b>Waiting at waypoint</b>");
		else
			statusLabel->setText("<b>Moving to waypoint</b>");

	}

}


template<typename WorkspaceStateType>
QWidget* WorkspaceWaypointController<WorkspaceStateType>::buildControlUI() {

	currentWaypointLabel = new QLabel("0");
	totalWaypointsLabel = new QLabel(QString::number(waypoint_list.size()));
	statusLabel = new QLabel("<b>Off</b>");

	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(new QLabel("Waypoints Completed: "));
	layout->addWidget(currentWaypointLabel);
	layout->addWidget(new QLabel(" / "));
	layout->addWidget(totalWaypointsLabel);
	layout->addWidget(new QLabel(" Status: "));
	layout->addWidget(statusLabel);
	layout->addWidget(new QWidget(), 1);

	QWidget* widget = new QWidget();
	widget->setLayout(layout);

	QTimer* timer = new QTimer(widget);
	connect(timer, &QTimer::timeout, this, &WorkspaceWaypointController<WorkspaceStateType>::updateUI);
	timer->start(100);

	return widget;

}

#endif // WORKSPACE_WAYPOINT_CONTROLLER_H
