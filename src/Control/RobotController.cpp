#include "RobotController.h"
#include <QString>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "QtGUIElements/Labels.h"


/**
 * @brief This constructor simply sets the control interval in milliseconds and the thread priority.
 * @param timestep_ms Control interval in milliseconds
 * @param priority Thread priority level (see Timing::Timer)
 */
RobotController::RobotController(double timestep_ms, int priority)
	: timestep_ms(timestep_ms),
	  priority(priority),
	  rateLabel(nullptr) {}


RobotController::~RobotController() {

	robotControllerThread.stopTicking();

}


/**
 * @brief Starts the controller.
 * @return True if successful
 */
bool RobotController::start() {

	// Initialize the variables used to compute the control rate
	lastControlTimestamp = -1.0;
	actualControlRate.seed(1000.0 / timestep_ms);

	// Start the control thread
	return robotControllerThread.startTicking(*this, &RobotController::runController, timestep_ms, name, priority);

}


/**
 * @brief Stops the controller.
 * @return  True if successful
 */
bool RobotController::stop() {

	// Set the rate label to indicate that it is not running
	if (rateLabel)
		rateLabel->setText("0.00");

	// Stop the control thread
	if (!robotControllerThread.stopTicking())
		return false;

	stopCleanup();

	return true;

}


/**
 * @brief Returns true if the controller is currently running.
 * @return
 */
bool RobotController::isRunning() {

	return robotControllerThread.isTicking();

}


/**
 * @brief This function is a wrapper around the control() function that is used to implement the
 * measurement of actual control rate and emits the stoppedDurToError() signal when control()
 * returns `false`.
 * @return
 */
bool RobotController::runController() {

	// If rateLabel exists (because buildUI() was called) then we want to compute the control rate and
	// display it
	if (rateLabel) {
		double time = Timing::getSysTimeHighPrecision();
		if (lastControlTimestamp > 0) {
			actualControlRate.update(1000.0 / (time - lastControlTimestamp));
			rateLabel->setText(QString::number(actualControlRate.value(), 'f', 2));
		}
		lastControlTimestamp = time;
	}

	// Run the control() function. If it returns false, emit the stoppedDueToError() signal.
	if (!control()) {

		emit stoppedDueToError();
		return false;

	}

	return true;

}


/**
 * @brief This function is a wrapper around the buildControlUI() that makes sure the default UI is always
 * included in the UI returned by buildUI(). RobotControlUI calls this function rather than buildControlUI().
 * This wrapper is here so controller implementations don't have to remember to include the default UI as
 * part of the UI they build in buildControlUI().
 * @return A QWidget that represents the user interface for this RobotController
 */
QWidget* RobotController::buildUI() {

	// Create the rate label
	rateLabel = new QLabel("0.00");
	PrePostfixLabelsWidget* fullRateLabel = new PrePostfixLabelsWidget("Control Rate: ", rateLabel, " / " + QString::number(1000.0 / timestep_ms) + " Hz");

	// Create the priority label
	QLabel* priorityLabel = new QLabel("Thread Priority: " + QString::number(priority));

	// Put both labels in a horizontal layout
	QHBoxLayout* robotControllerUILayout = new QHBoxLayout();
	robotControllerUILayout->addWidget(priorityLabel);
	robotControllerUILayout->addWidget(fullRateLabel);
	robotControllerUILayout->addWidget(new QWidget(), 1);

	// Create the UI widget
	QWidget* widget = new QWidget();

	// Check if the controller has a widget. If so, include it.
	QWidget* controlWidget = buildControlUI();
	if (controlWidget) {

		QVBoxLayout* layout = new QVBoxLayout();
		layout->addLayout(robotControllerUILayout);
		layout->addWidget(controlWidget);

		widget->setLayout(layout);

	}
	else
		widget->setLayout(robotControllerUILayout);

	return widget;

}
