#ifndef SCHEDULING_PID_CONTROLLER_H
#define SCHEDULING_PID_CONTROLLER_H

#include <Control/PID/PIDController.h>

/**
 * @brief The SchedulingPIDController class extends the PIDController class to allow for two sets
 * of gains and feedforward terms, a low set and a high set.
 *
 * The class allows you to switch between each set with the useHighGain() and useLowGain() functions.
 * The controller will smoothly transition between these two sets of gains as the compute() function
 * is repeatedly called. In other words, if you switch to high gains, each compute() call will
 * progressively get closer and closer to the desired gains until they are reached. The number of
 * compute() calls required for the transition to complete is determined by the `transition_counts`
 * parameter of the constructor and defaults to 500.
 *
 * The compute() function is not thread-safe, but the useHighGain() and useLowGain() functions are. In
 * other words, make sure only one thread is calling compute() at a time.
 */
template<int dim>
class SchedulingPIDController : public PIDController<dim> {

	public:
		using Value = typename PIDController<dim>::Value;
		using Error = typename PIDController<dim>::Error;
		using Derivative = typename PIDController<dim>::Derivative;
		using FeedForward = typename PIDController<dim>::FeedForward;
		using Gain = typename PIDController<dim>::Gain;

		SchedulingPIDController(const Gain& Pgain_low = Gain::Zero(),
								const Gain& Igain_low = Gain::Zero(),
								const Gain& Dgain_low = Gain::Zero(),
								const FeedForward& feedforward_low = FeedForward::Zero(),
								const Gain& Pgain_high = Gain::Zero(),
								const Gain& Igain_high = Gain::Zero(),
								const Gain& Dgain_high = Gain::Zero(),
								const FeedForward& feedforward_high = FeedForward::Zero(),
								int transition_counts = 500)
			: PIDController<dim>(Pgain_low, Igain_low, Dgain_low, feedforward_low),
			  Pgain_low(Pgain_low),
			  Igain_low(Igain_low),
			  Dgain_low(Dgain_low),
			  feedforward_low(feedforward_low),
			  Pgain_high(Pgain_high),
			  Igain_high(Igain_high),
			  Dgain_high(Dgain_high),
			  feedforward_high(feedforward_high),
			  transition_counts(transition_counts) {}

		void useHighGain() { low_selected = false; }
		void useLowGain() { low_selected = true; }

		Value compute(const Error& position_error, const Derivative& derivative_value = Derivative::Zero()) override {

			if (low_selected && (current_count > 0))
				current_count -= 1;

			if (!low_selected && (current_count < transition_counts))
				current_count += 1;

			double alpha = (transition_counts - current_count) / double(transition_counts);

			PIDController<dim>::Pgain = alpha * Pgain_low + (1.0 - alpha) * Pgain_high;
			PIDController<dim>::Igain = alpha * Igain_low + (1.0 - alpha) * Igain_high;
			PIDController<dim>::Dgain = alpha * Dgain_low + (1.0 - alpha) * Dgain_high;
			PIDController<dim>::feedforward = alpha * feedforward_low + (1.0 - alpha) * feedforward_high;

			return PIDController<dim>::compute(position_error, derivative_value);

		}

	private:
		Gain Pgain_low;
		Gain Igain_low;
		Gain Dgain_low;
		FeedForward feedforward_low;
		Gain Pgain_high;
		Gain Igain_high;
		Gain Dgain_high;
		FeedForward feedforward_high;

		bool low_selected = true;
		int transition_counts;
		int current_count = 0;

};
	
#endif // SCHEDULING_PID_CONTROLLER_H
