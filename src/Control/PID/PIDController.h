#ifndef PID_CONTROLLER_H
#define PID_CONTROLLER_H

#include <Eigen/Dense>

/**
 * @brief The PIDController class implements a simple arbitrary dimensional PID controller with an
 * optional feedforward term.
 *
 * This class is a template to allow for arbitrary n-dimensional values. It specifies the following
 * types:
 *
 * 1. Value - n-dimensional vector representing the value of the controller (result of the controller)
 * 2. Error - n-dimensional input vector representing the error
 * 3. Derivative - n-dimensional input vector representing the derivative of error
 * 4. FeedForward - n-dimensional feedforward term
 * 5. Gain - n-dimensional square matrix representing gain
 *
 * These types are simply type redefinitions of the fixed size Eigen::Matrix from the Eigen library.
 *
 * The proportional, integral, and derivative gains as well as the feedforward term are provided in
 * the constructor. The compute() function is then used with the current error and derivative of
 * error to compute the current PID control result. The integral of error will automatically be
 * computed as simply the accumulation of the provided error values (i.e the time interval is not
 * multiplied before accumulation. If you have determined an integrator gain that assumes the time
 * interval is used, then simply multiply the integral gain by the time interval you are using.).
 * The clear() function is used to clear the integrator. The p(), i(), d(), and ff() functions can
 * be used to access P, I, D, and feedforward values respectively.
 *
 * The compute() function is not thread-safe. Make sure that only one thread is calling compute() at
 * at time.
 */
template<int dim>
class PIDController {

	static_assert(dim > 0, "The dimension must be larger than 0.");

	public:
		using Value = Eigen::Matrix<double, dim,  1, Eigen::DontAlign>;
		using Error = Eigen::Matrix<double, dim,  1, Eigen::DontAlign>;
		using Derivative = Eigen::Matrix<double, dim,  1, Eigen::DontAlign>;
		using FeedForward = Eigen::Matrix<double, dim,  1, Eigen::DontAlign>;
		using Gain = Eigen::Matrix<double, dim,  dim, Eigen::DontAlign>;

		PIDController(const Gain& Pgain = Gain::Zero(),
					  const Gain& Igain = Gain::Zero(),
					  const Gain& Dgain = Gain::Zero(),
					  const FeedForward& feedforward = FeedForward::Zero())
			: Pgain(Pgain),
			  Igain(Igain),
			  Dgain(Dgain),
			  feedforward(feedforward) {}

		virtual Value compute(const Error& error, const Derivative& error_derivative = Derivative::Zero()) {

			Value control_effort = (Pgain * error) + (Igain * integral) + (Dgain * error_derivative) + feedforward;

			integral += error;

			return control_effort;

		}

		void clear() { integral = Value::Zero(); }

		Gain p() { return Pgain; } // return the p gain
		Gain i() { return Igain; } // return the i gain
		Gain d() { return Dgain; } // return the d gain
		FeedForward ff() { return feedforward; } // return the feed forward gain

	protected:
		Gain Pgain;
		Gain Igain;
		Gain Dgain;
		FeedForward feedforward;
		Error integral = Error::Zero();

};

#endif // PID_CONTROLLER_H
