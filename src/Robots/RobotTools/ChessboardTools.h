#ifndef CHESS_BOARD_TOOLS_H
#define CHESS_BOARD_TOOLS_H

#include <Robots/Tool.h>
#include <CV/ChessboardCalibrationPattern.h>


/**
 * @brief The ChessboardTool class represents the tool made of an acrylic rectangle attached to an
 * orange 3D printed angle bracket.
 *
 * The tool x direction is down away from the robot tool flange, the z direction is normal to the board,
 * and the y direction is z cross x. The origin of the tool is located at the top left "inner"
 * corner of the chessboard when looking at the board with the robot flange interface on top.
 * <b>IMPORTANT:</b> Make sure to attach the tool to the robot flange such that this description of
 * the axes is correct. This involves looking at the D-H parameters of the robot being used as well as
 * the specific tool transform for this Tool. For the case of the MH5Robot, the correct orientation would
 * cause the chessboard pattern to face down toward the floor when the robot is in the home pose (see
 * MH5Robot for details of its home pose).
 */
class ChessboardTool : public Tool, public ChessboardCalibrationPattern {

	public:
		ChessboardTool();

};


class ChessBoardSAMMTool : public Tool, public ChessboardCalibrationPattern {

	public:
		ChessBoardSAMMTool();

};

#endif // CHESS_BOARD_TOOLS_H
