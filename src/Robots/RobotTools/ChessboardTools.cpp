#include "ChessboardTools.h"
#include "RoboticsToolbox.h"


ChessboardTool::ChessboardTool() : ChessboardCalibrationPattern(6 /* rows */, 9 /* cols */, 11.5 /* mm */) {

	tool_transform = Robo::Pose6DOF(Robo::Vector3d(-31.1, -39, 18.7), Robo::Rotation(-90.0, Robo::UnitY()));
	safety_limit_radius = 110.0;	// mm

}


ChessBoardSAMMTool::ChessBoardSAMMTool() : ChessboardCalibrationPattern(6 /* rows */, 9 /* cols */, 11.5 /* mm */) {

	//In this configuration, the board is in the same frame as the SAMM, with the +X direction out into the hallway, the +z axis along the tool and the y being the Z x X.
	tool_transform = Robo::Pose6DOF(Robo::Vector3d(30.0, 85.0, 50.0), Robo::Rotation(179.5, Robo::UnitZ()) * Robo::Rotation(90, Robo::UnitY()));
	// x was -223.05
	//tool_transform = Robo::Pose6DOF(Robo::Vector3d( -223.05, 23.0,27.5), Robo::Rotation(180, Robo::UnitZ())*Robo::Rotation(89.3, Robo::UnitY())); //Robo::Rotation(180, Robo::UnitZ())

}
