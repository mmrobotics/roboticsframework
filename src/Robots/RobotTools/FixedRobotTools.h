#ifndef FIXED_ROBOT_TOOLS_H
#define FIXED_ROBOT_TOOLS_H

#include <Robots/Tool.h>


/**
 * @brief The ProbeTool class represents the orange 3D printed tool shaped like a pointed
 * probe.
 *
 * The tool frame has the same orientation as the last link of the robot and is translated
 * 41.6 millimeters in the z direction (i.e. the origin is at the tip of the probe). It
 * does not matter what orientation the tool is mounted since it is symmetrical about the
 * long axis of the tool.
 */
class ProbeTool : public Tool {

	public:
		ProbeTool();

};



/**
 * @brief The MagnetTool class represents a cylindrical magnet attached to the robot
 * tool flange. It is unknown which magnet is referred to.
 */
class MagnetTool : public Tool {

	public:
		MagnetTool();

};


/**
 * @brief The MagnetTool class represents a cylindrical magnet attached to the robot
 * tool flange. It is unknown which magnet is referred to.
 */
class LargeMagnetTool : public Tool {

	public:
		LargeMagnetTool();

};


class VisionProbeTool : public Tool {

	public:
		VisionProbeTool();

};

#endif // FIXED_ROBOT_TOOLS_H
