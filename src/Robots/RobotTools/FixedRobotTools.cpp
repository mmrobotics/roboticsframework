#include "FixedRobotTools.h"
#include "RoboticsToolbox.h"


ProbeTool::ProbeTool() {

	tool_transform = Robo::Pose6DOF(Robo::Vector3d(0.0, 0.0, 41.6), Robo::I33);
	// No safety radius is left at 0 on purpose to allow the probe to approach surfaces

}


MagnetTool::MagnetTool() {

	tool_transform = Robo::Pose6DOF(Robo::Vector3d(0.0 /* mm */, 0.0 /* mm */, 23.45 /* mm */), Robo::I33);

}


LargeMagnetTool::LargeMagnetTool() {

	tool_transform = Robo::Pose6DOF(Robo::Vector3d(0.0 /* mm */, 0.0 /* mm */, 25.875 /* mm */), Robo::I33);
//	tool_transform = Robo::Pose6DOF(Robo::Vector3d(0.0 /* mm */, 0.0 /* mm */, 30.875 /* mm */), Robo::Rotation(7,-5,0));

}


VisionProbeTool::VisionProbeTool() {

	tool_transform = Robo::Pose6DOF(Robo::Vector3d(0.0, 0.0, 296.3), Robo::I33);

}
