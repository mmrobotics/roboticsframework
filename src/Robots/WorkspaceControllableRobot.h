#ifndef WORKSPACE_CONTROLLABLE_ROBOT_H
#define WORKSPACE_CONTROLLABLE_ROBOT_H

#include <Robots/GenericRobot.h>
#include <Robots/RobotSafetyLimits.h>
#include <RoboticsToolbox.h>
#include <QLineEdit>


/**
 * @brief The WorkspaceControllableRobot class is a specific type of GenericRobot that is able to be controlled by
 * specifying the workspace state of the robot.
 *
 * The workspace state is considered some portion of the state of a 3D rigid body frame (i.e. position, orientation,
 * position and orientation, etc.). These portions of states correspond to the rigid body state types found in the
 * Robo namespace in RoboticsToolbox.h. The workspace state is always expressed relative to the base frame of the
 * robot.
 *
 * The desired state is set using the setWorkspaceState() functions. This function is overloaded to accept one of
 * the state types of the Robo namespace as well as a structure called RatesAndFailStatus. The implementation of
 * the setWorkspaceState() functions will cause the robot to linearly interpolate between its current state and the
 * desired state. This requires the caller to also specify the rate for interpolation and this is done by setting
 * the relevant variables in the RatesAndFailStatus parameter provided. See documentation for RatesAndFailStatus for
 * more details. The setWorkspaceState() function will always override any previously set desired state or rates but
 * will not override the state or rates that are not relevant to the particular setWorkspaceState() function. For
 * example, the setWorkspaceState() function that accepts a Robo::Position type, will override the desired position
 * and the corresponding translational speed setting, but will leave previous rotational settings alone. Put another
 * way, the user may set translational and rotational parts of the state and their corresponding rates independently.
 * Any new translation part (position or velocity) will override previous translational settings. Similarly, any
 * new rotational part (position or velocity) will override previous rotational settings. The current state can be
 * obtained by using the getWorkspaceState() function.
 *
 * Not every implementation of WorkspaceControllableRobot will allow setting or getting all of the different state
 * types. For example, it does not make sense to set the orientation or heading of an XYZ gantry robot since it
 * only has three degrees of freedom in position. In these scenarios, the getWorkspaceState() and setWorkspaceState()
 * functions will return `false` and print an error message to the console. This means that implementers of this class
 * need not implement all of the getWorkspaceState() and setWorkspaceState() functions, but they should implement as
 * many as are relevant for the particular robot being implemented.
 *
 * The WorkspaceControllableRobot also requires a RobotLimiter object. This is used to set workspace state safety
 * limits on the robot that should not be exceeded. It is the responsibility of the implementer to make sure that
 * these limits are respected.
 *
 * The class implements the buildUI() function to display a position jogging interface. This interface may not be
 * applicable to every type of robot, but it is implemented as a default interface that is likely to be useful
 * for most robots. It is also an example of how to implement the buildUI() function. If your robot needs a more
 * specific interface, then simply reimplement the buildUI() function in your derived class.
 *
 * <b>IMPLEMENTATION NOTES:</b> It is the responsibility of the implementer to make sure that the description of
 * this class and each of its functions are met. The interpolate() function that each state type has is useful to
 * make linear interpolation easy in your implementation. It is also <b>very important</b> that implementers of this
 * class make sure that all function implementations are thread-safe.
 */
class WorkspaceControllableRobot : public GenericRobot {

	Q_OBJECT

	private:
		void getWorkspaceStateNotImplemented(const char* type) const;
		void setWorkspaceStateNotImplemented(const char* type) const;

		QLineEdit* x_pos_box;
		QLineEdit* y_pos_box;
		QLineEdit* z_pos_box;
		double x_step = 0;
		double y_step = 0;
		double z_step = 0;
		double jogSpeed;
		double stepMax;

	private slots:
		void update();
		void xStepChanged(double val) { x_step = val; }
		void yStepChanged(double val) { y_step = val; }
		void zStepChanged(double val) { z_step = val; }
		void jogUp();
		void jogDown();
		void jogLeft();
		void jogRight();
		void jogFront();
		void jogBack();

	protected:
		RobotLimiter& robot_limiter;

	public:
		/**
		 * @brief Constructor.
		 * @param limiter The RobotLimiter for this robot
		 * @param jogSpeed The translational speed used in the default jog interface in mm/s
		 * @param stepMax The maximum jog distance used in the default jog interface in millimeters
		 */
		WorkspaceControllableRobot(RobotLimiter& limiter, double jogSpeed, double stepMax) : jogSpeed(jogSpeed), stepMax(stepMax), robot_limiter(limiter) { setName("Workspace Controllable Robot"); }

		virtual bool getWorkspaceState(Robo::Position& position) const;
		virtual bool getWorkspaceState(Robo::Heading& heading) const;
		virtual bool getWorkspaceState(Robo::Orientation& orientation) const;
		virtual bool getWorkspaceState(Robo::AngularVelocity& angularVelocity) const;
		virtual bool getWorkspaceState(Robo::Pose5DOF& pose5DOF) const;
		virtual bool getWorkspaceState(Robo::Pose6DOF& pose6DOF) const;
		virtual bool getWorkspaceState(Robo::RotatingPosition& rotatingPosition) const;
		virtual bool getWorkspaceState(Robo::RotatingPose5DOF& rotatingPose5DOF) const;

		/**
		 * @brief The RatesAndFailStatus struct is used to specify the interpolation rates when calling setWorkspaceState().
		 * It is also used to get more information about why setWorkspaceState() returned `false` if this occurs. If the
		 * function returns `false` it may set the `safetyLimitFailure` and/or the `ratesInvalid` variable to `true`.
		 */
		struct RatesAndFailStatus {

			/**
			 * @brief The speed at which translation will occur in mm/s. If the workspace state type does not have
			 * a translational component, this setting is ignored. The default value is 0 mm/s.
			 */
			double translation_speed = 0;
			/**
			 * @brief The speed at which rotation will occur in rad/s. If the workspace state type does not have a
			 * rotational component, this setting is ignored. The default value is 0 rad/s.
			 */
			double rotation_speed = 0;
			/**
			 * @brief The acceleration at which changes in translational velocity will occur in mm/s^2. If the
			 * workspace state type does not have a translational velocity component, this setting is ignored.
			 * The default value is 0 mm/s^2.
			 */
			double acceleration = 0;
			/**
			 * @brief The angular acceleration at which changes in angular velocity will occur in rad/s^2. If the
			 * workspace state type does not have an angular velocity component, this setting is ignored. The
			 * default value is 0 rad/s^2.
			 */
			double angular_acceleration = 0;
			/**
			 * @brief This variable will be set to `true` by setWorkspaceState() if the the safety limits will be
			 * violated by the provided desired state. Note that while the robot will make sure to never violate
			 * the safety limits, it may not necessarily check all of the intermediate states between the current
			 * state and the desired state during the setWorkspaceState() function. It may just check that the
			 * desired state doesn't violate the safety limits.
			 */
			bool safetyLimitFailure = false;
			/**
			 * @brief This variable will be set to `true` by setWorkspaceState() if any of the provided rates are
			 * invalid (e.g. they are too large for that particular robot).
			 */
			bool ratesInvalid = false;

		};

		virtual bool setWorkspaceState(const Robo::Position& position, RatesAndFailStatus& ratesAndFailStatus);
		virtual bool setWorkspaceState(const Robo::Heading& heading, RatesAndFailStatus& ratesAndFailStatus);
		virtual bool setWorkspaceState(const Robo::Orientation& orientation, RatesAndFailStatus& ratesAndFailStatus);
		virtual bool setWorkspaceState(const Robo::AngularVelocity& angularVelocity, RatesAndFailStatus& ratesAndFailStatus);
		virtual bool setWorkspaceState(const Robo::Pose5DOF& pose5DOF, RatesAndFailStatus& ratesAndFailStatus);
		virtual bool setWorkspaceState(const Robo::Pose6DOF& pose6DOF, RatesAndFailStatus& ratesAndFailStatus);
		virtual bool setWorkspaceState(const Robo::RotatingPosition& rotatingPosition, RatesAndFailStatus& ratesAndFailStatus);

		QWidget* buildUI() override;

};

#endif // WORKSPACE_CONTROLLABLE_ROBOT_H
