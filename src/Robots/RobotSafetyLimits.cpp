#include "RobotSafetyLimits.h"
#include <iostream>
#include <list>


/**
 * @brief Creates a WorkspacePositionLimit with the specified direction and lower/upper limits.
 * @param direction Direction of the positional limit. The provided vector will be unitized and must
 * not be the zero vector
 * @param low_limit Lower bound of the limit along the direction. Must be smaller than `high_limit`
 * @param high_limit Upper bound of the limit along the direction. Must be larger than `low_limit`
 */
WorkspacePositionLimit::WorkspacePositionLimit(const Robo::Vector3d& direction, double low_limit, double high_limit) {

	// Make sure the provided direction is not the zero vector
	if (direction == Robo::Zero()) {
		std::cerr << "WORKSPACE_POSITION_LIMIT: Workspace limit created with a zero direction vector! This limit will fail! ERROR" << std::endl;
		return;
	}

	// Make sure the high limit is larger than the low limit
	if (low_limit > high_limit) {
		std::cerr << "WORKSPACE_POSITION_LIMIT: Workspace limit created with a low_limit larger than the high_limit! This limit will fail! ERROR" << std::endl;
		return;
	}

	_direction = direction;
	_low_limit = low_limit;
	_high_limit = high_limit;

}


/**
 * @brief Checks whether the provided position is within the limits specified by this instance of
 * WorkspacePositionLimit. Allows for additional padding to be specified in millimeters
 * @param test_position The position to test
 * @param padding Additional padding in millimeters
 * @return True if the test passed
 */
bool WorkspacePositionLimit::test(const Robo::Position& test_position, double padding) const {

	double projection = _direction.dot(test_position.position);

	if ((projection < _low_limit + padding) || (projection > _high_limit - padding)) {
		std::cerr << "WORKSPACE POSITION LIMIT REACHED: The position of " << projection << " mm in the (" << _direction.transpose() << ") direction with padding of " << padding << " exceeded the allowable range of [ " << _low_limit << ", " << _high_limit << " ]! WARNING" << std::endl;
		return false;
	}

	return true;

}


/**
 * @brief Adds a WorkspacePositionLimit to the RobotLimiter that will get tested against whenever the
 * test() function is called.
 * @param new_limit A new limit
 */
void RobotLimiter::addLimit(const WorkspacePositionLimit& new_limit) {

	workspace_position_limit_list.push_back(new_limit);

}


/**
 * @brief Checks whether the provided position is within all of the limits provided to this instance of
 * RobotLimiter. Allows for additional padding to be specified in millimeters.
 * @param test_position The position to test
 * @param padding Additional padding in millimeters
 * @return True if the test passed
 */
bool RobotLimiter::test(const Robo::Position& test_position, double padding) {

	for (std::list<WorkspacePositionLimit>::iterator it = workspace_position_limit_list.begin(); it != workspace_position_limit_list.end(); ++it) {
		if (!it->test(test_position, padding))
			return false;
	}

	return true;

}
