#ifndef GENERIC_ROBOT_H
#define GENERIC_ROBOT_H

#include <QObject>
#include <QWidget>


/**
 * @brief The GenericRobot class is meant to act as the most basic interface for any robot.
 * It provides functions for enabling and disabling the robot as well as checking for errors.
 *
 * The class also declares a buildUI() function which is meant to be used in conjunction with
 * RobotControlUI to provide a way to display a manual robot control panel specific to the
 * particular robot implementation if desired. See the documentation for buildUI() for details.
 * Each GenericRobot has a name which is used to identify the robot in user interfaces. This
 * name is meant to be set by all child classes of GenericRobot that can be instantiated. The
 * name should be set in the constructor of the class using the setName() function (which has
 * protected access). A derived class will override the name of a parent class since the body
 * of a class constructor is executed after the constructors for parent classes. If a robot
 * implementation does not set its name, the name will be that of its nearest ancestor that has
 * set a name. If no ancestors have set a name, then the name of the robot will be "Unknown Robot".
 *
 * All of the implementations for the functions of this class except for buildUI() <b>must be
 * thread-safe</b>.
 */
class GenericRobot : public QObject {

	Q_OBJECT

	private:
		const char* name = "Unknown Robot";

	protected:
		/**
		 * @brief Sets the name of the robot which is used to identify it in user interfaces.
		 * This function should be called at the beginning of the constructor of any class that
		 * inherits from GenericRobot. If a robot implementation does not set its name, the
		 * name will be that of its nearest ancestor that has set a name. If no ancestors
		 * have set a name, then the name of the robot will be "Unknown Robot".
		 * @param name A name for the specific robot implementation
		 */
		void setName(const char* name) { this->name = name; }

	public:
		virtual ~GenericRobot() = default;
		/**
		 * @brief Returns the name of the specific robot implementation. If a robot does not
		 * set its name, this function will return "Unknown Robot".
		 * @return
		 */
		const char* getName() const { return name; }
		/**
		 * @brief Enables the robot.
		 * @return True if successful
		 */
		virtual bool enable() = 0;
		/**
		 * @brief Disables the robot.
		 * @return  True if successful
		 */
		virtual bool disable() = 0;
		/**
		 * @brief Returns true if the robot is currently enabled.
		 * @return
		 */
		virtual bool isEnabled() const = 0;
		/**
		 * @brief Returns true if the robot is currently disabled.
		 * @return
		 */
		virtual bool isDisabled() const = 0;
		/**
		 * @brief Returns true if an error has occurred with the robot.
		 * @return
		 */
		virtual bool errorOccurred() const = 0;
		/**
		 * @brief This function is used by RobotControlUI to get a widget from the robot that
		 * is meant to act as a manual control panel for the specific type of robot. By
		 * default, this function returns a `nullptr` which means that the robot does not have
		 * a user interface to display. If desired, each GenericRobot implementation can override
		 * this function to display manual controls specific to the type of robot. The UI is meant
		 * only for manual control of the robot and will be disabled by RobotControlUI when the
		 * robot is not enabled or a controller is running. As an example, the
		 * WorkspaceControllableRobot class overrides this function to display an interface that
		 * can be used to manually jog the position of the robot. This interface can be used to
		 * position the robot before starting a controller. Once the controller is started, it has
		 * control of the robot so RobotControlUI disables the robot interface. Your UI should be
		 * designed in the same spirit. The UI should not be used to interact with any particular
		 * type of RobotController. RobotControllers can build their own user interfaces for that
		 * purpose. See RobotController::buildControlUI() and RobotController::buildUI() for details.
		 *
		 * <b>IMPLEMENTATION:</b> You must be aware of a few things when implementing this function:
		 *
		 * 1. Treat this function as if it were the constructor for a class that inherits from QWidget,
		 * but instead of inheriting from QWidget, create a QWidget object in the function and build
		 * the UI into that widget. Then return the widget you created.
		 * 2. Qt widgets have a parent-child relationship called the object tree that is used to destroy
		 * widgets when the parent is destroyed. Therefore, all of the QWidget elements created in
		 * buildUI() should be created with the `new` keyword, and they should all be part of the object
		 * tree of the QWidget that gets returned by this function. Do not make your implementation
		 * class be the parent of any of the QWidgets that make up the UI and do not delete them in
		 * your implementation class's destructor. The RobotControlUI will take responsibility for
		 * cleaning up these resources. A QWidget is usually added to the object tree of another QWidget
		 * by Qt automatically when you call the addWidget(), addLayout(), setLayout(), etc. functions,
		 * so most of the time you will not need to worry about explicitly setting up the object tree
		 * as long as each widget is added to the UI through Qt functions that reassign parent-child
		 * relationships.
		 * 3. Non QWidget elements of the UI such as a QTimer are not automatically added to the object
		 * tree, but they should be added so RobotControlUI can take responsibility for these classes.
		 * For example, if your UI uses the timeout signal from a QTimer to update the UI, then you
		 * should explicitly set the parent of that QTimer as the QWidget returned by this function.
		 * <b>You should familiarize yourself with the Qt object tree before implementing this function.</b>
		 * 4. If you are inheriting from a class that overrides this function, you can call the parent
		 * class implementation of buildUI() to get the parent QWidget and incorporate it into the
		 * QWidget created in your implementation. If you do not do this, the parent widget will simply
		 * not appear. For example, if I create a robot that inherits from WorkspaceControllableRobot
		 * I can include the WorkspaceControllable robot UI into my UI by calling
		 * WorkspaceControllableRobot::buildUI() and adding the returned QWidget to my QWidget, or I can
		 * simply create my own QWidget and never create the WorkspaceControllableRobot UI, effectively
		 * hiding it.
		 * 5. Signals and slots can be defined in your implementation class to interact with the UI.
		 * To do this, you need to include the Q_OBJECT macro in your class (the class already inherits
		 * from QObject since GenericRobot inherits from QObject). The best place to connect signals to
		 * slots would be in the implementation of this function before you return the QWidget representing
		 * the UI. Keep in mind that if you use Qt::AutoConnection or Qt::QueuedConnection, then it is
		 * always safe to emit a signal from any thread (e.g. a thread you create in your implementation
		 * which manages the robot behind the scenes), but you should pay attention to which thread will
		 * be executing the slots in your class (usually the UI/main thread). If a slot will be accessing
		 * data that a different thread also needs to access, you need to make sure the access to that
		 * data is thread-safe.
		 * 6. The widget that you return will be placed inside a QGroupBox with the name of the robot.
		 * Therefore, you do not need to do anything to label or outline your widget visually to
		 * distinguish it from other parts of the RobotControlUI.
		 * @return A QWidget with no parent that represents the UI for manual control of the robot
		 */
		virtual QWidget* buildUI() { return nullptr; }

};

#endif // GENERIC_ROBOT_H
