#ifndef MH5PARAMETERS_H
#define MH5PARAMETERS_H


/**
 * The MH5 namespace contains structures and variables relevant to the MH5 Yaskawa Motoman robot arm
 * and its controller, the DX100.
 *
 * This namespace contains variables for the D-H parameters and joint resolution of the MH5 manipulator,
 * and contains command strings used when controlling the MH5 through the DX100's Ethernet Server Function.
 * It also contains structs representing status and alarm codes.
 *
 * The joint resolutions are in degrees/pulse, alpha and theta_offset are in degrees,
 * and a and d are in millimeters.
 *
 * Note that the D-H parameters given in this namespace result in rotation axis vectors (z-axes) for
 * joints 2, 4, and 6 (L, R, and T) which are reversed from that which the DX100 uses when controlling
 * these joints on the MH5. This can be verified by simply moving these joints independently with the
 * control pendant and noticing that a positive rotation command from the pendant results in a negative
 * joint rotation with respect to the D-H convention described here. This means that angles for these
 * joints, expressed with respect to the D-H convention in this namespace, must be multiplied by -1 when
 * sending and receiving joint pulses to/from the DX100 in order to achieve the expected direction of
 * rotation. Fortunately, this has been accounted for in the joint pulse resolution values defined in
 * this namespace for these joints by making them negative. Since joint angles are converted to/from
 * pulses in the MH5Communicator class immediately before and after sending and receiving joint positions,
 * you should not have to worry about this discrepancy. You should be able to work exclusively with joint
 * angles expressed with respect to the D-H convention of this namespace when dealing with MH5 related tasks
 * such as computing forward/reverse kinematics, etc.
 */
namespace MH5 {

	// Joint Resolution
	constexpr float DegResolutionJ1 = 360.0f * 23.0f / (2200.0f * 4096.0f);		// joint 1 pulse resolution (degs/pulse)
	constexpr float DegResolutionJ2 = -360.0f * 3.0f / (320.0f * 4096.0f);		// joint 2 pulse resolution (degs/pulse)
	constexpr float DegResolutionJ3 = 360.0f * 9.0f / (800.0f * 4096.0f);		// joint 3 pulse resolution (degs/pulse)
	constexpr float DegResolutionJ4 = -360.0f / (80.0f * 4096.0f);				// joint 4 pulse resolution (degs/pulse)
	constexpr float DegResolutionJ5 = 360.0f * 9.0f / (725.0f * 4096.0f);		// joint 5 pulse resolution (degs/pulse)
	constexpr float DegResolutionJ6 = -360.0f * 129.0f / (6500.0f * 4096.0f);	// joint 6 pulse resolution (degs/pulse)

	// DH Parameters
	const double theta_offset[6] = {0.0, 90.0, 0.0, 0.0, 0.0, 0.0}; // degrees
	const double alpha[6] = {90.0, 0.0, 90.0, -90.0, 90.0, 0.0}; // degrees
	const double d[6] = {0.0, 0.0, 0.0, 305.0, 0.0, 86.5}; // mm
	const double a[6] = {88.0, 310.0, 40.0, 0.0, 0.0, 0.0}; //mm

	// Command strings (see Ethernet Server Function documentation for meanings of the command strings)
	const char ROBOT_ACCESS_REQST[] = "CONNECT Robot_access Keep-Alive:-1\r\n";
	const char ROBOT_CMD_GET_ALARM[] = "HOSTCTRL_REQUEST RALARM 0\r\n";
	const char ROBOT_CMD_GET_STATUS[] = "HOSTCTRL_REQUEST RSTATS 0\r\n";
	const char ROBOT_CMD_SET_SERVO[] = "HOSTCTRL_REQUEST SVON 2\r\n";
	const char ROBOT_CMD_GET_POSJ[] = "HOSTCTRL_REQUEST RPOSJ 0\r\n";
	const char ROBOT_CMD_SET_POSJ_JOINT[] = "HOSTCTRL_REQUEST PMOVJ";
	const char ROBOT_CMD_SET_POSJ_WORK[] = "HOSTCTRL_REQUEST PMOVL";

	/**
	 * @brief The RobotAlarm struct is used to store the alarm codes and data received from the DX100 robot controller.
	 */
	struct RobotAlarm {
		int e1c; // error code 1
		int e1d; // error data 1
		int a1c; // alarm code 1
		int a1d; // alarm data 1
		int a2c; // alarm code 2
		int a2d; // alarm data 2
		int a3c; // etc.
		int a3d;
		int a4c;
		int a4d;
	};


	/**
	 * @brief The RobotStatus struct is used to store the status codes received from the DX100 robot controller (See
	 * Ethernet Server Function documentation for details on what each bit of the two bytes s1 and s2 mean).
	 */
	struct RobotStatus {
		int s1; // status byte 1
		int s2; // status byte 2
	};

}

#endif
