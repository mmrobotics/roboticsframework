#include "MH5Robot.h"
#include <iostream>
#include <cmath>

using namespace std;


MH5Robot::MH5Robot(RobotLimiter& limiter, Tool* tool)
	: SerialLinkRobot<6>(limiter, 40, 30),
	  ToolableRobot(tool) {

	setName("MH5 Robot");

	// Connect to the MH5 robot
	robot_communicator = new MH5Communicator();
	robot_communicator->connectToController("192.168.255.1");
	robot_communicator->establishAccess();

	// Make sure the servo is off
	robot_communicator->sendServo(false);
	
	// Initialize the current configuration of the robot
	if (!robot_communicator->recvJointPosition(currentCommandedConfiguration)) {
		std::cerr << "MH5 ROBOT: there was an error obtaining the current joint configuration of the robot arm. ERROR" << std::endl;
		return;
	}

	computeForwardKinematics(currentCommandedConfiguration, currentCommandedPose6DOF);
	lastCommandedPose6DOF = currentCommandedPose6DOF;
	desiredPose6DOF = currentCommandedPose6DOF;

	mh5ControlThread.startTicking(*this, &MH5Robot::controlMH5Robot, 1000.0 * controlThreadTimeInterval, "MH5ControlThread", 98);

}


MH5Robot::~MH5Robot() {

	if (isEnabled())
		disable();

	mh5ControlThread.stopTicking();

	delete robot_communicator;

}


bool MH5Robot::testLimits(const Robo::Pose6DOF& pose6DOF) {

	// Test the end frame soft limits (with tool if given)
	if (robot_tool) {
		if (!robot_limiter.test(pose6DOF, robot_tool->getSafetyRadius()))
			return false;
	}
	else {
		if (!robot_limiter.test(pose6DOF, 20.0))
			return false;
	}

	// Compute the position of the spherical wrist which corresponds to the distal end of the forearm of the MH5 robot
	Robo::Position wrist_position;
	if (robot_tool) {

		Robo::Matrixd33 rot = pose6DOF.orientation * robot_tool->getTransform().orientation.transpose();
		wrist_position.position = pose6DOF.position - rot * robot_tool->getTransform().position - MH5::d[5] * rot.col(2);

	}
	else {

		wrist_position.position = pose6DOF.position - MH5::d[5] * pose6DOF.orientation.col(2);

	}

	// Test soft limits at the wrist
	if (!robot_limiter.test(wrist_position, 70.0))
		return false;

	return true;

}


bool MH5Robot::controlMH5Robot() {

	if (enabled) {

		// Compute the next pose toward the goal
		dataMutex.lock();
		Robo::Pose6DOF newCommandedPose6DOF = currentCommandedPose6DOF.interpolate(desiredPose6DOF, desired_translation_speed, desired_rotation_speed, 0, 0, controlThreadTimeInterval);
		dataMutex.unlock();

		// Check that the safety limits are not violated
		if (!testLimits(newCommandedPose6DOF))
			return true;

		// Compute the corresponding joint configuration
		JointConfiguration newCommandedConfiguration;
		if (!computeInverseKinematics(newCommandedPose6DOF, newCommandedConfiguration))
			return false;

		// Lock the communication mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
		QMutexLocker locker(&communicationMutex);

		// Send the joint position to the robot
		if (robot_communicator->sendJointPosition(newCommandedConfiguration, 600, true)) {

			dataMutex.lock();

			// Save the time the last command occurred
			workspaceStateTimer.getTime();

			// Save the current commanded pose/configuration
			lastCommandedPose6DOF = currentCommandedPose6DOF;

			// Save the new commanded pose/configuration
			currentCommandedPose6DOF = newCommandedPose6DOF;
			currentCommandedConfiguration = newCommandedConfiguration;

			// Save the current rates (for use in computing the current workspace state)
			current_translation_speed = desired_translation_speed;
			current_rotation_speed = desired_rotation_speed;

			dataMutex.unlock();

		}

	}

	return true;

}


bool MH5Robot::enable() {

	if (enabled)
		return true;

	// Lock the communication mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
	QMutexLocker locker(&communicationMutex);

	// Make sure the robot is disabled so the "enabled" variable is not out of sync
	if (!robot_communicator->sendServo(false))
		return false;

	dataMutex.lock();
	// Re-initialize the current configuration of the robot
	if (!robot_communicator->recvJointPosition(currentCommandedConfiguration)) {
		std::cerr << "MH5 ROBOT: there was an error obtaining the current joint configuration of the robot arm. ERROR" << std::endl;
		dataMutex.unlock();
		return false;
	}
	computeForwardKinematics(currentCommandedConfiguration, currentCommandedPose6DOF);
	lastCommandedPose6DOF = currentCommandedPose6DOF;
	desiredPose6DOF = currentCommandedPose6DOF;
	dataMutex.unlock();

	// Tell the MH5 to turn on the servo
	if (!robot_communicator->sendServo(true))
		return false;

	enabled = true;

	return true;

}


bool MH5Robot::disable() {

	if (!enabled)
		return true;

	enabled = false;

	Timing::wait(2000 * controlThreadTimeInterval);

	// Lock the communication mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
	QMutexLocker locker(&communicationMutex);

	// Tell the MH5 to turn off the servo
	if (!robot_communicator->sendServo(false))
		return false;

	dataMutex.lock();
	// Re-initialize the current configuration of the robot
	if (!robot_communicator->recvJointPosition(currentCommandedConfiguration)) {
		std::cerr << "MH5 ROBOT: there was an error obtaining the current joint configuration of the robot arm. ERROR" << std::endl;
		dataMutex.unlock();
		return false;
	}
	computeForwardKinematics(currentCommandedConfiguration, currentCommandedPose6DOF);
	lastCommandedPose6DOF = currentCommandedPose6DOF;
	desiredPose6DOF = currentCommandedPose6DOF;
	dataMutex.unlock();

	return true;

}


bool MH5Robot::isEnabled() const {

	return enabled;

}


bool MH5Robot::isDisabled() const {

	return !isEnabled();

}


bool MH5Robot::errorOccurred() const {

	// Lock the communication mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
	QMutexLocker locker(&communicationMutex);

	MH5::RobotStatus status;
	if (robot_communicator->recvStatus(status)) {
		// Bit 4 of the second status byte is "alarm occurring"
		// Bit 5 of the second status byte is "error occurring"
		return (status.s2 & (1 << 4)) || (status.s2 & (1 << 5));
	}

	return false;

}


bool MH5Robot::getWorkspaceState(Robo::Position& position) const {

	Robo::Pose6DOF pose6DOF;
	if (!getWorkspaceState(pose6DOF))
		return false;

	position = pose6DOF.position;

	return true;

}


bool MH5Robot::getWorkspaceState(Robo::Orientation& orientation) const {

	Robo::Pose6DOF pose6DOF;
	if (!getWorkspaceState(pose6DOF))
		return false;

	orientation = pose6DOF.orientation;

	return true;

}


bool MH5Robot::getWorkspaceState(Robo::Pose6DOF& pose6DOF) const {

	// Lock the data mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
	QMutexLocker locker(&dataMutex);

	pose6DOF = lastCommandedPose6DOF.interpolate(currentCommandedPose6DOF, current_translation_speed, current_rotation_speed, 0, 0, workspaceStateTimer.elapsedTime().seconds());

	return true;

}


bool MH5Robot::setWorkspaceState(const Robo::Position& position, RatesAndFailStatus& ratesAndFailStatus) {

	dataMutex.lock();
	Robo::Pose6DOF pose6DOF = {position, desiredPose6DOF};
	dataMutex.unlock();

	// Check that the limits are not missed
	if (!testLimits(pose6DOF)) {
		ratesAndFailStatus.safetyLimitFailure = true;
		return false;
	}

	// Check that the specified rates are not too fast
	if (ratesAndFailStatus.translation_speed > 200.0) {
		ratesAndFailStatus.ratesInvalid = true;
		return false;
	}

	// Lock the data mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
	QMutexLocker locker(&dataMutex);

	// Set the new rates
	desired_translation_speed = ratesAndFailStatus.translation_speed;

	// Set the new desired pose
	desiredPose6DOF = pose6DOF;

	return true;

}


bool MH5Robot::setWorkspaceState(const Robo::Orientation& orientation, RatesAndFailStatus& ratesAndFailStatus) {

	dataMutex.lock();
	Robo::Pose6DOF pose6DOF = {desiredPose6DOF, orientation};
	dataMutex.unlock();

	if (fabs(pose6DOF.orientation.determinant() - 1.0) > 0.001) {
		std::cerr << "MH5 ROBOT: The desired orientation matrix does not have a determinant of one! ERROR." << std::endl;
		return false;
	}

	// Check that the limits are not missed
	if (!testLimits(pose6DOF)) {
		ratesAndFailStatus.safetyLimitFailure = true;
		return false;
	}

	// Check that the specified rates are not too fast
	if (ratesAndFailStatus.rotation_speed > 60.0 * M_PI / 180.0) {
		ratesAndFailStatus.ratesInvalid = true;
		return false;
	}

	// Lock the data mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
	QMutexLocker locker(&dataMutex);

	// Set the new rates
	desired_rotation_speed = ratesAndFailStatus.rotation_speed;

	// Set the new desired pose
	desiredPose6DOF = pose6DOF;

	return true;

}


bool MH5Robot::setWorkspaceState(const Robo::Pose6DOF& pose6DOF, RatesAndFailStatus& ratesAndFailStatus) {
	
	if (fabs(pose6DOF.orientation.determinant() - 1.0) > 0.001) {
		std::cerr << "MH5 ROBOT: The desired orientation matrix does not have a determinant of one! ERROR." << std::endl;
		return false;
	}

	// Check that the limits are not missed
	if (!testLimits(pose6DOF)) {
		ratesAndFailStatus.safetyLimitFailure = true;
		return false;
	}

	// Check that the specified rates are not too fast
	if (ratesAndFailStatus.translation_speed > 200.0 || ratesAndFailStatus.rotation_speed > 60.0 * M_PI / 180.0) {
		ratesAndFailStatus.ratesInvalid = true;
		return false;
	}

	// Lock the data mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
	QMutexLocker locker(&dataMutex);

	// Set the new rates
	desired_translation_speed = ratesAndFailStatus.translation_speed;
	desired_rotation_speed = ratesAndFailStatus.rotation_speed;

	// Set the new desired pose
	desiredPose6DOF = pose6DOF;

	return true;

}


bool MH5Robot::getJointConfiguration(JointConfiguration& joint_configuration) const {

	// Lock the data mutex (it is automatically unlocked when the QMutexLocker is destroyed upon returning from this function)
	QMutexLocker locker(&dataMutex);

	joint_configuration = currentCommandedConfiguration;

	return true;

}


bool MH5Robot::setJointConfiguration(const JointConfiguration& joint_configuration, RatesAndFailStatus& ratesAndFailStatus) {

	Robo::Pose6DOF newPose6DOF;
	computeForwardKinematics(joint_configuration, newPose6DOF);

	return setWorkspaceState(newPose6DOF, ratesAndFailStatus);

}


bool MH5Robot::computeForwardKinematics(const JointConfiguration& joint_configuration, Robo::Pose6DOF& pose6DOF) const {

	// Compute the orientation of all 6 frames expressed in frame 0 (base frame)
	Robo::Matrixd33 joint_transformations[6];
	for (int aa = 0; aa < 6; ++aa) {

		if (aa == 0)
			joint_transformations[aa] = Robo::I33;
		else
			joint_transformations[aa] = joint_transformations[aa - 1];

		joint_transformations[aa] = joint_transformations[aa] * Robo::Rotation(joint_configuration[aa] + MH5::theta_offset[aa], Robo::UnitZ());
		joint_transformations[aa] = joint_transformations[aa] * Robo::Rotation(MH5::alpha[aa], Robo::UnitX());

	}

	// Save the orientation of frame 6 (the last link of the MH5 manipulator)
	pose6DOF.orientation = joint_transformations[5];

	// Compute the position by walking the link inter-origin vectors
	pose6DOF.position = MH5::d[0] * Robo::UnitZ() + MH5::a[0] * joint_transformations[0].col(0);
	for (int aa = 1; aa < 6; ++aa) {
		pose6DOF.position += MH5::d[aa] * joint_transformations[aa - 1].col(2) + MH5::a[aa] * joint_transformations[aa].col(0);
	}

	// Add on the tool transform if applicable
	if (robot_tool) {
		pose6DOF.position += pose6DOF.orientation * robot_tool->getTransform().position;
		pose6DOF.orientation = pose6DOF.orientation * robot_tool->getTransform().orientation;
	}

	return true;

}


bool MH5Robot::computeInverseKinematics(const Robo::Pose6DOF& pose6DOF, JointConfiguration& joint_configuration) const {

	double theta1 = 0.0;
	double theta2 = 0.0;
	double theta3 = 0.0;
	double theta4 = 0.0;
	double theta5 = 0.0;
	double theta6 = 0.0;

	if (fabs(pose6DOF.orientation.determinant() - 1.0) > .001)
		return false;

	Robo::Vector3d tool_with_wrist_offset;
	Robo::Vector3d desired_arm_position;

	// Figure out the necessary position of the portion of the arm before the spherical joint
	if (robot_tool) {
		tool_with_wrist_offset = robot_tool->getTransform().position + MH5::d[5] * Robo::UnitZ();
		desired_arm_position = pose6DOF.position - pose6DOF.orientation * robot_tool->getTransform().orientation.transpose() * tool_with_wrist_offset;
	}
	else {
		desired_arm_position = pose6DOF.position - MH5::d[5] * pose6DOF.orientation.col(2);
	}

	// Compute the rotation around the 1st joint
	theta1 = atan2(desired_arm_position.y(), desired_arm_position.x());

	// Compute the rotation around the 3rd joint
	double x1 = sqrt(desired_arm_position.x() * desired_arm_position.x() + desired_arm_position.y() * desired_arm_position.y()) - MH5::a[0];
	double y1 = desired_arm_position.z();
	double l1 = MH5::a[1];
	double l2 = sqrt(MH5::a[2] * MH5::a[2] + MH5::d[3] * MH5::d[3]);
	double num = pow(l1 + l2, 2.0) - (x1 * x1 + y1 * y1);
	double den = (x1 * x1 + y1 * y1) - pow(l1 - l2, 2.0);
	double theta3_tmp = -2.0 * atan(sqrt(num/den));
	theta3 = M_PI/2.0 - atan(MH5::a[2] / MH5::d[3]) + theta3_tmp;

	// Compute the rotation around the 2nd joint
	theta2 = atan2(y1, x1) - atan2(l2 * sin(theta3_tmp), l1 + l2 * cos(theta3_tmp)) - M_PI/2.0;

	// Convert thetas 1-3 to degrees
	joint_configuration[0] = theta1 * 180.0/M_PI;
	joint_configuration[1] = theta2 * 180.0/M_PI;
	joint_configuration[2] = theta3 * 180.0/M_PI;

	// Compute the orientation of frame 3 (third link) expressed in frame 0 (base)
	Robo::Matrixd33 R_3_to_0 = Robo::I33;
	for (int aa = 0; aa < 3; ++aa) {
		R_3_to_0 = R_3_to_0 * Robo::Rotation(joint_configuration[aa] + MH5::theta_offset[aa], Robo::UnitZ()) * Robo::Rotation(MH5::alpha[aa], Robo::UnitX());
	}

	// Compute the needed orientation of frame 6 expressed in frame 3 (i.e. rotation from frame 3 to frame 6) to achieve the work configuration orientation
	Robo::Matrixd33 R_6_to_3;
	if (robot_tool)
		R_6_to_3 = R_3_to_0.transpose() * pose6DOF.orientation * robot_tool->getTransform().orientation.transpose();
	else
		R_6_to_3 = R_3_to_0.transpose() * pose6DOF.orientation;

	// Compute the rotation around the fourth joint
	theta4 = atan2(R_6_to_3(1, 2), R_6_to_3(0, 2)); // this corresponds to the elbow up position

	// Use the last known configuration to minimize the distance through which theta4 may have to travel
	double prior_theta4 = currentCommandedConfiguration[3] * M_PI/180.0;
	if (fabs(theta4 - prior_theta4) > 5.0 * M_PI/4.0)
		theta4 -= Robo::sign(theta4 - prior_theta4) * 2.0 * M_PI;
	else if (fabs(theta4 - prior_theta4) > 3.0 * M_PI/4.0)
		theta4 -= Robo::sign(theta4 - prior_theta4) * M_PI;

	// Compute the rotation around the fifth and sixth joints
	theta5 = atan2(R_6_to_3(0, 2) * cos(theta4) + R_6_to_3(1, 2) * sin(theta4), R_6_to_3(2, 2));
	theta6 = atan2(-R_6_to_3(0, 0) * sin(theta4) + R_6_to_3(1, 0) * cos(theta4), -R_6_to_3(0, 1) * sin(theta4) + R_6_to_3(1, 1) * cos(theta4));

	// Handle the degenerate cases of the spherical wrist (orientations for which two of the three joint axes are close to parallel)
	if (fabs(theta5) < .001) { // degenerate case
		theta6 = atan2(R_6_to_3(1, 0), R_6_to_3(0, 0)) - theta4;
	}
	else if (fabs(theta5 - M_PI) < .001) { // another degenerate case
		theta4 = -atan2(R_6_to_3(1, 0), R_6_to_3(1, 1));
		theta6 = 0;
	}

	// Use the last known configuration to minimize the distance through which theta6 may have to travel
	double prior_theta6 = currentCommandedConfiguration[5] * M_PI/180.0;
	if (fabs(theta6 - prior_theta6) > 5.0*M_PI/4.0)
		theta6 -= Robo::sign(theta6 - prior_theta6) * 2.0 * M_PI;

	// Convert thetas 4-6 to degrees
	joint_configuration[3] = theta4 * 180.0/M_PI;
	joint_configuration[4] = theta5 * 180.0/M_PI;
	joint_configuration[5] = theta6 * 180.0/M_PI;

	return true;

}


bool MH5Robot::computeJacobian(const JointConfiguration& joint_configuration, Jacobian& jacobian_matrix) const {

	// Initialize the jacobian matrix to zeros
	jacobian_matrix = Jacobian::Zero();

	// First, compute the forward kinematics to get the endpoint position and the z axis for each joint
	// Compute the orientation of all 6 frames expressed in frame 0 (base frame)
	Robo::Matrixd33 joint_transformations[6];
	for (int aa = 0; aa < 6; ++aa) {

		if (aa == 0)
			joint_transformations[aa] = Robo::I33;
		else
			joint_transformations[aa] = joint_transformations[aa - 1];

		joint_transformations[aa] = joint_transformations[aa] * Robo::Rotation(joint_configuration[aa] + MH5::theta_offset[aa], Robo::UnitZ());
		joint_transformations[aa] = joint_transformations[aa] * Robo::Rotation(MH5::alpha[aa], Robo::UnitX());

	}

	// Compute the endpoint position by walking the link inter-origin vectors.
	Robo::Vector3d endpointPosition = MH5::d[0] * Robo::UnitZ() + MH5::a[0] * joint_transformations[0].col(0);
	for (int aa = 1; aa < 6; ++aa) {
		endpointPosition += MH5::d[aa] * joint_transformations[aa - 1].col(2) + MH5::a[aa] * joint_transformations[aa].col(0);
	}

	// Add on the tool transform if applicable
	if (robot_tool) {
		endpointPosition += joint_transformations[5] * robot_tool->getTransform().position;
	}

	// Now form the jacobian
	jacobian_matrix.block(0, 0, 3, 1) = Robo::UnitZ().cross(endpointPosition / 1000.0);
	jacobian_matrix.block(3, 0, 3, 1) = Robo::UnitZ();

	Robo::Vector3d current_position = MH5::d[0] * Robo::UnitZ() + MH5::a[0] * joint_transformations[0].col(0);
	for (int aa = 1; aa < 6; ++aa) {

		jacobian_matrix.block(0, aa, 3, 1) = joint_transformations[aa - 1].col(2).cross((endpointPosition - current_position) / 1000.0);
		jacobian_matrix.block(3, aa, 3, 1) = joint_transformations[aa - 1].col(2);

		current_position += MH5::d[aa] * joint_transformations[aa - 1].col(2) + MH5::a[aa] * joint_transformations[aa].col(0);

	}

	return true;

}
