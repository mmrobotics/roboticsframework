#ifndef MH5_COMMUNICATOR_H
#define MH5_COMMUNICATOR_H

#include <string>
#include <Eigen/Dense>
#include <Robots/Motoman/MH5.h>


/**
 * @brief The MH5Communicator class is used to connect to and communicate with the DX100 controller
 * that controls the MH5 Yaskawa Motoman robot arm. It is used by MH5Robot and should not be used
 * directly.
 *
 * This class sends and receives commands to the DX100 controller which controls the MH5 robot. These
 * commands can be requests to get the robot status and state as well as commands to control its
 * movement and position. None of the functions of this class are thread-safe. It is the responsibility
 * of the user to make sure the functions are not called by multiple threads simultaneously. The choice
 * to omit thread-safety from the functions of this class is due to the fact that these functions
 * frequently call each other and many of them will call themselves recursively. Therefore, punting
 * thread safety to the user avoids the need to use an expensive recursive mutex.
 *
 * The DX100 will time out the server connection after 30 seconds of inactivity. The functions of this
 * class will automatically recognize this situation and reestablish the connection.
 */
class MH5Communicator {

	private:
		bool connected;
		std::string ip_address;
		int socket_fd;

		bool checkConnectionLost(ssize_t sendRecvResult, bool retry);
		bool checkForErrorResponse(char* recv_buffer);

	public:
		using JointConfiguration = Eigen::Matrix<double, 6, 1, Eigen::DontAlign>;
		MH5Communicator();
		MH5Communicator(const char* ip);
		~MH5Communicator();

		bool connectToController(const char* ip);
		bool establishAccess();
		bool closeConnection();
		bool isConnected() const;

		bool recvAlarm(MH5::RobotAlarm& alarm, bool retry = true);
		bool recvStatus(MH5::RobotStatus& status, bool retry = true);

		bool sendServo(bool on, bool retry = true);
		bool queryServoStatus(bool& servo_status, bool retry = true);

		bool recvJointPosition(JointConfiguration& jpos, bool retry = true);
		bool sendJointPosition(const JointConfiguration& jpos, float speed, bool linear_interpolation = false, bool retry = true);

		bool wait();

};

#endif
