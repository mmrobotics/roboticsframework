#include "MH5Communicator.h"
#include <iostream>
#include <cstdio>
#include <cstring>

// Linux specific headers
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

#define BUFFER_LENGTH 512


/**
 * @brief Constructs an MH5Communicator object which is used to control the MH5 Yaskawa Motoman.
 * The object is not connected initially.
 */
MH5Communicator::MH5Communicator() : connected(false) {}


/**
 * @brief Constructs an MH5Communicator object used to control the MH5 Yaskawa Motoman and
 * connects to it using the IP address specified and port 80. After this constructor, it is
 * not necessary to call connectToController().
 * @param ip IP address of the MH5 robot
 */
MH5Communicator::MH5Communicator(const char* ip) : connected(false) {

	connectToController(ip);

}


/**
 * @brief Closes the connection with the MH5 robot and destroys the MH5Communicator object.
 */
MH5Communicator::~MH5Communicator() {

	closeConnection();

}


/**
 * @brief Connects to the DX100 controller which controls the MH5 Yaskawa Motoman. If a connection
 * is already active, this function does nothing and the connection remains active even if the
 * specified IP address is different from the currently active connection.
 * @param ip IP address of the MH5 robot
 * @return True if the connection was successful
 */
bool MH5Communicator::connectToController(const char* ip) {

	if (connected)
		return true;

	std::cout << "MH5_ROBOT: Connecting to MH5 Robot..." << std::endl;

	// Create a socket
	if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		cerr << "MH5_ROBOT: Could NOT create a TCP/IP socket." << endl;
		connected = false;
		return false;
	}

	struct sockaddr_in serv_addr;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(uint16_t(80));
	
	memset(&(serv_addr.sin_zero), 0, 8);
	serv_addr.sin_addr.s_addr = inet_addr(ip);

	if (connect(socket_fd, reinterpret_cast<struct sockaddr*>(&serv_addr), sizeof(serv_addr)) < 0) {
		cerr << "MH5_ROBOT: Could not connect to " << ip << ":80" << endl;
		connected = false;
		return false;
	}

	cout << "MH5_ROBOT: Socket opened, connected to " << ip << ":80" << endl;

	connected = true;
	ip_address = ip;

	return true;

}


/**
 * @brief Sets up the DX100 to accept infinite commands. This sends the START Request as explained in the DX100 Ethernet
 * Server manual. This function should be called after each time connectToController() is called (e.g. if the DX100 closed
 * the socket connection due to time-out of 30 seconds).
 * @return True if successful
 */
bool MH5Communicator::establishAccess() {

	if (!connected)
		return false;
	
	if (checkConnectionLost(send(socket_fd, MH5::ROBOT_ACCESS_REQST, strlen(MH5::ROBOT_ACCESS_REQST), 0), false))
		return false;
	
	char recv_buffer[BUFFER_LENGTH] = {0}; // Initialize the entire array to zero
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), false))
		return false;
	
	if (checkForErrorResponse(recv_buffer))
		return false;

	cout << "MH5_ROBOT: Established access" << endl;
	
	return true;

}


/**
 * @brief Closes the connection to the Motoman DX100 controller.
 * @return
 */
bool MH5Communicator::closeConnection() {

	close(socket_fd);
	connected = false;
	cout << "MH5_ROBOT: Connection closed." << endl;

	return true;

}


/**
 * @brief Returns whether the MH5Communicator is connected.
 * @return True if connected
 */
bool MH5Communicator::isConnected() const {

	return connected;

}



/**
 * @brief Gets the current status of the alarms and errors from the MH5 robot.
 * @param alarm Structure in which the alarms are placed as output. This value is invalid
 * if the return value of the function is `false`.
 * @param retry If true, then if the connection has been lost, a new connection and a second
 * attempt will be made
 * @return True if successful
 */
bool MH5Communicator::recvAlarm(MH5::RobotAlarm& alarm, bool retry) {

	// Check for connection
	if (!connected)
		return false;
	
	// Send the command that requests the alarm status
	if (checkConnectionLost(send(socket_fd, MH5::ROBOT_CMD_GET_ALARM, strlen(MH5::ROBOT_CMD_GET_ALARM), 0), retry))
		return retry ? recvAlarm(alarm, false) : false;
	
	// Read the command response
	char recv_buffer[BUFFER_LENGTH] = {0}; // Initialize the entire array to zero
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? recvAlarm(alarm, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	// Read the requested data
	memset(recv_buffer, 0, BUFFER_LENGTH);
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? recvAlarm(alarm, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	int ints_found = sscanf(recv_buffer, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\r",
							&(alarm.e1c), &(alarm.e1d), &(alarm.a1c), &(alarm.a1d), &(alarm.a2c),
							&(alarm.a2d), &(alarm.a3c), &(alarm.a3d), &(alarm.a4c), &(alarm.a4d));

	// Make sure the requested data was complete
	if (ints_found < 10) {
		cerr << "MH5_ROBOT: an error occurred while parsing alarm data. ERROR" << endl;
		cerr << "  ** the message was: " << recv_buffer << " ** " << endl;
		return false;
	}

	return true;

}


/**
 * @brief Gets the status data from the MH5 robot.
 * @param status Structure in which the status is placed as output. This value is invalid if
 * the return value of the function is false.
 * @param retry If true, then if the connection has been lost, a new connection and a second
 * attempt will be made
 * @return True if successful
 */
bool MH5Communicator::recvStatus(MH5::RobotStatus& status, bool retry) {

	// Check for connection
	if (!connected)
		return false;

	// Send the command that requests the MH5 status
	if (checkConnectionLost(send(socket_fd, MH5::ROBOT_CMD_GET_STATUS, strlen(MH5::ROBOT_CMD_GET_STATUS), 0), retry))
		return retry ? recvStatus(status, false) : false;

	// Read the command response
	char recv_buffer[BUFFER_LENGTH] = {0}; // Initialize the entire array to zero
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? recvStatus(status, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	// Read the requested data
	memset(recv_buffer, 0, BUFFER_LENGTH);
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? recvStatus(status, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	int ints_found = sscanf(recv_buffer, "%d, %d\r", &(status.s1), &(status.s2));

	// Make sure the requested data was complete
	if (ints_found < 2) {
		cerr << "MH5_ROBOT: an error occurred while parsing status data. ERROR" << endl;
		cerr << "  ** the message was: " << recv_buffer << " ** " << endl;
		return false;
	}

	return true;

}


/**
 * @brief Turn the servo power on/off.
 * @param on If true, turn the servo power on, else turn if off
 * @param retry If true, then if the connection has been lost, a new connection and a second
 * attempt will be made
 * @return True if successful
 */
bool MH5Communicator::sendServo(bool on, bool retry) {

	// Check for connection
	if (!connected)
		return false;

	if (checkConnectionLost(send(socket_fd, MH5::ROBOT_CMD_SET_SERVO, strlen(MH5::ROBOT_CMD_SET_SERVO), 0), retry))
		return retry ? sendServo(on, false) : false;

	char recv_buffer[BUFFER_LENGTH] = {0}; // Initialize the entire array to zero
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? sendServo(on, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	char data_buffer[BUFFER_LENGTH] = {0}; // Initialize the entire array to zero
	int data_length = snprintf(data_buffer, BUFFER_LENGTH, "%d\r", static_cast<int>(on));

	if (checkConnectionLost(send(socket_fd, data_buffer, static_cast<size_t>(data_length), 0), retry))
		return retry ? sendServo(on, false) : false;

	memset(recv_buffer, 0, BUFFER_LENGTH);
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? sendServo(on, false) : false;

	// Check for an error response
	if (checkForErrorResponse(recv_buffer))
		return false;

	return true;

}


/**
 * @brief Queries the MH5 robot's status and then determines whether or not the servo has been turned on.
 * @param servo_status Variable in which the status is returned. True if the servo have been turned on.
 * This value is invalid if the return value of the function is false.
 * @param retry If true, then if the connection has been lost, a new connection and a second
 * attempt will be made
 * @return True if the status was successfully obtained
 */
bool MH5Communicator::queryServoStatus(bool& servo_status, bool retry) {

	MH5::RobotStatus status;
	if (recvStatus(status, retry)) {
		servo_status = status.s2 & (1 << 6); // Check the "servo on" status bit
		return true;
	}
	
	return false;

}


/**
 * @brief Returns the MH5 robot joint positions in degrees.
 * @param jpos Structure in which the joint positions are returned. This value is invalid if
 * the return value of the function is false.
 * @param retry If true, then if the connection has been lost, a new connection and a second
 * attempt will be made
 * @return True if successful
 */
bool MH5Communicator::recvJointPosition(JointConfiguration& jpos, bool retry) {

	if (!connected)
		return false;
	
	if (checkConnectionLost(send(socket_fd, MH5::ROBOT_CMD_GET_POSJ, strlen(MH5::ROBOT_CMD_GET_POSJ), 0), retry))
		return retry ? recvJointPosition(jpos, false) : false;

	char recv_buffer[BUFFER_LENGTH] = {0}; // Initialize the entire array to zero
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? recvJointPosition(jpos, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	memset(recv_buffer, 0, BUFFER_LENGTH);
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? recvJointPosition(jpos, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	int floats_found = sscanf(recv_buffer, "%lf, %lf, %lf, %lf, %lf, %lf", &(jpos[0]), &(jpos[1]), &(jpos[2]), &(jpos[3]), &(jpos[4]), &(jpos[5]));

	// Check to see if the message is good.
	if (floats_found < 6) {
		cerr << "MH5_ROBOT: an error occurred while parsing the joint space position. ERROR" << endl;
		cerr << "  ** the message was: " << recv_buffer << " ** " << endl;
		return false;
	}
	
	jpos[0] *= MH5::DegResolutionJ1;
	jpos[1] *= MH5::DegResolutionJ2;
	jpos[2] *= MH5::DegResolutionJ3;
	jpos[3] *= MH5::DegResolutionJ4;
	jpos[4] *= MH5::DegResolutionJ5;
	jpos[5] *= MH5::DegResolutionJ6;

	return true;

}


/**
 * @brief Commands the MH5 robot to move to a position in joint space. The input joint positions should
 * be in degrees.
 * @param jpos Commanded joint positions for the MH5 robot in degrees.
 * @param speed The speed at which the robot should move to the new location
 * @param linear_interpolation Whether to use linear interpolation or joint interpolation (`true` means use
 * linear interpolation)
 * @param retry If true, then if the connection has been lost, a new connection and a second
 * attempt will be made
 * @return True if successful
 */
bool MH5Communicator::sendJointPosition(const JointConfiguration& jpos, float speed, bool linear_interpolation, bool retry) {

	if (!connected)
		return false;

	float s = jpos[0] / MH5::DegResolutionJ1;
	float l = jpos[1] / MH5::DegResolutionJ2;
	float u = jpos[2] / MH5::DegResolutionJ3;
	float r = jpos[3] / MH5::DegResolutionJ4;
	float b = jpos[4] / MH5::DegResolutionJ5;
	float t = jpos[5] / MH5::DegResolutionJ6;

	// Form the command and data buffers
	int command_length, data_length;
	char command_buffer[BUFFER_LENGTH] = {0};	// Initialize the entire array to zero
	char data_buffer[BUFFER_LENGTH] = {0};		// Initialize the entire array to zero
	if (linear_interpolation) { // The interpolation happens in the work space

		data_length = snprintf(data_buffer, BUFFER_LENGTH, "0,%f,%f,%f,%f,%f,%f,%f,0,0,0,0,0,0,0\r",
							   static_cast<double>(speed),
							   static_cast<double>(s),
							   static_cast<double>(l),
							   static_cast<double>(u),
							   static_cast<double>(r),
							   static_cast<double>(b),
							   static_cast<double>(t));

		command_length = snprintf(command_buffer, BUFFER_LENGTH, "%s %d\r\n", MH5::ROBOT_CMD_SET_POSJ_WORK, data_length);

	}
	else { // The interpolation happens in the joint space

		data_length = snprintf(data_buffer, BUFFER_LENGTH, "%f,%f,%f,%f,%f,%f,%f,0,0,0,0,0,0,0\r",
							   static_cast<double>(speed),
							   static_cast<double>(s),
							   static_cast<double>(l),
							   static_cast<double>(u),
							   static_cast<double>(r),
							   static_cast<double>(b),
							   static_cast<double>(t));

		command_length = snprintf(command_buffer, BUFFER_LENGTH, "%s %d\r\n", MH5::ROBOT_CMD_SET_POSJ_JOINT, data_length);

	}

	if (checkConnectionLost(send(socket_fd, command_buffer, static_cast<size_t>(command_length), 0), retry))
		return retry ? sendJointPosition(jpos, false) : false;
	
	char recv_buffer[BUFFER_LENGTH] = {0}; // Initialize the entire array to zero
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? sendJointPosition(jpos, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	if (checkConnectionLost(send(socket_fd, data_buffer, static_cast<size_t>(data_length), 0), retry))
		return retry ? sendJointPosition(jpos, false) : false;

	memset(recv_buffer, 0, BUFFER_LENGTH);
	if (checkConnectionLost(recv(socket_fd, recv_buffer, BUFFER_LENGTH, 0), retry))
		return retry ? sendJointPosition(jpos, false) : false;

	if (checkForErrorResponse(recv_buffer))
		return false;

	return true;

}


/**
 * @brief Block until the robot reports that the command has been completed or an error happens.
 * In other words, this function will not return until the MH5 robot has reported the command as
 * completed or reports an error.
 * @return True if command was successful, false otherwise. This function also returns false if
 * the there is not an active connection to the MH5 robot.
 */
bool MH5Communicator::wait() {

	if (!connected)
		return false;

	bool success = false;
	MH5::RobotStatus status;
	while (recvStatus(status)) {

		// Check that the running status bit is off
		if (!(status.s1 & (1 << 3))) {
			success = true;
			break;
		}

		// Check the "alarm occurring" status bit
		if (status.s2 & (1 << 4))
			break;

		// Check the "error occurring" status bit
		if (status.s2 & (1 << 5))
			break;

	}

	return success;

}


/**
 * @brief This function checks the return value of the `send()` or `recv()` functions for
 * a value that indicates the connection is lost (a zero). If the `retry` paramater is
 * true, it tells the function to reestablish a connection so the caller can try to
 * communicate again. This function returns `true` if the connection was lost (even if
 * it was reestablished) and `false` otherwise.
 * @param sendRecvResult Return value of `send()` or `recv()`
 * @param retry Whether to reestablish the connection if it has been lost
 * @return
 */
bool MH5Communicator::checkConnectionLost(ssize_t sendRecvResult, bool retry) {

	if (sendRecvResult <= 0) {

		if (sendRecvResult == 0)
			cerr << "MH5_ROBOT: lost connection to " << ip_address << ":80 ERROR" << endl;
		else
			cerr << "MH5_ROBOT: A Linux sockets error occurred when trying to communicate with the DX100 (Linux socket error code: " << sendRecvResult << ") ERROR" << endl;

		connected = false;

		if (retry) {

			// Communication has been lost, reestablish the connection because we intend to retry the action
			cerr << "MH5_ROBOT: attempting to retry." << endl;
			connectToController(ip_address.c_str());
			establishAccess();

		}

		return true;

	}

	return false;

}


/**
 * @brief Parses the receive buffer for an error response. If an error was received, this
 * function returns `true` and prints information about the error to the console, otherwise
 * this function returns `false`.
 * @param recv_buffer The receive buffer
 * @return True if an error has been received
 */
bool MH5Communicator::checkForErrorResponse(char* recv_buffer) {

	if (strstr(recv_buffer, "ERROR:") || strstr(recv_buffer, "NG:")) {

		char* errorString = strtok(recv_buffer, "\r");
		std::cerr << "MH5_ROBOT: Couldn't complete command, received message from DX100 \"" << errorString << "\" ERROR" << std::endl;

		// Check code indicating the pendant emergency stop is pressed
		if (strstr(recv_buffer, "3450"))
			std::cerr << "    ** Is the pendant emergency stop button pressed? **" << std::endl;

		// Check code indicating the pendant is not in remote mode
		else if (strstr(recv_buffer, "2100"))
			std::cerr << "    ** Turn the key on the pendant to remote mode. **" << std::endl;

		// Check code indicating the servo power is not on
		else if (strstr(recv_buffer, "2070"))
			std::cerr << "    ** Make sure that the servo power is turned on. **" << std::endl;

		else
			std::cerr << "    ** See Interpreter Message List in DX100 Options Instructions for Data Transmission Function (section 5.5). **" << std::endl;

		return true;

	}

	return false;

}
