#ifndef MH5_ROBOT_H
#define MH5_ROBOT_H

#include <Robots/SerialLinkRobot.h>
#include <Robots/ToolableRobot.h>
#include <Robots/Tool.h>
#include <Robots/RobotSafetyLimits.h>
#include <Robots/Motoman/MH5Communicator.h>
#include <Robots/Motoman/MH5.h> // the D-H parameters are stored in here
#include <RoboticsToolbox.h>
#include <Utilities/Timing.h>
#include <QMutex>


/**
 * @brief The MH5Robot class is the implementation of the MH5 Yaskawa Motoman robot. It is the primary
 * interface for commanding the robot.
 *
 * Communication with the MH5 robot involves an Ethernet connection to the DX100 controller box underneath
 * the robot arm. This communication channel is absurdly slow, therefore, this class implements an
 * abstraction that is meant to hide the latency involved with the robot communication. This class runs a
 * thread that continuously commands the robot to the desired pose specified by setWorkspaceState() or
 * setJointConfiguration(). If you ask for the current state with getWorkspaceState() an internal model
 * of the robot will estimate the current state of the robot because it takes too long to ask the robot
 * directly. This estimation should be accurate to within a couple millimeters. If you instead use
 * getJointConfiguration(), this is only updated at the internal control rate (every 70 ms). Therefore,
 * getWorkspaceState() will often be more accurate than getJointConfiguration().
 *
 * The implementation only allows for setting position and orientation. The maximum translational rate
 * is 200 mm/s and the maximum angular rate is 60 deg/s. It has been observed that the time to communicate
 * with the robot depends on the rate of motion for the command. Therefore, the maximum rates have been
 * chosen to loosely balance the desire for a high control frequency and high maximum motion rates.
 */
class MH5Robot : public SerialLinkRobot<6>, public ToolableRobot {

	Q_OBJECT

	private:
		MH5Communicator* robot_communicator;
		bool enabled = false;

		Timing::Timer mh5ControlThread;
		const double controlThreadTimeInterval = 0.070;	// seconds
		double desired_translation_speed = 0, current_translation_speed = 0;	// mm/s
		double desired_rotation_speed = 0, current_rotation_speed = 0;			// rad/s
		Robo::Pose6DOF desiredPose6DOF;
		JointConfiguration currentCommandedConfiguration;
		Robo::Pose6DOF currentCommandedPose6DOF, lastCommandedPose6DOF;
		Timing::Timer workspaceStateTimer;
		mutable QMutex communicationMutex;
		mutable QMutex dataMutex;

		bool testLimits(const Robo::Pose6DOF& pose6DOF);
		bool controlMH5Robot();

	public:
		MH5Robot(RobotLimiter& limiter, Tool* tool = nullptr);
		~MH5Robot();

		bool enable() override;
		bool disable() override;

		bool isEnabled() const override;
		bool isDisabled() const override;
		bool errorOccurred() const override;

		bool getWorkspaceState(Robo::Position& position) const override;
		bool getWorkspaceState(Robo::Orientation& orientation) const override;
		bool getWorkspaceState(Robo::Pose6DOF& pose6DOF) const override;

		bool setWorkspaceState(const Robo::Position& position, RatesAndFailStatus& ratesAndFailStatus) override;
		bool setWorkspaceState(const Robo::Orientation& orientation, RatesAndFailStatus& ratesAndFailStatus) override;
		bool setWorkspaceState(const Robo::Pose6DOF& pose6DOF, RatesAndFailStatus& ratesAndFailStatus) override;

		bool getJointConfiguration(JointConfiguration& joint_configuration) const override;
		bool setJointConfiguration(const JointConfiguration& joint_configuration, RatesAndFailStatus& ratesAndFailStatus) override;

		bool computeForwardKinematics(const JointConfiguration& joint_configuration, Robo::Pose6DOF& pose6DOF) const final;
		bool computeInverseKinematics(const Robo::Pose6DOF& pose6DOF, JointConfiguration& joint_configuration) const final;
		bool computeJacobian(const JointConfiguration& joint_configuration, Jacobian& jacobian) const final;

};

#endif // MH5_ROBOT_H
