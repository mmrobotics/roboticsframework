#ifndef SERIAL_LINK_ROBOT_H
#define SERIAL_LINK_ROBOT_H

#include <Eigen/Dense>
#include <Robots/WorkspaceControllableRobot.h>


/**
 * @brief The SerialLinkRobot class represents a serial link robot. The number of joints in the robot is specified by
 * the template parameter `numJoints`. The robot must have at least one joint.
 *
 * The SerialLinkRobot class is a subclass of WorkspaceControllableRobot which provides an additional interface for
 * computing the forward and inverse kinematics of the robot as well as the manipulator Jacobian. The interface also
 * includes functions for controlling the robot by directly specifying the desired joint configuration. Currently
 * controlling the robot by setting the joint configuration still performs linear interpolation in the workspace.
 * Future work on the framework could allow for another function that performs joint space interpolation.
 *
 * <b>IMPLEMENTATION NOTE:</b>  It is the responsibility of the implementer to make sure that the description of
 * this class and each of its functions are met. The functions of this class must be thread-safe.
 *
 * @tparam numJoints The number of serial joints in the robot. Must be larger than 0.
 */
template<int numJoints>
class SerialLinkRobot : public WorkspaceControllableRobot {

	static_assert(numJoints > 0, "The SerialLinkRobot class requires that the number of joints be greater than 0.");

	public:
		/**
		 * @brief The JointConfiguration type represents the joint configuration of the SerialLinkRobot. It is a
		 * column vector. For prismatic joints, the units are in millimeters. For revolute joints the units are
		 * in degrees.
		 */
		using JointConfiguration = Eigen::Matrix<double, numJoints, 1, Eigen::DontAlign>;
		/**
		 * @brief The Jacobian type represents the robot manipulator Jacobian that maps joint velocities (mm/s for
		 * prismatic and deg/s for revolute) to end effector pose velocities.
		 */
		using Jacobian = Eigen::Matrix<double, 6, numJoints, Eigen::DontAlign>;


		SerialLinkRobot(RobotLimiter& limiter, double jogSpeed, double stepMax) : WorkspaceControllableRobot(limiter, jogSpeed, stepMax) { setName("Serial Link Robot"); }

		/**
		 * @brief Gets the current joint configuration of the robot. For prismatic joints, the units are in
		 * millimeters. For revolute joints the units are in degrees.
		 * @param joint_configuration The variable into which the current joint configuration is placed. If this
		 * function returns false, then this variable is not valid.
		 * @return True if successful
		 */
		virtual bool getJointConfiguration(JointConfiguration& joint_configuration) const  = 0;

		/**
		 * @brief Sets the desired joint configuration of the robot. For prismatic joints, the units are in
		 * millimeters. For revolute joints the units are in degrees. This function is the same as calling
		 * setWorkspaceState() but the 6 DOF pose is specified with the joint configuration. In other words,
		 * the provided joint configuration will be passed through forward kinematics and then setWorkspaceState()
		 * will be called on the resulting Robo::Pose6DOF.
		 * @param joint_configuration The desired joint configuration of the robot
		 * @param ratesAndFailStatus Struct used to specify linear interpolation rates (in workspace) and receive
		 * failure status
		 * @return True if successful
		 */
		virtual bool setJointConfiguration(const JointConfiguration& joint_configuration, RatesAndFailStatus& ratesAndFailStatus) = 0;

		/**
		 * @brief Computes the forward kinematics (i.e. workspace frame pose) for the robot given the joint configuration.
		 * @param joint_configuration Joint configuration. For prismatic joints, the units are in millimeters. For revolute
		 * joints the units are in degrees.
		 * @param pose6DOF The workspace frame pose of the end effector. Position is in millimeters.
		 * @return True if successful
		 */
		virtual bool computeForwardKinematics(const JointConfiguration& joint_configuration, Robo::Pose6DOF& pose6DOF) const = 0;

		/**
		 * @brief Computes the inverse kinematics (i.e. joint configuration) for the robot given the workspace frame pose.
		 * The robot may use knowledge about the current joint configuration to decide on a final joint configuration when
		 * multiple possible configurations exist that satisfy the provided workspace frame pose. If the robot has fewer
		 * than 6 joints, uncontrollable portions of the specified pose will be ignored.
		 * @param pose6DOF The workspace frame pose of the end effector. Position is in millimeters.
		 * @param joint_configuration Joint configuration. For prismatic joints, the units are in millimeters. For revolute
		 * joints the units are in degrees.
		 * @return True if successful
		 */
		virtual bool computeInverseKinematics(const Robo::Pose6DOF& pose6DOF, JointConfiguration& joint_configuration) const = 0;

		/**
		 * @brief Computes the manipulator Jacobian for the robot that maps joint velocities (mm/s for prismatic and deg/s for
		 * revolute) to end effector pose velocities. The Jacobian always has 6 rows. If the robot has fewer than 6 joints, the
		 * Jacobian will be built such that the uncontrollable directions will always result in zero no matter what joint
		 * velocities are used.
		 * @param joint_configuration Joint configuration at which to evaluate the Jacobian. For prismatic joints, the units
		 * are in millimeters. For revolute joints the units are in degrees.
		 * @param jacobian The resultant manipulator Jacobian
		 * @return True if successful
		 */
		virtual bool computeJacobian(const JointConfiguration& joint_configuration, Jacobian& jacobian) const = 0;

};

#endif // SERIAL_LINK_ROBOT_H
