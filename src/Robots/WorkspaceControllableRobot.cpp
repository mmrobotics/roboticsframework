#include "WorkspaceControllableRobot.h"
#include <iostream>
#include <QtWidgets>
#include "QtGUIElements/Buttons.h"
#include "QtGUIElements/Labels.h"


void WorkspaceControllableRobot::getWorkspaceStateNotImplemented(const char* type) const {

	std::cerr << "WORKSPACE_CONTROLLABLE_ROBOT: The getWorkspaceState(" << type << "&) function is not implemented for this particular type of robot! WARNING" << std::endl;

}

void WorkspaceControllableRobot::setWorkspaceStateNotImplemented(const char* type) const {

	std::cerr << "WORKSPACE_CONTROLLABLE_ROBOT: The setWorkspaceState(const " << type << "&) function is not implemented for this particular type of robot! WARNING" << std::endl;

}

/**
 * @brief Sets the `position` parameter to the current position of the robot. If this does not make sense for the
 * particular type of robot, this function will simply print an error message to the console.
 * @param position The variable into which the requested state is placed
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::getWorkspaceState(Robo::Position& position) const {

	Q_UNUSED(position)

	getWorkspaceStateNotImplemented("Robo::Position");

	return false;

}

/**
 * @brief Sets the `heading` parameter to the current heading of the robot. If this does not make sense for the
 * particular type of robot, this function will simply print an error message to the console.
 * @param heading The variable into which the requested state is placed
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::getWorkspaceState(Robo::Heading& heading) const {

	Q_UNUSED(heading)

	getWorkspaceStateNotImplemented("Robo::Heading");

	return false;

}

/**
 * @brief Sets the `orientation` parameter to the current orientation of the robot. If this does not make sense
 * for the particular type of robot, this function will simply print an error message to the console.
 * @param orientation The variable into which the requested state is placed
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::getWorkspaceState(Robo::Orientation& orientation) const {

	Q_UNUSED(orientation)

	getWorkspaceStateNotImplemented("Robo::Orientation");

	return false;

}

/**
 * @brief Sets the `angularVelocity` parameter to the current angular velocity of the robot. If this does not make
 * sense for the particular type of robot, this function will simply print an error message to the console.
 * @param angularVelocity The variable into which the requested state is placed
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::getWorkspaceState(Robo::AngularVelocity& angularVelocity) const {

	Q_UNUSED(angularVelocity)

	getWorkspaceStateNotImplemented("Robo::AngularVelocity");

	return false;

}

/**
 * @brief Sets the `pose5DOF` parameter to the current 5 DOF pose of the robot. If this does not make sense for the
 * particular type of robot, this function will simply print an error message to the console.
 * @param pose5DOF The variable into which the requested state is placed
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::getWorkspaceState(Robo::Pose5DOF& pose5DOF) const {

	Q_UNUSED(pose5DOF)

	getWorkspaceStateNotImplemented("Robo::Pose5DOF");

	return false;

}

/**
 * @brief Sets the `pose6DOF` parameter to the current 6 DOF pose of the robot. If this does not make sense for the
 * particular type of robot, this function will simply print an error message to the console.
 * @param pose6DOF The variable into which the requested state is placed
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::getWorkspaceState(Robo::Pose6DOF& pose6DOF) const {

	Q_UNUSED(pose6DOF)

	getWorkspaceStateNotImplemented("Robo::Pose6DOF");

	return false;

}

/**
 * @brief Sets the `rotatingPosition` parameter to the current rotating position of the robot. If this does not make
 * sense for the particular type of robot, this function will simply print an error message to the console.
 * @param rotatingPosition The variable into which the requested state is placed
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::getWorkspaceState(Robo::RotatingPosition& rotatingPosition) const {

	Q_UNUSED(rotatingPosition)

	getWorkspaceStateNotImplemented("Robo::RotatingPosition");

	return false;

}

/**
 * @brief Sets the `rotatingPose5DOF` parameter to the current rotating 5 DOF pose of the robot. If this does not
 * make sense for the particular type of robot, this function will simply print an error message to the console.
 * @param rotatingPose5DOF The variable into which the requested state is placed
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::getWorkspaceState(Robo::RotatingPose5DOF& rotatingPose5DOF) const {

	Q_UNUSED(rotatingPose5DOF)

	getWorkspaceStateNotImplemented("Robo::RotatingPose5DOF");

	return false;

}

/**
 * @brief Sets the desired position of the robot. If this function returns `false`, check the RatesAndFailStatus parameter
 * to determine if this failure occurred due to workspace safety limits being violated or invalid rates. This function uses
 * the `translation_speed` member of RatesAndFailStatus.
 * @param position Desired position of the robot
 * @param ratesAndFailStatus Struct used to specify interpolation rates and receive failure status
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::setWorkspaceState(const Robo::Position& position, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(position)
	Q_UNUSED(ratesAndFailStatus)

	setWorkspaceStateNotImplemented("Robo::Position");

	return false;

}

/**
 * @brief Sets the desired heading of the robot. If this function returns `false`, check the RatesAndFailStatus parameter
 * to determine if this failure occurred due to workspace safety limits being violated or invalid rates. This function uses
 * the `rotation_speed` member of RatesAndFailStatus.
 * @param heading Desired heading of the robot
 * @param ratesAndFailStatus Struct used to specify interpolation rates and receive failure status
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::setWorkspaceState(const Robo::Heading& heading, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(heading)
	Q_UNUSED(ratesAndFailStatus)

	setWorkspaceStateNotImplemented("Robo::Heading");

	return false;

}

/**
 * @brief Sets the desired orientation of the robot. If this function returns `false`, check the RatesAndFailStatus parameter
 * to determine if this failure occurred due to workspace safety limits being violated or invalid rates. This function uses
 * the `rotation_speed` member of RatesAndFailStatus.
 * @param orientation Desired orientation of the robot
 * @param ratesAndFailStatus Struct used to specify interpolation rates and receive failure status
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::setWorkspaceState(const Robo::Orientation& orientation, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(orientation)
	Q_UNUSED(ratesAndFailStatus)

	setWorkspaceStateNotImplemented("Robo::Orientation");

	return false;

}

/**
 * @brief Sets the desired angular velocity of the robot. If this function returns `false`, check the RatesAndFailStatus parameter
 * to determine if this failure occurred due to workspace safety limits being violated or invalid rates. This function uses
 * the `angular_acceleration` member of RatesAndFailStatus.
 * @param angularVelocity Desired angular velocity of the robot
 * @param ratesAndFailStatus Struct used to specify interpolation rates and receive failure status
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::setWorkspaceState(const Robo::AngularVelocity& angularVelocity, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(angularVelocity)
	Q_UNUSED(ratesAndFailStatus)

	setWorkspaceStateNotImplemented("Robo::AngularVelocity");

	return false;

}

/**
 * @brief Sets the desired 5 DOF pose of the robot. If this function returns `false`, check the RatesAndFailStatus parameter
 * to determine if this failure occurred due to workspace safety limits being violated or invalid rates. This function uses
 * the `translation_speed` and `rotation_speed` members of RatesAndFailStatus.
 * @param pose5DOF Desired 5 DOF pose of the robot
 * @param ratesAndFailStatus Struct used to specify interpolation rates and receive failure status
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::setWorkspaceState(const Robo::Pose5DOF& pose5DOF, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(pose5DOF)
	Q_UNUSED(ratesAndFailStatus)

	setWorkspaceStateNotImplemented("Robo::Pose5DOF");

	return false;

}

/**
 * @brief Sets the desired 6 DOF pose of the robot. If this function returns `false`, check the RatesAndFailStatus parameter
 * to determine if this failure occurred due to workspace safety limits being violated or invalid rates. This function uses
 * the `translation_speed` and `rotation_speed` members of RatesAndFailStatus.
 * @param pose6DOF Desired 6 DOF pose of the robot
 * @param ratesAndFailStatus Struct used to specify interpolation rates and receive failure status
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::setWorkspaceState(const Robo::Pose6DOF& pose6DOF, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(pose6DOF)
	Q_UNUSED(ratesAndFailStatus)

	setWorkspaceStateNotImplemented("Robo::Pose6DOF");

	return false;

}

/**
 * @brief Sets the desired rotating position of the robot. If this function returns `false`, check the RatesAndFailStatus parameter
 * to determine if this failure occurred due to workspace safety limits being violated or invalid rates. This function uses
 * the `translation_speed` and `angular_acceleration` members of RatesAndFailStatus.
 * @param rotatingPosition Desired rotating position of the robot
 * @param ratesAndFailStatus Struct used to specify interpolation rates and receive failure status
 * @return True if successful, false otherwise
 */
bool WorkspaceControllableRobot::setWorkspaceState(const Robo::RotatingPosition& rotatingPosition, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(rotatingPosition)
	Q_UNUSED(ratesAndFailStatus)

	setWorkspaceStateNotImplemented("Robo::RotatingPosition");

	return false;

}

QWidget* WorkspaceControllableRobot::buildUI() {

	/*** Position Section: ***/
	x_pos_box = new QLineEdit();
	y_pos_box = new QLineEdit();
	z_pos_box = new QLineEdit();
	x_pos_box->setReadOnly(true);
	y_pos_box->setReadOnly(true);
	z_pos_box->setReadOnly(true);

	StandardButton* button_up = new StandardButton("Jog +");
	StandardButton* button_down = new StandardButton("Jog -");
	StandardButton* button_left = new StandardButton("Jog +");
	StandardButton* button_right = new StandardButton("Jog -");
	StandardButton* button_front = new StandardButton("Jog +");
	StandardButton* button_back = new StandardButton("Jog -");

	QDoubleSpinBox* xSpinBox = new QDoubleSpinBox();
	QDoubleSpinBox* ySpinBox = new QDoubleSpinBox();
	QDoubleSpinBox* zSpinBox = new QDoubleSpinBox();
	xSpinBox->setRange(0, stepMax);
	ySpinBox->setRange(0, stepMax);
	zSpinBox->setRange(0, stepMax);
	x_step = stepMax / 2.0;
	y_step = stepMax / 2.0;
	z_step = stepMax / 2.0;
	xSpinBox->setValue(x_step);
	ySpinBox->setValue(y_step);
	zSpinBox->setValue(z_step);

	xSpinBox->setKeyboardTracking(false);
	ySpinBox->setKeyboardTracking(false);
	zSpinBox->setKeyboardTracking(false);

	QGridLayout* glayout1 = new QGridLayout();
	glayout1->addWidget(new PrePostfixLabelsWidget("X:", x_pos_box, "mm"), 0, 0, Qt::AlignCenter);
	glayout1->addWidget(new PrePostfixLabelsWidget("Y:", y_pos_box, "mm"), 0, 1, Qt::AlignCenter);
	glayout1->addWidget(new PrePostfixLabelsWidget("Z:", z_pos_box, "mm"), 0, 2, Qt::AlignCenter);
	glayout1->addWidget(button_up, 1, 2);
	glayout1->addWidget(button_down, 2, 2);
	glayout1->addWidget(button_left, 1, 1);
	glayout1->addWidget(button_right, 2, 1);
	glayout1->addWidget(button_front, 1, 0);
	glayout1->addWidget(button_back, 2, 0);

	glayout1->addWidget(new PrePostfixLabelsWidget(xSpinBox, "mm"), 3, 0, Qt::AlignCenter);
	glayout1->addWidget(new PrePostfixLabelsWidget(ySpinBox, "mm"), 3, 1, Qt::AlignCenter);
	glayout1->addWidget(new PrePostfixLabelsWidget(zSpinBox, "mm"), 3, 2, Qt::AlignCenter);

	QWidget* jogUI = new QWidget();
	jogUI->setLayout(glayout1);

	connect(xSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &WorkspaceControllableRobot::xStepChanged);
	connect(ySpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &WorkspaceControllableRobot::yStepChanged);
	connect(zSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &WorkspaceControllableRobot::zStepChanged);

	connect(button_up, &StandardButton::clicked, this, &WorkspaceControllableRobot::jogUp);
	connect(button_down, &StandardButton::clicked, this, &WorkspaceControllableRobot::jogDown);
	connect(button_front, &StandardButton::clicked, this, &WorkspaceControllableRobot::jogFront);
	connect(button_back, &StandardButton::clicked, this, &WorkspaceControllableRobot::jogBack);
	connect(button_left, &StandardButton::clicked, this, &WorkspaceControllableRobot::jogLeft);
	connect(button_right, &StandardButton::clicked, this, &WorkspaceControllableRobot::jogRight);

	QTimer* timer = new QTimer(jogUI);
	connect(timer, &QTimer::timeout, this, &WorkspaceControllableRobot::update);
	timer->start(250);

	return jogUI;

}

void WorkspaceControllableRobot::update() {

	Robo::Position position;
	if (!getWorkspaceState(position))
		return;

	x_pos_box->setText(QStringLiteral("%1").arg(position.position.x()));
	y_pos_box->setText(QStringLiteral("%1").arg(position.position.y()));
	z_pos_box->setText(QStringLiteral("%1").arg(position.position.z()));

}

void WorkspaceControllableRobot::jogUp() {

	Robo::Position position;
	if (!getWorkspaceState(position))
		return;


	position.position.z() = position.position.z() + z_step;

	RatesAndFailStatus ratesAndFailStatus = {jogSpeed};
	setWorkspaceState(position, ratesAndFailStatus);

}

void WorkspaceControllableRobot::jogDown() {

	Robo::Position position;
	if (!getWorkspaceState(position))
		return;

	position.position.z() = position.position.z() - z_step;

	RatesAndFailStatus ratesAndFailStatus = {jogSpeed};
	setWorkspaceState(position, ratesAndFailStatus);

}

void WorkspaceControllableRobot::jogLeft() {

	Robo::Position position;
	if (!getWorkspaceState(position))
		return;

	position.position.y() = position.position.y() + y_step;

	RatesAndFailStatus ratesAndFailStatus = {jogSpeed};
	setWorkspaceState(position, ratesAndFailStatus);

}

void WorkspaceControllableRobot::jogRight() {

	Robo::Position position;
	if (!getWorkspaceState(position))
		return;

	position.position.y() = position.position.y() - y_step;

	RatesAndFailStatus ratesAndFailStatus = {jogSpeed};
	setWorkspaceState(position, ratesAndFailStatus);

}

void WorkspaceControllableRobot::jogFront() {

	Robo::Position position;
	if (!getWorkspaceState(position))
		return;

	position.position.x() = position.position.x() + x_step;

	RatesAndFailStatus ratesAndFailStatus = {jogSpeed};
	setWorkspaceState(position, ratesAndFailStatus);

}

void WorkspaceControllableRobot::jogBack() {

	Robo::Position position;
	if (!getWorkspaceState(position))
		return;

	position.position.x() = position.position.x() - x_step;

	RatesAndFailStatus ratesAndFailStatus = {jogSpeed};
	setWorkspaceState(position, ratesAndFailStatus);

}
