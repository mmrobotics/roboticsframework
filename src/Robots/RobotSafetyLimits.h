#ifndef ROBOT_SAFETY_LIMITS_H
#define ROBOT_SAFETY_LIMITS_H

#include <list>
#include <RoboticsToolbox.h>


/**
 * @brief The WorkspacePositionLimit class represents a software limit for the 3D workspace position of a robot.
 *
 * The WorkspacePositionLimit is specified by specifying a direction vector and upper/lower bounds on that
 * direction in millimeters. For example, if you want to limit the maximum and minimum `x` position to be within
 * 50 mm of the origin, a WorkspacePositionLimit could be created where the direction vector is [1, 0, 0], the
 * upper limit is set to 50, and the lower limit is set to -50. The test() function is used to check if the position
 * meets these limits and allows for specifying additional padding from the actual limits if needed.
 *
 * The user will not usually need to call the test() function of this class directly. The WorkspacePositionLimit
 * class is designed to be added to a list of limits maintained by RobotLimiter which in turn is provided to a
 * WorkspaceControllableRobot. RobotLimiter::test() will check all the WorkspacePositionLimits that have been
 * provided to that instance of RobotLimiter and is intended to be called from within the particular
 * WorkspaceControllableRobot class.
 */
class WorkspacePositionLimit {

	private:
		double _low_limit;
		double _high_limit;
		Robo::UnitVector3d _direction;

	public:
		WorkspacePositionLimit(const Robo::Vector3d& direction, double low_limit /* mm */, double high_limit /* mm */);

		bool test(const Robo::Position& test_position, double padding = 0) const;

};


/**
 * @brief The RobotLimiter class is used to collect robot safety limits together and test them all at once with
 * the use of the test() function.
 *
 * Simply create a RobotLimiter object, use addLimit() to add limits, and then use test() to check against
 * those limits. The user will not usually need to call the test() function unless they are implementing a
 * new type of WorkspaceControllableRobot. The RobotLimiter should be given to a WorkspaceControllableRobot
 * so it can make sure it does not exceed the specified limits.
 *
 * <b>IMPLEMENTATION NOTE:</b> This class can be augmented to handle additional types of limits (other than
 * just positional limits). For each new type of limit, simply create another std::list for the type of limit,
 * then overload the addLimit() and test() functions to accept the type that the limit is limiting (e.g. you
 * could limit the workspace linear velocity in which case addLimit() would accept a new WorkspaceVelocityLimit
 * type and test() would accept Robo::Velocity objects). Then, iterate through the new type of list in the test()
 * function.
 */
class RobotLimiter {

	private:
		std::list<WorkspacePositionLimit> workspace_position_limit_list;

	public:
		void addLimit(const WorkspacePositionLimit& new_limit);

		bool test(const Robo::Position& test_position, double padding = 0);

};

#endif // ROBOT_SAFETY_LIMITS_H
