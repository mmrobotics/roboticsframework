#include "MH5SAMMDipoleRobot.h"


/**
 * @brief Constructs an MH5SAMMDipoleRobot.
 * @param limiter The safety limit for the robot
 * @param emitInterval_ms The interval for emitting the newDipoleState() signal in milliseconds
 */
MH5SAMMDipoleRobot::MH5SAMMDipoleRobot(RobotLimiter& limiter, double emitInterval_ms)
	: WorkspaceControllableRobot(limiter, 40, 30) {

	setName("MH5 SAMM Dipole Robot");

	// Register signal types
	qRegisterMetaType<Robo::RotatingPose5DOF>();

	// The MH5 robot will remain at this tool orientation consistently (i.e the SAMM will remain at
	// the same orientation at all times).
	mh5_fixed_orientation = Robo::Rotation(180.0, Robo::UnitZ());

	samm = new SAMM();
	mh5Robot = new MH5Robot(limiter, samm);

	emitStateThread.startTicking(*this, &MH5SAMMDipoleRobot::emitStateData, emitInterval_ms, "MH5SAMMDipoleRobotEmitStateThread", 99);

}


MH5SAMMDipoleRobot::~MH5SAMMDipoleRobot() {

	emitStateThread.stopTicking();

	delete mh5Robot;
	delete samm;

}


bool MH5SAMMDipoleRobot::enable() {

	if (!samm->enable())
		return false;

	if (!mh5Robot->enable()) {
		samm->disable();
		return false;
	}

	// Get the SAMM into the orientation that it is assumed to have in this class
	RatesAndFailStatus rates;
	rates.rotation_speed = 2 * M_PI / 180.0;
	if (!mh5Robot->setWorkspaceState(mh5_fixed_orientation, rates)) {
		mh5Robot->disable();
		samm->disable();
		return false;
	}
	Robo::Orientation currentOrientation;
	mh5Robot->getWorkspaceState(currentOrientation);
	while (!currentOrientation.hasArrived(mh5_fixed_orientation, 0, 0, 0, 0)) {
		Timing::wait(500);
		mh5Robot->getWorkspaceState(currentOrientation);
	}

	return true;

}


bool MH5SAMMDipoleRobot::disable() {

	return samm->disable() && mh5Robot->disable();

}


bool MH5SAMMDipoleRobot::isEnabled() const {

	return samm->isEnabled() && mh5Robot->isEnabled();

}


bool MH5SAMMDipoleRobot::isDisabled() const {

	return samm->isDisabled() && mh5Robot->isDisabled();

}


bool MH5SAMMDipoleRobot::errorOccurred() const  {

	return samm->errorOccurred() && mh5Robot->errorOccurred();

}


bool MH5SAMMDipoleRobot::getWorkspaceState(Robo::Position& position) const {

	// Since the SAMM is set as a tool for the MH5, the position of the magnet is
	// simply the position of the MH5
	return mh5Robot->getWorkspaceState(position);

}


bool MH5SAMMDipoleRobot::getWorkspaceState(Robo::Heading& heading) const {

	if (samm->errorOccurred())
		return false;

	Robo::Orientation mh5_orientation;
	if (!mh5Robot->getWorkspaceState(mh5_orientation))
		return false;

	// Get the heading from the SAMM and convert it to the robot frame
	heading = mh5_orientation.orientation * samm->getHeading();

	return true;

}


bool MH5SAMMDipoleRobot::getWorkspaceState(Robo::AngularVelocity& angularVelocity) const {

	if (samm->errorOccurred())
		return false;

	Robo::Orientation mh5_orientation;
	if (!mh5Robot->getWorkspaceState(mh5_orientation))
		return false;

	// Get the angular velocity from the SAMM and convert it to the robot frame
	angularVelocity = mh5_orientation.orientation * samm->getAngularVelocity();

	return true;

}


bool MH5SAMMDipoleRobot::getWorkspaceState(Robo::Pose5DOF& pose5DOF) const {

	if (samm->errorOccurred())
		return false;

	Robo::Pose6DOF mh5_pose6DOF;
	if (!mh5Robot->getWorkspaceState(mh5_pose6DOF))
		return false;

	// Since the SAMM is set as a tool for the MH5, the position of the magnet is
	// simply the position of the MH5
	pose5DOF.position = mh5_pose6DOF.position;

	// Get the heading from the SAMM and convert it to the robot frame
	pose5DOF.heading = mh5_pose6DOF.orientation * samm->getHeading();

	return true;

}


bool MH5SAMMDipoleRobot::getWorkspaceState(Robo::RotatingPosition& rotatingPosition) const {

	if (samm->errorOccurred())
		return false;

	Robo::Pose6DOF mh5_pose6DOF;
	if (!mh5Robot->getWorkspaceState(mh5_pose6DOF))
		return false;

	// Since the SAMM is set as a tool for the MH5, the position of the magnet is
	// simply the position of the MH5
	rotatingPosition.position = mh5_pose6DOF.position;

	// Get the angular velocity from the SAMM and convert it to the robot frame
	rotatingPosition.angularVelocity = mh5_pose6DOF.orientation * samm->getAngularVelocity();

    return true;

}


bool MH5SAMMDipoleRobot::getWorkspaceState(Robo::RotatingPose5DOF& rotatingPose5DOF) const {

	if (samm->errorOccurred())
		return false;

	Robo::Pose6DOF mh5_pose6DOF;
	if (!mh5Robot->getWorkspaceState(mh5_pose6DOF))
		return false;

	// Since the SAMM is set as a tool for the MH5, the position of the magnet is
	// simply the position of the MH5
	rotatingPose5DOF.position = mh5_pose6DOF.position;

	// Get the heading from the SAMM and convert it to the robot frame
	rotatingPose5DOF.heading = mh5_pose6DOF.orientation * samm->getHeading();

	// Get the angular velocity from the SAMM and convert it to the robot frame
	rotatingPose5DOF.angularVelocity = mh5_pose6DOF.orientation * samm->getAngularVelocity();

	return true;

}


bool MH5SAMMDipoleRobot::setWorkspaceState(const Robo::Position& position, RatesAndFailStatus& ratesAndFailStatus) {

	RatesAndFailStatus mh5RatesAndFailStatus;
	mh5RatesAndFailStatus.translation_speed = ratesAndFailStatus.translation_speed;

	// Set the desired pose for the MH5 robot
	if (!mh5Robot->setWorkspaceState(position, mh5RatesAndFailStatus)) {
		ratesAndFailStatus.ratesInvalid = mh5RatesAndFailStatus.ratesInvalid;
		ratesAndFailStatus.safetyLimitFailure = mh5RatesAndFailStatus.safetyLimitFailure;
		return false;
	}

	return true;

}


bool MH5SAMMDipoleRobot::setWorkspaceState(const Robo::Heading& heading, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(ratesAndFailStatus)

	// Set the desired heading for the SAMM (transform the heading from robot frame to SAMM frame)
	if (!samm->setHeading(mh5_fixed_orientation.orientation.transpose() * heading.heading))
		return false;

	return true;

}


bool MH5SAMMDipoleRobot::setWorkspaceState(const Robo::AngularVelocity& angularVelocity, RatesAndFailStatus& ratesAndFailStatus) {

	Q_UNUSED(ratesAndFailStatus)

	// Set the desired angular velocity for the SAMM (transform the angular velocity from robot frame to SAMM frame)
	if (!samm->setAngularVelocity(mh5_fixed_orientation.orientation.transpose() * angularVelocity.angularVelocity))
		return false;

	return true;

}


bool MH5SAMMDipoleRobot::setWorkspaceState(const Robo::Pose5DOF& pose5DOF, RatesAndFailStatus& ratesAndFailStatus) {

	RatesAndFailStatus mh5RatesAndFailStatus;
	mh5RatesAndFailStatus.translation_speed = ratesAndFailStatus.translation_speed;

	// Set the desired pose for the MH5 robot
	if (!mh5Robot->setWorkspaceState(static_cast<Robo::Position>(pose5DOF), mh5RatesAndFailStatus)) {
		ratesAndFailStatus.ratesInvalid = mh5RatesAndFailStatus.ratesInvalid;
		ratesAndFailStatus.safetyLimitFailure = mh5RatesAndFailStatus.safetyLimitFailure;
		return false;
	}

	// Set the desired heading for the SAMM (transform the heading from robot frame to SAMM frame)
	if (!samm->setHeading(mh5_fixed_orientation.orientation.transpose() * pose5DOF.heading))
		return false;

	return true;

}


bool MH5SAMMDipoleRobot::setWorkspaceState(const Robo::RotatingPosition& rotatingPosition, RatesAndFailStatus& ratesAndFailStatus) {

	RatesAndFailStatus mh5RatesAndFailStatus;
	mh5RatesAndFailStatus.translation_speed = ratesAndFailStatus.translation_speed;

	// Set the desired pose for the MH5 robot
	if (!mh5Robot->setWorkspaceState(static_cast<Robo::Position>(rotatingPosition), mh5RatesAndFailStatus)) {
		ratesAndFailStatus.ratesInvalid = mh5RatesAndFailStatus.ratesInvalid;
		ratesAndFailStatus.safetyLimitFailure = mh5RatesAndFailStatus.safetyLimitFailure;
		return false;
	}

	// Set the desired angular velocity for the SAMM (transform the angular velocity from robot frame to SAMM frame)
	if (!samm->setAngularVelocity(mh5_fixed_orientation.orientation.transpose() * rotatingPosition.angularVelocity))
		return false;

	return true;

}


bool MH5SAMMDipoleRobot::emitStateData() {

	Robo::RotatingPose5DOF state;

	long timestamp = Timing::getSysTime();

	if (!getWorkspaceState(state))
		return false;

	emit newDipoleState(state, timestamp);

	return true;

}
