#ifndef MH5_SAMM_ROTATING_DIPOLE_H
#define MH5_SAMM_ROTATING_DIPOLE_H

#include <Robots/WorkspaceControllableRobot.h>
#include <Robots/Motoman/MH5Robot.h>
#include <Robots/SAMM/SAMM.h>
#include <RoboticsToolbox.h>
#include <Utilities/Timing.h>


/**
 * @brief The MH5SAMMDipoleRobot class represents the combination of the MH5 Yaskawa Motoman robot with the SAMM robot attached as
 * the end effector. This class should be thought of as allowing you to control the state of a spherical magnet (i.e. position,
 * heading, angular velocity, etc.) without worrying about the underlying robots that realize this state.
 *
 * This class will create the MH5Robot and SAMM objects needed to interact with these robots, therefore, you do not need to (and
 * should not) create these classes yourself. This class will use the MH5Robot exclusively to position the SAMM (i.e. the
 * orientation of the SAMM will be constant) and will use the SAMM to orient the magnet. Since the MH5Robot and the SAMM are the
 * underlying robots that realize the desired state of the magnetic dipole, this hybrid robot is subject to the same limitations
 * as these robots. The maximum translational speed is that of the MH5Robot and the maximum angular velocity/acceleration are those
 * of the SAMM.
 *
 * This class also emits a signal called newDipoleState() which contains the current state of the spherical magnet inside the
 * SAMM.
 *
 * <b>IMPORTANT:</b> This class does not fulfill certain expectations of WorkspaceControllableRobot. The rotational components
 * of state are not linearly interpolated from the current state to the desired state. These are simply passed to the SAMM directly
 * and the internal controller of the SAMM achieves these rotational states independently. This means you only need to set the
 * `translational_speed` parameter of the RatesAndFailStatus parameters. If you want to control the rate at which the the rotational
 * components of state transition, you will have to do that yourself.
 *
 * All states are and must be described in the base frame of the MH5Robot (which is usually taken as the global frame in most cases).
 *
 * The SAMM must be mounted on the flange of the MH5 robot arm such that the notch in the flange is directly above motor 1 of the
 * SAMM. If this is confusing, make sure that the SAMM is mounted such that the y-axis of the SAMM frame and the y-axis of the MH5
 * robot end frame are in the same direction. The y-axis of the SAMM points toward motor 3 and the y-axis of the MH5 robot end frame
 * is in the plane of the flange and points directly away from the notch. For more information, see the documentation of the
 * MH5Robot and SAMM classes and the relevant lab wiki pages for both hardware items.
 */
class MH5SAMMDipoleRobot : public WorkspaceControllableRobot {

	Q_OBJECT

	public:
		MH5SAMMDipoleRobot(RobotLimiter& limiter, double emitInterval_ms = 10.0);
		~MH5SAMMDipoleRobot() override;

		bool enable() override;
		bool disable() override;

		bool isEnabled() const override;
		bool isDisabled() const override;
		bool errorOccurred() const override;

		bool getWorkspaceState(Robo::Position& position) const override;
		bool getWorkspaceState(Robo::Heading& heading) const override;
		bool getWorkspaceState(Robo::AngularVelocity& angularVelocity) const override;
		bool getWorkspaceState(Robo::Pose5DOF& pose5DOF) const override;
		bool getWorkspaceState(Robo::RotatingPosition& rotatingPosition) const override;
		bool getWorkspaceState(Robo::RotatingPose5DOF& rotatingPose5DOF) const override;

		bool setWorkspaceState(const Robo::Position& position, RatesAndFailStatus& ratesAndFailStatus) override;
		bool setWorkspaceState(const Robo::Heading& heading, RatesAndFailStatus& ratesAndFailStatus) override;
		bool setWorkspaceState(const Robo::AngularVelocity& angularVelocity, RatesAndFailStatus& ratesAndFailStatus) override;
		bool setWorkspaceState(const Robo::Pose5DOF& pose5DOF, RatesAndFailStatus& ratesAndFailStatus) override;
		bool setWorkspaceState(const Robo::RotatingPosition& rotatingPosition, RatesAndFailStatus& ratesAndFailStatus) override;

	signals:
		/**
		 * @brief Qt signal containing the current state of the spherical magnet inside the SAMM. The frequency that
		 * this signal is emitted is set in the constructor of this class.
		 * @param state Current state of the spherical magnet
		 * @param timestamp Timestamp in milliseconds corresponding to the state
		 */
		void newDipoleState(const Robo::RotatingPose5DOF& state, long timestamp);

	private:
		MH5Robot* mh5Robot;
		SAMM* samm;
		Robo::Orientation mh5_fixed_orientation;
		Timing::Timer emitStateThread;

		bool emitStateData();

};

#endif
