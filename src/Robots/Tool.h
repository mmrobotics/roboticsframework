#ifndef TOOL_H
#define TOOL_H

#include <RoboticsToolbox.h>


/**
 * @brief The Tool class represents a robot end effector tool.
 *
 * The Tool class is a base class that should be inherited by any classes representing
 * a tool that could be attached to a ToolableRobot. Every Tool object contains a tool
 * transform which should be set manually based on the geometric dimensions of the tool.
 * Each Tool also contains a safety limit radius that is meant to describe a sphere
 * centered at the tool frame origin which can be used by ToolableRobots that are also
 * WorkspaceControllableRobots to avoid moving the tool past workspace limits.
 */
class Tool {

	public:
		/**
		 * @brief Returns the tool transform which represents the position and
		 * orientation of the tool frame relative to the non-tooled workspace frame
		 * of a robot (i.e. the transform is expressed in the non-tooled workspace
		 * frame). Position is expressed in millimeters. The default value for a
		 * tool that does not set the transform is the default value of a Robo::Pose6DOF.
		 * @return
		 */
		const Robo::Pose6DOF& getTransform() const { return tool_transform; }
		/**
		 * @brief Returns the radius in millimeters of a sphere centered at the tool
		 * frame origin that can be used as a bounding region when checking workspace
		 * safety limits. The default value for a tool that does not set the radius
		 * is zero.
		 * @return
		 */
		double getSafetyRadius() const { return safety_limit_radius; }

	protected:
		/**
		 * @brief The tool transform. Represents the position and orientation of the
		 * tool frame relative to the non-tooled workspace frame of a robot. Position
		 * is expressed in millimeters. Defaults to the default value of Robo::Pose6DOF.
		 */
		Robo::Pose6DOF tool_transform;
		/**
		 * @brief The safety limit radius describes the radius in millimeters of a sphere
		 * centered at the tool frame origin that can be used as a bounding region when
		 * checking workspace limits. Defaults to zero.
		 */
		double safety_limit_radius = 0.0;	// mm

};

#endif
