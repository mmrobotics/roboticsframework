#ifndef TOOLABLEROBOT_H
#define TOOLABLEROBOT_H

#include <Robots/Tool.h>


/**
 * @brief The ToolableRobot class is inherited by robots that are capable of having a tool attached to them.
 *
 * Robots that inherit from this class will modify the behavior of their functions such that the workspace
 * state (position, orientation, velocity, etc.) will always refer to the tool frame rather than
 * the non-tooled workspace frame. For example, if a particular WorkspaceControllableRobot were to also be
 * a ToolableRobot, then its getWorkspaceState() and setWorkspaceState() functions will get and set
 * the state of the tool frame. Similarly, that particular robot would also use the tool's frame and safety
 * radius when checking whether the tool will cross workspace safety limits.
 *
 * If a Tool object is not provided to a ToolableRobot (i.e. it was given a `nullptr`), then the robot will
 * ignore any tool frame and behave as it normally would without a tool. In other words, the workspace will
 * always refer to the non-tooled workspace frame.
 *
 * <b>IMPLEMENTATION NOTE:</b> If you are implementing a new robot and want it to be a ToolableRobot, it is
 * your responsibility to fulfill the behavior as outlined here. You must check if a tool has been assigned,
 * and if so, you must behave as if the tool frame is the workspace frame of your robot.
 */
class ToolableRobot {

	protected:
		Tool* robot_tool;

	public:
		ToolableRobot(Tool* tool = nullptr) : robot_tool(tool) { }

		/**
		 * @brief Sets the tool of the ToolableRobot to `tool`.
		 * @param tool A new Tool for the robot
		 */
		void setTool(Tool* tool) { robot_tool = tool; }
		/**
		 * @brief Returns a pointer to the tool assigned to this instance of ToolableRobot.
		 * @return
		 */
		Tool* getTool() const { return robot_tool; }

};

#endif // TOOLABLEROBOT_H
