#ifndef SAMM_KALMAN_FILTER_H
#define SAMM_KALMAN_FILTER_H

#include <RoboticsToolbox.h>


/**
 * @brief The SAMMKalmanFilter class implements the Kalman filter used for sensing the heading and angular
 * velocity of the SAMM as described in: S. E. Wright, A. W. Mahoney, K. M. Popek, and J. J. Abbott,
 * "The Spherical-Actuator-Magnet Manipulator: A Permanent-Magnet Robotic End-Effector",
 * IEEE Trans. Robotics, 33(5):1013-1024, 2017.
 */
class SAMMKalmanFilter {

	public:
		SAMMKalmanFilter(double dt, Robo::Vector6d hallsInit);

		void predict(const Robo::Vector3d& tau);
		void update(const Robo::Vector9d& z);

		Robo::Vector6d getState() { return X; }

	private:
		double M_mag = 66.03;		// Scalar field strength (T)
		double u0  = 4e-7 * M_PI;	// Permeability of free space (T m/A)
		double dt;					// iteration time (s)

		Robo::Matrixd66 Q;	// Process noise covariance
		Robo::Matrixd99 R;	// Sensor noise covariance
		Robo::Matrixd66 P;	// State Estimate covariance

		Robo::Matrixd36 v;	// Sensing directions
		Robo::Matrixd36 p;	// Sensor positions (m)
		Robo::Matrixd96 H;	// Observer Jacobian
		Robo::Matrixd63 Ss;	// Field at sensors (T)

		Robo::Vector3d c_pos, c_neg, B_neg, B_pos;	// Friction and Sticktion limits
		Robo::Matrixd33 B;	// Modeled Friction
		Robo::Vector3d c;	// Modeled Sticktion

		Robo::Matrixd33 A;	// Actuation Matrix
		Robo::Matrixd33 J;	// Modeled Inertia (kgm^2)

		Robo::Vector6d X;	// State

		Robo::Matrixd33 calc_B(const Robo::Vector3d& w);
		Robo::Vector3d calc_c(const Robo::Vector3d& w);

		Robo::Vector6d Xdot(const Robo::Vector6d& x, const Robo::Vector3d& tau);
		Robo::Matrixd66 Pdot(const Robo::Vector6d& x, const Robo::Matrixd66& _P, const Robo::Matrixd66& Q);

		Robo::Matrixd66 createF(const Robo::Vector6d& state);

		Robo::Vector9d get_h();

};

#endif // SAMM_KALMAN_FILTER_H
