#include "SAMM.h"
#include <iostream>
#include <cmath>

extern "C" {
	#include "App626.h"
	#include "s626api.h"
}

// Analog output channels used to set the speed of the motors
#define M1		0
#define M2		1
#define M3		2

// Digital output channels used for setting motor direction
#define M1_DIR	0
#define M2_DIR	2
#define M3_DIR	4

// Digital output channels used for enabling the motors
#define M1_EN	1
#define M2_EN	3
#define M3_EN	5

#define BRD					0		// s626 board handle number
#define HIGH_VOLTS			8191	// Maximum value for analog output
#define MAX_MAG_SPD_HZ		3.0		// Max speed is 3 Hz
#define MAX_MOTOR_SPD_RADS	((MAX_MAG_SPD_HZ) * 2.0 * M_PI * (2.0 / 2.25) * 24.0)	// = 3840 RPM when MAX_MAG_SPD_HZ = 3.0


/**
 * @brief Creates an instance of the SAMM. Only create one instance of SAMM at a time.
 */
SAMM::SAMM() {

	setName("SAMM");

	// Set the tool transform of the SAMM and the safety limit radius of the tool
	tool_transform = Robo::Pose6DOF(Robo::Vector3d(0.0, 0.0, 148.0), Robo::Rotation(180, Robo::UnitY()));
	safety_limit_radius = 90.0;	// mm

	// Set up A and the "gear ratio"
	Robo::Vector3d a1 = Robo::Vector3d(1, 1, 0).normalized();
	Robo::Vector3d a2 = Robo::Vector3d(0, 0, -1).normalized();
	Robo::Vector3d a3 = Robo::Vector3d(1, -1, 0).normalized();
	A << a1, a2, a3;
	A *= -1;
	gearRatio = (50.8 / 57.8) * 24.0; // Gear ratio of ball -> wheel -> motor (i.e. including 24:1 gear box)

	// Control tuning params
	double Ku = 21;
	k_p_pointing = 0.5 * Ku; // 0.7 * Ku;
	k_p_rotating = 0.5 * Ku;

	// Initialize the measured sensor bias values
	sensorMeans << -0.6300, -0.3126, -0.2044, -0.1138, -0.0385, -0.0223,	// Hall effect sensors (mT)
				   -0.0092, -0.0021, 0.0104,								// Motor speeds (rpm)
				   0.0010, -0.0013, -0.0002;								// Motor currents (Nm)

	// Open the s626 DAQ board
	S626_OpenBoard(BRD, 0, nullptr, 1);

	// Make sure the SAMM is disabled
	disable();

	// Set up the ADC poll list
	// The ADC will poll the channels in the following order and they will use the 5 volt range
	BYTE poll_list[16];
	poll_list[0] = 0 | ADC_RANGE_5V;				// Channel 0 - Hall effect sensor
	poll_list[1] = 1 | ADC_RANGE_5V;				// Channel 1 - Hall effect sensor
	poll_list[2] = 2 | ADC_RANGE_5V;				// Channel 2 - Hall effect sensor
	poll_list[3] = 3 | ADC_RANGE_5V;				// Channel 3 - Hall effect sensor
	poll_list[4] = 4 | ADC_RANGE_5V;				// Channel 4 - Hall effect sensor
	poll_list[5] = 5 | ADC_RANGE_5V;				// Channel 5 - Hall effect sensor
	poll_list[6] = 6 | ADC_RANGE_5V;				// Channel 6 - Motor 1 speed
	poll_list[7] = 7 | ADC_RANGE_5V;				// Channel 7 - Motor 2 speed
	poll_list[8] = 8 | ADC_RANGE_5V;				// Channel 8 - Motor 3 speed
	poll_list[9] = 9 | ADC_RANGE_5V;				// Channel 9 - Motor 1 current
	poll_list[10] = 10 | ADC_RANGE_5V;				// Channel 10 - Motor 2 current
	poll_list[11] = 11 | ADC_RANGE_5V | ADC_EOPL;	// Channel 11 - Motor 3 current
	S626_ResetADC(BRD, poll_list);

	// Read the sensors so we have initial values to give to the Kalman filter
	readADC();

	// Check for errors with the s626
	if (checkErrors())
		return;

	// Create the Kalman filter
	observer = new SAMMKalmanFilter(dt, SensorVals.head<6>());

	// Start the sense and control thread
	sammThread.startTicking(*this, &SAMM::senseAndControl, 1000.0 * dt, "SAMM", 99);

}


SAMM::~SAMM() {

	// Stop the sense and control thread (must be done before closing the board or the thread may get stuck
	// and never join when stopped)
	sammThread.stopTicking();

	// Disable the SAMM
	disable();

	// Close the board
	S626_CloseBoard(BRD);

	delete observer;

}


/**
 * @brief Enables the SAMM.
 * @return
 */
bool SAMM::enable() {

	if (isInError)
		return false;

	// Set the enable digital output channels to active (this means low voltage or 0V on s626)
	WORD states = S626_DIOWriteBankGet(BRD, 0);
	states |= (1 << M1_EN);
	states |= (1 << M2_EN);
	states |= (1 << M3_EN);
	S626_DIOWriteBankSet(BRD, 0, states);

	// Check for errors with the s626
	if(checkErrors())
		return false;

	enabled = true;

	return true;

}


/**
 * @brief Disables the SAMM.
 * @return
 */
bool SAMM::disable() {

	// Set the SAMM to disabled
	enabled = false;

	// Wait for the sense and control loop to finish the control potion if it was
	// controlling when the enabled variable was changed.
	Timing::wait(5000.0 * dt);

	// Now we can set all the DAC outputs to disabled values because the sense and
	// control loop won't possibly touch them anymore.

	// Set analog outputs to 0 volts
	S626_WriteDAC(BRD, M1, 0);
	S626_WriteDAC(BRD, M2, 0);
	S626_WriteDAC(BRD, M3, 0);

	// Set all digital output channels to inactive (this means high voltage or 5V on s626)
	WORD states = S626_DIOWriteBankGet(BRD, 0);
	states &= ~(1 << M1_DIR);
	states &= ~(1 << M2_DIR);
	states &= ~(1 << M3_DIR);
	states &= ~(1 << M1_EN);
	states &= ~(1 << M2_EN);
	states &= ~(1 << M3_EN);
	S626_DIOWriteBankSet(BRD, 0, states);

	// Check for errors with the s626
	if (checkErrors())
		return false;

	return true;

}


/**
 * @brief Returns the current heading (magnetic moment) of the SAMM expressed in the
 * SAMM frame. If the result of errorOccurred() is true, then the result of this function
 * is invalid.
 * @return
 */
Robo::Vector3d SAMM::getHeading() const {

	stateMutex.lockForRead();
	Robo::Vector3d heading = currentState.head<3>();
	stateMutex.unlock();

	return heading;

}


/**
 * @brief Returns the current angular velocity in rad/s of the SAMM expressed in the SAMM
 * frame. If the result of errorOccurred() is true, then the result of this function
 * is invalid.
 * @return
 */
Robo::Vector3d SAMM::getAngularVelocity() const {

	stateMutex.lockForRead();
	Robo::Vector3d angularVelocity = currentState.tail<3>();
	stateMutex.unlock();

	return angularVelocity;

}


/**
 * @brief Returns the current Hall Effect sensor readings in mT. If the result of
 * errorOccurred() is true, then the result of this function is invalid.
 * @return
 */
Robo::Vector6d SAMM::getHallEffectSensors() const {

	stateMutex.lockForRead();
	Robo::Vector6d hallSensors = currentSensorVals.head<6>();
	stateMutex.unlock();

	return hallSensors;

}


/**
 * @brief Returns the current motor speeds in RPM. If the result of errorOccurred()
 * is true, then the result of this function is invalid.
 * @return
 */
Robo::Vector3d SAMM::getMotorSpeeds() const {

	stateMutex.lockForRead();
	Robo::Vector3d motorSpeeds = currentSensorVals.segment<3>(12);
	stateMutex.unlock();

	return motorSpeeds;

}


/**
 * @brief Returns the current motor currents in A. If the result of errorOccurred()
 * is true, then the result of this function is invalid.
 * @return
 */
Robo::Vector3d SAMM::getMotorCurrents() const {

	stateMutex.lockForRead();
	Robo::Vector3d motorCurrents = currentSensorVals.segment<3>(15);
	stateMutex.unlock();

	return motorCurrents;

}


/**
 * @brief Returns the current motor torques in Nm. This torque is at the motor
 * before any gear ratio is applied. If the result of errorOccurred() is true,
 * then the result of this function is invalid.
 * @return
 */
Robo::Vector3d SAMM::getMotorTorques() const {

	stateMutex.lockForRead();
	Robo::Vector3d motorTorques = currentSensorVals.segment<3>(18);
	stateMutex.unlock();

	return motorTorques;

}


/**
 * @brief Sets the desired heading (magnetic moment) of the SAMM. Any previously
 * set desired angular velocity becomes invalid and is therefore ignored.
 * @param desiredHeading The desired heading (magnetic moment)
 * @return True if successful
 */
bool SAMM::setHeading(const Robo::Vector3d& desiredHeading) {

	if (isInError)
		return false;

	controlMutex.lock();
	_desiredHeading = desiredHeading.normalized();
	controlMutex.unlock();

	mode = POINTING;

	return true;

}


/**
 * @brief Sets the desired angular velocity of the SAMM in rad/s. Any previously set
 * desired heading becomes invalid and is therefore ignored. Note that the
 * SAMM is only capable of rotating at a maximum of 6*pi rad/s or 3 Hz.
 * @param desiredAngularVelocity The desired angular velocity
 * @return True if successful
 */
bool SAMM::setAngularVelocity(const Robo::Vector3d& desiredAngularVelocity) {

	if (isInError)
		return false;

	controlMutex.lock();
	_desiredAngularVelocity = desiredAngularVelocity;
	controlMutex.unlock();

	mode = ROTATING;

	return true;

}


/**
 * @brief Checks for errors on the s626 and prints any errors to the standard error stream.
 * @return
 */
bool SAMM::checkErrors() {

	DWORD errorCode = S626_GetErrors(BRD);

	if (errorCode) {

		// Only deal with the errors if we haven't before, otherwise checkErrors() will be called
		// in infinite recursion because disable() is called here
		if (!isInError) {

			// Set the SAMM in error state
			isInError = true;

			// Try to disable the SAMM
			disable();

			std::cerr << "SAMM_COMMUNICATOR: An error occurred with the s626 DAQ card ERROR" << std::endl;

			// List errors
			if (errorCode & 0x00000001)
				std::cerr << " * Failed to open kernel-mode driver" << std::endl;
			if (errorCode & 0x00000002)
				std::cerr << " * Can't detect/register board" << std::endl;
			if (errorCode & 0x00000004)
				std::cerr << " * Memory allocation error" << std::endl;
			if (errorCode & 0x00000008)
				std::cerr << " * Can't lock DMA buffer" << std::endl;
			if (errorCode & 0x00000010)
				std::cerr << " * Can't launch interrupt thread" << std::endl;
			if (errorCode & 0x00000020)
				std::cerr << " * Can't enable IRQ" << std::endl;
			if (errorCode & 0x00000040)
				std::cerr << " * Missed interrupt" << std::endl;
			if (errorCode & 0x00000080)
				std::cerr << " * Can't instantiate board object" << std::endl;
			if (errorCode & 0x00000100)
				std::cerr << " * Unsupported kernel-mode driver version" << std::endl;
			if (errorCode & 0x00010000)
				std::cerr << " * D/A communication timeout" << std::endl;
			if (errorCode & 0x00020000)
				std::cerr << " * Illegal counter parameter" << std::endl;

		}

	}

	return isInError;

}


Robo::Vector3d SAMM::tau_offset_fixer(const Robo::Vector3d& tau_, const double& offset) {

	Robo::Vector3d tmp;
	for (unsigned int i = 0; i < 3; i++) {

		if (std::abs(tau_(i)) < offset)
			tmp(i) = 0.0;
		else
			tmp(i) = tau_(i) - std::copysign(offset, tau_(i));

	}

	return tmp;

}


/**
 * @brief This function is the main sense and control function and runs in its own thread at 500 Hz.
 * @return
 */
bool SAMM::senseAndControl() {

	if (isInError)
		return false;

	// Read the ADC and convert the results to relevant units
	readADC();

	// Perform Kalman filtering
	observer->predict(tau_offset_fixer(SensorVals.segment<3>(9), 0.006327 * 0.05 /*0.006327*/));
	observer->update(SensorVals.head<9>());

	// Save results of ADC read and Kalman filtering
	stateMutex.lockForWrite();
	currentState = observer->getState();
	currentSensorVals = SensorVals;
	stateMutex.unlock();

	// Perform control
	if (enabled) {

		switch(mode) {

			case POINTING:

				controlHeading();
				break;

			case ROTATING:

				controlAngularVelocity();
				break;

		}

	}

	return true;

}


void SAMM::readADC() {

	// Read the s626 ADC
	// IMPORTANT: The ADC reads the channels as 16-bit signed integers (e.g. the SHORT type in the
	// S626 API). For some reason however, the S626_ReadADC() function requires an array of 16-bit
	// unsigned integers (i.e. WORD) even though the documentation for the function shows that an
	// array of SHORT is required. This means that we need to cast all of the readings to 16-bit
	// signed integers (SHORT) before they get used.
	WORD data[16];	// Array into which analog inputs from ADC are read
	S626_ReadADC(BRD, data);

	// Interpret the Hall Effect readings in mT
	for (unsigned int i = 0; i < 6; i++)
		SensorVals(i) = findFieldStrength(SHORT(data[i]), 5.0 / 32767.0, 0.013, sensorMeans[i]);
	// Interpret the angular velocity of the magnet in rad/s
	for (unsigned int i = 6; i < 9; i++)
		SensorVals(i) = findVels(SHORT(data[i]), 5.0 / 32767.0, sensorMeans[i]);
	// Interpret the torque on the magnet in Nm
	for (unsigned int i = 9; i < 12; i++)
		SensorVals(i) = findTorque(SHORT(data[i]), 5.0 / 32767.0, sensorMeans[i]);
	// Interpret the motor speeds in rpm
	for (unsigned int i = 12; i < 15; i++)
		SensorVals(i) = SHORT(data[i - 6]) * 5.0 / 32767.0 * (3840.0 / 4.0);
	// Interpret the motor currents in A
	for (unsigned int i = 15; i < 18; i++)
		SensorVals(i) = SHORT(data[i - 6]) * 5.0 / 32767.0 * (1.0 / 4.0);
	// Interpret the motor torques in Nm
	for (unsigned int i = 18; i < 21; i++)
		SensorVals(i) = SensorVals(i - 3) * 47.5 / 1000.0;

	// Apply the A matrix (negative sign is embedded into A) to magnet angular velocity and torque
	SensorVals.segment<3>(6) = A * SensorVals.segment<3>(6);
	SensorVals.segment<3>(9) = A * SensorVals.segment<3>(9);

	SensorVals.head<12>() *= -1;

}


/**
 * @brief Calculates the magnetic field strength in mT measured by a Hall Effect sensor.
 * @param analogMeasurement Raw n-bit measurement from an ADC
 * @param res Resolution of the ADC in V/count
 * @param sensitivity Sensitivity of the Hall Effect sensor in V/mT
 * @param zero Measured zero point in mT
 * @return
 */
inline double SAMM::findFieldStrength(double analogMeasurement, double res, double sensitivity, double zero) {

	//                     (Volts)              (V/mT)        (mT)
	return ((analogMeasurement * res - 2.5) / sensitivity) - zero;

}


inline double SAMM::findVels(double analogMeasurement, double res, double offset) {

	//                (volts)         *   (RPM/volts)  / (gearRatio) -  (rpm)   *  (rpm to rad/s)
	return ((analogMeasurement * res)  * (3840.0 / 4.0) /  gearRatio  - offset) * 2.0 * M_PI / 60.0;

}


inline double SAMM::findTorque(double analogMeasurement, double res, double offset) {

	//                 (V)           *   (1A/4V)   * (mNm/A)*    (Nm/mNm)    * (gearRatio) -  (Nm)
	return (analogMeasurement * res) * (1.0 / 4.0) * (47.5) * (1.0 / 1000.0) *  gearRatio  - offset;

}


/**
 * @brief Sets the angular velocity of the magnet in rad/s. This function converts the magnet angular
 * velocity to the corresponding motor velocities and then calls driveM() with those motor velocities.
 * @param Vrad Desired angular velocity of the magnet in rad/s
 */
void SAMM::driveW(Robo::Vector3d Vrad) {

	// Convert to motor space velocities
	driveM(gearRatio * A.transpose() * Vrad);

}


/**
 * @brief Sets the velocity of each of the three motors according to the values
 * found in the Robo::Vector3d parameter. The first value sets motor 1, the second
 * sets motor 2, etc.
 * @param velocityVector The parameter specifying the velocity of each motor in rad/s
 */
void SAMM::driveM(Robo::Vector3d velocityVector) {

	// Saturate motor speeds and convert them from rad/s to corresponding analog output setpoint
	Robo::Vector3i motorInputs = (Robo::saturate(velocityVector, MAX_MOTOR_SPD_RADS) * (HIGH_VOLTS / MAX_MOTOR_SPD_RADS)).cast<int>();

	// Set the analog outputs that control the speed of each motor
	// The positive value is always used since direction is controlled
	// by the direction digital outputs.
	S626_WriteDAC(BRD, M1, abs(motorInputs(0)));
	S626_WriteDAC(BRD, M2, abs(motorInputs(1)));
	S626_WriteDAC(BRD, M3, abs(motorInputs(2)));

	// Set the digital outputs that control the direction of each motor
	// I believe the if-statements here are to add a "hysteresis" effect for direction
	// changes so that direction is not changing rapidly when the desired speed is near
	// zero.
	WORD states = S626_DIOWriteBankGet(BRD, 0);
	if (abs(velocityVector(0)) > 0.003)
		velocityVector(0) > 0 ? states &= ~(1 << M1_DIR) : states |= (1 << M1_DIR);
	if (abs(velocityVector(1)) > 0.003)
		velocityVector(1) > 0 ? states &= ~(1 << M2_DIR) : states |= (1 << M2_DIR);
	if (abs(velocityVector(2)) > 0.003)
		velocityVector(2) > 0 ? states &= ~(1 << M3_DIR) : states |= (1 << M3_DIR);
	S626_DIOWriteBankSet(BRD, 0, states);

	// Check for errors with the s626
	checkErrors();

}


/**
 * @brief Control law used when controlling the heading of the SAMM.
 */
void SAMM::controlHeading() {

	controlMutex.lock();
	Robo::Vector3d heading_desired = _desiredHeading;
	controlMutex.unlock();

	Robo::Vector3d heading = currentState.head<3>();

	Robo::Vector3d crossVec = heading.cross(heading_desired);
	double crossMag = crossVec.norm();

	driveW(k_p_pointing * std::asin(crossMag) * crossVec.normalized());

}


/**
 * @brief Control law used when controlling the angular velocity of the SAMM.
 */
void SAMM::controlAngularVelocity() {

	controlMutex.lock();
	Robo::Vector3d omega_desired = _desiredAngularVelocity;
	controlMutex.unlock();

	// Get the current rotation plane vector and the current heading
	Robo::Vector3d rplane = omega_desired.normalized();
	Robo::Vector3d heading = currentState.head<3>();

	// Find the projection of the heading onto the rotation plane
	Robo::Vector3d heading_proj = (Robo::I33 - rplane*rplane.transpose()) * heading;

	// If the heading projection is not large enough, then the heading is close to parallel with omega_desired
	// and we need to use a different vector.
	if (heading_proj.norm() < 0.05) {

		Robo::Vector3d r = Robo::Vector3d(0.772222329956, 0.508480669622, 0.38094629771);
		heading_proj = (Robo::I33 - rplane*rplane.transpose()) * r;

	}

	// Compute the error of the heading and the rotation plane
	Robo::Vector3d planeError = heading.cross(heading_proj.normalized());

	// Control input is feedforward term of desired velocity plus a proportional term times the plane error
	driveW(omega_desired + k_p_rotating * planeError);

}
