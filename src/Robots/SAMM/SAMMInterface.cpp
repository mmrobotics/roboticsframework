#include "SAMMInterface.h"
#include <cmath>
#include <GL/glu.h>
#include "Utilities/Timing.h"


/**
 * @brief Constructs a SAMMInterface object. Requires a SAMM object to interface with.
 * @param samm A SAMM object
 * @param parent
 */
SAMMInterface::SAMMInterface(SAMM& samm, QWidget *parent)
	: QWidget(parent),
	  samm(samm) {

	setWindowTitle("SAMM Interface");

	QVBoxLayout* leftLayout = new QVBoxLayout();

	// Add the SAMMVectorDisplayWidget to the top of the interface
	display = new SAMMVectorDisplayWidget();
	leftLayout->addWidget(display);
	leftLayout->addWidget(new QLabel("<b>Key:</b> Gray -> Desired | Dark Blue -> Heading | Orange -> Angular Velocity"));

	// Add the control type selection radio buttons
	pointingRadioButton = new QRadioButton("Heading");
	rotatingRadioButton = new QRadioButton("Angular Velocity");
	pointingRadioButton->setChecked(true);
	connect(pointingRadioButton, &QRadioButton::clicked, this, &SAMMInterface::controlTypeChanged);
	connect(rotatingRadioButton, &QRadioButton::clicked, this, &SAMMInterface::controlTypeChanged);
	QHBoxLayout* radioButtonLayout = new QHBoxLayout();
	radioButtonLayout->addWidget(pointingRadioButton);
	radioButtonLayout->addWidget(rotatingRadioButton);
	QGroupBox* controlTypeButtons = new QGroupBox("Control Type");
	controlTypeButtons->setLayout(radioButtonLayout);
	leftLayout->addWidget(controlTypeButtons, 1);

	// Set up the direction input section
	directionX = new QDoubleSpinBox();
	directionY = new QDoubleSpinBox();
	directionZ = new QDoubleSpinBox();
	directionX->setRange(-10.0, 10.0);
	directionY->setRange(-10.0, 10.0);
	directionZ->setRange(-10.0, 10.0);
	directionX->setDecimals(3);
	directionY->setDecimals(3);
	directionZ->setDecimals(3);
	directionX->setSingleStep(0.01);
	directionY->setSingleStep(0.01);
	directionZ->setSingleStep(0.01);
	directionX->setKeyboardTracking(false);
	directionY->setKeyboardTracking(false);
	directionZ->setKeyboardTracking(false);
	directionZ->setValue(1.0);
	connect(directionX, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SAMMInterface::directionChanged);
	connect(directionY, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SAMMInterface::directionChanged);
	connect(directionZ, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SAMMInterface::directionChanged);
	QHBoxLayout* directionInputLayout = new QHBoxLayout();
	directionInputLayout->addWidget(new QLabel("X: "), 0, Qt::AlignRight);
	directionInputLayout->addWidget(directionX);
	directionInputLayout->addWidget(new QLabel("Y: "), 0, Qt::AlignRight);
	directionInputLayout->addWidget(directionY);
	directionInputLayout->addWidget(new QLabel("Z: "), 0, Qt::AlignRight);
	directionInputLayout->addWidget(directionZ);

	// Set up the speed input section
	speedSpinBox = new QDoubleSpinBox();
	speedSpinBox->setRange(0, 3.0);
	speedSpinBox->setSingleStep(0.01);
	speedSpinBox->setDecimals(3);
	speedSpinBox->setKeyboardTracking(false);
	connect(speedSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SAMMInterface::speedSpinBoxValueChanged);
	speedSlider = new QSlider();
	speedSlider->setOrientation(Qt::Horizontal);
	speedSlider->setRange(0, 3000);
	connect(speedSlider, &QSlider::valueChanged, this, &SAMMInterface::speedSliderValueChanged);
	QHBoxLayout* speedLayout = new QHBoxLayout();
	speedLayout->addWidget(speedSpinBox);
	speedLayout->addWidget(speedSlider);
	speedSpinBox->setEnabled(false);
	speedSlider->setEnabled(false);

	// Put the direction input section and the speed input section together into the control input section
	QVBoxLayout* controlInputLayout = new QVBoxLayout();
	controlInputLayout->addWidget(new QLabel("<b>Direction (Unitized before use)</b>"));
	controlInputLayout->addLayout(directionInputLayout);
	controlInputLayout->addWidget(new QLabel("<b>Speed (Hz)</b>"));
	controlInputLayout->addLayout(speedLayout);
	controlInput = new QGroupBox("Desired Heading");
	controlInput->setLayout(controlInputLayout);
	leftLayout->addWidget(controlInput, 1);

	// Add the enable button
	enableButton = new SwitcherButton("ENABLE", "DISABLE");
	connect(enableButton, &SwitcherButton::clicked, this, &SAMMInterface::enableButtonPressed);
	leftLayout->addWidget(enableButton);

	// Set up the plotting section
	directionErrorPlot = new QCustomPlot();
	directionErrorPlot->addGraph();
	configurePlot(directionErrorPlot);

	actualSpeedPlot = new QCustomPlot();
	actualSpeedPlot->addGraph();
	configurePlot(actualSpeedPlot);

	speedErrorPlot = new QCustomPlot();
	speedErrorPlot->addGraph();
	configurePlot(speedErrorPlot);

	planeErrorPlot = new QCustomPlot();
	planeErrorPlot->addGraph();
	configurePlot(planeErrorPlot);

	sensorsPlot = new QCustomPlot();
	for (int i = 0; i < 6; i++)
		sensorsPlot->addGraph();
	sensorsPlot->graph(0)->setName("xA");
	sensorsPlot->graph(1)->setName("xB");
	sensorsPlot->graph(2)->setName("yA");
	sensorsPlot->graph(3)->setName("yB");
	sensorsPlot->graph(4)->setName("zA");
	sensorsPlot->graph(5)->setName("zB");
	sensorsPlot->graph(0)->setPen(QPen(Qt::red));
	sensorsPlot->graph(1)->setPen(QPen(Qt::darkRed));
	sensorsPlot->graph(2)->setPen(QPen(Qt::green));
	sensorsPlot->graph(3)->setPen(QPen(Qt::darkGreen));
	sensorsPlot->graph(4)->setPen(QPen(Qt::blue));
	sensorsPlot->graph(5)->setPen(QPen(Qt::darkBlue));
	configurePlot(sensorsPlot, true);

	motorSpeedsPlot = new QCustomPlot();
	for (int i = 0; i < 3; i++)
		motorSpeedsPlot->addGraph();
	motorSpeedsPlot->graph(0)->setName("M1");
	motorSpeedsPlot->graph(1)->setName("M2");
	motorSpeedsPlot->graph(2)->setName("M3");
	motorSpeedsPlot->graph(0)->setPen(QPen(Qt::red));
	motorSpeedsPlot->graph(1)->setPen(QPen(Qt::green));
	motorSpeedsPlot->graph(2)->setPen(QPen(Qt::blue));
	configurePlot(motorSpeedsPlot, true);

	motorCurrentsPlot = new QCustomPlot();
	for (int i = 0; i < 3; i++)
		motorCurrentsPlot->addGraph();
	motorCurrentsPlot->graph(0)->setName("M1");
	motorCurrentsPlot->graph(1)->setName("M2");
	motorCurrentsPlot->graph(2)->setName("M3");
	motorCurrentsPlot->graph(0)->setPen(QPen(Qt::red));
	motorCurrentsPlot->graph(1)->setPen(QPen(Qt::green));
	motorCurrentsPlot->graph(2)->setPen(QPen(Qt::blue));
	configurePlot(motorCurrentsPlot, true);

	motorTorquesPlot = new QCustomPlot();
	for (int i = 0; i < 3; i++)
		motorTorquesPlot->addGraph();
	motorTorquesPlot->graph(0)->setName("M1");
	motorTorquesPlot->graph(1)->setName("M2");
	motorTorquesPlot->graph(2)->setName("M3");
	motorTorquesPlot->graph(0)->setPen(QPen(Qt::red));
	motorTorquesPlot->graph(1)->setPen(QPen(Qt::green));
	motorTorquesPlot->graph(2)->setPen(QPen(Qt::blue));
	configurePlot(motorTorquesPlot, true);

	QVBoxLayout* plotsLayout = new QVBoxLayout();
	plotsLayout->addWidget(new QLabel("<b>Direction Error (Degrees)</b>"), 0, Qt::AlignCenter);
	plotsLayout->addWidget(directionErrorPlot, 1);
	plotsLayout->addWidget(new QLabel("<b>Actual Angular Speed (Hz)</b>"), 0, Qt::AlignCenter);
	plotsLayout->addWidget(actualSpeedPlot, 1);
	plotsLayout->addWidget(new QLabel("<b>Angular Speed Error (Hz)</b>"), 0, Qt::AlignCenter);
	plotsLayout->addWidget(speedErrorPlot, 1);
	plotsLayout->addWidget(new QLabel("<b>Plane Error (Degrees)</b>"), 0, Qt::AlignCenter);
	plotsLayout->addWidget(planeErrorPlot, 1);
	plotsLayout->addWidget(new QLabel("<b>Hall Effect Sensors (mT)</b>"), 0, Qt::AlignCenter);
	plotsLayout->addWidget(sensorsPlot, 1);
	plotsLayout->addWidget(new QLabel("<b>Motor Speeds (RPM)</b>"), 0, Qt::AlignCenter);
	plotsLayout->addWidget(motorSpeedsPlot, 1);
	plotsLayout->addWidget(new QLabel("<b>Motor Currents (A)</b>"), 0, Qt::AlignCenter);
	plotsLayout->addWidget(motorCurrentsPlot, 1);
	plotsLayout->addWidget(new QLabel("<b>Motor Torques (Nm)</b>"), 0, Qt::AlignCenter);
	plotsLayout->addWidget(motorTorquesPlot, 1);

	QWidget* plotsWidget = new QWidget();
	plotsWidget->setLayout(plotsLayout);

	plottingScrollArea = new QScrollArea();
	plottingScrollArea->setWidget(plotsWidget);
	plottingScrollArea->setWidgetResizable(true);
	plottingScrollArea->setMinimumWidth(840);
	plottingScrollArea->setBackgroundRole(QPalette::Light);

	// Put the overall layout together
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addLayout(leftLayout);
	layout->addWidget(plottingScrollArea, 1);
	setLayout(layout);

	// Store the starting time for plotting
	plottingStartTime_ms = Timing::getSysTime();

	// Start the update timer
	updateTimer = new QTimer(this);
	connect(updateTimer, &QTimer::timeout, this, &SAMMInterface::update);
	updateTimer->start(50);

}


/**
 * @brief A convenience function that is used to configure all plots. It is simply used
 * to avoid duplication of code.
 * @param plot A QCustomPlot object
 * @param withLegend If true, show the legend on the plot
 */
void SAMMInterface::configurePlot(QCustomPlot* plot, bool withLegend) {

	// Install the event filter so the scroll wheel will work properly.
	plot->installEventFilter(this);

	// Make the legend visible and place it in the top right of the plot, if requested
	if (withLegend) {
		QCPLayoutGrid* subLayout1 = new QCPLayoutGrid;
		plot->plotLayout()->addElement(0, 1, subLayout1);
		subLayout1->addElement(0, 0, new QCPLayoutElement);
		subLayout1->addElement(1, 0, plot->legend);
		subLayout1->addElement(2, 0, new QCPLayoutElement);
		plot->plotLayout()->setColumnStretchFactor(1, 0.001);
		plot->legend->setBorderPen(QPen(Qt::transparent));
		plot->legend->setVisible(true);
	}

	// Set the x-axis of the plot to use a time format (time data must be in seconds)
	QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
	timeTicker->setTimeFormat("%m:%s");
	plot->xAxis->setTicker(timeTicker);
	plot->xAxis->setLabel("Time");

	// Configure plot size policy
	plot->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));
	plot->setMinimumSize(QSize(800, plot->legend->minimumOuterSizeHint().height() + 150));

}


/**
 * @brief This function implements the window scrolling effect. Each time the plots are updated
 * with new data, this function should be called.
 * @param plot A QCustomPlot object
 * @param time The current time in seconds
 */
void SAMMInterface::scrollPlot(QCustomPlot* plot, double time) {

	plot->yAxis->rescale();
	plot->xAxis->setRange(time, 40, Qt::AlignRight);
	plot->replot();

}


/**
 * @brief This function is responsible for updating the user interface. It updates the 3D vector
 * window, the plots, and tells the SAMM to follow desired control settings
 */
void SAMMInterface::update() {

	if (samm.errorOccurred()) {

		static bool messageDisplayed = false;
		if (!messageDisplayed) {
			QMessageBox::critical(this, "SAMM Error Occurred!",
								  "A critical error has occurred with the SAMM. See the output on the console for details of this"
								  "error. This interface will no longer be updated.", QMessageBox::Ok);
			messageDisplayed = true;
		}

		return;

	}

	// Grab the SAMM state variables
	Robo::Vector3d heading = samm.getHeading();
	Robo::Vector3d angVel = samm.getAngularVelocity() / (2 * M_PI); // Convert to Hz
	Robo::Vector6d hallSensors = samm.getHallEffectSensors();
	Robo::Vector3d motorSpeeds = samm.getMotorSpeeds();
	Robo::Vector3d motorCurrents = samm.getMotorCurrents();
	Robo::Vector3d motorTorques = samm.getMotorTorques();

	// Update the SAMMVectorDisplayWidget and the control input to the SAMM
	display->setDipoleMoment(heading);
	double directionError, speedError, planeError;
	if (mode == HEADING) {

		// Update the 3D vector display
		display->setDesired(desiredDirection);
		display->setAngularVelocity(Robo::Vector3d::Zero());

		// Update the SAMM's control setting
		samm.setHeading(desiredDirection);

		// Compute errors
		directionError = std::asin(heading.cross(desiredDirection).norm()) * 180 / M_PI;
		if (heading.dot(desiredDirection) < 0.0)
			directionError = 180 - directionError;
		speedError = 0;
		planeError = 0;

	}
	else {

		// Update the 3D vector display (divide the angular velocity by 3 Hz so the maximum length is 1)
		display->setDesired(desiredSpeed / 3.0 * desiredDirection);
		display->setAngularVelocity(angVel / 3.0);

		// Update the SAMM's control settings (need to convert from Hz to rad/s)
		samm.setAngularVelocity(desiredSpeed * 2 * M_PI * desiredDirection);

		// Compute errors
		speedError = angVel.norm() - desiredSpeed;
		if (desiredSpeed > 0.0) {
			directionError = std::asin(angVel.normalized().cross(desiredDirection).norm()) * 180 / M_PI;
			if (angVel.dot(desiredDirection) < 0.0)
				directionError = 180 - directionError;
			Robo::Vector3d heading_proj = (Robo::I33 - desiredDirection*desiredDirection.transpose()) * heading;
			planeError = std::asin(heading.cross(heading_proj.normalized()).norm()) * 180 / M_PI;
		}
		else {
			directionError = 0.0;
			planeError = 0.0;
		}

	}

	// Compute the plotting time in seconds
	double time = (Timing::getSysTime() - plottingStartTime_ms) / 1000.0;

	// Update the plots
	directionErrorPlot->graph(0)->addData(time, directionError);
	scrollPlot(directionErrorPlot, time);
	actualSpeedPlot->graph(0)->addData(time, angVel.norm());
	scrollPlot(actualSpeedPlot, time);
	speedErrorPlot->graph(0)->addData(time, speedError);
	scrollPlot(speedErrorPlot, time);
	planeErrorPlot->graph(0)->addData(time, planeError);
	scrollPlot(planeErrorPlot, time);

	sensorsPlot->graph(0)->addData(time, hallSensors(2));
	sensorsPlot->graph(1)->addData(time, hallSensors(3));
	sensorsPlot->graph(2)->addData(time, hallSensors(4));
	sensorsPlot->graph(3)->addData(time, hallSensors(5));
	sensorsPlot->graph(4)->addData(time, hallSensors(0));
	sensorsPlot->graph(5)->addData(time, hallSensors(1));
	scrollPlot(sensorsPlot, time);

	motorSpeedsPlot->graph(0)->addData(time, motorSpeeds(0));
	motorSpeedsPlot->graph(1)->addData(time, motorSpeeds(1));
	motorSpeedsPlot->graph(2)->addData(time, motorSpeeds(2));
	scrollPlot(motorSpeedsPlot, time);

	motorCurrentsPlot->graph(0)->addData(time, motorCurrents(0));
	motorCurrentsPlot->graph(1)->addData(time, motorCurrents(1));
	motorCurrentsPlot->graph(2)->addData(time, motorCurrents(2));
	scrollPlot(motorCurrentsPlot, time);

	motorTorquesPlot->graph(0)->addData(time, motorTorques(0));
	motorTorquesPlot->graph(1)->addData(time, motorTorques(1));
	motorTorquesPlot->graph(2)->addData(time, motorTorques(2));
	scrollPlot(motorTorquesPlot, time);

}


/**
 * @brief This slot responds to the user clicking on the control type radio buttons.
 */
void SAMMInterface::controlTypeChanged() {

	if (pointingRadioButton->isChecked()) {

		controlInput->setTitle("Desired Heading");
		speedSpinBox->setEnabled(false);
		speedSlider->setEnabled(false);

		mode = HEADING;

	}
	else {

		controlInput->setTitle("Desired Angular Velocity");
		speedSpinBox->setEnabled(true);
		speedSlider->setEnabled(true);

		mode = ANGULARVELOCITY;

	}

}


/**
 * @brief This slot responds to the user modifying the desired direction control settings.
 */
void SAMMInterface::directionChanged() {

	// Make sure the user didn't try to ask for the zero vector. If so, resort to the default direction.
	if (directionX->value() == 0.0 && directionY->value() == 0.0 && directionZ->value() == 0.0) {

		QMessageBox::warning(this, "Cannot Use Zero Vector For Direction",
							 QStringLiteral("You attempted to set the direction to the zero vector. This is not "
											"allowed. Setting the direction to the default of (0, 0, 1)."),
							 QMessageBox::Ok);

		desiredDirection = Robo::UnitZ();
		directionZ->setValue(1.0);

	}
	else
		desiredDirection = Robo::Vector3d(directionX->value(), directionY->value(), directionZ->value()).normalized();

}


/**
 * @brief This slot responds to the user changing the desired speed with the spin box. It updates the slider
 * as well.
 * @param value New value
 */
void SAMMInterface::speedSpinBoxValueChanged(double value) {

	speedSlider->setValue(value * 1000);

	desiredSpeed = value;

}


/**
 * @brief This slot responds to the user changing the desired speed with the slider. It updates the spin box
 * as well.
 * @param value New value
 */
void SAMMInterface::speedSliderValueChanged(int value) {

	speedSpinBox->setValue(value / 1000.0);

}


/**
 * @brief This slot responds to the user pressing the enable/disable button. It simply toggles the enabled
 * status of the SAMM.
 */
void SAMMInterface::enableButtonPressed() {

	if (enableButton->getState()) {
		if (!samm.enable())
			enableButton->setState(false);
	}
	else
		samm.disable();

}


/**
 * @brief This event filter gets installed on all QCustomPlot objects in order to intercept scroll wheel events
 * from the mouse and send them to the scroll area's viewport. If this event filter is not installed on QCustomPlot
 * objects, then using the scroll wheel when the mouse is over plots will not scroll the scroll area.
 * @param e QEvent to filter
 * @return Whether the event was handled or not
 */
bool SAMMInterface::eventFilter(QObject*, QEvent* e) {

	// Send event to the scroll area's viewport if the event is a QEvent::Wheel type. Note that the event will
	// never make it to the widget it was intended for (which will be the QCustomPlot objects).
	if (e->type() == QEvent::Wheel)
		return QCoreApplication::sendEvent(plottingScrollArea->viewport(), e);

	// Otherwise allow the event to propagate
	return false;

}


////
////************************************   SAMMVectorDisplayWidget   ********************************************
////


SAMMVectorDisplayWidget::SAMMVectorDisplayWidget(QWidget* parent) : QOpenGLWidget(parent) {

	setFixedSize(500, 500);
	initializeGL();

}


void SAMMVectorDisplayWidget::setDesired(const Robo::Vector3d& desired) {

	this->desired = desired;

	update();

}


void SAMMVectorDisplayWidget::setDipoleMoment(const Robo::Vector3d& dipole_moment) {

	this->dipole_moment = dipole_moment;

	update();

}


void SAMMVectorDisplayWidget::setAngularVelocity(const Robo::Vector3d& ang_vel) {

	this->ang_vel = ang_vel;

	update();

}


void SAMMVectorDisplayWidget::initializeGL() {

	glClearColor(1.0, 1.0, 1.0, 1.0);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);

}


void SAMMVectorDisplayWidget::paintGL() {

	// Reset
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// Rotate the context
	glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
	glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
	glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);

	// Draw border
	glLineWidth(2.0);
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINE_LOOP);
	glVertex3f(-1, -1, 0.0);
	glVertex3f(-1, 1, 0.0);
	glVertex3f(1, 1, 0.0);
	glVertex3f(1, -1, 0.0);
	glEnd();

	// Draw gridlines
	glLineWidth(1.0);
	glBegin(GL_LINES);
	glVertex3f(-1, 0.0, 0.0);
	glVertex3f(1, 0.0, 0.0);
	glVertex3f(0.0, -1, 0.0);
	glVertex3f(0.0, 1, 0.0);
	glEnd();

	// Draw the coordinate axes
	glColor3f(1.0, 0.0, 0.0);
	drawArrow(90.0, 0.0, 0.5); // x axis
	glColor3f(0.0, 1.0, 0.0);
	drawArrow(90.0, 90.0, 0.5); // y axis
	glColor3f(0.0, 0.0, 1.0);
	drawArrow(0.0, 0.0, 0.5); // z axis

	// Draw the desired vector
	if (desired.norm() > 0.0) {
		glColor3f(0.4, 0.4, 0.4);
		drawArrow(xyzToAlpha(desired.x(), desired.y(), desired.z()), xyzToTheta(desired.x(), desired.y()), std::max(0.09, desired.norm()));
	}

	// Draw the SAMM dipole moment
	glColor3f(0.0, 0.0, 0.4);
	drawArrow(xyzToAlpha(dipole_moment.x(), dipole_moment.y(), dipole_moment.z()), xyzToTheta(dipole_moment.x(), dipole_moment.y()), 0.75);

	// Draw the SAMM angular velocity
	if (ang_vel.norm() > 0.0) {
		glColor3f(1.0, 0.67, 0.0);
		drawArrow(xyzToAlpha(ang_vel.x(), ang_vel.y(), ang_vel.z()), xyzToTheta(ang_vel.x(), ang_vel.y()),std::max(0.09, ang_vel.norm()));
	}

}


void SAMMVectorDisplayWidget::resizeGL(int width, int height) {

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-1.5, 1.5, -1.5, 1.5, -10.0, 10.0);

	glMatrixMode(GL_MODELVIEW);

}


void SAMMVectorDisplayWidget::mousePressEvent(QMouseEvent* event) {

	lastPos = event->pos();

}


void SAMMVectorDisplayWidget::mouseMoveEvent(QMouseEvent* event) {

	int dx = event->x() - lastPos.x();
	int dy = event->y() - lastPos.y();

	if (event->buttons() & Qt::LeftButton) {
		setXRotation(xRot + 8 * dy);
		setYRotation(yRot + 8 * dx);
	}
	else if (event->buttons() & Qt::RightButton) {
		setXRotation(xRot + 8 * dy);
		setZRotation(zRot + 8 * dx);
	}

	lastPos = event->pos();

}


void SAMMVectorDisplayWidget::NormalizeAngle(int& angle) {

	while (angle < 0)
		angle += 360 * 16;
	while (angle > 360 * 16)
		angle -= 360 * 16;

}


void SAMMVectorDisplayWidget::setXRotation(int angle) {

	NormalizeAngle(angle);
	if (angle != xRot) {
		xRot = angle;
		update();
	}

}


void SAMMVectorDisplayWidget::setYRotation(int angle) {

	NormalizeAngle(angle);
	if (angle != yRot) {
		yRot = angle;
		update();
	}

}


void SAMMVectorDisplayWidget::setZRotation(int angle) {

	NormalizeAngle(angle);
	if (angle != zRot) {
		zRot = angle;
		update();
	}

}


void SAMMVectorDisplayWidget::drawArrow(float alpha, float theta, float arrow_length) {

	 float cone_height = .09;

	 GLUquadric *object = gluNewQuadric();

	 float prev_line_width;
	 glGetFloatv(GL_LINE_WIDTH, &prev_line_width);

	 glLineWidth(3.0);

	 glPushMatrix();

	 glRotatef(theta, 0.0, 0.0, 1.0);
	 glRotatef(alpha, 0.0, 1.0, 0.0);

	 if (arrow_length > 0.0) {

		 glBegin(GL_LINES);
		 glVertex3f(0.0, 0.0, 0.0);
		 glVertex3f(0.0, 0.0, arrow_length-cone_height);
		 glEnd();

		 glTranslatef(0.0, 0.0, arrow_length-cone_height);

		 gluCylinder(object, .03, 0.0, cone_height, 5, 5);

	 }
	 else {
		 glBegin(GL_LINES);
		 glVertex3f(0.0, 0.0, 0.0);
		 glVertex3f(0.0, 0.0, arrow_length+cone_height);
		 glEnd();

		 glTranslatef(0.0, 0.0, arrow_length);

		 gluCylinder(object, 0.0, 0.03, cone_height, 5, 5);
	 }

	 glPopMatrix();

	 glLineWidth(prev_line_width);

	 gluDeleteQuadric(object);

}


void SAMMVectorDisplayWidget::drawProjection(float alpha, float theta, float arrow_length) {

	float prev_line_width;
	glGetFloatv(GL_LINE_WIDTH, &prev_line_width);

	glLineWidth(2.0);

	glPushMatrix();

	glRotatef(theta, 0.0, 0.0, 1.0);

	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(arrow_length*sin(alpha*M_PI/180.0), 0.0, 0.0);
	glEnd();

	glTranslatef(arrow_length*sin(alpha*M_PI/180.0), 0.0, 0.0);

	glLineWidth(2.0);

	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(0.0, 0.0, arrow_length*cos(alpha*M_PI/180.0));
	glEnd();

	glPopMatrix();

	glLineWidth(prev_line_width);

}


float SAMMVectorDisplayWidget::xyzToAlpha(float x, float y, float z) {

	double alpha_x = sqrt(x*x + y*y);
	double alpha_y = z;

	return (float)atan2(alpha_x, alpha_y) * 180.0 / M_PI;

}


float SAMMVectorDisplayWidget::xyzToTheta(float x, float y) {

	return (float)atan2(y, x) * 180.0 / M_PI;

}
