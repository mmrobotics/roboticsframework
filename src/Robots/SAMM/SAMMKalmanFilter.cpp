#include "SAMMKalmanFilter.h"
#include <iostream>
#include <cmath>
#include "Utilities/Optimization.h"


SAMMKalmanFilter::SAMMKalmanFilter(double dt, Robo::Vector6d hallsInit)
	: dt(dt) {

	// Set up process noise covariance
	Q << Robo::I66;
//	Q.block<3, 3>(0, 0) *= 50.0;		// Process cov -- direction
//	Q.block<3, 3>(3, 3) *= 10.0;		// Process cov -- velocity
	Q.block<3, 3>(0, 0) *= 2.0 * dt;	// Process cov -- direction
	Q.block<3, 3>(3, 3) *= 2.0;			// Process cov -- velocity

	// Set up sensor noise covariance
	R << 0.0029, 0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.0,
		 0.0,    0.0029, 0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.0,
		 0.0,    0.0,    0.0029, 0.0,    0.0,    0.0 ,   0.0,    0.0,    0.0,
		 0.0,    0.0,    0.0,    0.0028, 0.0,    0.0,    0.0,    0.0,    0.0,
		 0.0,    0.0,    0.0,    0.0,    0.0029, 0.0,    0.0,    0.0,    0.0,
		 0.0,    0.0,    0.0,    0.0,    0.0,    0.0028, 0.0,    0.0,    0.0,
		 0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.006,  0.0,    0.0,
		 0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.006,  0.0,
		 0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.0,    0.006;

	// State estimate covariance is the same as the process noise covariance
	P << Q;

	// Set up orientation of sensors (sensing directions) found using least squares
	// 6_1 v2 calibration - average error 1.52 deg!
	Robo::Vector3d zA = Robo::Vector3d(0.0052, -0.0604,  0.9982).normalized();	// +z
	Robo::Vector3d zB = Robo::Vector3d(0.0126,  0.0527,  0.9985).normalized();	// +z
	Robo::Vector3d xA = Robo::Vector3d(0.9952, -0.0969, -0.0148).normalized();	// +x
	Robo::Vector3d xB = Robo::Vector3d(0.9866, -0.1330, -0.0948).normalized();	// +x
	Robo::Vector3d yA = Robo::Vector3d(0.0453,  0.9983,  0.0359).normalized();	// +y
	Robo::Vector3d yB = Robo::Vector3d(0.0894,  0.9955, -0.0388).normalized();	// +y
	v << zA, zB , xA, xB, yA, yB;

	// Set up position of each sensor relative to the center of the magnet found using least squares
	// 6_1 v2 calibration
	p << -0.6112, -0.9532,  4.3797,  4.0287, -0.021,   0.0768,
		  0.1115, -0.1005,  0.0474,  0.4074,  3.7781, -3.5151,
		 50.81,   58.9559, 55.2707, 55.9271, 55.1808, 54.5799;
	p *= 0.001; // Convert from millimeters to meters

	// Compute H
	Robo::Matrixd33 Hp;
	for (int i = 0; i < 6; i++) {

		// Calculate the magnetic field 'H' at sensing location 'p_i'
		Robo::Vector3d ph = p.col(i).normalized();
		double pnorm = p.col(i).norm();
		Hp =  1000.0 * u0 * (3.0 * ph * ph.transpose() - Robo::I33) / (4.0 * M_PI * (std::pow(pnorm, 3)));

		// How much of that is in our sensing direction
		Ss.row(i) =  v.col(i).transpose() * Hp;

	}

	H.setZero();
	H.topLeftCorner(6, 3) << (Ss * M_mag);
	H.bottomRightCorner(3, 3) << Robo::I33;

	// Take initial guess at orientation
	Optimization::PseudoInverse<Robo::Matrixd63> Ss_(Ss, 1e-9);
	X << Ss_.matrixPInv() * hallsInit, Robo::Vector3d(0, 0, 0);

	c_pos <<  0.0632,  0.0411,  0.0436;
	c_neg << -0.0455, -0.0330, -0.0723;

	B_pos << 0.0001,  0.0014, 0.0001;
	B_neg << 0.00001, 0.0014, 0.0005;

	// Set up the actuation matrix A
	Robo::Vector3d a1 = Robo::Vector3d(1, 1, 0).normalized();
	Robo::Vector3d a2 = Robo::Vector3d(0, 0, -1).normalized();
	Robo::Vector3d a3 = Robo::Vector3d(1, -1, 0).normalized();
	A << a1, a2, a3;
	A *= -1;

	// Set up the modeled inertia matrix
	J = (2.0/5.0) * 0.515 * std::pow(25.4e-3, 2) * Robo::I33;									// Inertia from magnet
	J += A * (std::pow(2.0/2.25 ,2) * (1.0/2.0) * 0.0262 * std::pow(28.575e-3, 2) * Robo::I33);	// Inertia from omniwheel
	J += A * (std::pow(2.0/2.25, 2) * std::pow(24.0, 2) * 13.1e-7 * Robo::I33);					// Inertia from gearmotors

}


void SAMMKalmanFilter::predict(const Robo::Vector3d& tau) {

	B = calc_B(X.tail<3>());
	c = calc_c(X.tail<3>());

	// Perform Runge-Kutta to find process-estimate of state
	Robo::Vector6d x1, x2, x3, x4;
	Robo::Matrixd66 P1, P2, P3, P4;

	x1 = Xdot(X, tau);
	P1 = Pdot(X, P, Q);

	x2 = Xdot(X + 0.5 * dt * x1, tau);
	P2 = Pdot(X + 0.5 * dt * x1, P + 0.5 * dt * P1, Q);

	x3 = Xdot(X + 0.5 * dt * x2, tau);
	P3 = Pdot(X + 0.5 * dt * x2, P + 0.5 * dt * P2, Q);

	x4 = Xdot(X + dt * x3, tau);
	P4 = Pdot(X + dt * x3, P + dt * P3, Q);

	X = X + (dt / 6.0) * (x1 + 2.0 * x2 + 2.0 * x3 + x4);
	X.head<3>().normalize();

	// Create apriori estimate of process covariance
	P << P + (dt / 6.0) * (P1 + 2.0 * P2 + 2.0 * P3 + P4);

}


void SAMMKalmanFilter::update(const Robo::Vector9d& z) {

	Robo::Matrixd99 S = (H * P * H.transpose() + R);
	Robo::Matrixd69 K = P * H.transpose() * S.inverse(); // Compute Kalman gain

	X = X + K * (z - get_h());
	X.head<3>().normalize();

	P = (Robo::I66 - K * H) * P;

}


Robo::Matrixd33 SAMMKalmanFilter::calc_B(const Robo::Vector3d& w) {

	Robo::Vector3d wtmp = A.transpose() * w;

	Robo::Vector3d tmp;
	for (int i = 0; i < 3; i++) {

		if (wtmp(i) > 0)
			tmp(i) = B_pos(i);
		else
			tmp(i) = B_neg(i);

	}

	return A * (tmp.asDiagonal());

}


Robo::Vector3d SAMMKalmanFilter::calc_c(const Robo::Vector3d& w) {

	Robo::Vector3d wtmp = A.transpose() * w;

	Robo::Vector3d tmp;
	for (int i = 0; i < 3; i++) {

		if (wtmp(i) > 0)
			tmp(i) = c_pos(i);
		else
			tmp(i) = c_neg(i);

	}

	return A * tmp;

}


Robo::Vector6d SAMMKalmanFilter::Xdot(const Robo::Vector6d& x, const Robo::Vector3d& tau) {

	Robo::Vector6d u;
	Robo::Vector3d tau_prime;

	for (int i = 3; i < 6; i++) {

		if ((abs(x(i)) < 0.005) && (abs(tau(i - 3)) < abs(c(i - 3)))) {
			c(i - 3) = tau(i - 3);
			tau_prime(i - 3) = tau(i - 3) - abs(c(i - 3)) * Robo::sign(tau(i - 3));
		}
		else {
			tau_prime(i - 3) = tau(i - 3) - abs(c(i - 3)) * Robo::sign(tau(i - 3));
		}

	}

	u.topRows<3>() << x.tail<3>().cross(x.head<3>());
	u.bottomRows<3>() << (J.inverse() * (tau_prime - B * x.tail<3>()));

	return u;

}


Robo::Matrixd66 SAMMKalmanFilter::Pdot(const Robo::Vector6d& x, const Robo::Matrixd66& _P, const Robo::Matrixd66& Q) {

	Robo::Matrixd66 F = createF(x);

	return (F * _P + _P * F.transpose() + Q);

}


Robo::Matrixd66 SAMMKalmanFilter::createF(const Robo::Vector6d& state) {

	Robo::Matrixd66 Ff;

	Ff.setZero();
	Ff.topLeftCorner(3, 3)     << Robo::Skew(state.tail<3>());
	Ff.topRightCorner(3, 3)    << -Robo::Skew(state.head<3>());
	Ff.bottomRightCorner(3, 3) << -J.inverse() * B;

	return Ff;

}


Robo::Vector9d SAMMKalmanFilter::get_h() {

	Robo::Vector9d hz;

	hz.head<6>() = M_mag * Ss * X.head<3>();
	hz.tail<3>() = X.tail<3>();

	return hz;

}
