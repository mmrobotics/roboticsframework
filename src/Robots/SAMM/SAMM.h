#ifndef SAMM_H
#define SAMM_H

#include <Robots/SAMM/SAMMKalmanFilter.h>
#include <Robots/GenericRobot.h>
#include <Robots/Tool.h>
#include <RoboticsToolbox.h>
#include <Utilities/Timing.h>
#include <QReadWriteLock>
#include <QMutex>


/**
 * @brief The SAMM class is the interface used to interact with the SAMM. The implementation is done according
 * to: S. E. Wright, A. W. Mahoney, K. M. Popek, and J. J. Abbott,
 * "The Spherical-Actuator-Magnet Manipulator: A Permanent-Magnet Robotic End-Effector",
 * IEEE Trans. Robotics, 33(5):1013-1024, 2017.
 *
 * This class utilizes the Sensoray s626 DAQ and as such, no other class that makes use of this card can be used
 * at the same time. This class is used to set the heading (magnetic moment) of the SAMM or its angular velocity
 * (both expressed in the SAMM frame). The angular velocity of the SAMM is defined such that the heading of the
 * magnet is always perpendicular to the angular velocity, therefore it is not possible to simultaneously specify
 * the heading and the angular velocity. The sensing and control loop runs in its own thread at 500 Hz. The class
 * also provides several functions to access the current Hall Effect sensor readings, and motor states such as
 * speeds, currents, and torques.
 *
 * IMPLEMENTATION NOTES:
 *
 * 1. The DIO channels on the s626 are active low and this class is built to follow that pattern. That means that the
 * digital input pins on the motor speed controllers (Maxon ESCON 26/2 DC) used to set the direction and enable
 * status need to be configured for <b>active low</b>.
 *
 * 2. On the s626, it takes 0.2 milliseconds to perform analog output and 0.02 milliseconds per channel to perform
 * analog input (since there are 12 channels on the poll list, this takes 0.24 milliseconds to complete). You
 * should not attempt to read or write to analog I/O faster than those times. Ideally 2 millisecond intervals should
 * be the fastest and this is in fact the interval used in the implementation.
 */
class SAMM : public GenericRobot, public Tool {

	public:
		SAMM();
		~SAMM() override;

		bool enable() override;
		bool disable() override;

		bool isEnabled() const override { return enabled; }
		bool isDisabled() const override { return !enabled; }
		bool errorOccurred() const override { return isInError; }

		Robo::Vector3d getHeading() const;
		Robo::Vector3d getAngularVelocity() const;
		Robo::Vector6d getHallEffectSensors() const;
		Robo::Vector3d getMotorSpeeds() const;
		Robo::Vector3d getMotorCurrents() const;
		Robo::Vector3d getMotorTorques() const;

		bool setHeading(const Robo::Vector3d& desiredHeading);
		bool setAngularVelocity(const Robo::Vector3d& desiredAngularVelocity);

	private:
		Robo::Matrixd33 A;	// Actuation Matrix
		double gearRatio;	// Gear ratio of ball -> wheel -> motor (i.e. including 24:1 gear box)

		bool enabled;
		bool isInError = false;
		static constexpr double dt = 0.002;	// Control loop interval in seconds

		Eigen::Matrix<double, 21, 1, Eigen::DontAlign> SensorVals;	// Array of the most recently received ADC values (after conversion)
		Robo::Vector12d sensorMeans;								// Measured sensor bias values

		// Observer related variables
		mutable QReadWriteLock stateMutex;
		SAMMKalmanFilter* observer;
		Robo::Vector6d currentState;
		Eigen::Matrix<double, 21, 1, Eigen::DontAlign> currentSensorVals;

		// Control related variables
		QMutex controlMutex;
		double k_p_pointing;
		double k_p_rotating;
		Robo::Vector3d _desiredHeading;
		Robo::Vector3d _desiredAngularVelocity = Robo::Vector3d::Zero();
		enum Mode { POINTING, ROTATING };
		Mode mode = ROTATING;
		Timing::Timer sammThread;

		bool checkErrors();

		// Main sense and control loop functions
		Robo::Vector3d tau_offset_fixer(const Robo::Vector3d& tau_, const double& offset);
		bool senseAndControl();

		// Sensing functions
		void readADC();
		inline double findFieldStrength(double analogMeasurement, double res, double sensitivity, double zero);
		inline double findVels(double analogMeasurement, double res, double offset);
		inline double findTorque(double analogMeasurement, double res, double offset);

		// Driving functions
		void driveW(Robo::Vector3d Vrad);
		void driveM(Robo::Vector3d velocityVector);

		// Control functions
		void controlHeading();
		void controlAngularVelocity();

};

#endif // SAMM_H
