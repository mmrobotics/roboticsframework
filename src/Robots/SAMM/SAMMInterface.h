#ifndef SAMMINTERFACE_H
#define SAMMINTERFACE_H

#include <QtWidgets>
#include <Robots/SAMM/SAMM.h>
#include <QtGUIElements/Buttons.h>
#include <QtGUIElements/qcustomplot.h>
#include <RoboticsToolbox.h>


class SAMMVectorDisplayWidget; // Declaration needed to resolve circular dependency

/**
 * @brief The SAMMInterface class implements a user interface for controlling the SAMM. It is meant to be used when interacting
 * with the SAMM directly such as when you wish to control only the SAMM (and not any robot it may also be attached to as well)
 * or when you wish to verify/debug the SAMM's behavior.
 *
 * The interface requires a SAMM object to be provided to the constructor. Once created, it can be shown like any other QWidget.
 * The interface uses OpenGL and GLU and as such, these must be installed on your computer.
 *
 * Usage of the interface is simple. Choose whether you wish to control the heading or angular velocity of the SAMM (you cannot
 * simultaneously specify both). Then specify the direction of the desired value. If controlling heading, this chosen direction
 * will be the desired heading. If controlling angular velocity, this chosen direction will be the direction of the angular
 * velocity. The direction controls allow you to specify XYZ components of the direction. The specified vector will be unitized
 * before use, so you do not need to make sure you specify a unit vector. If you are controlling angular velocity, you will also
 * need to specify the speed of rotation. Once these settings are chosen, the SAMM is enabled using the ENABLE button. The control
 * settings can be changed while the SAMM is enabled.
 *
 * The interface provides a 3D representation of the current heading, angular velocity, and desired vector (heading or angular
 * velocity). There is a color key to determine which vector is which. When controlling heading, the angular velocity will not
 * be displayed. The 3D window can be rotated by clicking and dragging.
 *
 * In addition to the 3D vector window, the interface shows other relevant values in real-time on plots. The Direction Error plot
 * shows the current error in the actual and desired directions (heading or angular velocity). The Actual Angular Speed plot shows
 * the current angular speed of the SAMM. The Angular Speed Error and Plane Error plots are only relevant when controlling the
 * angular velocity. When controlling heading, they simply show 0. The Angular Speed Error plot shows the error in the angular
 * speed and the Plane Error plot shows the current error of the SAMM heading from the desired rotation plane (i.e. how well the
 * SAMM is maintaining the "heading is perpendicular to angular velocity" assumption). The rest of the plot are self-explanatory.
 */
class SAMMInterface : public QWidget {

	Q_OBJECT

	public:
		SAMMInterface(SAMM& samm, QWidget* parent = nullptr);

	private:
		SAMM& samm;
		SAMMVectorDisplayWidget* display;
		enum Mode { HEADING, ANGULARVELOCITY };
		Mode mode = HEADING;

		Robo::Vector3d desiredDirection = Robo::UnitZ();
		double desiredSpeed = 0;

		QTimer* updateTimer;

		// Widgets
		QRadioButton* pointingRadioButton;
		QRadioButton* rotatingRadioButton;
		QDoubleSpinBox* directionX;
		QDoubleSpinBox* directionY;
		QDoubleSpinBox* directionZ;
		QDoubleSpinBox* speedSpinBox;
		QSlider* speedSlider;
		QGroupBox* controlInput;
		SwitcherButton* enableButton;
		QScrollArea* plottingScrollArea;

		// Plots
		long plottingStartTime_ms;
		QCustomPlot* directionErrorPlot;
		QCustomPlot* actualSpeedPlot;
		QCustomPlot* speedErrorPlot;
		QCustomPlot* planeErrorPlot;
		QCustomPlot* sensorsPlot;
		QCustomPlot* motorSpeedsPlot;
		QCustomPlot* motorCurrentsPlot;
		QCustomPlot* motorTorquesPlot;

		void configurePlot(QCustomPlot* plot, bool withLegend = false);
		void scrollPlot(QCustomPlot* plot, double time);

	private slots:
		void update();
		void controlTypeChanged();
		void directionChanged();
		void speedSpinBoxValueChanged(double value);
		void speedSliderValueChanged(int value);
		void enableButtonPressed();

	protected:
		bool eventFilter(QObject*, QEvent* e) override;

};


class SAMMVectorDisplayWidget : public QOpenGLWidget {

	Q_OBJECT

	public:
		SAMMVectorDisplayWidget(QWidget* parent = 0);

		void setDesired(const Robo::Vector3d& desired);
		void setDipoleMoment(const Robo::Vector3d& dipole_moment);
		void setAngularVelocity(const Robo::Vector3d& ang_vel);

	protected:
		void initializeGL() override;
		void paintGL() override;
		void resizeGL(int width, int height) override;
		void mousePressEvent(QMouseEvent* event) override;
		void mouseMoveEvent(QMouseEvent* event) override;

	private:
		int xRot = 0;
		int yRot = 0;
		int zRot = 0;

		Robo::Vector3d desired;
		Robo::Vector3d dipole_moment;
		Robo::Vector3d ang_vel;

		QPoint lastPos;

		void NormalizeAngle(int& angle);
		void setXRotation(int angle);
		void setYRotation(int angle);
		void setZRotation(int angle);
		void drawArrow(float alpha, float theta, float length);
		void drawProjection(float alpha, float theta, float length);

		float xyzToAlpha(float x, float y, float z);
		float xyzToTheta(float x, float y);
};

#endif // SAMMINTERFACE_H
