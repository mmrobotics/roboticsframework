#ifndef BUTTONS_H
#define BUTTONS_H

#include <QtWidgets>
#include <QString>


/**
 * @brief The StandardButton class is simply a QPushButton with a unique visible appearance (style sheet)
 */
class StandardButton : public QPushButton {

	Q_OBJECT
	
	public:
		StandardButton(const QString& text_string, QWidget* parent = nullptr);

};


/**
 * @brief The SwitcherButton class is a QPushButton that has a `true` and a `false` state. Clicking the button
 * causes it to toggle between these two states. For each state, there is different text displayed on the button.
 * The button also has a unique appearance (style sheet). If you connect to the clicked() signal of this widget,
 * calling getState() will return the state after the toggle has taken effect.
 */
class SwitcherButton : public QPushButton {

	Q_OBJECT
	
	public:
		SwitcherButton(const QString& false_string, const QString& true_string, bool state = false, QWidget* parent = nullptr);

		bool getState();
		void setState(bool state);
		
	private:
		bool state;
		QString true_string;
		QString false_string;
		QString true_style_sheet;
		QString false_style_sheet;
		
		void clickEvent();

};

#endif // BUTTONS_H
