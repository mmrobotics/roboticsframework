#include "Labels.h"
#include <QHBoxLayout>


/**
 * @brief Wraps the provided widget with the provided prefix.
 * @param prefix QString for the label of the prefix
 * @param widget The QWidget to wrap
 * @param parent An optional parent widget
 */
PrePostfixLabelsWidget::PrePostfixLabelsWidget(const QString& prefix, QWidget* widget, QWidget* parent) : QWidget(parent) {

	QHBoxLayout* layout = new QHBoxLayout();

	layout->addWidget(new QLabel(prefix), 1, Qt::AlignRight);
	layout->addWidget(widget);

	setLayout(layout);

}


/**
 * @brief Wraps the provided widget with the provided postfix.
 * @param widget The QWidget to wrap
 * @param postfix QString for the label of the postfix
 * @param parent An optional parent widget
 */
PrePostfixLabelsWidget::PrePostfixLabelsWidget(QWidget* widget, const QString& postfix, QWidget* parent) : QWidget(parent) {

	QHBoxLayout* layout = new QHBoxLayout();

	layout->addWidget(widget);
	layout->addWidget(new QLabel(postfix), 1, Qt::AlignLeft);

	setLayout(layout);

}


/**
 * @brief Wraps the provided widget with the provided prefix and postfix
 * @param prefix QString for the label of the prefix
 * @param widget The QWidget to wrap
 * @param postfix QString for the label of the postfix
 * @param parent An optional parent widget
 */
PrePostfixLabelsWidget::PrePostfixLabelsWidget(const QString& prefix, QWidget* widget, const QString& postfix, QWidget* parent) : QWidget(parent) {

	QHBoxLayout* layout = new QHBoxLayout();

	layout->addWidget(new QLabel(prefix), 1, Qt::AlignRight);
	layout->addWidget(widget);
	layout->addWidget(new QLabel(postfix), 1, Qt::AlignLeft);

	setLayout(layout);

}
