#ifndef WINDOW_GROUP_H
#define WINDOW_GROUP_H

#include <vector>
#include <QObject>
#include <QWidget>
#include <QEvent>


/**
 * @brief The WindowGroup class is used to group QWidgets that are acting as windows together such that they
 * will all close when any one of them closes. Simply create a WindowGroup on the stack in main() and add all
 * the top level QWidgets using the addWidget() functions.
 */
class WindowGroup : public QObject {

	Q_OBJECT

	private:
		std::vector<QWidget*> widget_list;

	protected:
		bool eventFilter(QObject*, QEvent* e) override {

			if (e->type() == QEvent::Close)
				for (unsigned long i = 0; i < widget_list.size(); ++i)
						widget_list[i]->close();

			return false;

		}

	public:
		void addWidget(QWidget& widget) {

			widget_list.push_back(&widget);
			widget.installEventFilter(this);

		}

		void addWidget(QWidget* widget) {

			widget_list.push_back(widget);
			widget->installEventFilter(this);

		}

};
	
#endif // WINDOW_GROUP_H
