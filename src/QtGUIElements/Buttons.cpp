#include "Buttons.h"
#include <iostream>
#include <QtWidgets>
#include <QtGui>
#include <QString>


StandardButton::StandardButton(const QString& text_string, QWidget* parent) : QPushButton(parent) {
	
	setText(text_string);
	
	setStyleSheet("QPushButton { color: black; background-color: rgb(240, 240, 255); border-style: ridge; border-width: 2px; border-radius: 10px; border-color: black; min-width: 10em; padding: 6px; font-weight:bold;} QPushButton:pressed { background-color: rgb(100, 100, 100); border-style: inset;} QPushButton:disabled { color: rgb(180, 180, 180); border-color: rgb(160, 160, 160); background-color: rgb(240, 240, 240);}");
	
}


/**
 * @brief Creates and instance of SwitcherButton
 * @param false_string Text to display in the `true` state
 * @param true_string Text to display in the `false` state
 * @param initial_state The state the button should start in
 * @param parent The parent widget to this widget
 */
SwitcherButton::SwitcherButton(const QString& false_string, const QString& true_string, bool initial_state, QWidget* parent) : QPushButton(parent) {
	
	this->false_string = false_string;
	this->true_string = true_string;
	state = initial_state;

	true_style_sheet = "QPushButton { color: black; background-color: rgb(80, 255, 80); border-style: ridge; border-width: 2px; border-radius: 10px; border-color: black; min-width: 5em; padding: 6px; font-weight:bold;} QPushButton:pressed { background-color: rgb(255, 100, 100); border-style: inset;} QPushButton:disabled { color: rgb(180, 180, 180); border-color: rgb(160, 160, 160); background-color: rgb(240, 240, 240);}";
	false_style_sheet = "QPushButton { color: black; background-color: rgb(255, 200, 200); border-style: ridge; border-width: 2px; border-radius: 10px; border-color: black; min-width: 5em; padding: 6px; font-weight:bold;} QPushButton:pressed { background-color: rgb(255, 100, 100); border-style: inset;} QPushButton:disabled { color: rgb(180, 180, 180); border-color: rgb(160, 160, 160); background-color: rgb(240, 240, 240);}";
	
	if (state) {
		setText(true_string);
		setStyleSheet(true_style_sheet);
	}
	else {
		setText(false_string);
		setStyleSheet(false_style_sheet);
	}
	
	connect(this, &SwitcherButton::clicked, this, &SwitcherButton::clickEvent);

}


/**
 * @brief Sets the state of the button. This function does not emit any signals (e.g. it
 * does not emit the clicked() signal.
 * @param state Desired state of the button
 */
void SwitcherButton::setState(bool state) {

	this->state = state;

	if (state) {
		setText(true_string);
		setStyleSheet(true_style_sheet);
	}
	else {
		setText(false_string);
		setStyleSheet(false_style_sheet);
	}
	
}


/**
 * @brief Returns the state of the button
 * @return
 */
bool SwitcherButton::getState() {

	return state;

}


/**
 * @brief This function implements the toggling when the button is clicked.
 */
void SwitcherButton::clickEvent() {
	
	state = !state;	
	
	if (state) {
		setText(true_string);
		setStyleSheet(true_style_sheet);
	}
	else {
		setText(false_string);
		setStyleSheet(false_style_sheet);
	}

}
