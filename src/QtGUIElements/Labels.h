#ifndef LABELS_H
#define LABELS_H

#include <QLabel>
#include <QWidget>
#include <QString>


/**
 * @brief The PrePostfixLabelsWidget class is a convenience class that allows you to prepend and/or
 * append fixed QLabels to any QWidget.
 *
 * This class simply creates a widget with a QHBoxLayout and
 * puts the prefix, postfix, and widget into that layout. An example of usage for this class would
 * be when you want to append the units to a widget that displays some number (e.g. QSpinBox). Each
 * constructor is used for the three different cases of just prefix, just postfix, or both.
 */
class PrePostfixLabelsWidget : public QWidget {

	Q_OBJECT

	public:
		PrePostfixLabelsWidget(const QString& prefix, QWidget* widget, QWidget* parent = nullptr);
		PrePostfixLabelsWidget(QWidget* widget, const QString& postfix, QWidget* parent = nullptr);
		PrePostfixLabelsWidget(const QString& prefix, QWidget* widget, const QString& postfix, QWidget* parent = nullptr);

};


/**
 * @brief The CountingLabel class is a simple extension to QLabel which represents an increasing counter.
 * It allows for prefix and/or postfix text to appear around the count.
 */
class CountingLabel : public QLabel {

	Q_OBJECT

	private:
		QString prefix;
		QString postfix;
		unsigned long count = 0;

	public:
		CountingLabel(const char* prefix = "", const char* postfix = "", QWidget* parent = nullptr)
			: QLabel("0", parent), prefix(prefix), postfix(postfix) {}

		/**
		 * @brief Resets the counting label back to zero.
		 */
		void reset() { count = 0; setText(prefix + "0" + postfix); }

	public slots:
		/**
		 * @brief Adds one to the count in the counting label.
		 */
		void increment() { setText(prefix + QString::number(++count) + postfix); }

};

#endif // LABELS_H
